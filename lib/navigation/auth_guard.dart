import 'package:auto_route/auto_route.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/navigation/router.dart';

class AuthGuard extends AutoRouteGuard {
  @override
  Future<void> onNavigation(
      NavigationResolver resolver, StackRouter router) async {
    await getIt.allReady();
    if (getIt.get<UserBloc>().state is Logged) {
      resolver.next(true);
    } else {
      router.navigate(MainRoute());
    }
  }
}
