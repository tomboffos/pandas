// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

part of 'router.dart';

class _$AppRouter extends RootStackRouter {
  _$AppRouter({
    GlobalKey<NavigatorState>? navigatorKey,
    required this.authGuard,
  }) : super(navigatorKey);

  final AuthGuard authGuard;

  @override
  final Map<String, PageFactory> pagesMap = {
    MainRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const MainPage(),
      );
    },
    MovieRoute.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<MovieRouteArgs>(
          orElse: () => MovieRouteArgs(id: pathParams.getInt('id')));
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: MoviePage(
          key: args.key,
          id: args.id,
        ),
      );
    },
    ProfileRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const ProfilePage(),
      );
    },
    LibraryRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const LibraryPage(),
      );
    },
    NewPasswordRoute.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<NewPasswordRouteArgs>(
          orElse: () =>
              NewPasswordRouteArgs(hash: pathParams.getString('hash')));
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: NewPasswordPage(
          key: args.key,
          hash: args.hash,
        ),
      );
    },
    PlayerRoute.name: (routeData) {
      final pathParams = routeData.inheritedPathParams;
      final args = routeData.argsAs<PlayerRouteArgs>(
          orElse: () => PlayerRouteArgs(
                id: pathParams.getInt('id'),
                movieId: pathParams.getInt('movieId'),
                seasonNumber: pathParams.getInt('seasonNumber'),
              ));
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: PlayerPage(
          key: args.key,
          id: args.id,
          movieId: args.movieId,
          seasonNumber: args.seasonNumber,
        ),
      );
    },
    FaqRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const FaqPage(),
        fullscreenDialog: true,
      );
    },
    PrivacyPolicyRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const PrivacyPolicyPage(),
      );
    },
    UserAgreementRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const UserAgreementPage(),
      );
    },
    LotteryTermsRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const LotteryTermsPage(),
      );
    },
    NotFoundRoute.name: (routeData) {
      return MaterialPageX<dynamic>(
        routeData: routeData,
        child: const NotFoundPage(),
      );
    },
  };

  @override
  List<RouteConfig> get routes => [
        RouteConfig(
          MainRoute.name,
          path: '/',
        ),
        RouteConfig(
          MovieRoute.name,
          path: '/series/:id',
        ),
        RouteConfig(
          ProfileRoute.name,
          path: '/profile',
          guards: [authGuard],
        ),
        RouteConfig(
          LibraryRoute.name,
          path: '/library',
          guards: [authGuard],
        ),
        RouteConfig(
          NewPasswordRoute.name,
          path: '/new_password/:hash',
        ),
        RouteConfig(
          PlayerRoute.name,
          path: '/player/:id/:movieId/:seasonNumber',
        ),
        RouteConfig(
          FaqRoute.name,
          path: '/faq',
        ),
        RouteConfig(
          PrivacyPolicyRoute.name,
          path: '/privacy_policy',
        ),
        RouteConfig(
          UserAgreementRoute.name,
          path: '/user_agreement',
        ),
        RouteConfig(
          LotteryTermsRoute.name,
          path: '/lottery_terms',
        ),
        RouteConfig(
          NotFoundRoute.name,
          path: '*',
        ),
      ];
}

/// generated route for
/// [MainPage]
class MainRoute extends PageRouteInfo<void> {
  const MainRoute()
      : super(
          MainRoute.name,
          path: '/',
        );

  static const String name = 'MainRoute';
}

/// generated route for
/// [MoviePage]
class MovieRoute extends PageRouteInfo<MovieRouteArgs> {
  MovieRoute({
    Key? key,
    required int id,
  }) : super(
          MovieRoute.name,
          path: '/series/:id',
          args: MovieRouteArgs(
            key: key,
            id: id,
          ),
          rawPathParams: {'id': id},
        );

  static const String name = 'MovieRoute';
}

class MovieRouteArgs {
  const MovieRouteArgs({
    this.key,
    required this.id,
  });

  final Key? key;

  final int id;

  @override
  String toString() {
    return 'MovieRouteArgs{key: $key, id: $id}';
  }
}

/// generated route for
/// [ProfilePage]
class ProfileRoute extends PageRouteInfo<void> {
  const ProfileRoute()
      : super(
          ProfileRoute.name,
          path: '/profile',
        );

  static const String name = 'ProfileRoute';
}

/// generated route for
/// [LibraryPage]
class LibraryRoute extends PageRouteInfo<void> {
  const LibraryRoute()
      : super(
          LibraryRoute.name,
          path: '/library',
        );

  static const String name = 'LibraryRoute';
}

/// generated route for
/// [NewPasswordPage]
class NewPasswordRoute extends PageRouteInfo<NewPasswordRouteArgs> {
  NewPasswordRoute({
    Key? key,
    required String hash,
  }) : super(
          NewPasswordRoute.name,
          path: '/new_password/:hash',
          args: NewPasswordRouteArgs(
            key: key,
            hash: hash,
          ),
          rawPathParams: {'hash': hash},
        );

  static const String name = 'NewPasswordRoute';
}

class NewPasswordRouteArgs {
  const NewPasswordRouteArgs({
    this.key,
    required this.hash,
  });

  final Key? key;

  final String hash;

  @override
  String toString() {
    return 'NewPasswordRouteArgs{key: $key, hash: $hash}';
  }
}

/// generated route for
/// [PlayerPage]
class PlayerRoute extends PageRouteInfo<PlayerRouteArgs> {
  PlayerRoute({
    Key? key,
    required int id,
    required int movieId,
    required int seasonNumber,
  }) : super(
          PlayerRoute.name,
          path: '/player/:id/:movieId/:seasonNumber',
          args: PlayerRouteArgs(
            key: key,
            id: id,
            movieId: movieId,
            seasonNumber: seasonNumber,
          ),
          rawPathParams: {
            'id': id,
            'movieId': movieId,
            'seasonNumber': seasonNumber,
          },
        );

  static const String name = 'PlayerRoute';
}

class PlayerRouteArgs {
  const PlayerRouteArgs({
    this.key,
    required this.id,
    required this.movieId,
    required this.seasonNumber,
  });

  final Key? key;

  final int id;

  final int movieId;

  final int seasonNumber;

  @override
  String toString() {
    return 'PlayerRouteArgs{key: $key, id: $id, movieId: $movieId, seasonNumber: $seasonNumber}';
  }
}

/// generated route for
/// [FaqPage]
class FaqRoute extends PageRouteInfo<void> {
  const FaqRoute()
      : super(
          FaqRoute.name,
          path: '/faq',
        );

  static const String name = 'FaqRoute';
}

/// generated route for
/// [PrivacyPolicyPage]
class PrivacyPolicyRoute extends PageRouteInfo<void> {
  const PrivacyPolicyRoute()
      : super(
          PrivacyPolicyRoute.name,
          path: '/privacy_policy',
        );

  static const String name = 'PrivacyPolicyRoute';
}

/// generated route for
/// [UserAgreementPage]
class UserAgreementRoute extends PageRouteInfo<void> {
  const UserAgreementRoute()
      : super(
          UserAgreementRoute.name,
          path: '/user_agreement',
        );

  static const String name = 'UserAgreementRoute';
}

/// generated route for
/// [LotteryTermsPage]
class LotteryTermsRoute extends PageRouteInfo<void> {
  const LotteryTermsRoute()
      : super(
          LotteryTermsRoute.name,
          path: '/lottery_terms',
        );

  static const String name = 'LotteryTermsRoute';
}

/// generated route for
/// [NotFoundPage]
class NotFoundRoute extends PageRouteInfo<void> {
  const NotFoundRoute()
      : super(
          NotFoundRoute.name,
          path: '*',
        );

  static const String name = 'NotFoundRoute';
}
