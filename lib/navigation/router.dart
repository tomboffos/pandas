import 'package:auto_route/auto_route.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/data/movie/models/season/season.dart';

import 'package:nine_pandas/navigation/auth_guard.dart';
import 'package:nine_pandas/navigation/routes.dart';
import 'package:nine_pandas/presentation/faq/pages/faq_page.dart';
import 'package:nine_pandas/presentation/forgot_password/new_password_page/new_password_page.dart';
import 'package:nine_pandas/presentation/lottery/presentation/pages/lottery_page.dart';
import 'package:nine_pandas/presentation/lottery_terms/lottery_terms_page.dart';
import 'package:nine_pandas/presentation/privacy_policy/privacy_policy_page.dart';
import 'package:nine_pandas/presentation/library/pages/library_page.dart';
import 'package:nine_pandas/presentation/main_page/pages/main_page.dart';
import 'package:nine_pandas/presentation/movie/pages/movie_page.dart';
import 'package:nine_pandas/presentation/not_found/pages/not_found_page.dart';
import 'package:nine_pandas/presentation/player/pages/player_page.dart';
import 'package:nine_pandas/presentation/profile/pages/profile_page.dart';
import 'package:nine_pandas/presentation/user_agreement/user_agreement_page.dart';

part 'router.gr.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(path: Routes.main, page: MainPage),
    AutoRoute(path: Routes.series, page: MoviePage),
    AutoRoute(path: Routes.profile, page: ProfilePage, guards: [AuthGuard]),
    AutoRoute(path: Routes.library, page: LibraryPage, guards: [AuthGuard]),
    AutoRoute(path: Routes.newPassword, page: NewPasswordPage),
    AutoRoute(path: Routes.player, page: PlayerPage),
    AutoRoute(path: Routes.faq, fullscreenDialog: true, page: FaqPage),
    AutoRoute(path: Routes.privacyPolicy, page: PrivacyPolicyPage),
    AutoRoute(path: Routes.userAgreement, page: UserAgreementPage),
    AutoRoute(path: Routes.lotteryTerms, page: LotteryTermsPage),
    AutoRoute(path: '*', page: NotFoundPage),
  ],
)
class AppRouter extends _$AppRouter {
  AppRouter({required AuthGuard authGuard}) : super(authGuard: authGuard);
}
