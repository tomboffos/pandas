class Routes {
  Routes._();
  static const String main = '/';
  static const String series = '/series/:id';
  static const String profile = '/profile';
  static const String faq = '/faq';
  static const String library = '/library';
  static const String player = '/player/:id/:movieId/:seasonNumber';
  static const String privacyPolicy = '/privacy_policy';
  static const String newPassword = '/new_password/:hash';
  static const String userAgreement = '/user_agreement';
  static const String lotteryTerms = '/lottery_terms';
}
