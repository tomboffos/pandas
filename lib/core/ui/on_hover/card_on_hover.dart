import 'package:flutter/material.dart';

class CardOnHover extends StatefulWidget {
  const CardOnHover({required this.child, required this.isReleased});
  final bool isReleased;
  final Widget child;

  @override
  State<CardOnHover> createState() => _CardOnHoverState();
}

class _CardOnHoverState extends State<CardOnHover> {
  bool isHovered = false;

  void onEntered(bool isHovered) => setState(() {
        if (widget.isReleased) {
          this.isHovered = isHovered;
        }
      });

  @override
  Widget build(BuildContext context) {
    final hoveredTransorm =
        Matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)
          ..scale(1.03)
          ..translate(-10, -5);
    final transform = isHovered ? hoveredTransorm : Matrix4.identity();
    final double opacity = isHovered ? 0 : 1;
    return MouseRegion(
      onEnter: (event) => onEntered(
        true,
      ),
      onExit: (event) => onEntered(
        false,
      ),
      child: Stack(
        fit: StackFit.expand,
        children: [
          AnimatedContainer(
              duration: Duration(
                milliseconds: 300,
              ),
              curve: Curves.ease,
              transform: transform,
              child: widget.child),
          AnimatedOpacity(
            opacity: opacity,
            duration: Duration(
              milliseconds: 300,
            ),
            child: Container(
              color: Colors.black.withOpacity(0.2),
              width: double.infinity,
              height: double.infinity,
            ),
          )
        ],
      ),
    );
  }
}
