import 'package:flutter/material.dart';

class ListOnHover extends StatefulWidget {
  const ListOnHover({required this.builder});

  final Widget Function(bool isHovered) builder;

  @override
  State<ListOnHover> createState() => _ListOnHoverState();
}

class _ListOnHoverState extends State<ListOnHover> {
  bool isHovered = false;

  void onEntered(bool isHovered) => setState(() {
        this.isHovered = isHovered;
      });

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (event) => onEntered(
        true,
      ),
      onExit: (event) => onEntered(
        false,
      ),
      child: AnimatedContainer(
          duration: Duration(
            milliseconds: 300,
          ),
          curve: Curves.ease,
          child: widget.builder(isHovered)),
    );
  }
}
