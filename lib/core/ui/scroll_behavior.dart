import 'package:flutter/gestures.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';

class PandasScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
        PointerDeviceKind.touch,
        PointerDeviceKind.mouse,
      };
}
