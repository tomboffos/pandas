import 'package:nine_pandas/core/ui/ui_kit.dart';

class LayoutHandler extends StatelessWidget {
  static const double minDesktopWidth = 1025;
  static const double minTabletWidth = 800;
  final Widget mobileView;
  final Widget desktopView;
  final Widget tabletView;
  const LayoutHandler({
    Key? key,
    required this.mobileView,
    required this.desktopView,
    required this.tabletView,
  }) : super(key: key);

  static bool isMobileLayout(BuildContext context) {
    return (MediaQuery.of(context).size.width < minTabletWidth);
  }

  static bool isTabletLayout(BuildContext context) {
    return (MediaQuery.of(context).size.width < minDesktopWidth);
  }

  @override
  Widget build(BuildContext context) {
    return (MediaQuery.of(context).size.width < minTabletWidth)
        ? mobileView
        : (MediaQuery.of(context).size.width < minDesktopWidth)
            ? tabletView
            : desktopView;
  }
}
