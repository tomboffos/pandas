import 'package:flutter/material.dart';

import 'constants/app_colors.dart';

extension ThemeColorGetters on ThemeData {
  Color get baseContrastColor => Colors.white;
  Color get mainPageColor => AppColors.menuBackground;
  Color get baseAccentColor => AppColors.accent;
  Color get fieldBackgroundColor => AppColors.fieldBackground;
  Color get menuBackgroundColor => AppColors.menuBackground;
  Color get lightTextColor => AppColors.accent;
  Color get translationsDialogColor => Colors.white;
}

final appThemeData = ThemeData(
  textSelectionTheme: TextSelectionThemeData(
    cursorColor: Colors.white,
    selectionColor: Colors.white,
    selectionHandleColor: Colors.white,
  ),
  scrollbarTheme: ScrollbarThemeData(
    thumbColor: MaterialStateProperty.all(AppColors.veryDarkGray),
    thickness: MaterialStateProperty.all(3),
    mainAxisMargin: 8,
    crossAxisMargin: 8,
  ),
  hoverColor: Colors.transparent,
  splashColor: Colors.transparent,
  highlightColor: Colors.transparent,
  fontFamily: 'Roboto',
  brightness: Brightness.dark,
  primaryColor: AppColors.menuBackground,
  backgroundColor: AppColors.menuBackground,
  iconTheme: IconThemeData(color: Colors.white),
  primarySwatch: AppColors.accentSwatch,
  cardColor: AppColors.popupBackground,
  unselectedWidgetColor: AppColors.accentSwatch,
  toggleableActiveColor: AppColors.accentSwatch,
  dividerTheme: DividerThemeData(thickness: 1, color: AppColors.dividerColor),
);
