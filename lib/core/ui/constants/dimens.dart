class Dimens {
  const Dimens._();

  //for all screens
  static const double horizontalPadding = 16;
  static const double verticalPadding = 16;
  static const double padding = 16;
  static const double horizontalSpace = 16;
  static const double verticalSpace = 16;
  static const double cornerRadius = 12;
  static const double borderWidth = 2;
  static const double buttonWidth = 144;
  static const double buttonHeight = 48;
  static const double iconSize = 16;

  static const double elevation = 10;
  static const double blurSize = 10;
}
