import 'package:flutter/material.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

class AppColors {
  static Color accent = Colors.white;
  static Color primary = HexColor("#25272A");

  static Color popupBackground = HexColor('#1F1F1F');
  static Color greyTextColor = HexColor('#C6C6C6');
  static Color darkGreyTextColor = HexColor('#838383');
  static Color menuBackground = HexColor('#2D2D2D');
  static Color carcassBackground = HexColor('#202020');
  static Color neutralButtonBackground = HexColor("#23252B");
  static Color fieldBackground = Colors.white;
  static Color iconButtonBackground = HexColor("#4E4D51");
  static Color searchFieldUnactiveBackground = HexColor("#1C1E20");
  static Color androidIconColor = HexColor("#2BD85C");
  static Color dividerColor = HexColor('#474747');
  static Color highlightColor = HexColor('#FE0000');
  static Color greyText = HexColor('#C6C6C6');
  static Color buttonColor = HexColor('#204684');
  static Color backgroundTextInputColor = HexColor('#262626');
  static Color scaffoldBgColor = HexColor('#181818');
  static Color awardsBgColor = HexColor('#1B1B4040').withOpacity(0.25);
  static Color pressedTextColor = HexColor('#F3F3F3');

  static Color error = HexColor("#F14336");

  static MaterialColor accentSwatch = const MaterialColor(
    0xFFFFFFFF,
    const <int, Color>{
      50: const Color(0xFFFFFFFF),
      100: const Color(0xFFFFFFFF),
      200: const Color(0xFFFFFFFF),
      300: const Color(0xFFFFFFFF),
      400: const Color(0xFFFFFFFF),
      500: const Color(0xFFFFFFFF),
      600: const Color(0xFFFFFFFF),
      700: const Color(0xFFFFFFFF),
      800: const Color(0xFFFFFFFF),
      900: const Color(0xFFFFFFFF),
    },
  );

  static Color veryDarkGray = HexColor("#2A2A2A");
  static Color darkGray = HexColor("#545454");
  static Color mediumGray = HexColor("#7E7E7E");
  static Color lightGray = HexColor("#C8C8C8");
  static Color veryLightGray = HexColor("#F3F3F3");
  static Color orange = HexColor("#E37F0A");
  static Color green = HexColor("#29B851");

  static Color vk = HexColor("#4D76A1");
  static Color twitter = HexColor("#03A9F4");
  static Color fb = HexColor("#1877F2");
  static Color ok = HexColor("#FF9800");
  static Color mailru = HexColor("#005FF9");
  static Color instagram1 = HexColor("#CA38A9");
  static Color instagram2 = HexColor("#FF5440");
  static Color instagram3 = HexColor("#FFDB55");
  static Color buy1 = HexColor('#FE0000');
  static Color buy2 = HexColor('#04379A');
}
