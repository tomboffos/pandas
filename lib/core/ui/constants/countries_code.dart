import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/countries/model/country.dart';

import '../ui_kit.dart';

class Countries {
  static List<Country> getCountries(BuildContext context) => [
        Country(
            countryCode: 'Ru',
            countryName: AppLocalizations.of(context)!.russia,
            countryFlag: AppLocalizations.of(context)!.ruFlagIcon),
        Country(
            countryCode: 'En',
            countryName: AppLocalizations.of(context)!.unitedStates,
            countryFlag: AppLocalizations.of(context)!.enFlagIcon),
        Country(
            countryCode: 'Hi',
            countryName: AppLocalizations.of(context)!.india,
            countryFlag: AppLocalizations.of(context)!.hiFlagIcon),
        Country(
            countryCode: 'Tr',
            countryName: AppLocalizations.of(context)!.turkey,
            countryFlag: AppLocalizations.of(context)!.trFlagIcon),
        Country(
            countryCode: 'Fr',
            countryName: AppLocalizations.of(context)!.france,
            countryFlag: AppLocalizations.of(context)!.frFlagIcon),
        Country(
            countryCode: 'Es',
            countryName: AppLocalizations.of(context)!.spain,
            countryFlag: AppLocalizations.of(context)!.esFlagIcon),
        Country(
            countryCode: 'Pt',
            countryName: AppLocalizations.of(context)!.portugal,
            countryFlag: AppLocalizations.of(context)!.ptFlagIcon),
        Country(
            countryCode: 'Ar',
            countryName: AppLocalizations.of(context)!.arabiaSaudita,
            countryFlag: AppLocalizations.of(context)!.arFlagIcon),
      ];
}
