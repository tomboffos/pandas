import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

import 'constants/app_colors.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Messages {
  static showErrorMessage(BuildContext context, String? message) {
    if (message != null) {
      Flushbar(
        backgroundColor: Theme.of(context).backgroundColor,
        messageText: Text(message),
        titleText: Text(AppLocalizations.of(context)!.error),
        duration: Duration(seconds: 3),
        leftBarIndicatorColor: AppColors.error,
        icon: Icon(
          Icons.error,
          color: AppColors.error,
        ),
      )..show(context);
    }
  }

  static showInfoMessage(BuildContext context, String title, String message) {
    Flushbar(
      backgroundColor: Theme.of(context).backgroundColor,
      messageText: Text(message),
      titleText: Text(title),
      duration: Duration(seconds: 3),
      leftBarIndicatorColor: AppColors.accentSwatch,
      icon: Icon(
        Icons.info,
        color: AppColors.accentSwatch,
      ),
    )..show(context);
  }
}
