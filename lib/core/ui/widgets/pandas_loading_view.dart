import 'package:nine_pandas/core/ui/ui_kit.dart';

class PandasLoadingView extends StatelessWidget {
  const PandasLoadingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: LayoutHandler.isMobileLayout(context)
          ? MediaQuery.of(context).size.height * 0.75
          : null,
      decoration: BoxDecoration(
        color: AppColors.scaffoldBgColor,
      ),
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(
            AppColors.accentSwatch,
          ),
        ),
      ),
    );
  }
}
