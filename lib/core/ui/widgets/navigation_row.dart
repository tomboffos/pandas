import 'dart:ui';

import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NavigationRow extends StatelessWidget {
  const NavigationRow({
    Key? key,
    this.rootPageText,
    required this.currentPageText,
  }) : super(key: key);

  final String? rootPageText;
  final String currentPageText;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: rootPageText ?? AppLocalizations.of(context)!.home,
                style: TextStyle(
                    fontSize: 16,
                    height: 1.3,
                    color: AppColors.darkGreyTextColor,
                    fontWeight: FontWeight.w500,
                    fontFeatures: const [FontFeature.proportionalFigures()],
                    fontFamily: 'Roboto'),
              ),
              TextSpan(
                text: ' / ',
                style: TextStyle(
                    fontSize: 16,
                    height: 1.3,
                    color: AppColors.darkGreyTextColor,
                    fontWeight: FontWeight.w500,
                    fontFeatures: const [FontFeature.proportionalFigures()],
                    fontFamily: 'Roboto'),
              ),
              TextSpan(
                text: currentPageText,
                style: TextStyle(
                    fontSize: 16,
                    height: 1.3,
                    color: AppColors.greyText,
                    fontWeight: FontWeight.bold,
                    fontFeatures: const [FontFeature.proportionalFigures()],
                    fontFamily: 'Roboto'),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
