import 'package:nine_pandas/core/ui/ui_kit.dart';

class PandasTextButton extends StatelessWidget {
  const PandasTextButton({
    Key? key,
    required this.onTap,
    required this.text,
    this.textColor,
    this.fontWeight,
    this.fontSize,
    this.textDecoration,
  }) : super(key: key);
  final VoidCallback onTap;
  final String text;
  final Color? textColor;
  final FontWeight? fontWeight;
  final double? fontSize;
  final TextDecoration? textDecoration;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
          child: Text(text,
              style: TextStyle(
                color: textColor ?? Colors.white,
                fontWeight: fontWeight ?? FontWeight.w400,
                fontSize: fontSize ?? 16,
                decoration: textDecoration ?? TextDecoration.none,
              ))),
    );
  }
}
