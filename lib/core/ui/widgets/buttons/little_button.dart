import 'dart:ui';

import 'package:nine_pandas/core/ui/ui_kit.dart';

class LittleButton extends StatelessWidget {
  final String text;
  final Function onTap;

  const LittleButton({Key? key, required this.text, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 0,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Dimens.cornerRadius * 2),
        ),
        color: Theme.of(context).fieldBackgroundColor,
        child: InkWell(
          onTap: this.onTap as void Function()?,
          child: Padding(
            padding: const EdgeInsets.symmetric(
                vertical: Dimens.verticalPadding / 2,
                horizontal: Dimens.horizontalPadding),
            child: Text(this.text,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontFeatures: const [FontFeature.proportionalFigures()],
                    fontSize: 14)),
          ),
        ));
  }
}
