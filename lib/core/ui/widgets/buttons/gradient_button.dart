import 'package:flutter/material.dart';
import 'package:nine_pandas/core/ui/constants/app_colors.dart';
import 'package:nine_pandas/core/ui/constants/dimens.dart';

class GradientButton extends StatefulWidget {
  final Widget child;
  final VoidCallback onTap;
  final EdgeInsetsGeometry? padding;
  final double? height;

  const GradientButton({
    Key? key,
    required this.onTap,
    required this.child,
    this.height,
    this.padding,
  }) : super(key: key);

  @override
  State<GradientButton> createState() => _GradientButtonState();
}

class _GradientButtonState extends State<GradientButton> {
  bool isHovered = false;
  int counter = 0;

  void onEntered(bool isHovered) {
    setState(() {
      this.isHovered = isHovered;
    });
    if (isHovered) _startBgColorAnimationTimer(isHovered);
  }

  List<Color> get getColorsList => [
        AppColors.buy1,
        AppColors.buy2,
      ]..shuffle();

  List<Alignment> get getAlignments => [
        Alignment.bottomLeft,
        Alignment.bottomRight,
        Alignment.topRight,
        Alignment.topLeft,
      ];

  _startBgColorAnimationTimer(bool isHovered) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      counter++;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (event) => onEntered(
        true,
      ),
      onExit: (event) => onEntered(
        false,
      ),
      child: InkWell(
        onTap: widget.onTap,
        child: AnimatedContainer(
          duration: Duration(
            milliseconds: 1500,
          ),
          curve: Curves.ease,
          height: widget.height ?? Dimens.buttonHeight,
          padding: widget.padding ??
              const EdgeInsets.symmetric(
                vertical: Dimens.verticalPadding,
                horizontal: Dimens.horizontalPadding * 1.75,
              ),
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimens.cornerRadius * 0.75),
            gradient: LinearGradient(
              begin: getAlignments[counter % getAlignments.length],
              end: getAlignments[(counter + 2) % getAlignments.length],
              colors: getColorsList,
            ),
          ),
          child: widget.child,
        ),
      ),
    );
  }
}
