import 'dart:ui';

import '../../ui_kit.dart';

class CommonButton extends StatelessWidget {
  final String text;
  final Function onTap;
  final double? width;
  final bool isEnabled;
  final TextStyle? textStyle;

  const CommonButton({
    Key? key,
    required this.onTap,
    required this.text,
    this.width,
    this.isEnabled = true,
    this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      elevation: 0,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      color: isEnabled ? AppColors.accentSwatch : Colors.grey,
      child: InkWell(
        onTap: isEnabled ? this.onTap as void Function()? : null,
        child: SizedBox(
          width: width ?? Dimens.buttonWidth,
          height: Dimens.buttonHeight,
          child: Center(
            child: Text(this.text,
                style: textStyle ??
                    TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      fontFeatures: const [FontFeature.proportionalFigures()],
                    )),
          ),
        ),
      ),
    );
  }
}
