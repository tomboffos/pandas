import '../../ui_kit.dart';

class CancelButton extends StatelessWidget {
  final Widget child;
  final VoidCallback? onTap;
  final Color? backgroundColor;
  final Color? sideColor;
  final double? width;

  const CancelButton({
    Key? key,
    required this.onTap,
    required this.child,
    this.backgroundColor,
    this.sideColor,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      elevation: 0,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        side: BorderSide(width: 1.5, color: sideColor ?? Colors.white),
        borderRadius: BorderRadius.circular(Dimens.cornerRadius * 3 / 4),
      ),
      color: Colors.transparent,
      child: InkWell(
        onTap: this.onTap,
        child: SizedBox(
          width: width ?? Dimens.buttonWidth,
          height: Dimens.buttonHeight,
          child: Center(child: child),
        ),
      ),
    );
  }
}
