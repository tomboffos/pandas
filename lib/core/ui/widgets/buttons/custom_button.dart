import '../../ui_kit.dart';

class CustomButton extends StatelessWidget {
  final Widget child;
  final VoidCallback? onTap;
  final Color? backgroundColor;
  final Color? borderColor;
  final double? width;

  const CustomButton({
    Key? key,
    required this.onTap,
    required this.child,
    this.backgroundColor,
    this.borderColor,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      elevation: 0,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius * 3 / 4),
        side: BorderSide(color: borderColor ?? Colors.transparent, width: 1.5),
      ),
      color: backgroundColor ?? (Theme.of(context).baseAccentColor),
      child: InkWell(
        onTap: this.onTap,
        child: ConstrainedBox(
          constraints: BoxConstraints(minWidth: Dimens.buttonWidth / 2),
          child: SizedBox(
            height: Dimens.buttonHeight,
            child: Center(child: child),
          ),
        ),
      ),
    );
  }
}
