import 'package:pausable_timer/pausable_timer.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';

class PandasCarouselWidget extends StatefulWidget {
  final List<Widget> items;
  final double height;
  final Duration autoplayInterval;
  final Duration autoplayDuration;

  const PandasCarouselWidget({
    Key? key,
    required this.items,
    required this.height,
    required this.autoplayInterval,
    required this.autoplayDuration,
  }) : super(key: key);

  @override
  _PandasCarouselWidgetState createState() => new _PandasCarouselWidgetState();
}

class _PandasCarouselWidgetState extends State<PandasCarouselWidget> {
  PageController? _pageController;
  int _activeIndex = 0;
  PausableTimer? _timer;
  bool _autoplayEnabled = false;
  static const allMeaningIntBisected = 10000;

  int getIndex(int index) =>
      (index + widget.items.length) % widget.items.length;

  @override
  void initState() {
    super.initState();
    _activeIndex = 0;
    if (widget.items.length > 1) {
      _activeIndex += allMeaningIntBisected * widget.items.length;
      _autoplayEnabled = true;
    }

    _pageController = PageController(
      initialPage: _activeIndex,
      keepPage: false,
    );
    if (widget.items.length > 1) {
      _timer = PausableTimer(widget.autoplayInterval, () {
        if (mounted && _autoplayEnabled) {
          _pageController!.nextPage(
            duration: widget.autoplayDuration,
            curve: Curves.easeIn,
          );
        }
      });

      WidgetsBinding.instance.addPostFrameCallback((_) {
        _timer!.start();
      });
    }
  }

  @override
  void dispose() {
    _timer?.cancel();
    _pageController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: widget.height,
      child: Stack(
        fit: StackFit.expand,
        children: [
          PageView.builder(
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) => widget.items.length != 0
                ? widget.items[index % widget.items.length]
                : Container(),
            onPageChanged: (int index) {
              if (mounted)
                setState(() {
                  _activeIndex = index;
                });

              if (_autoplayEnabled && _timer!.isExpired) {
                _timer!.reset();
                _timer!.start();
              } else if (_autoplayEnabled) {
                _autoplayEnabled = false;
              }
            },
          ),
        ],
      ),
    );
  }
}
