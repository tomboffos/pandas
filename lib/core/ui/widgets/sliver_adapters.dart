import 'package:nine_pandas/core/ui/ui_kit.dart';

extension SliverAdapters on Widget {
  Widget get sliverBox => SliverToBoxAdapter(child: this);
  Widget get sliverFill => SliverFillRemaining(child: this);
}
