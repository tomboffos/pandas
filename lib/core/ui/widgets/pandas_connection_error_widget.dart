import '../ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PandasConnectionErrorWidget extends StatelessWidget {
  final VoidCallback reloadCallback;

  const PandasConnectionErrorWidget({
    Key? key,
    required this.reloadCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: Insets.a,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Gap.h2,
            Text(
              AppLocalizations.of(context)!.noInternetConnection,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w400,
                fontSize: 20,
              ),
            ),
            Gap.h2,
            Text(
              AppLocalizations.of(context)!.checkConnection,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: AppColors.mediumGray,
                fontWeight: FontWeight.w400,
                fontSize: 12,
              ),
            ),
            Gap.h2,
            InkWell(
              onTap: reloadCallback,
              child: Padding(
                padding: Insets.a,
                child: Text(
                  AppLocalizations.of(context)!.refresh,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Theme.of(context).baseAccentColor,
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                    decoration: TextDecoration.underline,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
