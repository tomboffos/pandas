import 'package:flutter/widgets.dart';
import 'package:nine_pandas/core/ui/constants/dimens.dart';

extension Gap on SizedBox {
  static SizedBox statusBar(BuildContext context) => SizedBox(
        height: MediaQuery.of(context).padding.top,
      );
  static SizedBox bottomInsets(BuildContext context) => SizedBox(
        height: MediaQuery.of(context).padding.bottom,
      );
  static const SizedBox zero = SizedBox.shrink();
  // vertical padding
  static const SizedBox h_25 = SizedBox(height: Dimens.verticalSpace * 0.25);
  static const SizedBox h_5 = SizedBox(height: Dimens.verticalSpace * 0.5);
  static const SizedBox h_75 = SizedBox(height: Dimens.verticalSpace * 0.75);
  static const SizedBox h = SizedBox(height: Dimens.verticalSpace);
  static const SizedBox h1_25 = SizedBox(height: Dimens.verticalSpace * 1.25);
  static const SizedBox h1_5 = SizedBox(height: Dimens.verticalSpace * 1.5);
  static const SizedBox h2 = SizedBox(height: Dimens.verticalSpace * 2);
  static const SizedBox h2_5 = SizedBox(height: Dimens.verticalSpace * 2.5);
  static const SizedBox h3 = SizedBox(height: Dimens.verticalSpace * 3);
  static const SizedBox h4 = SizedBox(height: Dimens.verticalSpace * 4);

  // horizontal padding
  static const SizedBox w_25 = SizedBox(width: Dimens.horizontalSpace * 0.25);
  static const SizedBox w_5 = SizedBox(width: Dimens.horizontalSpace * 0.5);
  static const SizedBox w_75 = SizedBox(width: Dimens.horizontalSpace * 0.75);
  static const SizedBox w = SizedBox(width: Dimens.horizontalSpace);
  static const SizedBox w1_5 = SizedBox(width: Dimens.horizontalSpace * 1.5);
  static const SizedBox w2 = SizedBox(width: Dimens.horizontalSpace * 2);
  static const SizedBox w4 = SizedBox(width: Dimens.horizontalSpace * 4);
}
