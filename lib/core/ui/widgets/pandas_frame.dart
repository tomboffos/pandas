import 'package:flutter/material.dart';

import 'package:nine_pandas/core/ui/constants/dimens.dart';

class PandasFrame extends StatelessWidget {
  final Widget? child;
  final double width;
  final double height;
  final VoidCallback? onTap;
  final Color? background;
  final EdgeInsetsGeometry? padding;

  const PandasFrame({
    Key? key,
    required this.width,
    required this.height,
    this.background,
    this.onTap,
    this.padding,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      child: InkWell(
        onTap: this.onTap,
        child: SizedBox(
          width: width,
          height: height,
          child: Center(
            child: Padding(
              padding: padding ?? const EdgeInsets.all(0),
              child: child,
            ),
          ),
        ),
      ),
    );
  }
}
