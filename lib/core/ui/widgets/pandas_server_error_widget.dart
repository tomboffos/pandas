import 'package:flutter_svg/flutter_svg.dart';

import '../ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PandasServerErrorWidget extends StatelessWidget {
  final VoidCallback reloadCallback;

  static const double serverErrorWidgetWidth = 330;
  static const double cancelButtonWidth = 150;

  const PandasServerErrorWidget({
    Key? key,
    required this.reloadCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(minHeight: MediaQuery.of(context).size.height * 0.7),
      child: Container(
        padding: Insets.a,
        width: PandasServerErrorWidget.serverErrorWidgetWidth,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                AppLocalizations.of(context)!.errorImage,
                height: Dimens.iconSize * 10,
              ),
              Gap.h4,
              Text(
                AppLocalizations.of(context)!.oops,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: AppColors.veryLightGray,
                  fontWeight: FontWeight.w400,
                  fontSize: 38,
                ),
              ),
              Gap.h,
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: AppLocalizations.of(context)!.serverConnection,
                      style: TextStyle(
                        color: AppColors.veryLightGray,
                        fontSize: 24,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    TextSpan(
                      text: '.',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              ),
              Gap.h3,
              RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: AppLocalizations.of(context)!.reloadPage,
                      style: TextStyle(
                        color: Colors.white.withOpacity(0.7),
                        fontSize: 18,
                      ),
                    ),
                    TextSpan(
                      text: '.',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ],
                ),
              ),
              Gap.h2,
              CancelButton(
                width: PandasServerErrorWidget.cancelButtonWidth,
                onTap: reloadCallback,
                sideColor: AppColors.lightGray,
                child: Text(
                  AppLocalizations.of(context)!.repeat,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
