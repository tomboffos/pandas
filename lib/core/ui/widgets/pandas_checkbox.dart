import '../ui_kit.dart';

class PandasCheckbox extends StatelessWidget {
  final bool value;
  final bool disabled;
  final double size;
  final Function(bool) onChanged;

  const PandasCheckbox({
    Key? key,
    this.size = Dimens.buttonHeight / 2,
    this.value = false,
    this.disabled = false,
    required this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        disabledColor: Colors.transparent,
        unselectedWidgetColor: Colors.transparent,
      ),
      child: SizedBox(
        width: size,
        height: size,
        child: Container(
          decoration: BoxDecoration(
            color: AppColors.carcassBackground,
            border: Border.all(
              color: Colors.white,
              width: Dimens.borderWidth / 2,
            ),
            borderRadius: BorderRadius.circular(
              Dimens.cornerRadius / 2,
            ),
          ),
          clipBehavior: Clip.hardEdge,
          child: Transform.scale(
            scale: size / Checkbox.width,
            child: Checkbox(
              hoverColor: Colors.transparent,
              focusColor: Colors.transparent,
              activeColor: Colors.transparent,
              checkColor: Colors.white,
              value: value,
              onChanged: disabled
                  ? null
                  : (value) {
                      onChanged(value ?? false);
                    },
            ),
          ),
        ),
      ),
    );
  }
}
