import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter/services.dart';

class PandasBaseTextInput extends StatefulWidget {
  final String? hintText;
  final bool? isError;
  final bool? isLimited;
  final TextEditingController? controller;
  final Function(String)? onFieldSubmitted;
  final FocusNode? focusNode;
  final List<TextInputFormatter>? inputFormatters;
  final TextInputType? keyboardType;
  final TextInputAction? textInputAction;
  final bool? isObscured;
  final bool? isEnabled;
  final bool? readOnly;
  final Widget? tail;
  final EdgeInsets? tailPadding;
  final Widget? lead;
  final int? minLines;
  final int? maxLines;
  final int? maxLength;
  final Function(String)? onChanged;

  static const MAX_LENGTH = 30;

  const PandasBaseTextInput({
    Key? key,
    this.isError,
    this.isLimited,
    this.controller,
    this.onFieldSubmitted,
    this.hintText,
    this.focusNode,
    this.inputFormatters,
    this.keyboardType,
    this.textInputAction,
    this.isObscured,
    this.isEnabled,
    this.lead,
    this.tail,
    this.tailPadding,
    this.minLines,
    this.maxLength,
    this.maxLines,
    this.readOnly,
    this.onChanged,
  }) : super(key: key);

  @override
  _PandasBaseTextInputState createState() => _PandasBaseTextInputState();
}

class _PandasBaseTextInputState extends State<PandasBaseTextInput> {
  @override
  Widget build(BuildContext context) {
    final border = UnderlineInputBorder(
        borderSide: BorderSide(
      color: widget.readOnly ?? false
          ? Colors.transparent
          : AppColors.greyTextColor,
      width: 1,
    ));

    final errorBorder = OutlineInputBorder(
        borderSide:
            BorderSide(color: AppColors.error, width: Dimens.borderWidth));

    return Theme(
      data: ThemeData(textSelectionColor: Colors.black),
      child: TextFormField(
        readOnly: widget.readOnly ?? false,
        autocorrect: false,
        enableSuggestions: false,
        enabled: widget.isEnabled ?? true,
        obscureText: widget.isObscured ?? false,
        controller: widget.controller,
        inputFormatters: widget.inputFormatters,
        style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w400,
            fontSize: 16,
            fontFamily: 'Roboto'),
        keyboardType: widget.keyboardType,
        textInputAction: widget.textInputAction,
        minLines: widget.minLines ?? 1,
        maxLines: widget.maxLines ?? 1,
        decoration: InputDecoration(
          fillColor: AppColors.backgroundTextInputColor,
          filled: true,
          hintText: widget.hintText,
          contentPadding: const EdgeInsets.only(
            top: (Dimens.buttonHeight - 14) / 2,
            bottom: (Dimens.buttonHeight - 14) / 2,
            left: Dimens.horizontalPadding,
            right: Dimens.horizontalPadding,
          ),
          hintStyle: TextStyle(
              color: AppColors.darkGray,
              fontWeight: FontWeight.w500,
              fontFamily: 'Roboto'),
          border: widget.isError ?? false ? errorBorder : border,
          enabledBorder: widget.isError ?? false ? errorBorder : border,
          focusedBorder: widget.isError ?? false ? errorBorder : border,
          prefixIcon: widget.lead != null
              ? Padding(
                  padding: Insets.a_75,
                  child: widget.lead,
                )
              : null,
          suffixIcon: widget.tail != null
              ? Padding(
                  padding: widget.tailPadding ?? Insets.w_75,
                  child: widget.tail,
                )
              : null,
        ),
        onChanged: widget.onChanged,
        focusNode: widget.focusNode,
        onFieldSubmitted: widget.onFieldSubmitted,
      ),
    );
  }
}
