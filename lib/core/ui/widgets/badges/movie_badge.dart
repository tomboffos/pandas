import 'package:nine_pandas/core/ui/ui_kit.dart';

class MovieBadge extends StatelessWidget {
  final Color textColor;
  final Color color;
  final String? text;
  final FontWeight? fontWeight;

  const MovieBadge({
    Key? key,
    required this.textColor,
    required this.color,
    required this.text,
    this.fontWeight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(Dimens.cornerRadius * 0.5),
      ),
      child: Padding(
        padding: Insets.h_25 + Insets.w_5,
        child: Text(
          text!,
          style: TextStyle(
            color: textColor,
            fontSize: 13,
            fontWeight: fontWeight ?? FontWeight.w500,
          ),
        ),
      ),
    );
  }
}
