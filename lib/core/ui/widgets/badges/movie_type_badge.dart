import 'package:nine_pandas/core/ui/ui_kit.dart';

class MovieTypeBadge extends StatelessWidget {
  final String type;

  const MovieTypeBadge({
    Key? key,
    required this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MovieBadge(
      color: Theme.of(context).highlightColor,
      textColor: Theme.of(context).primaryColor,
      text: type,
    );
  }
}
