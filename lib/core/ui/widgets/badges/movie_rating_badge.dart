import 'package:nine_pandas/core/ui/ui_kit.dart';

class MovieRatingBadge extends StatelessWidget {
  final double? rating;

  const MovieRatingBadge({
    Key? key,
    required this.rating,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MovieBadge(
      color: AppColors.green,
      textColor: Colors.white,
      text: rating?.toStringAsFixed(1) ?? '0.0',
    );
  }
}
