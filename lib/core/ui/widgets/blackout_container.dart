import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import '../ui_kit.dart';

class BlackoutContainer extends StatefulWidget {
  const BlackoutContainer({Key? key}) : super(key: key);

  @override
  State<BlackoutContainer> createState() => _BlackoutContainerState();
}

class _BlackoutContainerState extends State<BlackoutContainer> {
  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      opacity: Blackout.of(context).isBlackout ? 1.0 : 0.0,
      alwaysIncludeSemantics: true,
      duration: const Duration(milliseconds: 150),
      child: Visibility(
        visible: Blackout.of(context).isBlackout,
        child: Container(
          color: Colors.black.withOpacity(0.6),
        ),
      ),
    );
  }
}
