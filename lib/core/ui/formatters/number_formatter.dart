import 'package:flutter/services.dart';

class NumberTextInputFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    final newTextLength = newValue.text.length;
    int selectionIndex = newValue.selection.end;
    final newTextBuffer = StringBuffer();

    if (newTextLength >= 0) {
      if (newValue.text.startsWith(RegExp(r'[0-9]'))) {
        newTextBuffer.write('+');

        if (newValue.selection.end >= 1) selectionIndex++;
      }
    }

    newTextBuffer.write(newValue.text);

    return TextEditingValue(
      text: newTextBuffer.toString(),
      selection: TextSelection.collapsed(offset: selectionIndex),
    );
  }
}
