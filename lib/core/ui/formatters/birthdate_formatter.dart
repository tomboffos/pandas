import 'package:flutter/services.dart';

class BirthdateFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    var inputText = newValue.text;

    var bufferString = new StringBuffer();
    for (int i = 0; i < inputText.length; i++) {
      bufferString.write(inputText[i]);
      var nonZeroIndexValue = i + 1;
      if (nonZeroIndexValue == 8) {
        break;
      }
      if (nonZeroIndexValue % 2 == 0 && nonZeroIndexValue < 5) {
        bufferString.write('.');
      }
    }
    String string = bufferString.toString();
    return newValue.copyWith(
      text: string,
      selection: new TextSelection.collapsed(
        offset: string.length,
      ),
    );
  }
}
