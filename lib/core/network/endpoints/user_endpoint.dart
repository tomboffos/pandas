class UserEndpoint {
  static const String register = '/emailregister';
  static const String login = '/auth/token';
  static const String me = '/me';
  static const String forgetPassword = '/restorepass';
  static const String update = '/user';
  static const String changePassword = '/changepass';
}
