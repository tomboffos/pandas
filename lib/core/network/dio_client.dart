import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/network/api_config.dart';
import 'package:nine_pandas/core/network/interceptors/auth_interceptor.dart';

import 'base_endpoints.dart';

@injectable
class DioClient {
  static const CLIENT_TOKEN = 'dHYuOXBhbmRhczo1WHd1dHo2dg==';
  Dio? _dio;

  DioClient() {
    _dio = Dio()
      ..options.baseUrl = BaseEndpoints.baseUrl
      ..options.connectTimeout = HttpConfig.connectionTimeout
      ..options.receiveTimeout = HttpConfig.receiveTimeout;
    if (!kReleaseMode) {
      _dio!.interceptors
          .add(LogInterceptor(requestBody: true, responseBody: true));
    }
    _dio!.interceptors.add(AuthInterceptor(_dio));
  }

  Future<dynamic> get(
    String uri, {
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onReceiveProgress,
    bool? raw,
  }) async {
    final Response response = await _dio!.get(
      uri,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onReceiveProgress: onReceiveProgress,
    );
    if (raw ?? false) {
      return response;
    } else {
      return response.data;
    }
  }

  Future<dynamic> post(
    String uri, {
    data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final Response response = await _dio!.post(
      uri,
      data: data,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );
    return response.data;
  }

  Future<dynamic> put(
    String uri, {
    data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final Response response = await _dio!.put(
      uri,
      data: data,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );
    return response.data;
  }

  Future<dynamic> delete(
    String uri, {
    data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    ProgressCallback? onSendProgress,
    ProgressCallback? onReceiveProgress,
  }) async {
    final Response response = await _dio!.delete(
      uri,
      data: data,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
    );
    return response.data;
  }
}
