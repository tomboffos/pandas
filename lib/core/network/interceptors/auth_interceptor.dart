import 'package:dio/dio.dart';
import 'package:nine_pandas/core/network/endpoints/user_endpoint.dart';
import 'package:nine_pandas/data/token/model/token.dart';
import 'package:nine_pandas/data/token/shared_preferences/token_shared_preferences.dart';
import 'package:nine_pandas/injectable.dart';

import '../dio_client.dart';

class AuthInterceptor extends Interceptor {
  TokenSharedPreferences _sharedPreferences =
      getIt.get<TokenSharedPreferences>();

  final Dio? _dio;

  AuthInterceptor(this._dio);

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    if (await _sharedPreferences.isLoggedIn) {
      Token? token = _sharedPreferences.getToken();
      options.headers
          .putIfAbsent('Authorization', () => "Bearer ${token!.accessToken}");
    }
    handler.next(options);
  }

  @override
  void onError(DioError error, ErrorInterceptorHandler handler) async {
    if (error.response!.statusCode == 401 &&
        error.response!.requestOptions.path != '/auth/token' &&
        await _sharedPreferences.isLoggedIn) {
      final RequestOptions options = error.response!.requestOptions;
      final res = await _dio!.post(UserEndpoint.login,
          options: Options(headers: {
            'Authorization': 'Basic ${DioClient.CLIENT_TOKEN}',
          }),
          queryParameters: {
            'grant_type': 'refresh_token',
            'refresh_token': _sharedPreferences.getToken()!.refreshToken,
          });
      Token token = Token.fromJson(res.data);

      await _sharedPreferences.saveTokens(token);

      options.headers['Authorization'] = 'Bearer ${token.accessToken}';
      final Response response = await _dio!.fetch(options);

      return handler.resolve(response);
    }
    return handler.next(error);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    if (response.realUri.toString().contains(UserEndpoint.register) ||
        response.realUri.toString().contains(UserEndpoint.login)) {
      Token token = Token(
        accessToken: response.data['access_token'],
        refreshToken: response.data['refresh_token'],
      );

      _sharedPreferences.saveTokens(token);
    }

    super.onResponse(response, handler);
  }
}
