import 'package:nine_pandas/core/ui/ui_kit.dart';

class LifecycleEventHandler extends WidgetsBindingObserver {
  LifecycleEventHandler(
      {this.inactiveCallBack,
      this.pausedCallBack,
      this.detachedCallBack,
      this.resumeCallBack});

  final VoidCallback? inactiveCallBack;
  final VoidCallback? pausedCallBack;
  final VoidCallback? detachedCallBack;
  final VoidCallback? resumeCallBack;

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.inactive:
      case AppLifecycleState.paused:
      case AppLifecycleState.detached:
        detachedCallBack!();
        break;
      case AppLifecycleState.resumed:
        resumeCallBack!();
        break;
    }
  }
}
