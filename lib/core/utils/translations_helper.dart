import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

String getNumberToText(int number, BuildContext context) {
  switch (number) {
    case 1:
      return AppLocalizations.of(context)!.first;
    case 2:
      return AppLocalizations.of(context)!.second;
    case 3:
      return AppLocalizations.of(context)!.third;
    case 4:
      return AppLocalizations.of(context)!.fourth;
    case 5:
      return AppLocalizations.of(context)!.fifth;
    case 6:
      return AppLocalizations.of(context)!.sixth;
    case 7:
      return AppLocalizations.of(context)!.seventh;
    case 8:
      return AppLocalizations.of(context)!.eighth;
    case 9:
      return AppLocalizations.of(context)!.ninth;
    default:
      return AppLocalizations.of(context)!.first;
  }
}
