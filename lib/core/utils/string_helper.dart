import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';

get phoneMask => MaskTextInputFormatter(
    mask: '+# (###) ###-##-##', filter: {"#": RegExp(r'[0-9]')});

bool isPhoneNumber(String value) {
  String patttern = r'(^(?:[+])?[0-9]{10,12}$)';
  RegExp regExp = new RegExp(patttern);

  return value.isNotEmpty && regExp.hasMatch(value);
}

bool isEmailAddress(String? value) {
  if (value != null) {
    String patttern =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    RegExp regExp = new RegExp(patttern);

    return value.isNotEmpty && regExp.hasMatch(value);
  } else {
    return false;
  }
}

bool isPasswordCorrect(String? value) {
  if (value != null) {
    return value.length >= 6;
  } else {
    return false;
  }
}

bool isValidDate(String? input) {
  try {
    DateTime currentDate = DateTime.now();
    final date = DateFormat('d.m.y').parse(input!);

    if (date.year <= currentDate.year && date.year.toString().length == 4) {
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
}

String episodeTypeStringFromEnum(BuildContext context,
    {required Episode episode}) {
  switch (episode.episodeType) {
    case EpisodeType.trailer:
      return AppLocalizations.of(context)!.trailer;
    case EpisodeType.movie:
      return AppLocalizations.of(context)!.movie;
    case EpisodeType.episode:
      return AppLocalizations.of(context)!.episodeNumber(episode.episodeNumber);
    default:
      return '';
  }
}
