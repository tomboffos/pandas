import 'package:intl/intl.dart';

class DateHelper {
  static const String DATE_FORMAT = "yyyy-MM-ddThh:mm:ss";
  static const String RECOGNIZABLE_DATE_FORMAT = "dd.MM.yy";

  static DateTime stringToDate(String stringDate) {
    return DateFormat(DATE_FORMAT).parse(stringDate);
  }

  static String dateToString(DateTime date) {
    return DateFormat(DATE_FORMAT).format(date);
  }

  static DateTime recognizableStringToDate(String stringDate) {
    return DateFormat(RECOGNIZABLE_DATE_FORMAT).parse(stringDate);
  }

  static String dateToRecognizableString(DateTime date) {
    return DateFormat(RECOGNIZABLE_DATE_FORMAT).format(date);
  }

  static int hoursFromMinutes(int minutes) {
    return (minutes / 60).floor();
  }
}
