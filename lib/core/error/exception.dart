class ServerException implements Exception {
  //GOTO: <add required or ?>
  final int? code;
  final String? message;
  ServerException({this.code, this.message});
}

class CacheException implements Exception {}
