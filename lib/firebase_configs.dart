import 'package:firebase_core/firebase_core.dart';

class DefaultFirebaseConfig {
  static FirebaseOptions? get platformOptions {
    return const FirebaseOptions(
        apiKey: "AIzaSyC5PJXV9s8l4McJiaAA2lW4xqb7igwne-k",
        authDomain: "pandastv-d0830.firebaseapp.com",
        projectId: "pandastv-d0830",
        storageBucket: "pandastv-d0830.appspot.com",
        messagingSenderId: "388430884873",
        appId: "1:388430884873:web:3032fd7b55aec4b02688d8");
  }
}
