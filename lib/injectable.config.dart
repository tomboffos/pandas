// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;
import 'package:shared_preferences/shared_preferences.dart' as _i42;

import 'core/network/dio_client.dart' as _i6;
import 'data/age_restriction/age_restriction_repository_impl.dart' as _i61;
import 'data/analytics/analytics_repositories_implementation.dart' as _i5;
import 'data/change_password/change_password_repository_impl.dart' as _i65;
import 'data/change_password/datasources/change_password_remote_data_source.dart'
    as _i63;
import 'data/episode/datasources/episode_data_source.dart' as _i7;
import 'data/episode/episode_repository_impl.dart' as _i9;
import 'data/faq/datasources/faq_data_source.dart' as _i10;
import 'data/faq/faq_repository_impl.dart' as _i12;
import 'data/forgot_password/datasources/forgot_password_remote_data_source.dart'
    as _i13;
import 'data/forgot_password/forget_password_repository_impl.dart' as _i15;
import 'data/history/datasources/history_remote_data_source.dart' as _i18;
import 'data/history/repositories/history_repository_impl.dart' as _i20;
import 'data/html_reading_page/datasources/html_reading_page_data_source.dart'
    as _i21;
import 'data/html_reading_page/html_reading_page_repository_impl.dart' as _i23;
import 'data/library/datasourses/library_remote_data_source.dart' as _i24;
import 'data/library/library_repository_impl.dart' as _i26;
import 'data/localization/localization_repository_impl.dart' as _i75;
import 'data/localization/shared_preferences/localization_shared_preferences.dart'
    as _i27;
import 'data/movies/datasourses/movies_remote_data_source.dart' as _i29;
import 'data/movies/movies_repository_impl.dart' as _i31;
import 'data/sing_in/datasources/sing_in_remote_data_source.dart' as _i43;
import 'data/sing_in/sing_in_repository_impl.dart' as _i45;
import 'data/sing_up/datasources/sing_up_remote_data_source.dart' as _i46;
import 'data/sing_up/sing_up_repository_impl.dart' as _i48;
import 'data/token/datasource/token_remote_data_source.dart' as _i51;
import 'data/token/repositories/token_repository_impl.dart' as _i53;
import 'data/token/shared_preferences/token_shared_preferences.dart' as _i54;
import 'data/user/datasources/user_remote_data_source.dart' as _i55;
import 'data/user/user_repository_impl.dart' as _i57;
import 'domain/age_restriction/bloc/age_restriction_bloc.dart' as _i85;
import 'domain/age_restriction/domain/age_restriction_repository.dart' as _i60;
import 'domain/age_restriction/domain/usecases/get_confirmation.dart' as _i67;
import 'domain/age_restriction/domain/usecases/save_confirmation.dart' as _i80;
import 'domain/age_restriction/shared_preferences/age_restriction_shared_preferences.dart'
    as _i3;
import 'domain/analytics/analytics_repositories.dart' as _i4;
import 'domain/analytics/bloc/analytics_bloc.dart' as _i62;
import 'domain/analytics/usecases/send_application_launched.dart' as _i35;
import 'domain/analytics/usecases/send_movie_page_opened.dart' as _i36;
import 'domain/analytics/usecases/send_movie_played.dart' as _i37;
import 'domain/analytics/usecases/send_player_page_opened.dart' as _i38;
import 'domain/analytics/usecases/send_pressed_buy_button_movie_page.dart'
    as _i39;
import 'domain/analytics/usecases/send_pressed_buy_button_player_page.dart'
    as _i40;
import 'domain/analytics/usecases/send_pressed_watch_second_season_not_bought.dart'
    as _i41;
import 'domain/change_password/bloc/change_password_bloc.dart' as _i87;
import 'domain/change_password/repository/change_password_repository.dart'
    as _i64;
import 'domain/change_password/usecases/change_password.dart' as _i86;
import 'domain/episode/bloc/episode_bloc.dart' as _i96;
import 'domain/episode/repository/episode_repository.dart' as _i8;
import 'domain/episode/usecases/add_vote.dart' as _i59;
import 'domain/episode/usecases/get_episode_by_id.dart' as _i16;
import 'domain/episode/usecases/post_comment.dart' as _i33;
import 'domain/episode/usecases/sort_by_date.dart' as _i49;
import 'domain/episode/usecases/sort_by_likes.dart' as _i50;
import 'domain/faq/bloc/faq_bloc.dart' as _i97;
import 'domain/faq/repository/faq_repository.dart' as _i11;
import 'domain/faq/usecases/get_faq.dart' as _i17;
import 'domain/forgot_password/bloc/forgot_password_bloc.dart' as _i66;
import 'domain/forgot_password/repository/forgot_password_repository.dart'
    as _i14;
import 'domain/forgot_password/usecases/new_password.dart' as _i32;
import 'domain/forgot_password/usecases/recovery_password.dart' as _i34;
import 'domain/history/bloc/history_bloc.dart' as _i73;
import 'domain/history/history_repository.dart' as _i19;
import 'domain/history/usecases/add_history.dart' as _i58;
import 'domain/html_reading_page/bloc/html_reader_page_bloc.dart' as _i89;
import 'domain/html_reading_page/repository/html_reading_page_repository.dart'
    as _i22;
import 'domain/html_reading_page/usecases/get_html_text.dart' as _i68;
import 'domain/library/bloc/library_bloc.dart' as _i90;
import 'domain/library/repository/library_repository.dart' as _i25;
import 'domain/library/usecases/get_library.dart' as _i69;
import 'domain/localization/bloc/bloc/localization_bloc.dart' as _i91;
import 'domain/localization/repository/localization_repository.dart' as _i74;
import 'domain/localization/usecases/get_localization.dart' as _i88;
import 'domain/localization/usecases/save_localization.dart' as _i81;
import 'domain/message/bloc/message_bloc.dart' as _i28;
import 'domain/movie/bloc/bloc/movie_bloc.dart' as _i92;
import 'domain/movie/usecases/get_movie.dart' as _i71;
import 'domain/movies/bloc/bloc/movies_bloc.dart' as _i93;
import 'domain/movies/repository/main_page_repository.dart' as _i30;
import 'domain/movies/usecases/get_movies.dart' as _i72;
import 'domain/sing_in/bloc/sing_in_bloc.dart' as _i94;
import 'domain/sing_in/repository/sing_in_repository.dart' as _i44;
import 'domain/sing_in/usecases/login_user.dart' as _i76;
import 'domain/sing_up/bloc/sing_up_bloc.dart' as _i95;
import 'domain/sing_up/repository/sing_up_repository.dart' as _i47;
import 'domain/sing_up/usecases/register_user.dart' as _i79;
import 'domain/token/bloc/token_bloc.dart' as _i82;
import 'domain/token/token_repository.dart' as _i52;
import 'domain/token/usecases/refresh_token.dart' as _i78;
import 'domain/user/bloc/user_bloc.dart' as _i84;
import 'domain/user/repository/user_repository.dart' as _i56;
import 'domain/user/usecases/get_me.dart' as _i70;
import 'domain/user/usecases/logout.dart' as _i77;
import 'domain/user/usecases/update_user.dart' as _i83;
import 'injection_module.dart' as _i98; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
Future<_i1.GetIt> $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) async {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  final injectionModule = _$InjectionModule();
  gh.factory<_i3.AgeRestrictionSharedPreferences>(
      () => _i3.AgeRestrictionSharedPreferencesImpl());
  gh.singleton<_i4.AnalyticsRepository>(_i5.AnalyticsRepositoryImpl());
  gh.factory<_i6.DioClient>(() => _i6.DioClient());
  gh.factory<_i7.EpisodeDataSource>(
      () => _i7.EpisodeDataSourceImpl(get<_i6.DioClient>()));
  gh.factory<_i8.EpisodeRepository>(
      () => _i9.EpisodeRepositoryImp(get<_i7.EpisodeDataSource>()));
  gh.factory<_i10.FaqDataSource>(
      () => _i10.FaqDataSourceImpl(get<_i6.DioClient>()));
  gh.factory<_i11.FaqRepository>(
      () => _i12.FaqRepositoryImpl(get<_i10.FaqDataSource>()));
  gh.factory<_i13.ForgotPasswordRemoteDataSource>(
      () => _i13.ForgotPasswordRemoteDataSourceImpl(get<_i6.DioClient>()));
  gh.factory<_i14.ForgotPasswordRepository>(() =>
      _i15.ForgotPasswordRepositoryImpl(
          get<_i13.ForgotPasswordRemoteDataSource>()));
  gh.factory<_i16.GetEpisdeById>(
      () => _i16.GetEpisdeById(get<_i8.EpisodeRepository>()));
  gh.factory<_i17.GetFaq>(() => _i17.GetFaq(get<_i11.FaqRepository>()));
  gh.lazySingleton<_i18.HistoryRemoteDataSource>(
      () => _i18.HistoryRemoteDataSourceImpl(get<_i6.DioClient>()));
  gh.lazySingleton<_i19.HistoryRepository>(
      () => _i20.HistoryRepositoryImpl(get<_i18.HistoryRemoteDataSource>()));
  gh.factory<_i21.HtmlReadingPageDataSource>(
      () => _i21.HtmlReadingPageDataSourceImpl(get<_i6.DioClient>()));
  gh.factory<_i22.HtmlReadingPageRepository>(() =>
      _i23.HtmlReadingPageRepositoryImpl(
          get<_i21.HtmlReadingPageDataSource>()));
  gh.factory<_i24.LibraryRemoteDataSorces>(
      () => _i24.LibraryRemoteDataSorcesImpl(get<_i6.DioClient>()));
  gh.factory<_i25.LibraryRepository>(
      () => _i26.LibraryRepositoryImpl(get<_i24.LibraryRemoteDataSorces>()));
  gh.lazySingleton<_i27.LocalizationSharedPreferences>(
      () => _i27.LocalizationSharedPreferencesImpl());
  gh.factory<_i28.MessageBloc>(() => _i28.MessageBloc());
  gh.lazySingleton<_i29.MoviesRemoteDataSource>(
      () => _i29.MoviesRemoteDataSourceImpl(get<_i6.DioClient>()));
  gh.lazySingleton<_i30.MoviesRepository>(
      () => _i31.MoviesRepositoryImpl(get<_i29.MoviesRemoteDataSource>()));
  gh.factory<_i32.NewPassword>(
      () => _i32.NewPassword(get<_i14.ForgotPasswordRepository>()));
  gh.factory<_i33.PostComment>(
      () => _i33.PostComment(get<_i8.EpisodeRepository>()));
  gh.factory<_i34.RecoveryPassword>(
      () => _i34.RecoveryPassword(get<_i14.ForgotPasswordRepository>()));
  gh.factory<_i35.SendApplicationLaunched>(
      () => _i35.SendApplicationLaunched(get<_i4.AnalyticsRepository>()));
  gh.factory<_i36.SendMoviePageOpened>(
      () => _i36.SendMoviePageOpened(get<_i4.AnalyticsRepository>()));
  gh.factory<_i37.SendMoviePlayed>(
      () => _i37.SendMoviePlayed(get<_i4.AnalyticsRepository>()));
  gh.factory<_i38.SendPlayerPageOpened>(
      () => _i38.SendPlayerPageOpened(get<_i4.AnalyticsRepository>()));
  gh.factory<_i39.SendPressedBuyButtonMoviePage>(
      () => _i39.SendPressedBuyButtonMoviePage(get<_i4.AnalyticsRepository>()));
  gh.factory<_i40.SendPressedBuyButtonPlayerPage>(() =>
      _i40.SendPressedBuyButtonPlayerPage(get<_i4.AnalyticsRepository>()));
  gh.factory<_i41.SendPressedWatchSecondSeasonNotBought>(() =>
      _i41.SendPressedWatchSecondSeasonNotBought(
          get<_i4.AnalyticsRepository>()));
  await gh.factoryAsync<_i42.SharedPreferences>(
    () => injectionModule.prefs,
    preResolve: true,
  );
  gh.factory<_i43.SingInRemoteDataSource>(
      () => _i43.SingInRemoteDataSourceImpl(get<_i6.DioClient>()));
  gh.factory<_i44.SingInRepository>(
      () => _i45.SingInRepositoryImpl(get<_i43.SingInRemoteDataSource>()));
  gh.factory<_i46.SingUpRemoteDataSource>(
      () => _i46.SingUpRemoteDataSourceImpl(get<_i6.DioClient>()));
  gh.factory<_i47.SingUpRepository>(
      () => _i48.SingUpRepositoryImpl(get<_i46.SingUpRemoteDataSource>()));
  gh.factory<_i49.SortByDate>(
      () => _i49.SortByDate(get<_i8.EpisodeRepository>()));
  gh.factory<_i50.SortByLikes>(
      () => _i50.SortByLikes(get<_i8.EpisodeRepository>()));
  gh.factory<_i51.TokenRemoteDataSource>(
      () => _i51.TokenRemoteDataSourceImpl(get<_i6.DioClient>()));
  gh.factory<_i52.TokenRepository>(() => _i53.TokenRepositoryImpl(
      tokenRemoteDataSource: get<_i51.TokenRemoteDataSource>()));
  gh.lazySingleton<_i54.TokenSharedPreferences>(
      () => _i54.TokenSharedPreferencesImpl());
  gh.factory<_i55.UserRemoteDataSource>(
      () => _i55.UserRemoteDataSourceImpl(get<_i6.DioClient>()));
  gh.factory<_i56.UserRepository>(
      () => _i57.UserRepositoryImpl(get<_i55.UserRemoteDataSource>()));
  gh.factory<_i58.AddHistory>(
      () => _i58.AddHistory(get<_i19.HistoryRepository>()));
  gh.factory<_i59.AddVote>(() => _i59.AddVote(get<_i8.EpisodeRepository>()));
  gh.factory<_i60.AgeRestrictionRepository>(() =>
      _i61.AgeRestrictionRepositoryImpl(
          sharedPreferences: get<_i3.AgeRestrictionSharedPreferences>()));
  gh.lazySingleton<_i62.AnalyticsBloc>(() => _i62.AnalyticsBloc(
        get<_i35.SendApplicationLaunched>(),
        get<_i36.SendMoviePageOpened>(),
        get<_i37.SendMoviePlayed>(),
        get<_i38.SendPlayerPageOpened>(),
        get<_i39.SendPressedBuyButtonMoviePage>(),
        get<_i40.SendPressedBuyButtonPlayerPage>(),
        get<_i41.SendPressedWatchSecondSeasonNotBought>(),
      ));
  gh.factory<_i63.ChangePasswordRemoteDataSource>(
      () => _i63.ChangePasswordRemoteDataSourceImpl(get<_i6.DioClient>()));
  gh.factory<_i64.ChangePasswordRepository>(() =>
      _i65.ChangePasswordRepositoryImpl(
          get<_i63.ChangePasswordRemoteDataSource>()));
  gh.factory<_i66.ForgotPasswordBloc>(() => _i66.ForgotPasswordBloc(
        get<_i32.NewPassword>(),
        get<_i34.RecoveryPassword>(),
      ));
  gh.factory<_i67.GetConfirmation>(
      () => _i67.GetConfirmation(get<_i60.AgeRestrictionRepository>()));
  gh.factory<_i68.GetHtmlText>(
      () => _i68.GetHtmlText(get<_i22.HtmlReadingPageRepository>()));
  gh.factory<_i69.GetLibrary>(
      () => _i69.GetLibrary(get<_i25.LibraryRepository>()));
  gh.factory<_i70.GetMe>(() => _i70.GetMe(get<_i56.UserRepository>()));
  gh.factory<_i71.GetMovie>(() => _i71.GetMovie(get<_i30.MoviesRepository>()));
  gh.factory<_i72.GetMovies>(
      () => _i72.GetMovies(get<_i30.MoviesRepository>()));
  gh.lazySingleton<_i73.HistoryBloc>(
      () => _i73.HistoryBloc(get<_i58.AddHistory>()));
  gh.lazySingleton<_i74.LocalizationRepository>(() =>
      _i75.LocalizationRepositoryImpl(
          sharedPreferences: get<_i27.LocalizationSharedPreferences>()));
  gh.factory<_i76.LoginUser>(
      () => _i76.LoginUser(get<_i44.SingInRepository>()));
  gh.factory<_i77.Logout>(() => _i77.Logout(get<_i56.UserRepository>()));
  gh.factory<_i78.RefreshToken>(
      () => _i78.RefreshToken(get<_i52.TokenRepository>()));
  gh.factory<_i79.RegisterUser>(
      () => _i79.RegisterUser(get<_i47.SingUpRepository>()));
  gh.factory<_i80.SaveConfirmation>(
      () => _i80.SaveConfirmation(get<_i60.AgeRestrictionRepository>()));
  gh.factory<_i81.SaveLocalization>(
      () => _i81.SaveLocalization(get<_i74.LocalizationRepository>()));
  gh.singletonAsync<_i82.TokenBloc>(
      () => _i82.TokenBloc.create(refreshToken: get<_i78.RefreshToken>()));
  gh.factory<_i83.UpdateUser>(
      () => _i83.UpdateUser(get<_i56.UserRepository>()));
  gh.lazySingleton<_i84.UserBloc>(() => _i84.UserBloc(
        get<_i70.GetMe>(),
        get<_i77.Logout>(),
        get<_i83.UpdateUser>(),
      ));
  gh.factory<_i85.AgeRestrictionBloc>(() => _i85.AgeRestrictionBloc(
        get<_i67.GetConfirmation>(),
        get<_i80.SaveConfirmation>(),
      ));
  gh.factory<_i86.ChangePassword>(
      () => _i86.ChangePassword(get<_i64.ChangePasswordRepository>()));
  gh.factory<_i87.ChangePasswordBloc>(
      () => _i87.ChangePasswordBloc(get<_i86.ChangePassword>()));
  gh.factory<_i88.GetLocalization>(
      () => _i88.GetLocalization(get<_i74.LocalizationRepository>()));
  gh.factory<_i89.HtmlReadingPageBloc>(() => _i89.HtmlReadingPageBloc(
        get<_i68.GetHtmlText>(),
        get<_i88.GetLocalization>(),
      ));
  gh.factory<_i90.LibraryBloc>(() => _i90.LibraryBloc(
        get<_i69.GetLibrary>(),
        get<_i88.GetLocalization>(),
      ));
  gh.singleton<_i91.LocalizationBloc>(_i91.LocalizationBloc(
    get<_i88.GetLocalization>(),
    get<_i81.SaveLocalization>(),
  ));
  gh.factory<_i92.MovieBloc>(() => _i92.MovieBloc(
        getMovie: get<_i71.GetMovie>(),
        getLocalization: get<_i88.GetLocalization>(),
      ));
  gh.singleton<_i93.MoviesBloc>(_i93.MoviesBloc(
    get<_i72.GetMovies>(),
    get<_i88.GetLocalization>(),
  ));
  gh.factory<_i94.SingInBloc>(() => _i94.SingInBloc(
        get<_i76.LoginUser>(),
        get<_i84.UserBloc>(),
      ));
  gh.factory<_i95.SingUpBloc>(() => _i95.SingUpBloc(
        get<_i79.RegisterUser>(),
        get<_i84.UserBloc>(),
      ));
  gh.factory<_i96.EpisodeBloc>(() => _i96.EpisodeBloc(
        get<_i16.GetEpisdeById>(),
        get<_i88.GetLocalization>(),
        get<_i33.PostComment>(),
        get<_i59.AddVote>(),
        get<_i50.SortByLikes>(),
        get<_i49.SortByDate>(),
      ));
  gh.factory<_i97.FaqBloc>(() => _i97.FaqBloc(
        get<_i17.GetFaq>(),
        get<_i88.GetLocalization>(),
      ));
  return get;
}

class _$InjectionModule extends _i98.InjectionModule {}
