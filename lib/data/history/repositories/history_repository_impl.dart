import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/data/history/modal/history.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:nine_pandas/domain/history/history_repository.dart';

import '../datasources/history_remote_data_source.dart';

@LazySingleton(as: HistoryRepository)
class HistoryRepositoryImpl implements HistoryRepository {
  final HistoryRemoteDataSource _historyRemoteDataSource;

  HistoryRepositoryImpl(this._historyRemoteDataSource);
  @override
  Future<Either<Failure, History>> addHistory(int episodeId) async {
    try {
      final history = await _historyRemoteDataSource.addHistory(episodeId);

      return Right(history);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure());
      }
      return Left(UnknownFailure());
    }
  }
}
