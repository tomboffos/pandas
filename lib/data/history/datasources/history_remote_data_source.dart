import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/data/history/modal/history.dart';

import 'constants/endpoints.dart';

abstract class HistoryRemoteDataSource {
  Future<History> addHistory(int episodeId);
}

@LazySingleton(as: HistoryRemoteDataSource)
class HistoryRemoteDataSourceImpl implements HistoryRemoteDataSource {
  DioClient? dioClient;

  HistoryRemoteDataSourceImpl(this.dioClient);

  @override
  Future<History> addHistory(int episodeId) async {
    try {
      final res = await dioClient!.post(
        Endpoints.history,
        queryParameters: {
          'episodeId': episodeId,
        },
      );

      return History.fromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
            code: e.response!.statusCode, message: e.response!.statusMessage);
      }
      throw e;
    }
  }
}
