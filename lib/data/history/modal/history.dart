import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/data/user/models/user.dart';

part 'history.freezed.dart';
part 'history.g.dart';

@freezed
class History with _$History {
  const History._();

  const factory History({
    required int id,
    required String createdAt,
    required String updatedAt,
    required User user,
    required Episode episode,
  }) = _History;

  factory History.fromJson(Map<String, Object?> json) =>
      _$HistoryFromJson(json);
}
