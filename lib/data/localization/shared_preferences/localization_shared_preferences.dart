import 'package:injectable/injectable.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:universal_io/io.dart';

import 'constants/preferences.dart';

abstract class LocalizationSharedPreferences {
  Future saveConfirmation(String locale);
  Future getConfirmation();
}

@LazySingleton(as: LocalizationSharedPreferences)
class LocalizationSharedPreferencesImpl
    implements LocalizationSharedPreferences {
  final prefs = getIt<SharedPreferences>();

  @override
  Future saveConfirmation(String locale) async {
    final result = await prefs.setString(
      Preferences.currentLocale,
      locale,
    );
    return result;
  }

  @override
  Future getConfirmation() async {
    final result = prefs.getString(
      Preferences.currentLocale,
    );
    if (result != null) {
      return result;
    } else {
      return Platform.localeName.split('-')[0] == 'ru' ? 'Ru' : 'En';
    }
  }
}
