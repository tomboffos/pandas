import 'package:injectable/injectable.dart';
import 'package:nine_pandas/domain/localization/repository/localization_repository.dart';

import 'shared_preferences/localization_shared_preferences.dart';

@LazySingleton(as: LocalizationRepository)
class LocalizationRepositoryImpl implements LocalizationRepository {
  final LocalizationSharedPreferences sharedPreferences;

  LocalizationRepositoryImpl({
    required this.sharedPreferences,
  });

  @override
  Future getLocalization() async {
    return await sharedPreferences.getConfirmation();
  }

  @override
  Future saveLocalization(String locale) {
    return sharedPreferences.saveConfirmation(locale);
  }
}
