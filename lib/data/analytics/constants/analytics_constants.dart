class AnalyticsConstants {
  AnalyticsConstants._();

  static const String applicationLaunchedEvent = 'application_launched';
  static const String moviePageOpenedEvent = 'movie_page_opened';
  static const String moviePlayedEvent = 'movie_played';
  static const String playerPageOpenedEvent = 'player_page_opened_event';
  static const String pressedWatchSecondNotBoughtEvent =
      'pressed_watch_second_season_not_bought';
  static const String pressedBuyButtonMoviePageEvent =
      'pressed_buy_button_movie_page';
  static const String pressedBuyButtonPalyerPageEvent =
      'pressed_buy_button_player_page';

  static const String webPlatform = 'web';

  static const String platformField = 'platform_type';
  static const String movieIdField = 'movie_id';
  static const String movieTitleField = 'movie_title';
  static const String playerErrorEvent = 'player_error';
  static const String userField = 'user_id';
  static const String errorField = 'error';
  static const String stackField = 'stack';
  static const String linkField = 'link';
  static const String networkField = 'network';
  static const String versionField = 'version';
  static const String buildField = 'build';
  static const String seriesSeason = 'season';
  static const String seriesEpisode = 'episode';
}
