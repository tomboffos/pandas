import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/domain/analytics/analytics_repositories.dart';

import 'constants/analytics_constants.dart';

@Singleton(as: AnalyticsRepository)
class AnalyticsRepositoryImpl implements AnalyticsRepository {
  AnalyticsRepositoryImpl() {
    FirebaseAnalytics.instance;
  }

  @override
  Future sendApplicationLaunchedEvent(int? userId) {
    return FirebaseAnalytics.instance.logEvent(
      name: AnalyticsConstants.applicationLaunchedEvent,
      parameters: <String, dynamic>{
        AnalyticsConstants.platformField: AnalyticsConstants.webPlatform,
        if (userId != null) AnalyticsConstants.userField: userId,
      },
    );
  }

  @override
  Future sendMoviePageOpenedEvent(
      String movieId, String movieTitle, int? userId) {
    return FirebaseAnalytics.instance.logEvent(
      name: AnalyticsConstants.moviePageOpenedEvent,
      parameters: <String, dynamic>{
        AnalyticsConstants.movieIdField: movieId,
        AnalyticsConstants.movieTitleField: movieTitle,
        AnalyticsConstants.platformField: AnalyticsConstants.webPlatform,
        if (userId != null) AnalyticsConstants.userField: userId,
      },
    );
  }

  @override
  Future sendMoviePlayedEvent(String movieId, String movieTitle, String? season,
      String? episode, int? userId) {
    return FirebaseAnalytics.instance.logEvent(
      name: AnalyticsConstants.moviePlayedEvent,
      parameters: <String, dynamic>{
        AnalyticsConstants.movieIdField: movieId,
        AnalyticsConstants.movieTitleField: movieTitle,
        if (season != null) AnalyticsConstants.seriesSeason: season,
        if (episode != null) AnalyticsConstants.seriesEpisode: episode,
        AnalyticsConstants.platformField: AnalyticsConstants.webPlatform,
        if (userId != null) AnalyticsConstants.userField: userId,
      },
    );
  }

  @override
  Future sendPlayerPageOpenedEvent(String movieId, String movieTitle,
      String? season, String? episode, int? userId) {
    return FirebaseAnalytics.instance.logEvent(
      name: AnalyticsConstants.playerPageOpenedEvent,
      parameters: <String, dynamic>{
        AnalyticsConstants.movieIdField: movieId,
        AnalyticsConstants.movieTitleField: movieTitle,
        if (season != null) AnalyticsConstants.seriesSeason: season,
        if (episode != null) AnalyticsConstants.seriesEpisode: episode,
        AnalyticsConstants.platformField: AnalyticsConstants.webPlatform,
        if (userId != null) AnalyticsConstants.userField: userId,
      },
    );
  }

  @override
  Future sendPressedBuyButtonMoviePageEvent(
      String movieId, String movieTitle, int? userId) {
    return FirebaseAnalytics.instance.logEvent(
      name: AnalyticsConstants.pressedBuyButtonMoviePageEvent,
      parameters: <String, dynamic>{
        AnalyticsConstants.movieIdField: movieId,
        AnalyticsConstants.movieTitleField: movieTitle,
        AnalyticsConstants.platformField: AnalyticsConstants.webPlatform,
        if (userId != null) AnalyticsConstants.userField: userId,
      },
    );
  }

  @override
  Future sendPressedBuyButtonPlayerPageEvent(String movieId, String movieTitle,
      String? season, String? episode, int? userId) {
    return FirebaseAnalytics.instance.logEvent(
      name: AnalyticsConstants.pressedBuyButtonPalyerPageEvent,
      parameters: <String, dynamic>{
        AnalyticsConstants.movieIdField: movieId,
        AnalyticsConstants.movieTitleField: movieTitle,
        if (season != null) AnalyticsConstants.seriesSeason: season,
        if (episode != null) AnalyticsConstants.seriesEpisode: episode,
        AnalyticsConstants.platformField: AnalyticsConstants.webPlatform,
        if (userId != null) AnalyticsConstants.userField: userId,
      },
    );
  }

  @override
  Future sendPressedWatchSecondSeasonNotBoughtEvent(
      String movieId, String movieTitle, int? userId) {
    return FirebaseAnalytics.instance.logEvent(
      name: AnalyticsConstants.pressedWatchSecondNotBoughtEvent,
      parameters: <String, dynamic>{
        AnalyticsConstants.movieIdField: movieId,
        AnalyticsConstants.movieTitleField: movieTitle,
        AnalyticsConstants.platformField: AnalyticsConstants.webPlatform,
        if (userId != null) AnalyticsConstants.userField: userId,
      },
    );
  }
}
