import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/sing_up/repository/sing_up_repository.dart';

import 'datasources/sing_up_remote_data_source.dart';

@Injectable(as: SingUpRepository)
class SingUpRepositoryImpl implements SingUpRepository {
  final SingUpRemoteDataSource _singUpRemoteDataSource;
  SingUpRepositoryImpl(this._singUpRemoteDataSource);

  Future<Either<Failure, dynamic>> registerUser({
    required String userName,
    required String name,
    required String password,
  }) async {
    try {
      final res =
          await _singUpRemoteDataSource.registerUser(userName, name, password);

      return Right(res);
    } catch (e) {
      if (e is ServerException && e.code == 400) {
        return Left(ServerFailure(message: e.message));
      }

      return Left(UnavailableCacheFailure());
    }
  }
}
