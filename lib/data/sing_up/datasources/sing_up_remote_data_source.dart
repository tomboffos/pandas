import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/core/network/endpoints/user_endpoint.dart';

import 'constants/endpoint.dart';

abstract class SingUpRemoteDataSource {
  Future registerUser(String userName, String name, String password);
}

@Injectable(as: SingUpRemoteDataSource)
class SingUpRemoteDataSourceImpl implements SingUpRemoteDataSource {
  final DioClient dioClient;

  SingUpRemoteDataSourceImpl(this.dioClient);

  @override
  Future registerUser(String userName, String name, String password) async {
    Map<String, dynamic> body = {
      'username': userName,
      'name': name,
      'password': password,
      'emailNotification': false,
      'smsNotification': false,
    };
    try {
      final res = await dioClient.post(Endpoint.register, data: body);

      return res;
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
            code: e.response!.statusCode, message: e.response!.statusMessage);
      }
      throw e;
    }
  }
}
