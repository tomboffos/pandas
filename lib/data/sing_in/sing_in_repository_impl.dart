import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/sing_in/repository/sing_in_repository.dart';

import 'datasources/sing_in_remote_data_source.dart';

@Injectable(as: SingInRepository)
class SingInRepositoryImpl implements SingInRepository {
  final SingInRemoteDataSource _singInRemoteDataSource;
  SingInRepositoryImpl(this._singInRemoteDataSource);

  @override
  Future<Either<Failure, dynamic>> loginUser({
    required String username,
    required String password,
  }) async {
    try {
      final user = await _singInRemoteDataSource.loginUser(username, password);

      return Right(user);
    } catch (e) {
      if (e is ServerException && e.code == 400) {
        return Left(ServerFailure(message: e.message));
      }

      return Left(UnavailableCacheFailure());
    }
  }
}
