import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/data/token/model/token.dart';

import 'constants/endpoint.dart';

abstract class SingInRemoteDataSource {
  Future loginUser(String userName, String password);
}

@Injectable(as: SingInRemoteDataSource)
class SingInRemoteDataSourceImpl implements SingInRemoteDataSource {
  final DioClient _dioClient;

  SingInRemoteDataSourceImpl(this._dioClient);

  @override
  Future loginUser(String username, String password) async {
    Map<String, String> body = {
      'username': username,
      'password': password,
      'grant_type': 'password',
    };
    try {
      final res = await _dioClient.post(
        Endpoint.login,
        data: body,
        options: Options(
          contentType: 'application/x-www-form-urlencoded',
          headers: {
            'Authorization': 'Basic ${DioClient.CLIENT_TOKEN}',
          },
        ),
      );
      return Token.fromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
          code: e.response!.statusCode,
          message: e.response!.statusMessage,
        );
      }
    }
  }
}
