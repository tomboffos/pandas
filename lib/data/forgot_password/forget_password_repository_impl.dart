import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/forgot_password/repository/forgot_password_repository.dart';

import 'datasources/forgot_password_remote_data_source.dart';

@Injectable(as: ForgotPasswordRepository)
class ForgotPasswordRepositoryImpl implements ForgotPasswordRepository {
  final ForgotPasswordRemoteDataSource _forgotPasswordRemoteDataSource;
  ForgotPasswordRepositoryImpl(this._forgotPasswordRemoteDataSource);

  @override
  Future<Either<Failure, dynamic>> newPassword(
      String hash, String password, String repeatPassword) async {
    try {
      final res = await _forgotPasswordRemoteDataSource.newPassword(
          hash, password, repeatPassword);

      return Right(res);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure(message: e.message));
      }

      return Left(UnknownFailure());
    }
  }

  @override
  Future<Either<Failure, dynamic>> recoveryPassword(String email) async {
    try {
      final res = await _forgotPasswordRemoteDataSource.recoveryPassword(email);

      return Right(res);
    } catch (e) {
      if (e is ServerException && e.code == 400) {
        return Left(ServerFailure(message: e.message));
      }

      return Left(UnavailableCacheFailure());
    }
  }
}
