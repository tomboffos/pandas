import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/core/network/endpoints/user_endpoint.dart';

abstract class ForgotPasswordRemoteDataSource {
  Future recoveryPassword(String email);
  Future newPassword(String hash, String password, String repeatPassword);
}

@Injectable(as: ForgotPasswordRemoteDataSource)
class ForgotPasswordRemoteDataSourceImpl
    implements ForgotPasswordRemoteDataSource {
  final DioClient dioClient;

  ForgotPasswordRemoteDataSourceImpl(this.dioClient);

  @override
  Future recoveryPassword(String email) async {
    Map<String, dynamic> body = {'email': email};

    try {
      final res = await dioClient.get(UserEndpoint.forgetPassword,
          queryParameters: body);

      return res;
    } catch (e) {
      if (e is DioError) {
        if (e.response!.statusCode == 403) {
          return UserNotExists();
        }
        throw ServerException(
            code: e.response?.statusCode ?? 400,
            message: e.response?.statusMessage ?? e.toString());
      }
    }
  }

  @override
  Future newPassword(
      String hash, String password, String repeatPassword) async {
    Map<String, dynamic> body = {
      'hash': hash,
      'password': password,
      'repeatPassword': repeatPassword,
    };

    try {
      final res = await dioClient.post(UserEndpoint.forgetPassword,
          queryParameters: body);

      return res;
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
            code: e.response?.statusCode ?? 400,
            message: e.response?.statusMessage ?? e.toString());
      }
    }
  }
}
