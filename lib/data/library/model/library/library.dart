import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/data/library/model/libraryData/library_data.dart';
import 'package:nine_pandas/data/movie/models/ads_banner/ads_banner.dart';

part 'library.freezed.dart';
part 'library.g.dart';

@freezed
class Library with _$Library {
  const Library._();
  const factory Library({
    required AdsBanner adsBanner,
    required List<LibraryData> results,
  }) = _Library;

  factory Library.fromJson(Map<String, dynamic> json) =>
      _$LibraryFromJson(json);
}
