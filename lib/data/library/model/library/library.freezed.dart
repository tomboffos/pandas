// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'library.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Library _$LibraryFromJson(Map<String, dynamic> json) {
  return _Library.fromJson(json);
}

/// @nodoc
mixin _$Library {
  AdsBanner get adsBanner => throw _privateConstructorUsedError;
  List<LibraryData> get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LibraryCopyWith<Library> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LibraryCopyWith<$Res> {
  factory $LibraryCopyWith(Library value, $Res Function(Library) then) =
      _$LibraryCopyWithImpl<$Res, Library>;
  @useResult
  $Res call({AdsBanner adsBanner, List<LibraryData> results});

  $AdsBannerCopyWith<$Res> get adsBanner;
}

/// @nodoc
class _$LibraryCopyWithImpl<$Res, $Val extends Library>
    implements $LibraryCopyWith<$Res> {
  _$LibraryCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? adsBanner = null,
    Object? results = null,
  }) {
    return _then(_value.copyWith(
      adsBanner: null == adsBanner
          ? _value.adsBanner
          : adsBanner // ignore: cast_nullable_to_non_nullable
              as AdsBanner,
      results: null == results
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<LibraryData>,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $AdsBannerCopyWith<$Res> get adsBanner {
    return $AdsBannerCopyWith<$Res>(_value.adsBanner, (value) {
      return _then(_value.copyWith(adsBanner: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_LibraryCopyWith<$Res> implements $LibraryCopyWith<$Res> {
  factory _$$_LibraryCopyWith(
          _$_Library value, $Res Function(_$_Library) then) =
      __$$_LibraryCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({AdsBanner adsBanner, List<LibraryData> results});

  @override
  $AdsBannerCopyWith<$Res> get adsBanner;
}

/// @nodoc
class __$$_LibraryCopyWithImpl<$Res>
    extends _$LibraryCopyWithImpl<$Res, _$_Library>
    implements _$$_LibraryCopyWith<$Res> {
  __$$_LibraryCopyWithImpl(_$_Library _value, $Res Function(_$_Library) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? adsBanner = null,
    Object? results = null,
  }) {
    return _then(_$_Library(
      adsBanner: null == adsBanner
          ? _value.adsBanner
          : adsBanner // ignore: cast_nullable_to_non_nullable
              as AdsBanner,
      results: null == results
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<LibraryData>,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Library extends _Library {
  const _$_Library(
      {required this.adsBanner, required final List<LibraryData> results})
      : _results = results,
        super._();

  factory _$_Library.fromJson(Map<String, dynamic> json) =>
      _$$_LibraryFromJson(json);

  @override
  final AdsBanner adsBanner;
  final List<LibraryData> _results;
  @override
  List<LibraryData> get results {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_results);
  }

  @override
  String toString() {
    return 'Library(adsBanner: $adsBanner, results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Library &&
            (identical(other.adsBanner, adsBanner) ||
                other.adsBanner == adsBanner) &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, adsBanner, const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LibraryCopyWith<_$_Library> get copyWith =>
      __$$_LibraryCopyWithImpl<_$_Library>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LibraryToJson(
      this,
    );
  }
}

abstract class _Library extends Library {
  const factory _Library(
      {required final AdsBanner adsBanner,
      required final List<LibraryData> results}) = _$_Library;
  const _Library._() : super._();

  factory _Library.fromJson(Map<String, dynamic> json) = _$_Library.fromJson;

  @override
  AdsBanner get adsBanner;
  @override
  List<LibraryData> get results;
  @override
  @JsonKey(ignore: true)
  _$$_LibraryCopyWith<_$_Library> get copyWith =>
      throw _privateConstructorUsedError;
}
