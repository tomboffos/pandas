// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Library _$$_LibraryFromJson(Map<String, dynamic> json) => _$_Library(
      adsBanner: AdsBanner.fromJson(json['adsBanner'] as Map<String, dynamic>),
      results: (json['results'] as List<dynamic>)
          .map((e) => LibraryData.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_LibraryToJson(_$_Library instance) =>
    <String, dynamic>{
      'adsBanner': instance.adsBanner,
      'results': instance.results,
    };
