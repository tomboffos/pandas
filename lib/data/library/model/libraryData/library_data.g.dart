// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'library_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LibraryData _$$_LibraryDataFromJson(Map<String, dynamic> json) =>
    _$_LibraryData(
      id: json['id'] as int,
      movieCollection:
          Movie.fromJson(json['movieCollection'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_LibraryDataToJson(_$_LibraryData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'movieCollection': instance.movieCollection,
    };
