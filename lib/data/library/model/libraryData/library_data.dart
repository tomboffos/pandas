import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';

part 'library_data.freezed.dart';
part 'library_data.g.dart';

@freezed
class LibraryData with _$LibraryData {
  const LibraryData._();
  const factory LibraryData({
    required int id,
    required Movie movieCollection,
  }) = _LibraryData;

  factory LibraryData.fromJson(Map<String, dynamic> json) =>
      _$LibraryDataFromJson(json);
}
