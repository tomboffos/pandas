// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'library_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LibraryData _$LibraryDataFromJson(Map<String, dynamic> json) {
  return _LibraryData.fromJson(json);
}

/// @nodoc
mixin _$LibraryData {
  int get id => throw _privateConstructorUsedError;
  Movie get movieCollection => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LibraryDataCopyWith<LibraryData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LibraryDataCopyWith<$Res> {
  factory $LibraryDataCopyWith(
          LibraryData value, $Res Function(LibraryData) then) =
      _$LibraryDataCopyWithImpl<$Res, LibraryData>;
  @useResult
  $Res call({int id, Movie movieCollection});

  $MovieCopyWith<$Res> get movieCollection;
}

/// @nodoc
class _$LibraryDataCopyWithImpl<$Res, $Val extends LibraryData>
    implements $LibraryDataCopyWith<$Res> {
  _$LibraryDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? movieCollection = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      movieCollection: null == movieCollection
          ? _value.movieCollection
          : movieCollection // ignore: cast_nullable_to_non_nullable
              as Movie,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $MovieCopyWith<$Res> get movieCollection {
    return $MovieCopyWith<$Res>(_value.movieCollection, (value) {
      return _then(_value.copyWith(movieCollection: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_LibraryDataCopyWith<$Res>
    implements $LibraryDataCopyWith<$Res> {
  factory _$$_LibraryDataCopyWith(
          _$_LibraryData value, $Res Function(_$_LibraryData) then) =
      __$$_LibraryDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, Movie movieCollection});

  @override
  $MovieCopyWith<$Res> get movieCollection;
}

/// @nodoc
class __$$_LibraryDataCopyWithImpl<$Res>
    extends _$LibraryDataCopyWithImpl<$Res, _$_LibraryData>
    implements _$$_LibraryDataCopyWith<$Res> {
  __$$_LibraryDataCopyWithImpl(
      _$_LibraryData _value, $Res Function(_$_LibraryData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? movieCollection = null,
  }) {
    return _then(_$_LibraryData(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      movieCollection: null == movieCollection
          ? _value.movieCollection
          : movieCollection // ignore: cast_nullable_to_non_nullable
              as Movie,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LibraryData extends _LibraryData {
  const _$_LibraryData({required this.id, required this.movieCollection})
      : super._();

  factory _$_LibraryData.fromJson(Map<String, dynamic> json) =>
      _$$_LibraryDataFromJson(json);

  @override
  final int id;
  @override
  final Movie movieCollection;

  @override
  String toString() {
    return 'LibraryData(id: $id, movieCollection: $movieCollection)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LibraryData &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.movieCollection, movieCollection) ||
                other.movieCollection == movieCollection));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, id, movieCollection);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LibraryDataCopyWith<_$_LibraryData> get copyWith =>
      __$$_LibraryDataCopyWithImpl<_$_LibraryData>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LibraryDataToJson(
      this,
    );
  }
}

abstract class _LibraryData extends LibraryData {
  const factory _LibraryData(
      {required final int id,
      required final Movie movieCollection}) = _$_LibraryData;
  const _LibraryData._() : super._();

  factory _LibraryData.fromJson(Map<String, dynamic> json) =
      _$_LibraryData.fromJson;

  @override
  int get id;
  @override
  Movie get movieCollection;
  @override
  @JsonKey(ignore: true)
  _$$_LibraryDataCopyWith<_$_LibraryData> get copyWith =>
      throw _privateConstructorUsedError;
}
