import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:nine_pandas/domain/library/repository/library_repository.dart';

import 'datasourses/library_remote_data_source.dart';
import 'model/library/library.dart';

@Injectable(as: LibraryRepository)
class LibraryRepositoryImpl implements LibraryRepository {
  final LibraryRemoteDataSorces libraryRemoteDataSorces;
  LibraryRepositoryImpl(this.libraryRemoteDataSorces);

  @override
  Future<Either<Failure, Library>> getLibrary(String locale) async {
    try {
      final library = await libraryRemoteDataSorces.getMovies(locale);
      return Right(library);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure(message: e.message));
      }
      throw e;
    }
  }
}
