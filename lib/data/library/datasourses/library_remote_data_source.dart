import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/network/dio_client.dart';

import '../model/library/library.dart';
import 'constants/endpoints.dart';

abstract class LibraryRemoteDataSorces {
  Future<Library> getMovies(String locale);
}

@Injectable(as: LibraryRemoteDataSorces)
class LibraryRemoteDataSorcesImpl implements LibraryRemoteDataSorces {
  DioClient dioClient;

  LibraryRemoteDataSorcesImpl(this.dioClient);

  @override
  Future<Library> getMovies(String locale) async {
    try {
      final result = await dioClient.get(
        Endpoints.library,
        queryParameters: {
          'locale': locale,
        },
      );
      return Library.fromJson(result);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
          code: e.response!.statusCode,
          message: e.response!.statusMessage,
        );
      }

      throw e;
    }
  }
}
