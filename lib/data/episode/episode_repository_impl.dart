import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/data/episode/datasources/episode_data_source.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/player/models/comment.dart';
import 'package:nine_pandas/domain/episode/repository/episode_repository.dart';

import 'models/episode/episode.dart';

@Injectable(as: EpisodeRepository)
class EpisodeRepositoryImp implements EpisodeRepository {
  final EpisodeDataSource _episodeDataSource;

  EpisodeRepositoryImp(this._episodeDataSource);

  @override
  Future<Either<Failure, Episode>> getEpisodeById(
      int movieId, int id, String locale) async {
    try {
      final episode =
          await _episodeDataSource.fetchEpisodeById(movieId, id, locale);

      return Right(episode);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure());
      }
      return Left(UnknownFailure());
    }
  }

  @override
  Future<Either<Failure, Comment>> commentUser({
    required String comment,
    required int movieCollection,
    required int episodeId,
  }) async {
    try {
      final commentEntity = await _episodeDataSource.postComment(
          comment: comment,
          movieCollection: movieCollection,
          episodeId: episodeId);

      return Right(commentEntity);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure());
      }
      return Left(UnknownFailure());
    }
  }

  @override
  Future<Either<Failure, dynamic>> addVote(
      {required int commentId, required int vote}) async {
    try {
      final response = await _episodeDataSource.addVotePost(
          commentId: commentId, vote: vote);

      return Right(response);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure());
      }
      return Left(UnknownFailure());
    }
  }

  @override
  Either<Failure, Episode> sortByLikes({required Episode episode}) {
    try {
      if (episode.comments != null) {
        List<Comment> comments = episode.comments!.map((e) => e).toList();

        comments.sort((comment, lastComment) =>
            lastComment.voteUp!.compareTo(comment.voteUp!));
        Episode newEpisode = episode.copyWith(comments: comments);

        return Right(newEpisode);
      }
      return Right(episode);
    } catch (e) {
      return Left(UnknownFailure());
    }
  }

  @override
  Either<Failure, Episode> sortByDate({required Episode episode}) {
    try {
      if (episode.comments != null) {
        List<Comment> comments = episode.comments!.map((e) => e).toList();

        comments.sort((comment, lastComment) =>
            lastComment.createdAt.compareTo(comment.createdAt));
        Episode newEpisode = episode.copyWith(comments: comments);

        return Right(newEpisode);
      }
      return Right(episode);
    } catch (e) {
      return Left(UnknownFailure());
    }
  }
}
