// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'episode.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Episode _$EpisodeFromJson(Map<String, dynamic> json) {
  return _Episode.fromJson(json);
}

/// @nodoc
mixin _$Episode {
  int get id => throw _privateConstructorUsedError;
  int get episodeNumber => throw _privateConstructorUsedError;
  EpisodeInfo? get data => throw _privateConstructorUsedError;
  MovieInfo? get movieCollection => throw _privateConstructorUsedError;
  List<Season>? get seasons => throw _privateConstructorUsedError;
  List<Comment>? get comments => throw _privateConstructorUsedError;
  int? get videoDuration => throw _privateConstructorUsedError;
  Player? get player => throw _privateConstructorUsedError;
  EpisodeType? get episodeType => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $EpisodeCopyWith<Episode> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EpisodeCopyWith<$Res> {
  factory $EpisodeCopyWith(Episode value, $Res Function(Episode) then) =
      _$EpisodeCopyWithImpl<$Res, Episode>;
  @useResult
  $Res call(
      {int id,
      int episodeNumber,
      EpisodeInfo? data,
      MovieInfo? movieCollection,
      List<Season>? seasons,
      List<Comment>? comments,
      int? videoDuration,
      Player? player,
      EpisodeType? episodeType});

  $EpisodeInfoCopyWith<$Res>? get data;
  $MovieInfoCopyWith<$Res>? get movieCollection;
  $PlayerCopyWith<$Res>? get player;
}

/// @nodoc
class _$EpisodeCopyWithImpl<$Res, $Val extends Episode>
    implements $EpisodeCopyWith<$Res> {
  _$EpisodeCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? episodeNumber = null,
    Object? data = freezed,
    Object? movieCollection = freezed,
    Object? seasons = freezed,
    Object? comments = freezed,
    Object? videoDuration = freezed,
    Object? player = freezed,
    Object? episodeType = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      episodeNumber: null == episodeNumber
          ? _value.episodeNumber
          : episodeNumber // ignore: cast_nullable_to_non_nullable
              as int,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as EpisodeInfo?,
      movieCollection: freezed == movieCollection
          ? _value.movieCollection
          : movieCollection // ignore: cast_nullable_to_non_nullable
              as MovieInfo?,
      seasons: freezed == seasons
          ? _value.seasons
          : seasons // ignore: cast_nullable_to_non_nullable
              as List<Season>?,
      comments: freezed == comments
          ? _value.comments
          : comments // ignore: cast_nullable_to_non_nullable
              as List<Comment>?,
      videoDuration: freezed == videoDuration
          ? _value.videoDuration
          : videoDuration // ignore: cast_nullable_to_non_nullable
              as int?,
      player: freezed == player
          ? _value.player
          : player // ignore: cast_nullable_to_non_nullable
              as Player?,
      episodeType: freezed == episodeType
          ? _value.episodeType
          : episodeType // ignore: cast_nullable_to_non_nullable
              as EpisodeType?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $EpisodeInfoCopyWith<$Res>? get data {
    if (_value.data == null) {
      return null;
    }

    return $EpisodeInfoCopyWith<$Res>(_value.data!, (value) {
      return _then(_value.copyWith(data: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $MovieInfoCopyWith<$Res>? get movieCollection {
    if (_value.movieCollection == null) {
      return null;
    }

    return $MovieInfoCopyWith<$Res>(_value.movieCollection!, (value) {
      return _then(_value.copyWith(movieCollection: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $PlayerCopyWith<$Res>? get player {
    if (_value.player == null) {
      return null;
    }

    return $PlayerCopyWith<$Res>(_value.player!, (value) {
      return _then(_value.copyWith(player: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_EpisodeCopyWith<$Res> implements $EpisodeCopyWith<$Res> {
  factory _$$_EpisodeCopyWith(
          _$_Episode value, $Res Function(_$_Episode) then) =
      __$$_EpisodeCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      int episodeNumber,
      EpisodeInfo? data,
      MovieInfo? movieCollection,
      List<Season>? seasons,
      List<Comment>? comments,
      int? videoDuration,
      Player? player,
      EpisodeType? episodeType});

  @override
  $EpisodeInfoCopyWith<$Res>? get data;
  @override
  $MovieInfoCopyWith<$Res>? get movieCollection;
  @override
  $PlayerCopyWith<$Res>? get player;
}

/// @nodoc
class __$$_EpisodeCopyWithImpl<$Res>
    extends _$EpisodeCopyWithImpl<$Res, _$_Episode>
    implements _$$_EpisodeCopyWith<$Res> {
  __$$_EpisodeCopyWithImpl(_$_Episode _value, $Res Function(_$_Episode) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? episodeNumber = null,
    Object? data = freezed,
    Object? movieCollection = freezed,
    Object? seasons = freezed,
    Object? comments = freezed,
    Object? videoDuration = freezed,
    Object? player = freezed,
    Object? episodeType = freezed,
  }) {
    return _then(_$_Episode(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      episodeNumber: null == episodeNumber
          ? _value.episodeNumber
          : episodeNumber // ignore: cast_nullable_to_non_nullable
              as int,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as EpisodeInfo?,
      movieCollection: freezed == movieCollection
          ? _value.movieCollection
          : movieCollection // ignore: cast_nullable_to_non_nullable
              as MovieInfo?,
      seasons: freezed == seasons
          ? _value._seasons
          : seasons // ignore: cast_nullable_to_non_nullable
              as List<Season>?,
      comments: freezed == comments
          ? _value._comments
          : comments // ignore: cast_nullable_to_non_nullable
              as List<Comment>?,
      videoDuration: freezed == videoDuration
          ? _value.videoDuration
          : videoDuration // ignore: cast_nullable_to_non_nullable
              as int?,
      player: freezed == player
          ? _value.player
          : player // ignore: cast_nullable_to_non_nullable
              as Player?,
      episodeType: freezed == episodeType
          ? _value.episodeType
          : episodeType // ignore: cast_nullable_to_non_nullable
              as EpisodeType?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Episode extends _Episode {
  const _$_Episode(
      {required this.id,
      required this.episodeNumber,
      required this.data,
      required this.movieCollection,
      required final List<Season>? seasons,
      required final List<Comment>? comments,
      required this.videoDuration,
      required this.player,
      this.episodeType})
      : _seasons = seasons,
        _comments = comments,
        super._();

  factory _$_Episode.fromJson(Map<String, dynamic> json) =>
      _$$_EpisodeFromJson(json);

  @override
  final int id;
  @override
  final int episodeNumber;
  @override
  final EpisodeInfo? data;
  @override
  final MovieInfo? movieCollection;
  final List<Season>? _seasons;
  @override
  List<Season>? get seasons {
    final value = _seasons;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<Comment>? _comments;
  @override
  List<Comment>? get comments {
    final value = _comments;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final int? videoDuration;
  @override
  final Player? player;
  @override
  final EpisodeType? episodeType;

  @override
  String toString() {
    return 'Episode(id: $id, episodeNumber: $episodeNumber, data: $data, movieCollection: $movieCollection, seasons: $seasons, comments: $comments, videoDuration: $videoDuration, player: $player, episodeType: $episodeType)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Episode &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.episodeNumber, episodeNumber) ||
                other.episodeNumber == episodeNumber) &&
            (identical(other.data, data) || other.data == data) &&
            (identical(other.movieCollection, movieCollection) ||
                other.movieCollection == movieCollection) &&
            const DeepCollectionEquality().equals(other._seasons, _seasons) &&
            const DeepCollectionEquality().equals(other._comments, _comments) &&
            (identical(other.videoDuration, videoDuration) ||
                other.videoDuration == videoDuration) &&
            (identical(other.player, player) || other.player == player) &&
            (identical(other.episodeType, episodeType) ||
                other.episodeType == episodeType));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      episodeNumber,
      data,
      movieCollection,
      const DeepCollectionEquality().hash(_seasons),
      const DeepCollectionEquality().hash(_comments),
      videoDuration,
      player,
      episodeType);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_EpisodeCopyWith<_$_Episode> get copyWith =>
      __$$_EpisodeCopyWithImpl<_$_Episode>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_EpisodeToJson(
      this,
    );
  }
}

abstract class _Episode extends Episode {
  const factory _Episode(
      {required final int id,
      required final int episodeNumber,
      required final EpisodeInfo? data,
      required final MovieInfo? movieCollection,
      required final List<Season>? seasons,
      required final List<Comment>? comments,
      required final int? videoDuration,
      required final Player? player,
      final EpisodeType? episodeType}) = _$_Episode;
  const _Episode._() : super._();

  factory _Episode.fromJson(Map<String, dynamic> json) = _$_Episode.fromJson;

  @override
  int get id;
  @override
  int get episodeNumber;
  @override
  EpisodeInfo? get data;
  @override
  MovieInfo? get movieCollection;
  @override
  List<Season>? get seasons;
  @override
  List<Comment>? get comments;
  @override
  int? get videoDuration;
  @override
  Player? get player;
  @override
  EpisodeType? get episodeType;
  @override
  @JsonKey(ignore: true)
  _$$_EpisodeCopyWith<_$_Episode> get copyWith =>
      throw _privateConstructorUsedError;
}
