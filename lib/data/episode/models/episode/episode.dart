import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/data/movie/models/episode_info/episode_info.dart';
import 'package:nine_pandas/data/movie/models/movie_info/movie_info.dart';
import 'package:nine_pandas/data/movie/models/season/season.dart';
import 'package:nine_pandas/data/player/models/comment.dart';
import 'package:nine_pandas/data/player/models/player/player.dart';

part 'episode.freezed.dart';
part 'episode.g.dart';

@freezed
class Episode with _$Episode {
  const Episode._();

  const factory Episode({
    required int id,
    required int episodeNumber,
    required EpisodeInfo? data,
    required MovieInfo? movieCollection,
    required List<Season>? seasons,
    required List<Comment>? comments,
    required int? videoDuration,
    required Player? player,
    final EpisodeType? episodeType,
  }) = _Episode;

  factory Episode.fromJson(Map<String, dynamic> json) =>
      _$EpisodeFromJson(json);
}

enum EpisodeType { trailer, movie, episode }
