// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'episode.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Episode _$$_EpisodeFromJson(Map<String, dynamic> json) => _$_Episode(
      id: json['id'] as int,
      episodeNumber: json['episodeNumber'] as int,
      data: json['data'] == null
          ? null
          : EpisodeInfo.fromJson(json['data'] as Map<String, dynamic>),
      movieCollection: json['movieCollection'] == null
          ? null
          : MovieInfo.fromJson(json['movieCollection'] as Map<String, dynamic>),
      seasons: (json['seasons'] as List<dynamic>?)
          ?.map((e) => Season.fromJson(e as Map<String, dynamic>))
          .toList(),
      comments: (json['comments'] as List<dynamic>?)
          ?.map((e) => Comment.fromJson(e as Map<String, dynamic>))
          .toList(),
      videoDuration: json['videoDuration'] as int?,
      player: json['player'] == null
          ? null
          : Player.fromJson(json['player'] as Map<String, dynamic>),
      episodeType:
          $enumDecodeNullable(_$EpisodeTypeEnumMap, json['episodeType']),
    );

Map<String, dynamic> _$$_EpisodeToJson(_$_Episode instance) =>
    <String, dynamic>{
      'id': instance.id,
      'episodeNumber': instance.episodeNumber,
      'data': instance.data,
      'movieCollection': instance.movieCollection,
      'seasons': instance.seasons,
      'comments': instance.comments,
      'videoDuration': instance.videoDuration,
      'player': instance.player,
      'episodeType': _$EpisodeTypeEnumMap[instance.episodeType],
    };

const _$EpisodeTypeEnumMap = {
  EpisodeType.trailer: 'trailer',
  EpisodeType.movie: 'movie',
  EpisodeType.episode: 'episode',
};
