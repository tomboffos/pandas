class Endpoints {
  Endpoints._();

  static const String episode = '/episodes';

  static const String comment = '/comments';

  static const String votes = '/votes';
}
