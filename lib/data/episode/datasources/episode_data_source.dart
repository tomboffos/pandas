import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/data/movie/datasources/constants/endpoints.dart';
import 'package:nine_pandas/data/episode/datasources/constants/endpoint.dart'
    as episode;
import 'package:nine_pandas/data/player/models/comment.dart';

import '../models/episode/episode.dart';

abstract class EpisodeDataSource {
  Future<Episode> fetchEpisodeById(int movieId, int id, String locale);

  Future<Comment> postComment(
      {required String comment,
      required int movieCollection,
      required int episodeId});

  Future addVotePost({required int commentId, required int vote});
}

@Injectable(as: EpisodeDataSource)
class EpisodeDataSourceImpl implements EpisodeDataSource {
  final DioClient dioClient;

  EpisodeDataSourceImpl(this.dioClient);

  @override
  Future<Episode> fetchEpisodeById(int movieId, int id, String locale) async {
    try {
      final res = await dioClient.get(
          '${Endpoints.movieCollection}/$movieId${episode.Endpoints.episode}',
          queryParameters: {'locale': locale, 'episodeId': id});

      return Episode.fromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
          code: e.response!.statusCode,
          message: e.response!.statusMessage,
        );
      }

      throw e;
    }
  }

  @override
  Future<Comment> postComment(
      {required String comment,
      required int movieCollection,
      required int episodeId}) async {
    Map<String, dynamic> body = {
      'comment': comment,
      'movieCollection': {'id': movieCollection},
      'episode': {'id': episodeId}
    };
    try {
      final res = await dioClient.post(episode.Endpoints.comment, data: body);

      return Comment.fromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
          code: e.response!.statusCode,
          message: e.response!.statusMessage,
        );
      }

      throw e;
    }
  }

  @override
  Future addVotePost({required int commentId, required int vote}) async {
    final body = {'vote': vote};

    try {
      final res = await dioClient.post(
          episode.Endpoints.comment + '/$commentId' + episode.Endpoints.votes,
          data: body);

      return res;
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
          code: e.response!.statusCode,
          message: e.response!.statusMessage,
        );
      }

      throw e;
    }
  }
}
