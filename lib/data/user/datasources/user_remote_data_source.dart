import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/core/network/endpoints/user_endpoint.dart';
import 'package:nine_pandas/data/token/model/token.dart';
import 'package:nine_pandas/data/user/models/user.dart';

abstract class UserRemoteDataSource {
  Future registerUser(String userName, String name, String password);

  Future loginUser(String userName, String password);

  Future getMe();

  Future updateUser(
      {String? name,
      String? gender,
      String? phone,
      String? birthdate,
      required bool emailNotification,
      required bool smsNotification,
      required int userId});
}

@Injectable(as: UserRemoteDataSource)
class UserRemoteDataSourceImpl implements UserRemoteDataSource {
  final DioClient dioClient;

  UserRemoteDataSourceImpl(this.dioClient);

  @override
  Future registerUser(String userName, String name, String password) async {
    Map<String, dynamic> body = {
      'username': userName,
      'name': name,
      'password': password,
      'emailNotification': false,
      'smsNotification': false,
    };
    try {
      final res = await dioClient.post(UserEndpoint.register, data: body);

      return res;
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
            code: e.response!.statusCode, message: e.response!.statusMessage);
      }
      throw e;
    }
  }

  @override
  Future loginUser(String username, String password) async {
    Map<String, String> body = {
      'username': username,
      'password': password,
      'grant_type': 'password',
    };
    try {
      final res = await dioClient.post(
        UserEndpoint.login,
        data: body,
        options: Options(
          contentType: 'application/x-www-form-urlencoded',
          headers: {
            'Authorization': 'Basic ${DioClient.CLIENT_TOKEN}',
          },
        ),
      );
      return Token.fromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
          code: e.response!.statusCode,
          message: e.response!.statusMessage,
        );
      }
    }
  }

  @override
  Future getMe() async {
    try {
      final res = await dioClient.get(UserEndpoint.me);

      return User.fromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
            code: e.response?.statusCode ?? 400,
            message: e.response?.statusMessage ?? e.toString());
      }
    }
  }

  @override
  Future updateUser(
      {String? name,
      String? gender,
      String? phone,
      String? birthdate,
      required bool emailNotification,
      required bool smsNotification,
      required int userId}) async {
    Map body = {
      'emailNotification': emailNotification,
      'smsNotification': smsNotification,
    };

    if (name != null) body['name'] = name;
    if (phone != null) body['phone'] = phone;
    if (birthdate != null)
      body['birthdate'] =
          birthdate.split('.').reversed.join('-') + " 00:00:00.980";
    if (gender != null) body['gender'] = gender;

    try {
      final res =
          await dioClient.put('${UserEndpoint.update}/$userId', data: body);

      return res;
    } catch (e) {
      if (e is DioError)
        throw ServerException(
            code: e.response?.statusCode ?? 400,
            message: e.response?.statusMessage ?? e.toString());
    }
  }
}
