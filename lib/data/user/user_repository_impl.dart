import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/token/shared_preferences/token_shared_preferences.dart';
import 'package:nine_pandas/data/user/datasources/user_remote_data_source.dart';
import 'package:nine_pandas/data/user/models/user.dart';
import 'package:nine_pandas/domain/user/repository/user_repository.dart';
import 'package:nine_pandas/injectable.dart';

@Injectable(as: UserRepository)
class UserRepositoryImpl implements UserRepository {
  final UserRemoteDataSource userRemoteDataSource;
  UserRepositoryImpl(this.userRemoteDataSource);

  TokenSharedPreferences _sharedPreferences =
      getIt.get<TokenSharedPreferences>();

  Future<Either<Failure, dynamic>> registerUser({
    required String userName,
    required String name,
    required String password,
  }) async {
    try {
      final res =
          await userRemoteDataSource.registerUser(userName, name, password);

      return Right(res);
    } catch (e) {
      if (e is ServerException && e.code == 400) {
        return Left(ServerFailure(message: e.message));
      }

      return Left(UnavailableCacheFailure());
    }
  }

  @override
  Future<Either<Failure, dynamic>> loginUser({
    required String username,
    required String password,
  }) async {
    try {
      final user = await userRemoteDataSource.loginUser(username, password);

      return Right(user);
    } catch (e) {
      if (e is ServerException && e.code == 400) {
        return Left(ServerFailure(message: e.message));
      }

      return Left(UnavailableCacheFailure());
    }
  }

  @override
  Future<Either<Failure, User>> getMe() async {
    try {
      final user = await userRemoteDataSource.getMe();
      return Right(user);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure(message: e.message));
      }

      return Left(UnknownFailure());
    }
  }

  @override
  Future logout() async {
    await _sharedPreferences.removeToken();
  }

  @override
  Future<Either<Failure, User>> updateUser(
      {String? name,
      String? gender,
      String? phone,
      String? birthdate,
      required bool emailNotification,
      required bool smsNotification,
      required int userId}) async {
    try {
      final res = await userRemoteDataSource.updateUser(
          name: name,
          gender: gender,
          phone: phone,
          birthdate: birthdate,
          emailNotification: emailNotification,
          smsNotification: smsNotification,
          userId: userId);

      return Right(User.fromJson(res));
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure(message: e.message));
      }

      return Left(UnknownFailure());
    }
  }
}
