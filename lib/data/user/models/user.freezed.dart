// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

User _$UserFromJson(Map<String, dynamic> json) {
  return _User.fromJson(json);
}

/// @nodoc
mixin _$User {
  String get name => throw _privateConstructorUsedError;
  String get username => throw _privateConstructorUsedError;
  String get phone => throw _privateConstructorUsedError;
  String get gender => throw _privateConstructorUsedError;
  bool get emailNotification => throw _privateConstructorUsedError;
  bool get smsNotification => throw _privateConstructorUsedError;
  bool get islocked => throw _privateConstructorUsedError;
  bool get isVerified => throw _privateConstructorUsedError;
  bool get isPremium => throw _privateConstructorUsedError;
  bool get isadmin => throw _privateConstructorUsedError;
  int get id => throw _privateConstructorUsedError;
  DateTime? get birthdate => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UserCopyWith<User> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res, User>;
  @useResult
  $Res call(
      {String name,
      String username,
      String phone,
      String gender,
      bool emailNotification,
      bool smsNotification,
      bool islocked,
      bool isVerified,
      bool isPremium,
      bool isadmin,
      int id,
      DateTime? birthdate});
}

/// @nodoc
class _$UserCopyWithImpl<$Res, $Val extends User>
    implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? username = null,
    Object? phone = null,
    Object? gender = null,
    Object? emailNotification = null,
    Object? smsNotification = null,
    Object? islocked = null,
    Object? isVerified = null,
    Object? isPremium = null,
    Object? isadmin = null,
    Object? id = null,
    Object? birthdate = freezed,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      emailNotification: null == emailNotification
          ? _value.emailNotification
          : emailNotification // ignore: cast_nullable_to_non_nullable
              as bool,
      smsNotification: null == smsNotification
          ? _value.smsNotification
          : smsNotification // ignore: cast_nullable_to_non_nullable
              as bool,
      islocked: null == islocked
          ? _value.islocked
          : islocked // ignore: cast_nullable_to_non_nullable
              as bool,
      isVerified: null == isVerified
          ? _value.isVerified
          : isVerified // ignore: cast_nullable_to_non_nullable
              as bool,
      isPremium: null == isPremium
          ? _value.isPremium
          : isPremium // ignore: cast_nullable_to_non_nullable
              as bool,
      isadmin: null == isadmin
          ? _value.isadmin
          : isadmin // ignore: cast_nullable_to_non_nullable
              as bool,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      birthdate: freezed == birthdate
          ? _value.birthdate
          : birthdate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserCopyWith<$Res> implements $UserCopyWith<$Res> {
  factory _$$_UserCopyWith(_$_User value, $Res Function(_$_User) then) =
      __$$_UserCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String name,
      String username,
      String phone,
      String gender,
      bool emailNotification,
      bool smsNotification,
      bool islocked,
      bool isVerified,
      bool isPremium,
      bool isadmin,
      int id,
      DateTime? birthdate});
}

/// @nodoc
class __$$_UserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res, _$_User>
    implements _$$_UserCopyWith<$Res> {
  __$$_UserCopyWithImpl(_$_User _value, $Res Function(_$_User) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? username = null,
    Object? phone = null,
    Object? gender = null,
    Object? emailNotification = null,
    Object? smsNotification = null,
    Object? islocked = null,
    Object? isVerified = null,
    Object? isPremium = null,
    Object? isadmin = null,
    Object? id = null,
    Object? birthdate = freezed,
  }) {
    return _then(_$_User(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      username: null == username
          ? _value.username
          : username // ignore: cast_nullable_to_non_nullable
              as String,
      phone: null == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String,
      gender: null == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String,
      emailNotification: null == emailNotification
          ? _value.emailNotification
          : emailNotification // ignore: cast_nullable_to_non_nullable
              as bool,
      smsNotification: null == smsNotification
          ? _value.smsNotification
          : smsNotification // ignore: cast_nullable_to_non_nullable
              as bool,
      islocked: null == islocked
          ? _value.islocked
          : islocked // ignore: cast_nullable_to_non_nullable
              as bool,
      isVerified: null == isVerified
          ? _value.isVerified
          : isVerified // ignore: cast_nullable_to_non_nullable
              as bool,
      isPremium: null == isPremium
          ? _value.isPremium
          : isPremium // ignore: cast_nullable_to_non_nullable
              as bool,
      isadmin: null == isadmin
          ? _value.isadmin
          : isadmin // ignore: cast_nullable_to_non_nullable
              as bool,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      birthdate: freezed == birthdate
          ? _value.birthdate
          : birthdate // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_User implements _User {
  const _$_User(
      {required this.name,
      required this.username,
      required this.phone,
      required this.gender,
      required this.emailNotification,
      required this.smsNotification,
      required this.islocked,
      required this.isVerified,
      required this.isPremium,
      required this.isadmin,
      required this.id,
      required this.birthdate});

  factory _$_User.fromJson(Map<String, dynamic> json) => _$$_UserFromJson(json);

  @override
  final String name;
  @override
  final String username;
  @override
  final String phone;
  @override
  final String gender;
  @override
  final bool emailNotification;
  @override
  final bool smsNotification;
  @override
  final bool islocked;
  @override
  final bool isVerified;
  @override
  final bool isPremium;
  @override
  final bool isadmin;
  @override
  final int id;
  @override
  final DateTime? birthdate;

  @override
  String toString() {
    return 'User(name: $name, username: $username, phone: $phone, gender: $gender, emailNotification: $emailNotification, smsNotification: $smsNotification, islocked: $islocked, isVerified: $isVerified, isPremium: $isPremium, isadmin: $isadmin, id: $id, birthdate: $birthdate)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_User &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.username, username) ||
                other.username == username) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.emailNotification, emailNotification) ||
                other.emailNotification == emailNotification) &&
            (identical(other.smsNotification, smsNotification) ||
                other.smsNotification == smsNotification) &&
            (identical(other.islocked, islocked) ||
                other.islocked == islocked) &&
            (identical(other.isVerified, isVerified) ||
                other.isVerified == isVerified) &&
            (identical(other.isPremium, isPremium) ||
                other.isPremium == isPremium) &&
            (identical(other.isadmin, isadmin) || other.isadmin == isadmin) &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.birthdate, birthdate) ||
                other.birthdate == birthdate));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      name,
      username,
      phone,
      gender,
      emailNotification,
      smsNotification,
      islocked,
      isVerified,
      isPremium,
      isadmin,
      id,
      birthdate);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserCopyWith<_$_User> get copyWith =>
      __$$_UserCopyWithImpl<_$_User>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UserToJson(
      this,
    );
  }
}

abstract class _User implements User {
  const factory _User(
      {required final String name,
      required final String username,
      required final String phone,
      required final String gender,
      required final bool emailNotification,
      required final bool smsNotification,
      required final bool islocked,
      required final bool isVerified,
      required final bool isPremium,
      required final bool isadmin,
      required final int id,
      required final DateTime? birthdate}) = _$_User;

  factory _User.fromJson(Map<String, dynamic> json) = _$_User.fromJson;

  @override
  String get name;
  @override
  String get username;
  @override
  String get phone;
  @override
  String get gender;
  @override
  bool get emailNotification;
  @override
  bool get smsNotification;
  @override
  bool get islocked;
  @override
  bool get isVerified;
  @override
  bool get isPremium;
  @override
  bool get isadmin;
  @override
  int get id;
  @override
  DateTime? get birthdate;
  @override
  @JsonKey(ignore: true)
  _$$_UserCopyWith<_$_User> get copyWith => throw _privateConstructorUsedError;
}
