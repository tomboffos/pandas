import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';
part 'user.g.dart';

@freezed
class User with _$User {
  const factory User(
      {required String name,
      required String username,
      required String phone,
      required String gender,
      required bool emailNotification,
      required bool smsNotification,
      required bool islocked,
      required bool isVerified,
      required bool isPremium,
      required bool isadmin,
      required int id,
      required DateTime? birthdate}) = _User;

  factory User.fromJson(Map<String, Object?> json) => _$UserFromJson(json);
}
