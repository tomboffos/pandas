// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_User _$$_UserFromJson(Map<String, dynamic> json) => _$_User(
      name: json['name'] as String,
      username: json['username'] as String,
      phone: json['phone'] as String,
      gender: json['gender'] as String,
      emailNotification: json['emailNotification'] as bool,
      smsNotification: json['smsNotification'] as bool,
      islocked: json['islocked'] as bool,
      isVerified: json['isVerified'] as bool,
      isPremium: json['isPremium'] as bool,
      isadmin: json['isadmin'] as bool,
      id: json['id'] as int,
      birthdate: json['birthdate'] == null
          ? null
          : DateTime.parse(json['birthdate'] as String),
    );

Map<String, dynamic> _$$_UserToJson(_$_User instance) => <String, dynamic>{
      'name': instance.name,
      'username': instance.username,
      'phone': instance.phone,
      'gender': instance.gender,
      'emailNotification': instance.emailNotification,
      'smsNotification': instance.smsNotification,
      'islocked': instance.islocked,
      'isVerified': instance.isVerified,
      'isPremium': instance.isPremium,
      'isadmin': instance.isadmin,
      'id': instance.id,
      'birthdate': instance.birthdate?.toIso8601String(),
    };
