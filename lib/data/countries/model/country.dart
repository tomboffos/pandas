class Country {
  final String countryCode, countryName, countryFlag;
  Country(
      {required this.countryCode,
      required this.countryName,
      required this.countryFlag});
}
