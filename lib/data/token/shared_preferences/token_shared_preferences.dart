import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:nine_pandas/data/token/model/token.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'preferences/preferences.dart';

abstract class TokenSharedPreferences {
  Future<bool> get isLoggedIn;
  Future saveTokens(Token token);
  Token? getToken();
  Future removeToken();
}

@LazySingleton(as: TokenSharedPreferences)
class TokenSharedPreferencesImpl implements TokenSharedPreferences {
  final prefs = getIt<SharedPreferences>();
  Future<bool> get isLoggedIn async {
    return (prefs.getString(Preferences.token) != null &&
        prefs.getString(Preferences.token)!.isNotEmpty);
  }

  Future saveTokens(Token token) async {
    return await prefs.setString(Preferences.token, json.encode(token));
  }

  Token? getToken() {
    if (prefs.getString(Preferences.token) == null) {
      return null;
    } else {
      return Token.fromJson(json.decode(prefs.getString(Preferences.token)!));
    }
  }

  Future removeToken() {
    return prefs.remove(Preferences.token);
  }
}
