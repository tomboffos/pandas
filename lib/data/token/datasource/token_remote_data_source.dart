import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/core/network/endpoints/user_endpoint.dart';

import '../model/token.dart';

abstract class TokenRemoteDataSource {
  Future<Token> refreshToken(Token lastToken);
  Future<Token> getToken();
}

@Injectable(as: TokenRemoteDataSource)
class TokenRemoteDataSourceImpl implements TokenRemoteDataSource {
  DioClient dioClient;

  TokenRemoteDataSourceImpl(this.dioClient);

  @override
  Future<Token> refreshToken(Token lastToken) async {
    try {
      final res = await dioClient.post(UserEndpoint.login,
          options: Options(headers: {
            'Authorization': 'Basic ${DioClient.CLIENT_TOKEN}',
          }),
          queryParameters: {
            'grant_type': 'refresh_token',
            'refresh_token': lastToken.refreshToken,
          });
      return Token.fromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
            code: e.response?.statusCode, message: e.response?.statusMessage);
      }
      rethrow;
    }
  }

  @override
  Future<Token> getToken() async {
    try {
      return Token.fromJson(await dioClient.get(UserEndpoint.login));
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
            code: e.response?.statusCode, message: e.response?.statusMessage);
      }
      rethrow;
    }
  }
}
