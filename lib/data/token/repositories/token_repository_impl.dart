import 'package:injectable/injectable.dart';
import 'package:nine_pandas/data/token/datasource/token_remote_data_source.dart';
import 'package:nine_pandas/data/token/model/token.dart';
import 'package:nine_pandas/data/token/shared_preferences/token_shared_preferences.dart';
import 'package:nine_pandas/domain/token/token_repository.dart';
import 'package:nine_pandas/injectable.dart';

@Injectable(as: TokenRepository)
class TokenRepositoryImpl implements TokenRepository {
  final TokenRemoteDataSource tokenRemoteDataSource;
  final TokenSharedPreferences _sharedPreferences =
      getIt.get<TokenSharedPreferences>();

  TokenRepositoryImpl({
    required this.tokenRemoteDataSource,
  });
  @override
  Future<Token?> refreshToken() async {
    final Token? currentToken = _sharedPreferences.getToken();
    if (currentToken != null) {
      final refreshedToken =
          await tokenRemoteDataSource.refreshToken(currentToken);
      await _sharedPreferences.saveTokens(refreshedToken);

      return refreshedToken;
    } else {
      return null;
    }
  }
}
