import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/change_password/repository/change_password_repository.dart';

import 'datasources/change_password_remote_data_source.dart';

@Injectable(as: ChangePasswordRepository)
class ChangePasswordRepositoryImpl implements ChangePasswordRepository {
  final ChangePasswordRemoteDataSource _changePasswordRemoteDataSource;
  ChangePasswordRepositoryImpl(this._changePasswordRemoteDataSource);
  @override
  Future<Either<Failure, dynamic>> changePassword({
    required String oldPassword,
    required String newPassword,
    required String repeatPassword,
  }) async {
    try {
      final res = await _changePasswordRemoteDataSource.changePassword(
          oldPassword, newPassword, repeatPassword);

      return Right(res);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure());
      }
      throw e;
    }
  }
}
