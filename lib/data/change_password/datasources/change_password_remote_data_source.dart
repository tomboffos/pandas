import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/core/network/endpoints/user_endpoint.dart';

abstract class ChangePasswordRemoteDataSource {
  Future changePassword(
      String oldPassword, String newPassword, String repeatPassword);
}

@Injectable(as: ChangePasswordRemoteDataSource)
class ChangePasswordRemoteDataSourceImpl
    implements ChangePasswordRemoteDataSource {
  final DioClient dioClient;

  ChangePasswordRemoteDataSourceImpl(this.dioClient);
  @override
  Future changePassword(
      String oldPassword, String newPassword, String repeatPassword) async {
    Map<String, dynamic> body = {
      'oldPassword': oldPassword,
      'newPassword': newPassword,
      'confirmPassword': repeatPassword
    };

    try {
      final res = await dioClient.post(UserEndpoint.changePassword, data: body);

      return res;
    } catch (e) {
      if (e is DioError) {
        if (e.response!.statusCode == 400) {
          return OldPasswordIncorrect();
        }
        throw ServerException(
            code: e.response?.statusCode ?? 400,
            message: e.response?.statusMessage ?? e.toString());
      }
    }
  }
}
