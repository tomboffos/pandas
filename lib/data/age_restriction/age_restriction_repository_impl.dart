import 'package:injectable/injectable.dart';
import 'package:nine_pandas/domain/age_restriction/domain/age_restriction_repository.dart';
import 'package:nine_pandas/domain/age_restriction/shared_preferences/age_restriction_shared_preferences.dart';

@Injectable(as: AgeRestrictionRepository)
class AgeRestrictionRepositoryImpl implements AgeRestrictionRepository {
  final AgeRestrictionSharedPreferences sharedPreferences;

  AgeRestrictionRepositoryImpl({
    required this.sharedPreferences,
  });

  @override
  Future getConfirmation() async {
    return await sharedPreferences.getConfirmation();
  }

  @override
  Future saveConfirmation(bool ageConfirmation) {
    return sharedPreferences.saveConfirmation(ageConfirmation);
  }
}
