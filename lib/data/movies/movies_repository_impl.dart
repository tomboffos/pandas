import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';
import 'package:nine_pandas/domain/movies/repository/main_page_repository.dart';

import 'datasourses/movies_remote_data_source.dart';

@LazySingleton(as: MoviesRepository)
class MoviesRepositoryImpl implements MoviesRepository {
  final MoviesRemoteDataSource mainPageRemoteDataSource;
  MoviesRepositoryImpl(this.mainPageRemoteDataSource);

  @override
  Future<Either<Failure, List<Movie>>> getMovies(String locale) async {
    try {
      final listSeries = await mainPageRemoteDataSource.getMovies(locale);
      return Right(listSeries);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure());
      }
      throw e;
    }
  }

  @override
  Future<Either<Failure, Movie>> getMovieById(int id, String locale) async {
    try {
      final movie = await mainPageRemoteDataSource.getMovieById(id, locale);
      return Right(movie);
    } catch (e) {
      if (e is ServerException) {
        return Left(ServerFailure());
      }
      throw e;
    }
  }
}
