import 'package:nine_pandas/core/network/base_endpoints.dart';

class Endpoints {
  Endpoints._();

  static const String movieCollection =
      BaseEndpoints.baseUrl + '/moviecollection';
}
