import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';

import 'constants/endpoints.dart';

abstract class MoviesRemoteDataSource {
  Future<List<Movie>> getMovies(String locale);
  Future<Movie> getMovieById(int id, String locale);
}

@LazySingleton(as: MoviesRemoteDataSource)
class MoviesRemoteDataSourceImpl implements MoviesRemoteDataSource {
  DioClient dioClient;

  MoviesRemoteDataSourceImpl(this.dioClient);
  @override
  Future<List<Movie>> getMovies(String locale) async {
    try {
      final result =
          await dioClient.get(Endpoints.movieCollection, queryParameters: {
        'locale': locale,
      });
      List<Movie> movies = [];
      if (result != null) {
        result.forEach((movieJson) {
          movies.add(Movie.fromJson(movieJson));
        });
      }
      return movies;
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
          code: e.response!.statusCode,
          message: e.response!.statusMessage,
        );
      }
      throw e;
    }
  }

  @override
  Future<Movie> getMovieById(int id, String locale) async {
    try {
      final res = await dioClient.get(
        '${Endpoints.movieCollection}/$id',
        queryParameters: {
          'locale': locale,
        },
      );
      return Movie.fromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
          code: e.response!.statusCode,
          message: e.response!.statusMessage,
        );
      }
      throw e;
    }
  }
}
