import 'package:freezed_annotation/freezed_annotation.dart';

part 'comment.freezed.dart';
part 'comment.g.dart';

@freezed
class Comment with _$Comment {
  const factory Comment(
      {required int id,
      required String comment,
      required int? voteUp,
      required int? voteDown,
      required int? userVote,
      required DateTime createdAt,
      required dynamic user}) = _Comment;

  factory Comment.fromJson(Map<String, Object?> json) =>
      _$CommentFromJson(json);
}
