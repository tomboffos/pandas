// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'player.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Player _$$_PlayerFromJson(Map<String, dynamic> json) => _$_Player(
      translator: json['translator'] as String?,
      token: json['token'] as String?,
      iframeUrl: json['iframe_url'] as String?,
      translatorId: json['translator_id'] as int?,
      quality: json['quality'] as String?,
    );

Map<String, dynamic> _$$_PlayerToJson(_$_Player instance) => <String, dynamic>{
      'translator': instance.translator,
      'token': instance.token,
      'iframe_url': instance.iframeUrl,
      'translator_id': instance.translatorId,
      'quality': instance.quality,
    };
