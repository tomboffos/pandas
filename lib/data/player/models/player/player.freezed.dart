// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'player.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Player _$PlayerFromJson(Map<String, dynamic> json) {
  return _Player.fromJson(json);
}

/// @nodoc
mixin _$Player {
  String? get translator => throw _privateConstructorUsedError;
  String? get token => throw _privateConstructorUsedError;
  @JsonKey(name: 'iframe_url')
  String? get iframeUrl => throw _privateConstructorUsedError;
  @JsonKey(name: 'translator_id')
  int? get translatorId => throw _privateConstructorUsedError;
  String? get quality => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PlayerCopyWith<Player> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PlayerCopyWith<$Res> {
  factory $PlayerCopyWith(Player value, $Res Function(Player) then) =
      _$PlayerCopyWithImpl<$Res, Player>;
  @useResult
  $Res call(
      {String? translator,
      String? token,
      @JsonKey(name: 'iframe_url') String? iframeUrl,
      @JsonKey(name: 'translator_id') int? translatorId,
      String? quality});
}

/// @nodoc
class _$PlayerCopyWithImpl<$Res, $Val extends Player>
    implements $PlayerCopyWith<$Res> {
  _$PlayerCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? translator = freezed,
    Object? token = freezed,
    Object? iframeUrl = freezed,
    Object? translatorId = freezed,
    Object? quality = freezed,
  }) {
    return _then(_value.copyWith(
      translator: freezed == translator
          ? _value.translator
          : translator // ignore: cast_nullable_to_non_nullable
              as String?,
      token: freezed == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      iframeUrl: freezed == iframeUrl
          ? _value.iframeUrl
          : iframeUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      translatorId: freezed == translatorId
          ? _value.translatorId
          : translatorId // ignore: cast_nullable_to_non_nullable
              as int?,
      quality: freezed == quality
          ? _value.quality
          : quality // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PlayerCopyWith<$Res> implements $PlayerCopyWith<$Res> {
  factory _$$_PlayerCopyWith(_$_Player value, $Res Function(_$_Player) then) =
      __$$_PlayerCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String? translator,
      String? token,
      @JsonKey(name: 'iframe_url') String? iframeUrl,
      @JsonKey(name: 'translator_id') int? translatorId,
      String? quality});
}

/// @nodoc
class __$$_PlayerCopyWithImpl<$Res>
    extends _$PlayerCopyWithImpl<$Res, _$_Player>
    implements _$$_PlayerCopyWith<$Res> {
  __$$_PlayerCopyWithImpl(_$_Player _value, $Res Function(_$_Player) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? translator = freezed,
    Object? token = freezed,
    Object? iframeUrl = freezed,
    Object? translatorId = freezed,
    Object? quality = freezed,
  }) {
    return _then(_$_Player(
      translator: freezed == translator
          ? _value.translator
          : translator // ignore: cast_nullable_to_non_nullable
              as String?,
      token: freezed == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String?,
      iframeUrl: freezed == iframeUrl
          ? _value.iframeUrl
          : iframeUrl // ignore: cast_nullable_to_non_nullable
              as String?,
      translatorId: freezed == translatorId
          ? _value.translatorId
          : translatorId // ignore: cast_nullable_to_non_nullable
              as int?,
      quality: freezed == quality
          ? _value.quality
          : quality // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Player extends _Player {
  const _$_Player(
      {required this.translator,
      required this.token,
      @JsonKey(name: 'iframe_url') required this.iframeUrl,
      @JsonKey(name: 'translator_id') required this.translatorId,
      required this.quality})
      : super._();

  factory _$_Player.fromJson(Map<String, dynamic> json) =>
      _$$_PlayerFromJson(json);

  @override
  final String? translator;
  @override
  final String? token;
  @override
  @JsonKey(name: 'iframe_url')
  final String? iframeUrl;
  @override
  @JsonKey(name: 'translator_id')
  final int? translatorId;
  @override
  final String? quality;

  @override
  String toString() {
    return 'Player(translator: $translator, token: $token, iframeUrl: $iframeUrl, translatorId: $translatorId, quality: $quality)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Player &&
            (identical(other.translator, translator) ||
                other.translator == translator) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.iframeUrl, iframeUrl) ||
                other.iframeUrl == iframeUrl) &&
            (identical(other.translatorId, translatorId) ||
                other.translatorId == translatorId) &&
            (identical(other.quality, quality) || other.quality == quality));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, translator, token, iframeUrl, translatorId, quality);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PlayerCopyWith<_$_Player> get copyWith =>
      __$$_PlayerCopyWithImpl<_$_Player>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PlayerToJson(
      this,
    );
  }
}

abstract class _Player extends Player {
  const factory _Player(
      {required final String? translator,
      required final String? token,
      @JsonKey(name: 'iframe_url') required final String? iframeUrl,
      @JsonKey(name: 'translator_id') required final int? translatorId,
      required final String? quality}) = _$_Player;
  const _Player._() : super._();

  factory _Player.fromJson(Map<String, dynamic> json) = _$_Player.fromJson;

  @override
  String? get translator;
  @override
  String? get token;
  @override
  @JsonKey(name: 'iframe_url')
  String? get iframeUrl;
  @override
  @JsonKey(name: 'translator_id')
  int? get translatorId;
  @override
  String? get quality;
  @override
  @JsonKey(ignore: true)
  _$$_PlayerCopyWith<_$_Player> get copyWith =>
      throw _privateConstructorUsedError;
}
