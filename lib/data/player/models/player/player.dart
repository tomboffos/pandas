import 'package:freezed_annotation/freezed_annotation.dart';

part 'player.freezed.dart';
part 'player.g.dart';

@freezed
class Player with _$Player {
  const Player._();
  const factory Player({
    required String? translator,
    required String? token,
    @JsonKey(name: 'iframe_url') required String? iframeUrl,
    @JsonKey(name: 'translator_id') required int? translatorId,
    required String? quality,
  }) = _Player;

  factory Player.fromJson(Map<String, dynamic> json) => _$PlayerFromJson(json);
}
