// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comment.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Comment _$$_CommentFromJson(Map<String, dynamic> json) => _$_Comment(
      id: json['id'] as int,
      comment: json['comment'] as String,
      voteUp: json['voteUp'] as int?,
      voteDown: json['voteDown'] as int?,
      userVote: json['userVote'] as int?,
      createdAt: DateTime.parse(json['createdAt'] as String),
      user: json['user'],
    );

Map<String, dynamic> _$$_CommentToJson(_$_Comment instance) =>
    <String, dynamic>{
      'id': instance.id,
      'comment': instance.comment,
      'voteUp': instance.voteUp,
      'voteDown': instance.voteDown,
      'userVote': instance.userVote,
      'createdAt': instance.createdAt.toIso8601String(),
      'user': instance.user,
    };
