// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'episode_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

EpisodeInfo _$EpisodeInfoFromJson(Map<String, dynamic> json) {
  return _EpisodeInfo.fromJson(json);
}

/// @nodoc
mixin _$EpisodeInfo {
  String get language => throw _privateConstructorUsedError;
  String get episodeTitle => throw _privateConstructorUsedError;
  String? get logo => throw _privateConstructorUsedError;
  String get episodeDescription => throw _privateConstructorUsedError;
  String get episodePoster => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $EpisodeInfoCopyWith<EpisodeInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EpisodeInfoCopyWith<$Res> {
  factory $EpisodeInfoCopyWith(
          EpisodeInfo value, $Res Function(EpisodeInfo) then) =
      _$EpisodeInfoCopyWithImpl<$Res, EpisodeInfo>;
  @useResult
  $Res call(
      {String language,
      String episodeTitle,
      String? logo,
      String episodeDescription,
      String episodePoster});
}

/// @nodoc
class _$EpisodeInfoCopyWithImpl<$Res, $Val extends EpisodeInfo>
    implements $EpisodeInfoCopyWith<$Res> {
  _$EpisodeInfoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? language = null,
    Object? episodeTitle = null,
    Object? logo = freezed,
    Object? episodeDescription = null,
    Object? episodePoster = null,
  }) {
    return _then(_value.copyWith(
      language: null == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      episodeTitle: null == episodeTitle
          ? _value.episodeTitle
          : episodeTitle // ignore: cast_nullable_to_non_nullable
              as String,
      logo: freezed == logo
          ? _value.logo
          : logo // ignore: cast_nullable_to_non_nullable
              as String?,
      episodeDescription: null == episodeDescription
          ? _value.episodeDescription
          : episodeDescription // ignore: cast_nullable_to_non_nullable
              as String,
      episodePoster: null == episodePoster
          ? _value.episodePoster
          : episodePoster // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_EpisodeInfoCopyWith<$Res>
    implements $EpisodeInfoCopyWith<$Res> {
  factory _$$_EpisodeInfoCopyWith(
          _$_EpisodeInfo value, $Res Function(_$_EpisodeInfo) then) =
      __$$_EpisodeInfoCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String language,
      String episodeTitle,
      String? logo,
      String episodeDescription,
      String episodePoster});
}

/// @nodoc
class __$$_EpisodeInfoCopyWithImpl<$Res>
    extends _$EpisodeInfoCopyWithImpl<$Res, _$_EpisodeInfo>
    implements _$$_EpisodeInfoCopyWith<$Res> {
  __$$_EpisodeInfoCopyWithImpl(
      _$_EpisodeInfo _value, $Res Function(_$_EpisodeInfo) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? language = null,
    Object? episodeTitle = null,
    Object? logo = freezed,
    Object? episodeDescription = null,
    Object? episodePoster = null,
  }) {
    return _then(_$_EpisodeInfo(
      language: null == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      episodeTitle: null == episodeTitle
          ? _value.episodeTitle
          : episodeTitle // ignore: cast_nullable_to_non_nullable
              as String,
      logo: freezed == logo
          ? _value.logo
          : logo // ignore: cast_nullable_to_non_nullable
              as String?,
      episodeDescription: null == episodeDescription
          ? _value.episodeDescription
          : episodeDescription // ignore: cast_nullable_to_non_nullable
              as String,
      episodePoster: null == episodePoster
          ? _value.episodePoster
          : episodePoster // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_EpisodeInfo extends _EpisodeInfo {
  const _$_EpisodeInfo(
      {required this.language,
      required this.episodeTitle,
      required this.logo,
      required this.episodeDescription,
      required this.episodePoster})
      : super._();

  factory _$_EpisodeInfo.fromJson(Map<String, dynamic> json) =>
      _$$_EpisodeInfoFromJson(json);

  @override
  final String language;
  @override
  final String episodeTitle;
  @override
  final String? logo;
  @override
  final String episodeDescription;
  @override
  final String episodePoster;

  @override
  String toString() {
    return 'EpisodeInfo(language: $language, episodeTitle: $episodeTitle, logo: $logo, episodeDescription: $episodeDescription, episodePoster: $episodePoster)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_EpisodeInfo &&
            (identical(other.language, language) ||
                other.language == language) &&
            (identical(other.episodeTitle, episodeTitle) ||
                other.episodeTitle == episodeTitle) &&
            (identical(other.logo, logo) || other.logo == logo) &&
            (identical(other.episodeDescription, episodeDescription) ||
                other.episodeDescription == episodeDescription) &&
            (identical(other.episodePoster, episodePoster) ||
                other.episodePoster == episodePoster));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, language, episodeTitle, logo,
      episodeDescription, episodePoster);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_EpisodeInfoCopyWith<_$_EpisodeInfo> get copyWith =>
      __$$_EpisodeInfoCopyWithImpl<_$_EpisodeInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_EpisodeInfoToJson(
      this,
    );
  }
}

abstract class _EpisodeInfo extends EpisodeInfo {
  const factory _EpisodeInfo(
      {required final String language,
      required final String episodeTitle,
      required final String? logo,
      required final String episodeDescription,
      required final String episodePoster}) = _$_EpisodeInfo;
  const _EpisodeInfo._() : super._();

  factory _EpisodeInfo.fromJson(Map<String, dynamic> json) =
      _$_EpisodeInfo.fromJson;

  @override
  String get language;
  @override
  String get episodeTitle;
  @override
  String? get logo;
  @override
  String get episodeDescription;
  @override
  String get episodePoster;
  @override
  @JsonKey(ignore: true)
  _$$_EpisodeInfoCopyWith<_$_EpisodeInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
