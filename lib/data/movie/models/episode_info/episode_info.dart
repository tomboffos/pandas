import 'package:freezed_annotation/freezed_annotation.dart';

part 'episode_info.freezed.dart';
part 'episode_info.g.dart';

@freezed
class EpisodeInfo with _$EpisodeInfo {
  const EpisodeInfo._();

  const factory EpisodeInfo({
    required String language,
    required String episodeTitle,
    required String? logo,
    required String episodeDescription,
    required String episodePoster,
  }) = _EpisodeInfo;

  factory EpisodeInfo.fromJson(Map<String, dynamic> json) =>
      _$EpisodeInfoFromJson(json);
}
