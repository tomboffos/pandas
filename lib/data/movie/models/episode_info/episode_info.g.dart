// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'episode_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_EpisodeInfo _$$_EpisodeInfoFromJson(Map<String, dynamic> json) =>
    _$_EpisodeInfo(
      language: json['language'] as String,
      episodeTitle: json['episodeTitle'] as String,
      logo: json['logo'] as String?,
      episodeDescription: json['episodeDescription'] as String,
      episodePoster: json['episodePoster'] as String,
    );

Map<String, dynamic> _$$_EpisodeInfoToJson(_$_EpisodeInfo instance) =>
    <String, dynamic>{
      'language': instance.language,
      'episodeTitle': instance.episodeTitle,
      'logo': instance.logo,
      'episodeDescription': instance.episodeDescription,
      'episodePoster': instance.episodePoster,
    };
