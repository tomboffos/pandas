// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'season.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Season _$$_SeasonFromJson(Map<String, dynamic> json) => _$_Season(
      id: json['id'] as int,
      seasonNumber: json['seasonNumber'] as int,
      episodes: (json['episodes'] as List<dynamic>)
          .map((e) => Episode.fromJson(e as Map<String, dynamic>))
          .toList(),
      price: (json['price'] as num?)?.toDouble(),
      isPurchased: json['isPurchased'] as bool?,
    );

Map<String, dynamic> _$$_SeasonToJson(_$_Season instance) => <String, dynamic>{
      'id': instance.id,
      'seasonNumber': instance.seasonNumber,
      'episodes': instance.episodes.map((e) => e.toJson()).toList(),
      'price': instance.price,
      'isPurchased': instance.isPurchased,
    };
