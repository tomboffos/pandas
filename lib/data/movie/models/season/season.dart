import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';

part 'season.freezed.dart';
part 'season.g.dart';

@freezed
class Season with _$Season {
  const Season._();
  @JsonSerializable(explicitToJson: true)
  const factory Season({
    required int id,
    required int seasonNumber,
    required List<Episode> episodes,
    required double? price,
    required bool? isPurchased,
  }) = _Season;

  factory Season.fromJson(Map<String, dynamic> json) => _$SeasonFromJson(json);
}
