// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'images.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Images _$$_ImagesFromJson(Map<String, dynamic> json) => _$_Images(
      desktop: json['desktop'] as String,
      tablet: json['tablet'] as String,
      mobile: json['mobile'] as String,
    );

Map<String, dynamic> _$$_ImagesToJson(_$_Images instance) => <String, dynamic>{
      'desktop': instance.desktop,
      'tablet': instance.tablet,
      'mobile': instance.mobile,
    };
