import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/data/movie/models/images/images.dart';

part 'banner.freezed.dart';
part 'banner.g.dart';

@freezed
class Banner with _$Banner {
  const Banner._();

  const factory Banner({
    required int id,
    required String image,
    required Images images,
  }) = _Banner;

  factory Banner.fromJson(Map<String, dynamic> json) => _$BannerFromJson(json);
}
