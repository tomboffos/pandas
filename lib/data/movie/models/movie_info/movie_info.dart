import 'package:freezed_annotation/freezed_annotation.dart';

part 'movie_info.freezed.dart';
part 'movie_info.g.dart';

@freezed
class MovieInfo with _$MovieInfo {
  const MovieInfo._();

  const factory MovieInfo({
    @Default('ru') String language,
    required String movieTitle,
    required String movieDescription,
    required String? movieReleaseTag,
    required String movieSubTitle,
    required String logo,
    required String? trailer,
  }) = _MovieInfo;

  factory MovieInfo.fromJson(Map<String, dynamic> json) =>
      _$MovieInfoFromJson(json);
}
