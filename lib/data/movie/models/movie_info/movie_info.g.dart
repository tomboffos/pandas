// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MovieInfo _$$_MovieInfoFromJson(Map<String, dynamic> json) => _$_MovieInfo(
      language: json['language'] as String? ?? 'ru',
      movieTitle: json['movieTitle'] as String,
      movieDescription: json['movieDescription'] as String,
      movieReleaseTag: json['movieReleaseTag'] as String?,
      movieSubTitle: json['movieSubTitle'] as String,
      logo: json['logo'] as String,
      trailer: json['trailer'] as String?,
    );

Map<String, dynamic> _$$_MovieInfoToJson(_$_MovieInfo instance) =>
    <String, dynamic>{
      'language': instance.language,
      'movieTitle': instance.movieTitle,
      'movieDescription': instance.movieDescription,
      'movieReleaseTag': instance.movieReleaseTag,
      'movieSubTitle': instance.movieSubTitle,
      'logo': instance.logo,
      'trailer': instance.trailer,
    };
