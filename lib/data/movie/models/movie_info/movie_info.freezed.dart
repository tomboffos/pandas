// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'movie_info.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

MovieInfo _$MovieInfoFromJson(Map<String, dynamic> json) {
  return _MovieInfo.fromJson(json);
}

/// @nodoc
mixin _$MovieInfo {
  String get language => throw _privateConstructorUsedError;
  String get movieTitle => throw _privateConstructorUsedError;
  String get movieDescription => throw _privateConstructorUsedError;
  String? get movieReleaseTag => throw _privateConstructorUsedError;
  String get movieSubTitle => throw _privateConstructorUsedError;
  String get logo => throw _privateConstructorUsedError;
  String? get trailer => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MovieInfoCopyWith<MovieInfo> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieInfoCopyWith<$Res> {
  factory $MovieInfoCopyWith(MovieInfo value, $Res Function(MovieInfo) then) =
      _$MovieInfoCopyWithImpl<$Res, MovieInfo>;
  @useResult
  $Res call(
      {String language,
      String movieTitle,
      String movieDescription,
      String? movieReleaseTag,
      String movieSubTitle,
      String logo,
      String? trailer});
}

/// @nodoc
class _$MovieInfoCopyWithImpl<$Res, $Val extends MovieInfo>
    implements $MovieInfoCopyWith<$Res> {
  _$MovieInfoCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? language = null,
    Object? movieTitle = null,
    Object? movieDescription = null,
    Object? movieReleaseTag = freezed,
    Object? movieSubTitle = null,
    Object? logo = null,
    Object? trailer = freezed,
  }) {
    return _then(_value.copyWith(
      language: null == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      movieTitle: null == movieTitle
          ? _value.movieTitle
          : movieTitle // ignore: cast_nullable_to_non_nullable
              as String,
      movieDescription: null == movieDescription
          ? _value.movieDescription
          : movieDescription // ignore: cast_nullable_to_non_nullable
              as String,
      movieReleaseTag: freezed == movieReleaseTag
          ? _value.movieReleaseTag
          : movieReleaseTag // ignore: cast_nullable_to_non_nullable
              as String?,
      movieSubTitle: null == movieSubTitle
          ? _value.movieSubTitle
          : movieSubTitle // ignore: cast_nullable_to_non_nullable
              as String,
      logo: null == logo
          ? _value.logo
          : logo // ignore: cast_nullable_to_non_nullable
              as String,
      trailer: freezed == trailer
          ? _value.trailer
          : trailer // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_MovieInfoCopyWith<$Res> implements $MovieInfoCopyWith<$Res> {
  factory _$$_MovieInfoCopyWith(
          _$_MovieInfo value, $Res Function(_$_MovieInfo) then) =
      __$$_MovieInfoCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String language,
      String movieTitle,
      String movieDescription,
      String? movieReleaseTag,
      String movieSubTitle,
      String logo,
      String? trailer});
}

/// @nodoc
class __$$_MovieInfoCopyWithImpl<$Res>
    extends _$MovieInfoCopyWithImpl<$Res, _$_MovieInfo>
    implements _$$_MovieInfoCopyWith<$Res> {
  __$$_MovieInfoCopyWithImpl(
      _$_MovieInfo _value, $Res Function(_$_MovieInfo) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? language = null,
    Object? movieTitle = null,
    Object? movieDescription = null,
    Object? movieReleaseTag = freezed,
    Object? movieSubTitle = null,
    Object? logo = null,
    Object? trailer = freezed,
  }) {
    return _then(_$_MovieInfo(
      language: null == language
          ? _value.language
          : language // ignore: cast_nullable_to_non_nullable
              as String,
      movieTitle: null == movieTitle
          ? _value.movieTitle
          : movieTitle // ignore: cast_nullable_to_non_nullable
              as String,
      movieDescription: null == movieDescription
          ? _value.movieDescription
          : movieDescription // ignore: cast_nullable_to_non_nullable
              as String,
      movieReleaseTag: freezed == movieReleaseTag
          ? _value.movieReleaseTag
          : movieReleaseTag // ignore: cast_nullable_to_non_nullable
              as String?,
      movieSubTitle: null == movieSubTitle
          ? _value.movieSubTitle
          : movieSubTitle // ignore: cast_nullable_to_non_nullable
              as String,
      logo: null == logo
          ? _value.logo
          : logo // ignore: cast_nullable_to_non_nullable
              as String,
      trailer: freezed == trailer
          ? _value.trailer
          : trailer // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_MovieInfo extends _MovieInfo {
  const _$_MovieInfo(
      {this.language = 'ru',
      required this.movieTitle,
      required this.movieDescription,
      required this.movieReleaseTag,
      required this.movieSubTitle,
      required this.logo,
      required this.trailer})
      : super._();

  factory _$_MovieInfo.fromJson(Map<String, dynamic> json) =>
      _$$_MovieInfoFromJson(json);

  @override
  @JsonKey()
  final String language;
  @override
  final String movieTitle;
  @override
  final String movieDescription;
  @override
  final String? movieReleaseTag;
  @override
  final String movieSubTitle;
  @override
  final String logo;
  @override
  final String? trailer;

  @override
  String toString() {
    return 'MovieInfo(language: $language, movieTitle: $movieTitle, movieDescription: $movieDescription, movieReleaseTag: $movieReleaseTag, movieSubTitle: $movieSubTitle, logo: $logo, trailer: $trailer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MovieInfo &&
            (identical(other.language, language) ||
                other.language == language) &&
            (identical(other.movieTitle, movieTitle) ||
                other.movieTitle == movieTitle) &&
            (identical(other.movieDescription, movieDescription) ||
                other.movieDescription == movieDescription) &&
            (identical(other.movieReleaseTag, movieReleaseTag) ||
                other.movieReleaseTag == movieReleaseTag) &&
            (identical(other.movieSubTitle, movieSubTitle) ||
                other.movieSubTitle == movieSubTitle) &&
            (identical(other.logo, logo) || other.logo == logo) &&
            (identical(other.trailer, trailer) || other.trailer == trailer));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, language, movieTitle,
      movieDescription, movieReleaseTag, movieSubTitle, logo, trailer);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MovieInfoCopyWith<_$_MovieInfo> get copyWith =>
      __$$_MovieInfoCopyWithImpl<_$_MovieInfo>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MovieInfoToJson(
      this,
    );
  }
}

abstract class _MovieInfo extends MovieInfo {
  const factory _MovieInfo(
      {final String language,
      required final String movieTitle,
      required final String movieDescription,
      required final String? movieReleaseTag,
      required final String movieSubTitle,
      required final String logo,
      required final String? trailer}) = _$_MovieInfo;
  const _MovieInfo._() : super._();

  factory _MovieInfo.fromJson(Map<String, dynamic> json) =
      _$_MovieInfo.fromJson;

  @override
  String get language;
  @override
  String get movieTitle;
  @override
  String get movieDescription;
  @override
  String? get movieReleaseTag;
  @override
  String get movieSubTitle;
  @override
  String get logo;
  @override
  String? get trailer;
  @override
  @JsonKey(ignore: true)
  _$$_MovieInfoCopyWith<_$_MovieInfo> get copyWith =>
      throw _privateConstructorUsedError;
}
