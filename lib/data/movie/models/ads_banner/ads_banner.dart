import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/data/movie/models/images/images.dart';

part 'ads_banner.freezed.dart';
part 'ads_banner.g.dart';

@freezed
class AdsBanner with _$AdsBanner {
  const AdsBanner._();
  const factory AdsBanner({
    required int id,
    required String description,
    required Images images,
    required String type,
    required bool isActive,
    required String? link,
  }) = _AdsBanner;

  factory AdsBanner.fromJson(Map<String, dynamic> json) =>
      _$AdsBannerFromJson(json);
}
