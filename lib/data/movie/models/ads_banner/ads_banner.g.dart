// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ads_banner.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AdsBanner _$$_AdsBannerFromJson(Map<String, dynamic> json) => _$_AdsBanner(
      id: json['id'] as int,
      description: json['description'] as String,
      images: Images.fromJson(json['images'] as Map<String, dynamic>),
      type: json['type'] as String,
      isActive: json['isActive'] as bool,
      link: json['link'] as String?,
    );

Map<String, dynamic> _$$_AdsBannerToJson(_$_AdsBanner instance) =>
    <String, dynamic>{
      'id': instance.id,
      'description': instance.description,
      'images': instance.images,
      'type': instance.type,
      'isActive': instance.isActive,
      'link': instance.link,
    };
