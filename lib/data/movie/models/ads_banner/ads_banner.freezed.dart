// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'ads_banner.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

AdsBanner _$AdsBannerFromJson(Map<String, dynamic> json) {
  return _AdsBanner.fromJson(json);
}

/// @nodoc
mixin _$AdsBanner {
  int get id => throw _privateConstructorUsedError;
  String get description => throw _privateConstructorUsedError;
  Images get images => throw _privateConstructorUsedError;
  String get type => throw _privateConstructorUsedError;
  bool get isActive => throw _privateConstructorUsedError;
  String? get link => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AdsBannerCopyWith<AdsBanner> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AdsBannerCopyWith<$Res> {
  factory $AdsBannerCopyWith(AdsBanner value, $Res Function(AdsBanner) then) =
      _$AdsBannerCopyWithImpl<$Res, AdsBanner>;
  @useResult
  $Res call(
      {int id,
      String description,
      Images images,
      String type,
      bool isActive,
      String? link});

  $ImagesCopyWith<$Res> get images;
}

/// @nodoc
class _$AdsBannerCopyWithImpl<$Res, $Val extends AdsBanner>
    implements $AdsBannerCopyWith<$Res> {
  _$AdsBannerCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? description = null,
    Object? images = null,
    Object? type = null,
    Object? isActive = null,
    Object? link = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      images: null == images
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as Images,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: null == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      link: freezed == link
          ? _value.link
          : link // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $ImagesCopyWith<$Res> get images {
    return $ImagesCopyWith<$Res>(_value.images, (value) {
      return _then(_value.copyWith(images: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_AdsBannerCopyWith<$Res> implements $AdsBannerCopyWith<$Res> {
  factory _$$_AdsBannerCopyWith(
          _$_AdsBanner value, $Res Function(_$_AdsBanner) then) =
      __$$_AdsBannerCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String description,
      Images images,
      String type,
      bool isActive,
      String? link});

  @override
  $ImagesCopyWith<$Res> get images;
}

/// @nodoc
class __$$_AdsBannerCopyWithImpl<$Res>
    extends _$AdsBannerCopyWithImpl<$Res, _$_AdsBanner>
    implements _$$_AdsBannerCopyWith<$Res> {
  __$$_AdsBannerCopyWithImpl(
      _$_AdsBanner _value, $Res Function(_$_AdsBanner) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? description = null,
    Object? images = null,
    Object? type = null,
    Object? isActive = null,
    Object? link = freezed,
  }) {
    return _then(_$_AdsBanner(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      description: null == description
          ? _value.description
          : description // ignore: cast_nullable_to_non_nullable
              as String,
      images: null == images
          ? _value.images
          : images // ignore: cast_nullable_to_non_nullable
              as Images,
      type: null == type
          ? _value.type
          : type // ignore: cast_nullable_to_non_nullable
              as String,
      isActive: null == isActive
          ? _value.isActive
          : isActive // ignore: cast_nullable_to_non_nullable
              as bool,
      link: freezed == link
          ? _value.link
          : link // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AdsBanner extends _AdsBanner {
  const _$_AdsBanner(
      {required this.id,
      required this.description,
      required this.images,
      required this.type,
      required this.isActive,
      required this.link})
      : super._();

  factory _$_AdsBanner.fromJson(Map<String, dynamic> json) =>
      _$$_AdsBannerFromJson(json);

  @override
  final int id;
  @override
  final String description;
  @override
  final Images images;
  @override
  final String type;
  @override
  final bool isActive;
  @override
  final String? link;

  @override
  String toString() {
    return 'AdsBanner(id: $id, description: $description, images: $images, type: $type, isActive: $isActive, link: $link)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AdsBanner &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.description, description) ||
                other.description == description) &&
            (identical(other.images, images) || other.images == images) &&
            (identical(other.type, type) || other.type == type) &&
            (identical(other.isActive, isActive) ||
                other.isActive == isActive) &&
            (identical(other.link, link) || other.link == link));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode =>
      Object.hash(runtimeType, id, description, images, type, isActive, link);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AdsBannerCopyWith<_$_AdsBanner> get copyWith =>
      __$$_AdsBannerCopyWithImpl<_$_AdsBanner>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AdsBannerToJson(
      this,
    );
  }
}

abstract class _AdsBanner extends AdsBanner {
  const factory _AdsBanner(
      {required final int id,
      required final String description,
      required final Images images,
      required final String type,
      required final bool isActive,
      required final String? link}) = _$_AdsBanner;
  const _AdsBanner._() : super._();

  factory _AdsBanner.fromJson(Map<String, dynamic> json) =
      _$_AdsBanner.fromJson;

  @override
  int get id;
  @override
  String get description;
  @override
  Images get images;
  @override
  String get type;
  @override
  bool get isActive;
  @override
  String? get link;
  @override
  @JsonKey(ignore: true)
  _$$_AdsBannerCopyWith<_$_AdsBanner> get copyWith =>
      throw _privateConstructorUsedError;
}
