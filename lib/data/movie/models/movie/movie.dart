// ignore_for_file: invalid_annotation_target

import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/data/movie/models/ads_banner/ads_banner.dart';
import 'package:nine_pandas/data/movie/models/banner/banner.dart';
import 'package:nine_pandas/data/movie/models/posters/posters.dart';
import 'package:nine_pandas/data/movie/models/season/season.dart';

import '../actor/actor.dart';
import '../genre/genre.dart';
import '../movie_info/movie_info.dart';

part 'movie.freezed.dart';
part 'movie.g.dart';

@freezed
class Movie with _$Movie {
  const Movie._();
  @JsonSerializable(explicitToJson: true)
  const factory Movie({
    required int id,
    required String poster,
    required double? imdbRating,
    required double? kinopoiskRating,
    required Posters? posters,
    required MovieInfo? data,
    required bool isReleased,
    required List<Genre>? genres,
    required List<Actor>? actors,
    required List<Season>? seasons,
    required List<Banner>? banners,
    required AdsBanner? adsBanner,
  }) = _Movie;

  factory Movie.fromJson(Map<String, dynamic> json) => _$MovieFromJson(json);
}
