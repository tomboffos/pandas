// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Movie _$$_MovieFromJson(Map<String, dynamic> json) => _$_Movie(
      id: json['id'] as int,
      poster: json['poster'] as String,
      imdbRating: (json['imdbRating'] as num?)?.toDouble(),
      kinopoiskRating: (json['kinopoiskRating'] as num?)?.toDouble(),
      posters: json['posters'] == null
          ? null
          : Posters.fromJson(json['posters'] as Map<String, dynamic>),
      data: json['data'] == null
          ? null
          : MovieInfo.fromJson(json['data'] as Map<String, dynamic>),
      isReleased: json['isReleased'] as bool,
      genres: (json['genres'] as List<dynamic>?)
          ?.map((e) => Genre.fromJson(e as Map<String, dynamic>))
          .toList(),
      actors: (json['actors'] as List<dynamic>?)
          ?.map((e) => Actor.fromJson(e as Map<String, dynamic>))
          .toList(),
      seasons: (json['seasons'] as List<dynamic>?)
          ?.map((e) => Season.fromJson(e as Map<String, dynamic>))
          .toList(),
      banners: (json['banners'] as List<dynamic>?)
          ?.map((e) => Banner.fromJson(e as Map<String, dynamic>))
          .toList(),
      adsBanner: json['adsBanner'] == null
          ? null
          : AdsBanner.fromJson(json['adsBanner'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_MovieToJson(_$_Movie instance) => <String, dynamic>{
      'id': instance.id,
      'poster': instance.poster,
      'imdbRating': instance.imdbRating,
      'kinopoiskRating': instance.kinopoiskRating,
      'posters': instance.posters?.toJson(),
      'data': instance.data?.toJson(),
      'isReleased': instance.isReleased,
      'genres': instance.genres?.map((e) => e.toJson()).toList(),
      'actors': instance.actors?.map((e) => e.toJson()).toList(),
      'seasons': instance.seasons?.map((e) => e.toJson()).toList(),
      'banners': instance.banners?.map((e) => e.toJson()).toList(),
      'adsBanner': instance.adsBanner?.toJson(),
    };
