// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'movie.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Movie _$MovieFromJson(Map<String, dynamic> json) {
  return _Movie.fromJson(json);
}

/// @nodoc
mixin _$Movie {
  int get id => throw _privateConstructorUsedError;
  String get poster => throw _privateConstructorUsedError;
  double? get imdbRating => throw _privateConstructorUsedError;
  double? get kinopoiskRating => throw _privateConstructorUsedError;
  Posters? get posters => throw _privateConstructorUsedError;
  MovieInfo? get data => throw _privateConstructorUsedError;
  bool get isReleased => throw _privateConstructorUsedError;
  List<Genre>? get genres => throw _privateConstructorUsedError;
  List<Actor>? get actors => throw _privateConstructorUsedError;
  List<Season>? get seasons => throw _privateConstructorUsedError;
  List<Banner>? get banners => throw _privateConstructorUsedError;
  AdsBanner? get adsBanner => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MovieCopyWith<Movie> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MovieCopyWith<$Res> {
  factory $MovieCopyWith(Movie value, $Res Function(Movie) then) =
      _$MovieCopyWithImpl<$Res, Movie>;
  @useResult
  $Res call(
      {int id,
      String poster,
      double? imdbRating,
      double? kinopoiskRating,
      Posters? posters,
      MovieInfo? data,
      bool isReleased,
      List<Genre>? genres,
      List<Actor>? actors,
      List<Season>? seasons,
      List<Banner>? banners,
      AdsBanner? adsBanner});

  $PostersCopyWith<$Res>? get posters;
  $MovieInfoCopyWith<$Res>? get data;
  $AdsBannerCopyWith<$Res>? get adsBanner;
}

/// @nodoc
class _$MovieCopyWithImpl<$Res, $Val extends Movie>
    implements $MovieCopyWith<$Res> {
  _$MovieCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? poster = null,
    Object? imdbRating = freezed,
    Object? kinopoiskRating = freezed,
    Object? posters = freezed,
    Object? data = freezed,
    Object? isReleased = null,
    Object? genres = freezed,
    Object? actors = freezed,
    Object? seasons = freezed,
    Object? banners = freezed,
    Object? adsBanner = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      poster: null == poster
          ? _value.poster
          : poster // ignore: cast_nullable_to_non_nullable
              as String,
      imdbRating: freezed == imdbRating
          ? _value.imdbRating
          : imdbRating // ignore: cast_nullable_to_non_nullable
              as double?,
      kinopoiskRating: freezed == kinopoiskRating
          ? _value.kinopoiskRating
          : kinopoiskRating // ignore: cast_nullable_to_non_nullable
              as double?,
      posters: freezed == posters
          ? _value.posters
          : posters // ignore: cast_nullable_to_non_nullable
              as Posters?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as MovieInfo?,
      isReleased: null == isReleased
          ? _value.isReleased
          : isReleased // ignore: cast_nullable_to_non_nullable
              as bool,
      genres: freezed == genres
          ? _value.genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<Genre>?,
      actors: freezed == actors
          ? _value.actors
          : actors // ignore: cast_nullable_to_non_nullable
              as List<Actor>?,
      seasons: freezed == seasons
          ? _value.seasons
          : seasons // ignore: cast_nullable_to_non_nullable
              as List<Season>?,
      banners: freezed == banners
          ? _value.banners
          : banners // ignore: cast_nullable_to_non_nullable
              as List<Banner>?,
      adsBanner: freezed == adsBanner
          ? _value.adsBanner
          : adsBanner // ignore: cast_nullable_to_non_nullable
              as AdsBanner?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $PostersCopyWith<$Res>? get posters {
    if (_value.posters == null) {
      return null;
    }

    return $PostersCopyWith<$Res>(_value.posters!, (value) {
      return _then(_value.copyWith(posters: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $MovieInfoCopyWith<$Res>? get data {
    if (_value.data == null) {
      return null;
    }

    return $MovieInfoCopyWith<$Res>(_value.data!, (value) {
      return _then(_value.copyWith(data: value) as $Val);
    });
  }

  @override
  @pragma('vm:prefer-inline')
  $AdsBannerCopyWith<$Res>? get adsBanner {
    if (_value.adsBanner == null) {
      return null;
    }

    return $AdsBannerCopyWith<$Res>(_value.adsBanner!, (value) {
      return _then(_value.copyWith(adsBanner: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_MovieCopyWith<$Res> implements $MovieCopyWith<$Res> {
  factory _$$_MovieCopyWith(_$_Movie value, $Res Function(_$_Movie) then) =
      __$$_MovieCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String poster,
      double? imdbRating,
      double? kinopoiskRating,
      Posters? posters,
      MovieInfo? data,
      bool isReleased,
      List<Genre>? genres,
      List<Actor>? actors,
      List<Season>? seasons,
      List<Banner>? banners,
      AdsBanner? adsBanner});

  @override
  $PostersCopyWith<$Res>? get posters;
  @override
  $MovieInfoCopyWith<$Res>? get data;
  @override
  $AdsBannerCopyWith<$Res>? get adsBanner;
}

/// @nodoc
class __$$_MovieCopyWithImpl<$Res> extends _$MovieCopyWithImpl<$Res, _$_Movie>
    implements _$$_MovieCopyWith<$Res> {
  __$$_MovieCopyWithImpl(_$_Movie _value, $Res Function(_$_Movie) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? poster = null,
    Object? imdbRating = freezed,
    Object? kinopoiskRating = freezed,
    Object? posters = freezed,
    Object? data = freezed,
    Object? isReleased = null,
    Object? genres = freezed,
    Object? actors = freezed,
    Object? seasons = freezed,
    Object? banners = freezed,
    Object? adsBanner = freezed,
  }) {
    return _then(_$_Movie(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      poster: null == poster
          ? _value.poster
          : poster // ignore: cast_nullable_to_non_nullable
              as String,
      imdbRating: freezed == imdbRating
          ? _value.imdbRating
          : imdbRating // ignore: cast_nullable_to_non_nullable
              as double?,
      kinopoiskRating: freezed == kinopoiskRating
          ? _value.kinopoiskRating
          : kinopoiskRating // ignore: cast_nullable_to_non_nullable
              as double?,
      posters: freezed == posters
          ? _value.posters
          : posters // ignore: cast_nullable_to_non_nullable
              as Posters?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as MovieInfo?,
      isReleased: null == isReleased
          ? _value.isReleased
          : isReleased // ignore: cast_nullable_to_non_nullable
              as bool,
      genres: freezed == genres
          ? _value._genres
          : genres // ignore: cast_nullable_to_non_nullable
              as List<Genre>?,
      actors: freezed == actors
          ? _value._actors
          : actors // ignore: cast_nullable_to_non_nullable
              as List<Actor>?,
      seasons: freezed == seasons
          ? _value._seasons
          : seasons // ignore: cast_nullable_to_non_nullable
              as List<Season>?,
      banners: freezed == banners
          ? _value._banners
          : banners // ignore: cast_nullable_to_non_nullable
              as List<Banner>?,
      adsBanner: freezed == adsBanner
          ? _value.adsBanner
          : adsBanner // ignore: cast_nullable_to_non_nullable
              as AdsBanner?,
    ));
  }
}

/// @nodoc

@JsonSerializable(explicitToJson: true)
class _$_Movie extends _Movie {
  const _$_Movie(
      {required this.id,
      required this.poster,
      required this.imdbRating,
      required this.kinopoiskRating,
      required this.posters,
      required this.data,
      required this.isReleased,
      required final List<Genre>? genres,
      required final List<Actor>? actors,
      required final List<Season>? seasons,
      required final List<Banner>? banners,
      required this.adsBanner})
      : _genres = genres,
        _actors = actors,
        _seasons = seasons,
        _banners = banners,
        super._();

  factory _$_Movie.fromJson(Map<String, dynamic> json) =>
      _$$_MovieFromJson(json);

  @override
  final int id;
  @override
  final String poster;
  @override
  final double? imdbRating;
  @override
  final double? kinopoiskRating;
  @override
  final Posters? posters;
  @override
  final MovieInfo? data;
  @override
  final bool isReleased;
  final List<Genre>? _genres;
  @override
  List<Genre>? get genres {
    final value = _genres;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<Actor>? _actors;
  @override
  List<Actor>? get actors {
    final value = _actors;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<Season>? _seasons;
  @override
  List<Season>? get seasons {
    final value = _seasons;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<Banner>? _banners;
  @override
  List<Banner>? get banners {
    final value = _banners;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final AdsBanner? adsBanner;

  @override
  String toString() {
    return 'Movie(id: $id, poster: $poster, imdbRating: $imdbRating, kinopoiskRating: $kinopoiskRating, posters: $posters, data: $data, isReleased: $isReleased, genres: $genres, actors: $actors, seasons: $seasons, banners: $banners, adsBanner: $adsBanner)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Movie &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.poster, poster) || other.poster == poster) &&
            (identical(other.imdbRating, imdbRating) ||
                other.imdbRating == imdbRating) &&
            (identical(other.kinopoiskRating, kinopoiskRating) ||
                other.kinopoiskRating == kinopoiskRating) &&
            (identical(other.posters, posters) || other.posters == posters) &&
            (identical(other.data, data) || other.data == data) &&
            (identical(other.isReleased, isReleased) ||
                other.isReleased == isReleased) &&
            const DeepCollectionEquality().equals(other._genres, _genres) &&
            const DeepCollectionEquality().equals(other._actors, _actors) &&
            const DeepCollectionEquality().equals(other._seasons, _seasons) &&
            const DeepCollectionEquality().equals(other._banners, _banners) &&
            (identical(other.adsBanner, adsBanner) ||
                other.adsBanner == adsBanner));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      poster,
      imdbRating,
      kinopoiskRating,
      posters,
      data,
      isReleased,
      const DeepCollectionEquality().hash(_genres),
      const DeepCollectionEquality().hash(_actors),
      const DeepCollectionEquality().hash(_seasons),
      const DeepCollectionEquality().hash(_banners),
      adsBanner);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_MovieCopyWith<_$_Movie> get copyWith =>
      __$$_MovieCopyWithImpl<_$_Movie>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_MovieToJson(
      this,
    );
  }
}

abstract class _Movie extends Movie {
  const factory _Movie(
      {required final int id,
      required final String poster,
      required final double? imdbRating,
      required final double? kinopoiskRating,
      required final Posters? posters,
      required final MovieInfo? data,
      required final bool isReleased,
      required final List<Genre>? genres,
      required final List<Actor>? actors,
      required final List<Season>? seasons,
      required final List<Banner>? banners,
      required final AdsBanner? adsBanner}) = _$_Movie;
  const _Movie._() : super._();

  factory _Movie.fromJson(Map<String, dynamic> json) = _$_Movie.fromJson;

  @override
  int get id;
  @override
  String get poster;
  @override
  double? get imdbRating;
  @override
  double? get kinopoiskRating;
  @override
  Posters? get posters;
  @override
  MovieInfo? get data;
  @override
  bool get isReleased;
  @override
  List<Genre>? get genres;
  @override
  List<Actor>? get actors;
  @override
  List<Season>? get seasons;
  @override
  List<Banner>? get banners;
  @override
  AdsBanner? get adsBanner;
  @override
  @JsonKey(ignore: true)
  _$$_MovieCopyWith<_$_Movie> get copyWith =>
      throw _privateConstructorUsedError;
}
