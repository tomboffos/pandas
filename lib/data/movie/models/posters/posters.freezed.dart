// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'posters.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Posters _$PostersFromJson(Map<String, dynamic> json) {
  return _Posters.fromJson(json);
}

/// @nodoc
mixin _$Posters {
  String get desktop => throw _privateConstructorUsedError;
  String get tablet => throw _privateConstructorUsedError;
  String get mobile => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $PostersCopyWith<Posters> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PostersCopyWith<$Res> {
  factory $PostersCopyWith(Posters value, $Res Function(Posters) then) =
      _$PostersCopyWithImpl<$Res, Posters>;
  @useResult
  $Res call({String desktop, String tablet, String mobile});
}

/// @nodoc
class _$PostersCopyWithImpl<$Res, $Val extends Posters>
    implements $PostersCopyWith<$Res> {
  _$PostersCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? desktop = null,
    Object? tablet = null,
    Object? mobile = null,
  }) {
    return _then(_value.copyWith(
      desktop: null == desktop
          ? _value.desktop
          : desktop // ignore: cast_nullable_to_non_nullable
              as String,
      tablet: null == tablet
          ? _value.tablet
          : tablet // ignore: cast_nullable_to_non_nullable
              as String,
      mobile: null == mobile
          ? _value.mobile
          : mobile // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_PostersCopyWith<$Res> implements $PostersCopyWith<$Res> {
  factory _$$_PostersCopyWith(
          _$_Posters value, $Res Function(_$_Posters) then) =
      __$$_PostersCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String desktop, String tablet, String mobile});
}

/// @nodoc
class __$$_PostersCopyWithImpl<$Res>
    extends _$PostersCopyWithImpl<$Res, _$_Posters>
    implements _$$_PostersCopyWith<$Res> {
  __$$_PostersCopyWithImpl(_$_Posters _value, $Res Function(_$_Posters) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? desktop = null,
    Object? tablet = null,
    Object? mobile = null,
  }) {
    return _then(_$_Posters(
      desktop: null == desktop
          ? _value.desktop
          : desktop // ignore: cast_nullable_to_non_nullable
              as String,
      tablet: null == tablet
          ? _value.tablet
          : tablet // ignore: cast_nullable_to_non_nullable
              as String,
      mobile: null == mobile
          ? _value.mobile
          : mobile // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Posters extends _Posters {
  const _$_Posters(
      {required this.desktop, required this.tablet, required this.mobile})
      : super._();

  factory _$_Posters.fromJson(Map<String, dynamic> json) =>
      _$$_PostersFromJson(json);

  @override
  final String desktop;
  @override
  final String tablet;
  @override
  final String mobile;

  @override
  String toString() {
    return 'Posters(desktop: $desktop, tablet: $tablet, mobile: $mobile)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Posters &&
            (identical(other.desktop, desktop) || other.desktop == desktop) &&
            (identical(other.tablet, tablet) || other.tablet == tablet) &&
            (identical(other.mobile, mobile) || other.mobile == mobile));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, desktop, tablet, mobile);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PostersCopyWith<_$_Posters> get copyWith =>
      __$$_PostersCopyWithImpl<_$_Posters>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_PostersToJson(
      this,
    );
  }
}

abstract class _Posters extends Posters {
  const factory _Posters(
      {required final String desktop,
      required final String tablet,
      required final String mobile}) = _$_Posters;
  const _Posters._() : super._();

  factory _Posters.fromJson(Map<String, dynamic> json) = _$_Posters.fromJson;

  @override
  String get desktop;
  @override
  String get tablet;
  @override
  String get mobile;
  @override
  @JsonKey(ignore: true)
  _$$_PostersCopyWith<_$_Posters> get copyWith =>
      throw _privateConstructorUsedError;
}
