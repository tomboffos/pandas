import 'package:freezed_annotation/freezed_annotation.dart';

part 'posters.freezed.dart';
part 'posters.g.dart';

@freezed
class Posters with _$Posters {
  const Posters._();
  const factory Posters({
    required String desktop,
    required String tablet,
    required String mobile,
  }) = _Posters;

  factory Posters.fromJson(Map<String, dynamic> json) =>
      _$PostersFromJson(json);
}
