// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'posters.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Posters _$$_PostersFromJson(Map<String, dynamic> json) => _$_Posters(
      desktop: json['desktop'] as String,
      tablet: json['tablet'] as String,
      mobile: json['mobile'] as String,
    );

Map<String, dynamic> _$$_PostersToJson(_$_Posters instance) =>
    <String, dynamic>{
      'desktop': instance.desktop,
      'tablet': instance.tablet,
      'mobile': instance.mobile,
    };
