import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/html_reading_page/repository/html_reading_page_repository.dart';

import 'datasources/html_reading_page_data_source.dart';
import 'models/html_reading.dart';

@Injectable(as: HtmlReadingPageRepository)
class HtmlReadingPageRepositoryImpl implements HtmlReadingPageRepository {
  final HtmlReadingPageDataSource remoteDataSource;

  HtmlReadingPageRepositoryImpl(this.remoteDataSource);

  Future<Either<Failure, HtmlReading>> getHtmlDoc(
      String docType, String locale) async {
    try {
      final res = await remoteDataSource.getHtmlDoc(docType, locale);

      return Right(res);
    } catch (e) {
      if (e is ServerException && e.code == 400) {
        return Left(ServerFailure(message: e.message));
      }
      return Left(UnavailableCacheFailure());
    }
  }
}
