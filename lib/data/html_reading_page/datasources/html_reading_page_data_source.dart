import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/data/html_reading_page/datasources/constants/privacy_endpoint.dart';

import '../models/html_reading.dart';

abstract class HtmlReadingPageDataSource {
  Future getHtmlDoc(String docType, String locale);
}

@Injectable(as: HtmlReadingPageDataSource)
class HtmlReadingPageDataSourceImpl implements HtmlReadingPageDataSource {
  final DioClient dioClient;

  HtmlReadingPageDataSourceImpl(this.dioClient);

  @override
  Future getHtmlDoc(String docType, String locale) async {
    try {
      final res = await dioClient.get(
        Endpoints.docs,
        queryParameters: {
          'docType': docType,
          'locale': locale,
        },
      );

      return HtmlReading.fromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
            code: e.response!.statusCode, message: e.response!.statusMessage);
      }
      throw (e);
    }
  }
}
