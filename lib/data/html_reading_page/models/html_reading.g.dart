// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'html_reading.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_HtmlReading _$$_HtmlReadingFromJson(Map<String, dynamic> json) =>
    _$_HtmlReading(
      id: json['id'] as int,
      type: json['type'] as String,
      data: DocData.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_HtmlReadingToJson(_$_HtmlReading instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'data': instance.data,
    };
