// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'doc_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DocData _$DocDataFromJson(Map<String, dynamic> json) => DocData(
      title: json['title'] as String,
      text: json['text'] as String,
    );

Map<String, dynamic> _$DocDataToJson(DocData instance) => <String, dynamic>{
      'title': instance.title,
      'text': instance.text,
    };
