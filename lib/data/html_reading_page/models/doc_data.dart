import 'package:freezed_annotation/freezed_annotation.dart';

part 'doc_data.g.dart';

@JsonSerializable()
class DocData {
  final String title;
  final String text;

  DocData({required this.title, required this.text});

  factory DocData.fromJson(Map<String, dynamic> json) =>
      _$DocDataFromJson(json);

  Map<String, dynamic> toJson() => _$DocDataToJson(this);
}
