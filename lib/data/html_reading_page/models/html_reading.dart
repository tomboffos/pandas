import 'package:freezed_annotation/freezed_annotation.dart';

import 'doc_data.dart';

part 'html_reading.freezed.dart';
part 'html_reading.g.dart';

@freezed
class HtmlReading with _$HtmlReading {
  const HtmlReading._();
  const factory HtmlReading({
    required int id,
    required String type,
    required DocData data,
  }) = _HtmlReading;

  factory HtmlReading.fromJson(Map<String, dynamic> json) =>
      _$HtmlReadingFromJson(json);
}

class HtmlDocType {
  static const String privacy = 'privacy';
  static const String agreement = 'agreement';
  static const String lottery = 'lottery';
}
