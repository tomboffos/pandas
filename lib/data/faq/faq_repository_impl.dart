import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/faq/datasources/faq_data_source.dart';
import 'package:nine_pandas/domain/faq/repository/faq_repository.dart';

import 'models/faq_model.dart';

@Injectable(as: FaqRepository)
class FaqRepositoryImpl implements FaqRepository {
  final FaqDataSource dataSource;

  FaqRepositoryImpl(this.dataSource);

  Future<Either<Failure, List<FaqModel>>> getFaq(String locale) async {
    try {
      final res = await dataSource.getFaq(locale);
      return Right(res);
    } catch (e) {
      if (e is ServerException && e.code == 400) {
        return Left(ServerFailure(message: e.message));
      }

      return Left(UnavailableCacheFailure());
    }
  }
}
