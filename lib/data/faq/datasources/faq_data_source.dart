import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/exception.dart';
import 'package:nine_pandas/core/network/dio_client.dart';
import 'package:nine_pandas/core/network/endpoints/faq_endpoint.dart';
import 'package:nine_pandas/data/faq/models/faq_model.dart';

abstract class FaqDataSource {
  Future<List<FaqModel>> getFaq(String locale);
}

@Injectable(as: FaqDataSource)
class FaqDataSourceImpl implements FaqDataSource {
  final DioClient dioClient;

  FaqDataSourceImpl(this.dioClient);

  Future<List<FaqModel>> getFaq(String locale) async {
    try {
      final res = await dioClient.get(
        FaqEndpoint.faq,
        queryParameters: {
          'locale': locale,
        },
      );

      return FaqModel.listFromJson(res);
    } catch (e) {
      if (e is DioError) {
        throw ServerException(
            code: e.response!.statusCode, message: e.response!.statusMessage);
      }
      throw (e);
    }
  }
}
