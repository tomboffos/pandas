import 'package:freezed_annotation/freezed_annotation.dart';

part 'faq_data.g.dart';

@JsonSerializable()
class FaqData {
  final String title;
  final String text;

  FaqData({required this.title, required this.text});

  factory FaqData.fromJson(Map<String, dynamic> json) =>
      _$FaqDataFromJson(json);

  Map<String, dynamic> toJson() => _$FaqDataToJson(this);
}
