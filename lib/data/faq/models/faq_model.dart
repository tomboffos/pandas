import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/data/faq/models/faq_data.dart';

part 'faq_model.g.dart';

@JsonSerializable(explicitToJson: true)
class FaqModel {
  final int id;
  final FaqData data;

  FaqModel({required this.id, required this.data});

  static List<FaqModel> listFromJson(List<dynamic> json) {
    final res = <FaqModel>[];
    json.forEach((faq) {
      res.add(FaqModel.fromJson(faq));
    });
    return res;
  }

  factory FaqModel.fromJson(Map<String, dynamic> json) =>
      _$FaqModelFromJson(json);

  Map<String, dynamic> toJson() => _$FaqModelToJson(this);
}
