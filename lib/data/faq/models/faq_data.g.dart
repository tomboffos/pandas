// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'faq_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FaqData _$FaqDataFromJson(Map<String, dynamic> json) => FaqData(
      title: json['title'] as String,
      text: json['text'] as String,
    );

Map<String, dynamic> _$FaqDataToJson(FaqData instance) => <String, dynamic>{
      'title': instance.title,
      'text': instance.text,
    };
