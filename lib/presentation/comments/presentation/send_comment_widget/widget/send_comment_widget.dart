import 'dart:ui';

import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SendCommentWidget extends StatefulWidget {
  final Function(String) onSendComment;

  SendCommentWidget({Key? key, required this.onSendComment}) : super(key: key);

  @override
  State<SendCommentWidget> createState() => _SendCommentWidgetState();
}

class _SendCommentWidgetState extends State<SendCommentWidget> {
  TextEditingController _reviewController = TextEditingController();

  static const double _sendButtonWidth = 170;

  @override
  void dispose() {
    _reviewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              AppLocalizations.of(context)!.userpicPlaceholderImage,
              height: Dimens.iconSize * 2,
              width: Dimens.iconSize * 2,
            ),
            Gap.w_75,
            Expanded(
              child: PandasBaseTextInput(
                hintText: AppLocalizations.of(context)!.commentHint,
                keyboardType: TextInputType.text,
                controller: _reviewController,
                minLines: 4,
                maxLines: 99,
                isLimited: false,
              ),
            )
          ],
        ),
        Gap.h,
        Align(
          alignment: Alignment.topRight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                onTap: () => _reviewController.clear(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: Dimens.verticalPadding * 0.75,
                    horizontal: Dimens.horizontalPadding * 1.75,
                  ),
                  child: Text(
                    AppLocalizations.of(context)!.cancel,
                    style: TextStyle(
                        fontSize: 12,
                        height: 1.3,
                        color: AppColors.lightGray,
                        fontFeatures: const [FontFeature.proportionalFigures()],
                        fontFamily: 'Roboto'),
                  ),
                ),
              ),
              Gap.w1_5,
              CustomButton(
                onTap: () {
                  if (_reviewController.text.trim().isNotEmpty) {
                    widget.onSendComment(_reviewController.text.trim());
                  }
                  _reviewController.clear();
                },
                width: _sendButtonWidth,
                backgroundColor: AppColors.buttonColor,
                child: Text(
                  AppLocalizations.of(context)!.sendComment,
                  style: TextStyle(
                      fontSize: 12,
                      height: 1.3,
                      color: AppColors.veryLightGray,
                      fontFeatures: const [FontFeature.proportionalFigures()],
                      fontFamily: 'Roboto'),
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
