import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/core/utils/date_helper.dart';
import 'package:nine_pandas/data/player/models/comment.dart';

class CommentCard extends StatefulWidget {
  final Function onTapUpVote;
  final Function onTapDownVote;
  final Comment comment;

  CommentCard(
      {Key? key,
      required this.onTapUpVote,
      required this.onTapDownVote,
      required this.comment})
      : super(key: key);

  @override
  State<CommentCard> createState() => _CommentCardState();
}

class _CommentCardState extends State<CommentCard> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Avatar(imageUrl: null),
        Gap.w,
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UserInfo(
                userName: widget.comment.user?['name'] ?? '',
                comment: widget.comment,
              ),
              Gap.h_25,
              Text(
                widget.comment.comment,
                style: TextStyle(
                    fontSize: 14,
                    color: AppColors.greyTextColor,
                    height: 1.3,
                    fontFeatures: const [FontFeature.proportionalFigures()],
                    fontFamily: 'Roboto'),
              ),
              Gap.h_5,
              VoteButtons(
                comment: widget.comment,
                onTapUpVote: widget.onTapUpVote,
                onTapDownVote: widget.onTapDownVote,
              )
            ],
          ),
        ),
      ],
    );
  }
}

class UserInfo extends StatefulWidget {
  final String userName;
  final Comment comment;
  const UserInfo({Key? key, required this.userName, required this.comment})
      : super(key: key);

  @override
  State<UserInfo> createState() => _UserInfoState();
}

class _UserInfoState extends State<UserInfo> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          widget.userName,
          style: TextStyle(
            fontSize: 12,
            height: 1.3,
            color: AppColors.veryLightGray,
          ),
        ),
        Gap.w_25,
        Container(
          width: Dimens.iconSize * 0.25,
          height: Dimens.iconSize * 0.25,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: AppColors.highlightColor,
          ),
        ),
        Gap.w_25,
        Text(
          DateHelper.dateToRecognizableString(widget.comment.createdAt),
          style: TextStyle(
            fontSize: 12,
            height: 1.3,
            color: AppColors.darkGreyTextColor,
          ),
        ),
      ],
    );
  }
}

class Avatar extends StatelessWidget {
  const Avatar({Key? key, required this.imageUrl}) : super(key: key);
  final String? imageUrl;

  @override
  Widget build(BuildContext context) {
    if (imageUrl != null) {
      return CircleAvatar(
        radius: Dimens.iconSize * 0.75,
        backgroundImage: CachedNetworkImageProvider(imageUrl!),
      );
    } else {
      return CircleAvatar(
        radius: Dimens.iconSize * 0.75,
        child:
            Image.asset(AppLocalizations.of(context)!.userpicPlaceholderImage),
      );
    }
  }
}

class VoteButtons extends StatefulWidget {
  VoteButtons({
    Key? key,
    required this.onTapUpVote,
    required this.onTapDownVote,
    required this.comment,
  }) : super(key: key);

  final Function onTapUpVote;
  final Function onTapDownVote;
  final Comment comment;

  @override
  State<VoteButtons> createState() => _VoteButtonsState();
}

class _VoteButtonsState extends State<VoteButtons> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        InkWell(
          onTap: widget.onTapUpVote as void Function()?,
          child: Row(
            children: [
              SvgPicture.asset(
                widget.comment.userVote == 1
                    ? AppLocalizations.of(context)!.thumbUpIconFilled
                    : AppLocalizations.of(context)!.thumbUpIcon,
                width: Dimens.iconSize,
                height: Dimens.iconSize,
              ),
              Gap.w_5,
              Text(
                widget.comment.voteUp?.toString() ?? '0',
                style: TextStyle(
                  fontSize: 14,
                  color: AppColors.darkGreyTextColor,
                  height: 1.3,
                ),
              ),
            ],
          ),
        ),
        Gap.w,
        InkWell(
          onTap: widget.onTapDownVote as void Function()?,
          child: Row(
            children: [
              SvgPicture.asset(
                widget.comment.userVote == -1
                    ? AppLocalizations.of(context)!.thumbDownIconFilled
                    : AppLocalizations.of(context)!.thumbDownIcon,
                width: Dimens.iconSize,
                height: Dimens.iconSize,
              ),
              Gap.w_5,
              Text(
                widget.comment.voteDown?.abs().toString() ?? '0',
                style: TextStyle(
                  fontSize: 14,
                  color: AppColors.darkGreyTextColor,
                  height: 1.3,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
