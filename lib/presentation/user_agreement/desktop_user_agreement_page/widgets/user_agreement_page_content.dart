import '../../../../../../core/ui/ui_kit.dart';
import 'user_agreement.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class UserAgreementPageContent extends StatelessWidget {
  final String htmlData;

  const UserAgreementPageContent({Key? key, required this.htmlData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(minHeight: MediaQuery.of(context).size.height / 1.25),
      child: Padding(
        padding: Insets.w10,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Gap.h1_5,
            NavigationRow(
              currentPageText: AppLocalizations.of(context)!.userAgreement,
            ),
            Gap.h1_5,
            UserAgreementWidget(htmlData: htmlData),
          ],
        ),
      ),
    );
  }
}
