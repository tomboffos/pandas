import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/data/html_reading_page/models/html_reading.dart';
import 'package:nine_pandas/domain/html_reading_page/bloc/html_reader_page_bloc.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import '../../../../../core/ui/ui_kit.dart';
import 'widgets/user_agreement_page_content.dart';

class TabletUserAgreementPage extends StatefulWidget {
  const TabletUserAgreementPage({Key? key}) : super(key: key);

  @override
  State<TabletUserAgreementPage> createState() =>
      _TabletUserAgreementPageState();
}

class _TabletUserAgreementPageState extends State<TabletUserAgreementPage> {
  late final HtmlReadingPageBloc _policyBloc;

  @override
  void initState() {
    _policyBloc = BlocProvider.of<HtmlReadingPageBloc>(context);
    super.initState();
  }

  @override
  void dispose() {
    _policyBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: BlocProvider.value(
        value: _policyBloc,
        child: BlocBuilder<HtmlReadingPageBloc, HtmlReadingPageState>(
          builder: (context, state) {
            return state.when(
              loaded: (policy) => Column(
                children: [
                  NavigationHeader(),
                  Expanded(
                    child: Stack(
                      children: [
                        SingleChildScrollView(
                          child: Padding(
                            padding: Insets.w2,
                            child: Column(
                              children: [
                                UserAgreementPageContent(
                                  htmlData: policy.data.text,
                                ),
                                Gap.h2,
                                Footer(),
                              ],
                            ),
                          ),
                        ),
                        BlackoutContainer(),
                      ],
                    ),
                  )
                ],
              ),
              loading: () => PandasLoadingView(),
              error: () => PandasServerErrorWidget(
                reloadCallback: () => _policyBloc.add(
                  HtmlReadingPageEvent.fetchData(
                    HtmlDocType.privacy,
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
