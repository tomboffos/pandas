import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';

class UserAgreementWidget extends StatelessWidget {
  const UserAgreementWidget({Key? key, required this.htmlData})
      : super(key: key);

  final String htmlData;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [
        HtmlWidget(htmlData),
      ]),
    );
  }
}
