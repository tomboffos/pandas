import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/data/html_reading_page/models/html_reading.dart';
import 'package:nine_pandas/domain/html_reading_page/bloc/html_reader_page_bloc.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';
import 'package:nine_pandas/presentation/user_agreement/tablet_user_agreement_page/tablet_user_agreement_page.dart';

import 'desktop_user_agreement_page/desktop_user_agreement_page.dart';
import 'mobile_user_agreement_page/mobile_user_agreement_page.dart';

class UserAgreementPage extends StatefulWidget {
  const UserAgreementPage({Key? key}) : super(key: key);

  @override
  State<UserAgreementPage> createState() => UserAgreementPageState();
}

class UserAgreementPageState extends State<UserAgreementPage> {
  bool isBlackout = false;
  late final HtmlReadingPageBloc _htmlReadingPageBloc =
      getIt.get<HtmlReadingPageBloc>();

  @override
  void initState() {
    _htmlReadingPageBloc
        .add(HtmlReadingPageEvent.fetchData(HtmlDocType.agreement));
    super.initState();
  }

  @override
  void dispose() {
    _htmlReadingPageBloc.close();
    super.dispose();
  }

  void isBlackoutChange(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: isBlackoutChange,
      child: BlocListener<LocalizationBloc, LocalizationState>(
        listener: (context, state) {
          _htmlReadingPageBloc
              .add(HtmlReadingPageEvent.fetchData(HtmlDocType.agreement));
        },
        child: BlocProvider.value(
          value: _htmlReadingPageBloc,
          child: LayoutHandler(
            mobileView: MobileUserAgreementPage(),
            desktopView: DesktopUserAgreementPage(),
            tabletView: TabletUserAgreementPage(),
          ),
        ),
      ),
    );
  }
}
