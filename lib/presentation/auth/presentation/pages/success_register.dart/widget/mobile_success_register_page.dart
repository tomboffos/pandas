import 'dart:ui';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../../../core/ui/ui_kit.dart';

class MobileSuccessRegisterPage extends StatelessWidget {
  const MobileSuccessRegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: const EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: MobileSuccessRegisterPopup(),
    );
  }
}

class MobileSuccessRegisterPopup extends StatefulWidget {
  const MobileSuccessRegisterPopup({Key? key}) : super(key: key);

  static const EdgeInsets insets = EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0);

  static const double singInFormFieldHeight = 150;
  static const double singInFormFieldWidth = 556;

  @override
  State<MobileSuccessRegisterPopup> createState() =>
      _MobileSuccessRegisterPopupState();
}

class _MobileSuccessRegisterPopupState
    extends State<MobileSuccessRegisterPopup> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: MobileSuccessRegisterPopup.insets,
            width: MobileSuccessRegisterPopup.singInFormFieldWidth,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: MobileSuccessRegisterPopup.singInFormFieldHeight),
              child: Column(
                children: [
                  Gap.h2,
                  SvgPicture.asset(
                    AppLocalizations.of(context)!.doneIcon,
                    height: Dimens.iconSize * 3,
                  ),
                  Gap.h,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context)!.successRegister,
                          textAlign: TextAlign.center,
                          maxLines: 4,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
