import 'package:nine_pandas/core/ui/ui_kit.dart';

import 'widget/desktop_success_register_page.dart';
import 'widget/mobile_success_register_page.dart';

class SuccessRegisterPage extends StatefulWidget {
  const SuccessRegisterPage({Key? key}) : super(key: key);

  @override
  State<SuccessRegisterPage> createState() => _SuccessRegisterPageState();
}

class _SuccessRegisterPageState extends State<SuccessRegisterPage> {
  @override
  Widget build(BuildContext context) {
    return LayoutHandler(
      mobileView: MobileSuccessRegisterPage(),
      desktopView: DesktopAndTabletSuccessRegisterPage(),
      tabletView: DesktopAndTabletSuccessRegisterPage(),
    );
  }
}
