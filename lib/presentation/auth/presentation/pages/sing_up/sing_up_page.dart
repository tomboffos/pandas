import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/sing_up/bloc/sing_up_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

import 'desktop_and_tablet_sing_up_page.dart';
import 'mobile_sing_up_page.dart';

class SingUpPage extends StatefulWidget {
  SingUpPage({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  State<SingUpPage> createState() => _SingUpPageState();
}

class _SingUpPageState extends State<SingUpPage> {
  late final SingUpBloc _singUpBloc;

  @override
  void initState() {
    super.initState();

    _singUpBloc = getIt.get<SingUpBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return PointerInterceptor(
      child: BlocProvider.value(
        value: _singUpBloc,
        child: LayoutHandler(
          mobileView: MobileSingUpPage(title: widget.title),
          desktopView: DesktopAndTabletSingUpPage(title: widget.title),
          tabletView: DesktopAndTabletSingUpPage(title: widget.title),
        ),
      ),
    );
  }
}
