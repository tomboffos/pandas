import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/utils/string_helper.dart';
import 'package:nine_pandas/domain/message/bloc/message_bloc.dart';
import 'package:nine_pandas/domain/sing_up/bloc/sing_up_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/email_confirmation/email_confirmation.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_in/sing_in_page.dart';

import '../../../../../core/ui/ui_kit.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DesktopAndTabletSingUpPage extends StatelessWidget {
  DesktopAndTabletSingUpPage({Key? key, this.title}) : super(key: key);
  final String? title;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: DesktopAndTabletSingUpFormField(title: title),
    );
  }
}

class DesktopAndTabletSingUpFormField extends StatefulWidget {
  DesktopAndTabletSingUpFormField({Key? key, this.title}) : super(key: key);
  final String? title;

  static const EdgeInsets insets = EdgeInsets.fromLTRB(52.0, 40.0, 52.0, 52.0);

  static const double singUpFormFieldHeight = 400;
  static const double singUpFormFieldWidth = 556;

  @override
  State<DesktopAndTabletSingUpFormField> createState() =>
      _DesktopAndTabletSingUpFormFieldState();
}

class _DesktopAndTabletSingUpFormFieldState
    extends State<DesktopAndTabletSingUpFormField> {
  bool isChecked = false;
  bool isObscured = true;
  bool isNameValid = false;
  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool isEmailError = false;

  final _formKey = GlobalKey<FormState>();
  TextEditingController _nameTextController = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();

  late final SingUpBloc _singUpBloc;

  @override
  void initState() {
    super.initState();
    _singUpBloc = BlocProvider.of<SingUpBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SingUpBloc, SingUpState>(
      listener: (context, state) {
        state.whenOrNull(registerSuccess: (userName) async {
          Navigator.pop(context);

          await showDialog<String>(
            barrierColor: Colors.black.withOpacity(0.6),
            context: context,
            builder: (BuildContext context) => EmailConfirmationPage(
              email: userName,
            ),
          );
        }, error: (e) {
          isEmailError = true;
        });
      },
      builder: (context, state) {
        return SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                padding: DesktopAndTabletSingUpFormField.insets,
                width: DesktopAndTabletSingUpFormField.singUpFormFieldWidth,
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minHeight: DesktopAndTabletSingUpFormField
                          .singUpFormFieldHeight),
                  child: state.maybeWhen(
                    loading: () => Center(
                      child: CircularProgressIndicator(),
                    ),
                    orElse: () => Form(
                      key: _formKey,
                      autovalidateMode: AutovalidateMode.always,
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Expanded(
                                child: Text(
                                  widget.title ??
                                      AppLocalizations.of(context)!.register,
                                  textAlign: TextAlign.center,
                                  maxLines: 2,
                                  style: TextStyle(
                                    fontSize: widget.title != null ? 20 : 28,
                                    fontWeight: FontWeight.w500,
                                    fontFeatures: const [
                                      FontFeature.proportionalFigures()
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Gap.h2,
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppLocalizations.of(context)!.name,
                                style: TextStyle(
                                  color: AppColors.greyTextColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              Gap.h_5,
                              PandasBaseTextInput(
                                controller: _nameTextController,
                                hintText: AppLocalizations.of(context)!.name,
                                onFieldSubmitted: (_) {
                                  FocusScope.of(context).nextFocus();
                                },
                                onChanged: (value) {
                                  if (value.length >= 3) {
                                    setState(() {
                                      isNameValid = true;
                                    });
                                  } else {
                                    setState(() {
                                      isNameValid = false;
                                    });
                                  }
                                },
                              ),
                              Gap.h_25,
                              Gap.h,
                              Text(
                                AppLocalizations.of(context)!.email,
                                style: TextStyle(
                                  color: AppColors.greyTextColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              Gap.h_5,
                              PandasBaseTextInput(
                                isError: isEmailError,
                                controller: _emailTextController,
                                hintText: AppLocalizations.of(context)!.email,
                                onFieldSubmitted: (_) {
                                  FocusScope.of(context).nextFocus();
                                },
                                onChanged: (value) {
                                  if (isEmailAddress(value)) {
                                    setState(() {
                                      isEmailValid = true;
                                    });
                                  } else {
                                    setState(() {
                                      isEmailValid = false;
                                    });
                                  }
                                },
                              ),
                              Gap.h_25,
                              Gap.h,
                              Text(
                                AppLocalizations.of(context)!.password,
                                style: TextStyle(
                                  color: AppColors.greyTextColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              Gap.h_5,
                              PandasBaseTextInput(
                                controller: _passwordTextController,
                                hintText:
                                    AppLocalizations.of(context)!.password,
                                isObscured: isObscured,
                                onFieldSubmitted: (_) {
                                  FocusScope.of(context).nextFocus();
                                },
                                onChanged: (value) {
                                  if (value.length >= 6) {
                                    setState(() {
                                      isPasswordValid = true;
                                    });
                                  } else {
                                    setState(() {
                                      isPasswordValid = false;
                                    });
                                  }
                                },
                                tail: Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    IconButton(
                                      icon: Icon(
                                        isObscured
                                            ? Icons.visibility_outlined
                                            : Icons.visibility_off_outlined,
                                        color: Colors.white,
                                        size: Dimens.iconSize * 1.2,
                                      ),
                                      onPressed: () => setState(() {
                                        isObscured = !isObscured;
                                      }),
                                    ),
                                  ],
                                ),
                              ),
                              Gap.h_25,
                              Gap.h1_25,
                              Row(
                                children: [
                                  PandasCheckbox(
                                      size: 22,
                                      value: isChecked,
                                      onChanged: (value) {
                                        setState(() {
                                          isChecked = value;
                                        });
                                      }),
                                  Gap.w_5,
                                  Gap.w_25,
                                  Flexible(
                                    child: RichText(
                                      maxLines: 3,
                                      text: TextSpan(
                                        children: [
                                          TextSpan(
                                            text: AppLocalizations.of(context)!
                                                .agreeWith,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12,
                                            ),
                                          ),
                                          TextSpan(
                                            text: AppLocalizations.of(context)!
                                                .privacyPolicy,
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () =>
                                                  AutoRouter.of(context)
                                                      .navigate(
                                                          PrivacyPolicyRoute()),
                                            style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                              color: Colors.white,
                                              fontSize: 12,
                                            ),
                                          ),
                                          TextSpan(
                                            text: AppLocalizations.of(context)!
                                                .and,
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12,
                                            ),
                                          ),
                                          TextSpan(
                                            text: AppLocalizations.of(context)!
                                                .termsOfUse,
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () =>
                                                  AutoRouter.of(context)
                                                      .navigate(
                                                          UserAgreementRoute()),
                                            style: TextStyle(
                                              decoration:
                                                  TextDecoration.underline,
                                              color: Colors.white,
                                              fontSize: 12,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Gap.h1_25,
                              if (isEmailError)
                                TweenAnimationBuilder(
                                  duration: Duration(milliseconds: 300),
                                  curve: Curves.ease,
                                  tween: Tween(begin: 1.0, end: 0.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        AppLocalizations.of(context)!
                                            .userAlreadyExists,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: AppColors.error,
                                          fontFeatures: const [
                                            FontFeature.proportionalFigures()
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  builder: (context, value, child) {
                                    return Transform.translate(
                                      offset: Offset(
                                        0.0,
                                        double.parse(value.toString()) * -5,
                                      ),
                                      child: child,
                                    );
                                  },
                                ),
                              Gap.h1_25,
                              CustomButton(
                                onTap: () async {
                                  if (isChecked &&
                                      isNameValid &&
                                      isEmailValid &&
                                      isPasswordValid) {
                                    _singUpBloc.add(
                                      SingUpEvent.register(
                                        _emailTextController.text,
                                        _nameTextController.text,
                                        _passwordTextController.text,
                                        BlocProvider.of<MessageBloc>(
                                          context,
                                        ),
                                      ),
                                    );
                                  }
                                },
                                child: Text(
                                  AppLocalizations.of(context)!.registration,
                                  style: TextStyle(
                                    fontSize: 18,
                                    color: isChecked &&
                                            isNameValid &&
                                            isEmailValid &&
                                            isPasswordValid
                                        ? Colors.white
                                        : AppColors.darkGreyTextColor,
                                    fontFeatures: const [
                                      FontFeature.proportionalFigures()
                                    ],
                                  ),
                                ),
                                width: double.infinity,
                                backgroundColor: isChecked &&
                                        isNameValid &&
                                        isEmailValid &&
                                        isPasswordValid
                                    ? AppColors.buttonColor
                                    : AppColors.veryDarkGray,
                              ),
                              Gap.h2,
                            ],
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                    text: AppLocalizations.of(context)!
                                        .alreadyRegistered,
                                    style: TextStyle(
                                      color: AppColors.darkGreyTextColor,
                                    )),
                                TextSpan(
                                  text: AppLocalizations.of(context)!.enter,
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () async {
                                      Navigator.of(context).pop();

                                      await showDialog<String>(
                                        barrierDismissible: false,
                                        barrierColor:
                                            Colors.black.withOpacity(0.6),
                                        context: context,
                                        builder: (BuildContext context) =>
                                            SingInPage(),
                                      );
                                    },
                                  style: TextStyle(
                                    color: Colors.white,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                right: 12,
                top: 12,
                child: IconButton(
                  onPressed: () => Navigator.of(context).pop(),
                  icon: Icon(
                    Icons.close,
                    color: AppColors.greyText,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
