import 'dart:ui';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../../../core/ui/ui_kit.dart';

class MobileEmailConfirmationPage extends StatelessWidget {
  const MobileEmailConfirmationPage({Key? key, required this.email})
      : super(key: key);
  final String email;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: const EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: MobileEmailConfirmationPopup(email: email),
    );
  }
}

class MobileEmailConfirmationPopup extends StatefulWidget {
  const MobileEmailConfirmationPopup({Key? key, required this.email})
      : super(key: key);
  final String email;

  static const EdgeInsets insets = EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0);

  static const double singInFormFieldHeight = 150;
  static const double singInFormFieldWidth = 556;

  @override
  State<MobileEmailConfirmationPopup> createState() =>
      _MobileEmailConfirmationPopupState();
}

class _MobileEmailConfirmationPopupState
    extends State<MobileEmailConfirmationPopup> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: MobileEmailConfirmationPopup.insets,
            width: MobileEmailConfirmationPopup.singInFormFieldWidth,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight:
                      MobileEmailConfirmationPopup.singInFormFieldHeight),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context)!.thanskForSingUp,
                          textAlign: TextAlign.center,
                          maxLines: 4,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Gap.h,
                  Text(
                    AppLocalizations.of(context)!
                        .sendForConfirmation(widget.email),
                    textAlign: TextAlign.center,
                    maxLines: 4,
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      fontFeatures: const [FontFeature.proportionalFigures()],
                    ),
                  ),
                  Gap.h,
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomButton(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.sendAgain,
                          style: TextStyle(
                            fontSize: 18,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        width: double.infinity,
                        backgroundColor: AppColors.veryDarkGray,
                      ),
                      Gap.h,
                      CancelButton(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.back,
                          style: TextStyle(
                            fontSize: 18,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        width: double.infinity,
                        backgroundColor: AppColors.buttonColor,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
