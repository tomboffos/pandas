import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

import 'widget/desktop_and_tablet_email_confirmation_page.dart';
import 'widget/mobile_email_confirmation_page.dart';

class EmailConfirmationPage extends StatefulWidget {
  const EmailConfirmationPage({Key? key, required this.email})
      : super(key: key);
  final String email;

  @override
  State<EmailConfirmationPage> createState() => _EmailConfirmationPageState();
}

class _EmailConfirmationPageState extends State<EmailConfirmationPage> {
  @override
  Widget build(BuildContext context) {
    return PointerInterceptor(
      child: LayoutHandler(
        mobileView: MobileEmailConfirmationPage(email: widget.email),
        desktopView: DesktopAndTabletEmailConfirmationPage(email: widget.email),
        tabletView: DesktopAndTabletEmailConfirmationPage(email: widget.email),
      ),
    );
  }
}
