import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/sing_in/bloc/sing_in_bloc.dart';
import 'package:nine_pandas/domain/sing_up/bloc/sing_up_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

import 'desktop_and_tablet_sing_in_page.dart';
import 'mobile_sing_in_page.dart';

class SingInPage extends StatefulWidget {
  const SingInPage({Key? key}) : super(key: key);

  @override
  State<SingInPage> createState() => _SingInPageState();
}

class _SingInPageState extends State<SingInPage> {
  late final SingInBloc _singInBloc;

  @override
  void initState() {
    super.initState();

    _singInBloc = getIt.get<SingInBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return PointerInterceptor(
      child: BlocProvider.value(
        value: _singInBloc,
        child: LayoutHandler(
          mobileView: MobileSingInPage(),
          desktopView: DesktopAndTabletSingInPage(),
          tabletView: DesktopAndTabletSingInPage(),
        ),
      ),
    );
  }
}
