import 'dart:ui';

import 'package:nine_pandas/core/utils/string_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/domain/message/bloc/message_bloc.dart';
import 'package:nine_pandas/domain/sing_in/bloc/sing_in_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/bloc/sing_in_recovery_bloc/bloc/sing_in_recovery_bloc_bloc.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_up/sing_up_page.dart';
import 'package:nine_pandas/presentation/forgot_password/recovery_password_popup/recovery_password_popup.dart';

import '../../../../../core/ui/ui_kit.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DesktopAndTabletSingInPage extends StatelessWidget {
  const DesktopAndTabletSingInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => SingInRecoveryBloc(),
      child: AlertDialog(
        contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Dimens.cornerRadius),
        ),
        backgroundColor: AppColors.carcassBackground,
        content: BlocBuilder<SingInRecoveryBloc, SingInRecoveryState>(
          builder: (context, state) {
            if (state is RecoveryFieldOpened) {
              return RecoveryPasswordPopoup();
            } else {
              return DesktopAndTabletSingInFormField();
            }
          },
        ),
      ),
    );
  }
}

class DesktopAndTabletSingInFormField extends StatefulWidget {
  const DesktopAndTabletSingInFormField({Key? key}) : super(key: key);

  static const EdgeInsets insets = EdgeInsets.fromLTRB(52.0, 40.0, 52.0, 52.0);

  static const double singInFormFieldHeight = 400;
  static const double singInFormFieldWidth = 556;

  @override
  State<DesktopAndTabletSingInFormField> createState() =>
      _DesktopAndTabletSingInFormFieldState();
}

class _DesktopAndTabletSingInFormFieldState
    extends State<DesktopAndTabletSingInFormField> {
  bool isObscured = true;
  bool isEmailError = false;
  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool isPasswordError = false;
  bool isUserNotVerified = false;

  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();

  late final SingInBloc _singInBloc;

  @override
  void initState() {
    super.initState();
    _singInBloc = BlocProvider.of<SingInBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          BlocListener<UserBloc, UserState>(
            listener: (context, state) {
              state.whenOrNull(
                userNotVerified: () {
                  setState(() {
                    isEmailError = true;
                    isUserNotVerified = true;
                  });
                },
                logged: (user) => Navigator.of(context).pop(),
              );
            },
            child: BlocConsumer<SingInBloc, SingInState>(
              listener: (context, state) {
                state.maybeWhen(
                    error: (error) {
                      isEmailError = true;
                      isPasswordError = true;
                    },
                    orElse: () => Container());
              },
              builder: (context, state) {
                return Container(
                  padding: DesktopAndTabletSingInFormField.insets,
                  width: DesktopAndTabletSingInFormField.singInFormFieldWidth,
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: DesktopAndTabletSingInFormField
                            .singInFormFieldHeight),
                    child: state.maybeMap(
                      loading: (value) => Center(
                        child: CircularProgressIndicator(),
                      ),
                      loginSuccess: (value) => Center(
                        child: CircularProgressIndicator(),
                      ),
                      orElse: () => Form(
                        key: _formKey,
                        autovalidateMode: AutovalidateMode.always,
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Text(
                                    AppLocalizations.of(context)!.enterAccount,
                                    textAlign: TextAlign.center,
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                      fontFeatures: const [
                                        FontFeature.proportionalFigures()
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Gap.h2,
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  AppLocalizations.of(context)!.email,
                                  style: TextStyle(
                                    color: AppColors.greyTextColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    fontFeatures: const [
                                      FontFeature.proportionalFigures()
                                    ],
                                  ),
                                ),
                                Gap.h_5,
                                PandasBaseTextInput(
                                  isError: isEmailError,
                                  controller: _emailTextController,
                                  hintText: AppLocalizations.of(context)!.email,
                                  onFieldSubmitted: (_) {
                                    FocusScope.of(context).nextFocus();
                                  },
                                  onChanged: (value) {
                                    if (isEmailAddress(value)) {
                                      setState(() {
                                        isEmailValid = true;
                                      });
                                    } else {
                                      setState(() {
                                        isEmailValid = false;
                                      });
                                    }
                                  },
                                  tail: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      if (isEmailError)
                                        Image.asset(
                                          AppLocalizations.of(context)!
                                              .errorIcon,
                                          height: Dimens.iconSize,
                                          filterQuality: FilterQuality.high,
                                          width: Dimens.iconSize,
                                        ),
                                      Gap.w_25,
                                      Gap.w_5,
                                    ],
                                  ),
                                ),
                                Gap.h_25,
                                Gap.h,
                                Text(
                                  AppLocalizations.of(context)!.password,
                                  style: TextStyle(
                                    color: AppColors.greyTextColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                    fontFeatures: const [
                                      FontFeature.proportionalFigures()
                                    ],
                                  ),
                                ),
                                Gap.h_5,
                                PandasBaseTextInput(
                                  isError: isPasswordError,
                                  controller: _passwordTextController,
                                  hintText:
                                      AppLocalizations.of(context)!.password,
                                  isObscured: isObscured,
                                  onFieldSubmitted: (_) {
                                    FocusScope.of(context).nextFocus();
                                  },
                                  onChanged: (value) {
                                    if (value.length >= 6) {
                                      setState(() {
                                        isPasswordValid = true;
                                      });
                                    } else {
                                      setState(() {
                                        isPasswordValid = false;
                                      });
                                    }
                                  },
                                  tail: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      if (isPasswordError)
                                        Image.asset(
                                          AppLocalizations.of(context)!
                                              .errorIcon,
                                          height: Dimens.iconSize,
                                          filterQuality: FilterQuality.high,
                                          width: Dimens.iconSize,
                                        ),
                                      IconButton(
                                        icon: Icon(
                                          isObscured
                                              ? Icons.visibility_outlined
                                              : Icons.visibility_off_outlined,
                                          color: Colors.white,
                                          size: Dimens.iconSize * 1.2,
                                        ),
                                        onPressed: () => setState(() {
                                          isObscured = !isObscured;
                                        }),
                                      ),
                                    ],
                                  ),
                                ),
                                Gap.h1_25,
                                if (isEmailError && isPasswordError)
                                  TweenAnimationBuilder(
                                    duration: Duration(milliseconds: 200),
                                    curve: Curves.ease,
                                    tween: Tween(begin: 1.0, end: 0.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          AppLocalizations.of(context)!
                                              .invalidEmailOrPassword,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 16,
                                            color: AppColors.error,
                                            fontFeatures: const [
                                              FontFeature.proportionalFigures()
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    builder: (context, value, child) {
                                      return Transform.translate(
                                        offset: Offset(
                                          0.0,
                                          double.parse(value.toString()) * -5,
                                        ),
                                        child: child,
                                      );
                                    },
                                  ),
                                Gap.h1_25,
                                if (isUserNotVerified)
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        AppLocalizations.of(context)!
                                            .userNotVerified,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: AppColors.error,
                                          fontFeatures: const [
                                            FontFeature.proportionalFigures()
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                Gap.h1_25,
                                CustomButton(
                                  onTap: () {
                                    if (isEmailValid && isPasswordValid) {
                                      _singInBloc.add(
                                        SingInEvent.login(
                                          _emailTextController.text,
                                          _passwordTextController.text,
                                          BlocProvider.of<MessageBloc>(context),
                                        ),
                                      );
                                    }
                                  },
                                  child: Text(
                                    AppLocalizations.of(context)!.next,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: isEmailValid && isPasswordValid
                                          ? Colors.white
                                          : AppColors.darkGreyTextColor,
                                      fontFeatures: const [
                                        FontFeature.proportionalFigures()
                                      ],
                                    ),
                                  ),
                                  width: double.infinity,
                                  backgroundColor:
                                      isEmailValid && isPasswordValid
                                          ? AppColors.buttonColor
                                          : AppColors.veryDarkGray,
                                ),
                                Gap.h2,
                              ],
                            ),
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: AppLocalizations.of(context)!
                                        .forgotPassword,
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () async {
                                        context
                                            .read<SingInRecoveryBloc>()
                                            .add(OpenRecoveryFieldEvent());
                                      },
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: AppColors.pressedTextColor,
                                      decoration: TextDecoration.underline,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Gap.h1_25,
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(
                                    text: AppLocalizations.of(context)!
                                        .dontHaveAccount,
                                    style: TextStyle(
                                      color: AppColors.darkGreyTextColor,
                                    ),
                                  ),
                                  TextSpan(
                                    text: AppLocalizations.of(context)!
                                        .registration,
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () async {
                                        Navigator.of(context).pop();
                                        await showDialog<String>(
                                          barrierDismissible: false,
                                          barrierColor:
                                              Colors.black.withOpacity(0.6),
                                          context: context,
                                          builder: (BuildContext context) =>
                                              SingUpPage(),
                                        );
                                      },
                                    style: TextStyle(
                                      color: AppColors.pressedTextColor,
                                      decoration: TextDecoration.underline,
                                      fontSize: 14,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
