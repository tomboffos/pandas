import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'sing_in_recovery_bloc_event.dart';
part 'sing_in_recovery_bloc_state.dart';

class SingInRecoveryBloc
    extends Bloc<SingInRecoveryEvent, SingInRecoveryState> {
  SingInRecoveryBloc() : super(SingInFieldOpened()) {
    on<SingInRecoveryEvent>((event, emit) {
      if (event is OpenRecoveryFieldEvent) {
        emit(RecoveryFieldOpened());
      }
      if (event is OpenSingInFieldEvent) {
        emit(SingInFieldOpened());
      }
    });
  }
}
