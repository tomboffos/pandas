part of 'sing_in_recovery_bloc_bloc.dart';

abstract class SingInRecoveryState extends Equatable {
  const SingInRecoveryState();

  @override
  List<Object> get props => [];
}

class SingInFieldOpened extends SingInRecoveryState {}

class RecoveryFieldOpened extends SingInRecoveryState {}
