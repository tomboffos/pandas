part of 'sing_in_recovery_bloc_bloc.dart';

abstract class SingInRecoveryEvent extends Equatable {
  const SingInRecoveryEvent();

  @override
  List<Object> get props => [];
}

class OpenSingInFieldEvent extends SingInRecoveryEvent {}

class OpenRecoveryFieldEvent extends SingInRecoveryEvent {}
