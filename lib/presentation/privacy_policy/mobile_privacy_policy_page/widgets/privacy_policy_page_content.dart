import '../../../../../../core/ui/ui_kit.dart';
import 'privacy_policy.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PrivacyPolicyPageContent extends StatelessWidget {
  final String htmlData;

  const PrivacyPolicyPageContent({Key? key, required this.htmlData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(minHeight: MediaQuery.of(context).size.height),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Gap.h1_5,
          NavigationRow(
            currentPageText: AppLocalizations.of(context)!.privacyPolicy,
          ),
          Gap.h1_5,
          PrivacyPolicyWidget(htmlData: htmlData),
        ],
      ),
    );
  }
}
