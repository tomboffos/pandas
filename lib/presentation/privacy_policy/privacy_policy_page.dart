import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/data/html_reading_page/models/html_reading.dart';
import 'package:nine_pandas/domain/html_reading_page/bloc/html_reader_page_bloc.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import 'desktop_privacy_policy_page/desktop_privacy_policy_page.dart';
import 'mobile_privacy_policy_page/mobile_privacy_policy_page.dart';
import 'tablet_privacy_policy_page/tablet_privacy_policy_page.dart';

class PrivacyPolicyPage extends StatefulWidget {
  const PrivacyPolicyPage({Key? key}) : super(key: key);

  @override
  State<PrivacyPolicyPage> createState() => PrivacyPolicyPageState();
}

class PrivacyPolicyPageState extends State<PrivacyPolicyPage> {
  bool isBlackout = false;
  late final HtmlReadingPageBloc _htmlReadingPageBloc =
      getIt.get<HtmlReadingPageBloc>();

  @override
  void initState() {
    _htmlReadingPageBloc.add(HtmlReadingPageEvent.fetchData(
      HtmlDocType.privacy,
    ));
    super.initState();
  }

  @override
  void dispose() {
    _htmlReadingPageBloc.close();
    super.dispose();
  }

  void isBlackoutChange(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: isBlackoutChange,
      child: BlocListener<LocalizationBloc, LocalizationState>(
        listener: (context, state) {
          _htmlReadingPageBloc
              .add(HtmlReadingPageEvent.fetchData(HtmlDocType.privacy));
        },
        child: BlocProvider.value(
          value: _htmlReadingPageBloc,
          child: LayoutHandler(
            mobileView: MobilePrivacyPolicyPage(),
            desktopView: DesktopPrivacyPolicyPage(),
            tabletView: TabletPrivacyPolicyPage(),
          ),
        ),
      ),
    );
  }
}
