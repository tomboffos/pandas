import 'package:universal_io/io.dart';
import 'dart:ui' as ui;

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/domain/age_restriction/bloc/age_restriction_bloc.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/injectable.dart';

import '../../../../../../core/ui/ui_kit.dart';

class MobileAgeRestrictionPage extends StatelessWidget {
  const MobileAgeRestrictionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: const EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: MobileAgeRestrictionPopup(),
    );
  }
}

class MobileAgeRestrictionPopup extends StatefulWidget {
  const MobileAgeRestrictionPopup({Key? key}) : super(key: key);

  static const EdgeInsets insets = EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0);

  static const double singInFormFieldHeight = 250;
  static const double singInFormFieldWidth = 556;

  @override
  State<MobileAgeRestrictionPopup> createState() =>
      _MobileAgeRestrictionPopupState();
}

class _MobileAgeRestrictionPopupState extends State<MobileAgeRestrictionPopup> {
  final AgeRestrictionBloc _ageRestrictionBloc =
      getIt.get<AgeRestrictionBloc>();

  bool isRuContent = Platform.localeName.split('-')[0] == 'ru' ? true : false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: MobileAgeRestrictionPopup.insets,
            width: MobileAgeRestrictionPopup.singInFormFieldWidth,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: MobileAgeRestrictionPopup.singInFormFieldHeight),
              child: Column(
                children: [
                  Image.asset(
                    AppLocalizations.of(context)!.ageRestrictionIcon,
                    height: Dimens.iconSize * 4,
                    width: Dimens.iconSize * 4.5,
                  ),
                  Gap.h,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context)!.ageRestriction,
                          textAlign: TextAlign.center,
                          maxLines: 4,
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            fontFeatures: const [
                              ui.FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Gap.h2,
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomButton(
                        onTap: () {
                          _ageRestrictionBloc
                              .add(AgeRestrictionSaveConfirmationEvent(true));
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.proofOfAge,
                          style: TextStyle(
                            fontSize: 18,
                            fontFeatures: const [
                              ui.FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        width: double.infinity,
                        backgroundColor: AppColors.buttonColor,
                      ),
                      Gap.h_5,
                      CancelButton(
                        onTap: () {
                          // window.close();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.leaveSite,
                          style: TextStyle(
                            fontSize: 18,
                            fontFeatures: const [
                              ui.FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        width: double.infinity,
                        backgroundColor: AppColors.veryDarkGray,
                      ),
                      Gap.h2,
                      Text(
                        AppLocalizations.of(context)!
                            .warningAboutResponsibility,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: AppColors.darkGreyTextColor,
                          fontSize: 12,
                          fontFeatures: const [
                            ui.FontFeature.proportionalFigures()
                          ],
                        ),
                      ),
                      Gap.h1_5,
                      InkWell(
                        onTap: () {
                          getIt.get<LocalizationBloc>().add(
                                LocalizationEvent.changeLanguage(
                                    isRuContent ? 'En' : 'Ru'),
                              );
                          isRuContent = !isRuContent;
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .changeLanguageAgeRestriction,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.greyTextColor,
                            decoration: TextDecoration.underline,
                            fontFeatures: const [
                              ui.FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
