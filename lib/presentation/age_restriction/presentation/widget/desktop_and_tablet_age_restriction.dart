import 'dart:ui';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/domain/age_restriction/bloc/age_restriction_bloc.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:universal_io/io.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../../core/ui/ui_kit.dart';

class DesktopAndTabletAgeRestrictionPage extends StatelessWidget {
  DesktopAndTabletAgeRestrictionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: DesktopAndTabletAgeRestrictionPopup(),
    );
  }
}

class DesktopAndTabletAgeRestrictionPopup extends StatefulWidget {
  DesktopAndTabletAgeRestrictionPopup({Key? key}) : super(key: key);

  static const EdgeInsets insets = EdgeInsets.fromLTRB(52.0, 40.0, 52.0, 40.0);

  static const double singInFormFieldHeight = 250;
  static const double singInFormFieldWidth = 556;

  @override
  State<DesktopAndTabletAgeRestrictionPopup> createState() =>
      _DesktopAndTabletAgeRestrictionPopupState();
}

class _DesktopAndTabletAgeRestrictionPopupState
    extends State<DesktopAndTabletAgeRestrictionPopup> {
  final AgeRestrictionBloc _ageRestrictionBloc =
      getIt.get<AgeRestrictionBloc>();

  bool isRuContent = Platform.localeName.split('-')[0] == 'ru' ? true : false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: DesktopAndTabletAgeRestrictionPopup.insets,
            width: DesktopAndTabletAgeRestrictionPopup.singInFormFieldWidth,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: DesktopAndTabletAgeRestrictionPopup
                      .singInFormFieldHeight),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Gap.h2,
                  Image.asset(
                    AppLocalizations.of(context)!.ageRestrictionIcon,
                    height: Dimens.iconSize * 4,
                    width: Dimens.iconSize * 4.5,
                  ),
                  Gap.h,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context)!.ageRestriction,
                          textAlign: TextAlign.center,
                          maxLines: 4,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Gap.h2,
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomButton(
                        onTap: () {
                          _ageRestrictionBloc
                              .add(AgeRestrictionSaveConfirmationEvent(true));
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.proofOfAge,
                          style: TextStyle(
                            fontSize: 18,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        width: double.infinity,
                        backgroundColor: AppColors.buttonColor,
                      ),
                      Gap.h_5,
                      CancelButton(
                        onTap: () {
                          launchUrl(
                            Uri.parse('https://frrfrr.ru'),
                          );
                        },
                        child: Text(
                          AppLocalizations.of(context)!.leaveSite,
                          style: TextStyle(
                            fontSize: 18,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        width: double.infinity,
                        backgroundColor: AppColors.veryDarkGray,
                      ),
                      Gap.h2,
                      Text(
                        AppLocalizations.of(context)!
                            .warningAboutResponsibility,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: AppColors.darkGreyTextColor,
                          fontSize: 12,
                          fontFeatures: const [
                            FontFeature.proportionalFigures()
                          ],
                        ),
                      ),
                      Gap.h1_5,
                      InkWell(
                        onTap: () {
                          getIt.get<LocalizationBloc>().add(
                                LocalizationEvent.changeLanguage(
                                    isRuContent ? 'En' : 'Ru'),
                              );
                          isRuContent = !isRuContent;
                        },
                        child: Text(
                          AppLocalizations.of(context)!
                              .changeLanguageAgeRestriction,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: AppColors.greyTextColor,
                            decoration: TextDecoration.underline,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => launchUrl(
                Uri.parse('https://frrfrr.ru'),
              ),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
