import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

import '../widget/desktop_and_tablet_age_restriction.dart';
import '../widget/mobile_age_restriction.dart';

class AgeRestriction extends StatefulWidget {
  const AgeRestriction({Key? key}) : super(key: key);

  @override
  State<AgeRestriction> createState() => _AgeRestrictionState();
}

class _AgeRestrictionState extends State<AgeRestriction> {
  @override
  Widget build(BuildContext context) {
    return PointerInterceptor(
      child: LayoutHandler(
        mobileView: MobileAgeRestrictionPage(),
        desktopView: DesktopAndTabletAgeRestrictionPage(),
        tabletView: DesktopAndTabletAgeRestrictionPage(),
      ),
    );
  }
}
