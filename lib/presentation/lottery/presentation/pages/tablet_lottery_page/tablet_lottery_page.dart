import 'dart:ui';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../../core/ui/ui_kit.dart';

class TabletLotteryPage extends StatelessWidget {
  const TabletLotteryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: TabletLotteryPopup(),
    );
  }
}

class TabletLotteryPopup extends StatefulWidget {
  const TabletLotteryPopup({Key? key}) : super(key: key);

  static const EdgeInsets insets = EdgeInsets.fromLTRB(41.0, 40.0, 41.0, 52.0);

  static const double singInFormFieldHeight = 400;
  static const double singInFormFieldWidth = 556;

  @override
  State<TabletLotteryPopup> createState() => _TabletLotteryPopupState();
}

class _TabletLotteryPopupState extends State<TabletLotteryPopup> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: TabletLotteryPopup.insets,
            width: TabletLotteryPopup.singInFormFieldWidth,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: TabletLotteryPopup.singInFormFieldHeight),
              child: Column(
                children: [
                  Gap.h3,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context)!.thanksForSupportProject,
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Gap.h1_5,
                  Card(
                    color: Colors.white.withOpacity(0.1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(Dimens.cornerRadius),
                    ),
                    child: Padding(
                      padding: Insets.a_75,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              AppLocalizations.of(context)!.lotteryiPhone,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Gap.h_5,
                            Text(
                              AppLocalizations.of(context)!.likeLastTime,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Gap.h_5,
                            Text(
                              AppLocalizations.of(context)!
                                  .leftCommentTakePlaceComment,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontSize: 14,
                                letterSpacing: 0.02,
                                height: 1.3,
                              ),
                            ),
                            Gap.h_75,
                            Text(
                              AppLocalizations.of(context)!.lotterySeveniPhones,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontSize: 14,
                                letterSpacing: 0.02,
                                height: 1.3,
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Gap.h1_5,
                  CustomButton(
                    onTap: () {},
                    child: Text(
                      AppLocalizations.of(context)!.payRussianCard,
                      style: TextStyle(
                        fontSize: 18,
                        color: AppColors.veryLightGray,
                        fontFeatures: const [FontFeature.proportionalFigures()],
                      ),
                    ),
                    width: 323,
                    backgroundColor: AppColors.buttonColor,
                  ),
                  Gap.h_5,
                  CustomButton(
                    onTap: () {},
                    child: Text(
                      AppLocalizations.of(context)!.payForeignCard,
                      style: TextStyle(
                        fontSize: 18,
                        color: AppColors.greyTextColor,
                        fontFeatures: const [FontFeature.proportionalFigures()],
                      ),
                    ),
                    width: 323,
                    backgroundColor: Colors.transparent,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
