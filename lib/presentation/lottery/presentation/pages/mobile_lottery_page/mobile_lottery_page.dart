import 'dart:ui';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../../core/ui/ui_kit.dart';

class MobileLotteryPage extends StatelessWidget {
  const MobileLotteryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: const EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: MobileLotteryPopup(),
    );
  }
}

class MobileLotteryPopup extends StatefulWidget {
  const MobileLotteryPopup({Key? key}) : super(key: key);

  static const EdgeInsets insets =
      const EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0);

  static const double singInFormFieldHeight = 400;
  static const double singInFormFieldWidth = 556;

  @override
  State<MobileLotteryPopup> createState() => _MobileLotteryPopupState();
}

class _MobileLotteryPopupState extends State<MobileLotteryPopup> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: MobileLotteryPopup.insets,
            width: MobileLotteryPopup.singInFormFieldWidth,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: MobileLotteryPopup.singInFormFieldHeight),
              child: Column(
                children: [
                  Gap.h2,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context)!.thanksForSupportProject,
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            height: 26 / 20,
                            letterSpacing: 0.02,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Gap.h1_5,
                  Card(
                    color: Colors.white.withOpacity(0.1),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(Dimens.cornerRadius),
                    ),
                    child: Padding(
                      padding: Insets.a_75,
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              AppLocalizations.of(context)!.lotteryiPhone,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontSize: 16,
                                height: 20 / 16,
                                letterSpacing: 0.02,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Gap.h_5,
                            Text(
                              AppLocalizations.of(context)!.likeLastTime,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontSize: 14,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Gap.h_5,
                            Text(
                              AppLocalizations.of(context)!
                                  .leftCommentTakePlaceComment,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                letterSpacing: 0.02,
                                height: 1.3,
                              ),
                            ),
                            Gap.h_75,
                            Text(
                              AppLocalizations.of(context)!.lotterySeveniPhones,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                letterSpacing: 0.02,
                                height: 1.3,
                              ),
                            ),
                          ]),
                    ),
                  ),
                  Gap.h1_5,
                  CustomButton(
                    onTap: () {},
                    child: Text(
                      AppLocalizations.of(context)!.payRussianCard,
                      style: TextStyle(
                        fontSize: 18,
                        color: AppColors.veryLightGray,
                        fontFeatures: const [FontFeature.proportionalFigures()],
                      ),
                    ),
                    width: 323,
                    backgroundColor: AppColors.buttonColor,
                  ),
                  Gap.h_5,
                  CustomButton(
                    onTap: () {},
                    child: Text(
                      AppLocalizations.of(context)!.payForeignCard,
                      style: TextStyle(
                        fontSize: 18,
                        color: AppColors.greyTextColor,
                        fontFeatures: const [FontFeature.proportionalFigures()],
                      ),
                    ),
                    width: 323,
                    backgroundColor: Colors.transparent,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
