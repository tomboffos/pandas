import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

import '../../../../core/ui/ui_kit.dart';
import 'desktop_lottery_page/desktop_lottery_page.dart';
import 'mobile_lottery_page/mobile_lottery_page.dart';
import 'tablet_lottery_page/tablet_lottery_page.dart';

class LotteryPage extends StatefulWidget {
  const LotteryPage({Key? key}) : super(key: key);

  @override
  State<LotteryPage> createState() => _LotteryPageState();
}

class _LotteryPageState extends State<LotteryPage> {
  bool isBlackout = false;

  void setBlackoutValue(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: setBlackoutValue,
      child: PointerInterceptor(
        child: LayoutHandler(
          mobileView: MobileLotteryPage(),
          desktopView: DesktopLotteryPage(),
          tabletView: TabletLotteryPage(),
        ),
      ),
    );
  }
}
