import 'package:nine_pandas/core/ui/ui_kit.dart';

import 'desktop_and_tablet_email_confirmation_after_editing.dart';

class EmailConfirmationAfterEditingPage extends StatefulWidget {
  const EmailConfirmationAfterEditingPage({Key? key, required this.email})
      : super(key: key);
  final String email;

  @override
  State<EmailConfirmationAfterEditingPage> createState() =>
      _EmailConfirmationAfterEditingPageState();
}

class _EmailConfirmationAfterEditingPageState
    extends State<EmailConfirmationAfterEditingPage> {
  @override
  Widget build(BuildContext context) {
    return LayoutHandler(
      mobileView: DesktopAndTabletEmailConfirmationAfterEditingPage(
          email: widget.email),
      desktopView: DesktopAndTabletEmailConfirmationAfterEditingPage(
          email: widget.email),
      tabletView: DesktopAndTabletEmailConfirmationAfterEditingPage(
          email: widget.email),
    );
  }
}
