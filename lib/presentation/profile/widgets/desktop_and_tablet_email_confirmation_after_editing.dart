import 'dart:ui';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../../../core/ui/ui_kit.dart';

class DesktopAndTabletEmailConfirmationAfterEditingPage
    extends StatelessWidget {
  const DesktopAndTabletEmailConfirmationAfterEditingPage(
      {Key? key, required this.email})
      : super(key: key);
  final String email;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: DesktopAndTabletEmailConfirmationAfterEditingPopup(email: email),
    );
  }
}

class DesktopAndTabletEmailConfirmationAfterEditingPopup
    extends StatefulWidget {
  const DesktopAndTabletEmailConfirmationAfterEditingPopup(
      {Key? key, required this.email})
      : super(key: key);
  final String email;

  static const EdgeInsets insets = EdgeInsets.fromLTRB(52.0, 40.0, 52.0, 52.0);

  static const double singInFormFieldHeight = 150;
  static const double singInFormFieldWidth = 556;

  @override
  State<DesktopAndTabletEmailConfirmationAfterEditingPopup> createState() =>
      _DesktopAndTabletEmailConfirmationAfterEditingPopupState();
}

class _DesktopAndTabletEmailConfirmationAfterEditingPopupState
    extends State<DesktopAndTabletEmailConfirmationAfterEditingPopup> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: DesktopAndTabletEmailConfirmationAfterEditingPopup.insets,
            width: DesktopAndTabletEmailConfirmationAfterEditingPopup
                .singInFormFieldWidth,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: DesktopAndTabletEmailConfirmationAfterEditingPopup
                      .singInFormFieldHeight),
              child: Column(
                children: [
                  Gap.h2,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          AppLocalizations.of(context)!.youHaveChangedEmail,
                          textAlign: TextAlign.center,
                          maxLines: 4,
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w500,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  Gap.h,
                  Text(
                    AppLocalizations.of(context)!
                        .sendChangedEmailForConfirmation(widget.email),
                    textAlign: TextAlign.center,
                    maxLines: 4,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      fontFeatures: const [FontFeature.proportionalFigures()],
                    ),
                  ),
                  Gap.h2,
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomButton(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          AppLocalizations.of(context)!.sendAgain,
                          style: TextStyle(
                            fontSize: 18,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        width: double.infinity,
                        backgroundColor: AppColors.buttonColor,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
