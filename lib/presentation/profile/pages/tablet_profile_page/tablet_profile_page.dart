import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import '../../../../../core/ui/ui_kit.dart';
import 'widgets/profile_page_content.dart';

class TabletProfilePage extends StatefulWidget {
  const TabletProfilePage({Key? key}) : super(key: key);

  @override
  State<TabletProfilePage> createState() => _TabletProfilePageState();
}

class _TabletProfilePageState extends State<TabletProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: Column(
        children: [
          NavigationHeader(),
          Expanded(
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      Gap.h1_5,
                      ProfilePageContent(),
                      Gap.h2,
                      Footer(),
                    ],
                  ),
                ),
                BlackoutContainer(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
