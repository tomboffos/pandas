import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import 'desktop_profile_page/desktop_profile_page.dart';
import 'mobile_profile_page/mobile_profile_page.dart';
import 'tablet_profile_page/tablet_profile_page.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => ProfilePageState();
}

class ProfilePageState extends State<ProfilePage> {
  bool isBlackout = false;

  void isBlackoutChange(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: isBlackoutChange,
      child: LayoutHandler(
        mobileView: MobileProfilePage(),
        desktopView: DesktopProfilePage(),
        tabletView: TabletProfilePage(),
      ),
    );
  }
}
