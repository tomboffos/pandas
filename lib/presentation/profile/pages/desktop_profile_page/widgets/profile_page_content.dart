import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/injectable.dart';

import '../../../../../../core/ui/ui_kit.dart';
import 'user_configs.dart';

class ProfilePageContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Insets.w10,
      child: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              NavigationRow(
                rootPageText: AppLocalizations.of(context)!.personalAccount,
                currentPageText: AppLocalizations.of(context)!.profile,
              ),
              state.maybeWhen(
                logged: (user) => UserConfigs(
                  oldEmail: user.username,
                  oldName: user.name,
                  oldPhone: user.phone,
                  oldBirthdate: user.birthdate?.toLocal().toString() ?? null,
                  gender: user.gender,
                  emailNotification: user.emailNotification,
                  smsNotification: user.smsNotification,
                ),
                orElse: () => Container(
                    height: MediaQuery.of(context).size.height,
                    child: PandasLoadingView()),
                error: (e) => PandasServerErrorWidget(
                    reloadCallback: () =>
                        getIt.get<UserBloc>().add(UserEvent.authorize())),
              ),
            ],
          );
        },
      ),
    );
  }
}
