import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nine_pandas/core/ui/formatters/birthdate_formatter.dart';
import 'package:nine_pandas/core/ui/formatters/number_formatter.dart';
import 'package:nine_pandas/core/utils/string_helper.dart';
import 'package:nine_pandas/domain/message/bloc/message_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/presentation/change_password/change_password_popup.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import '../../../../../../core/ui/ui_kit.dart';

class UserConfigs extends StatefulWidget {
  final String? oldEmail;
  final String? oldName;
  final String? oldPhone;
  final String? oldBirthdate;
  final String? gender;
  final bool smsNotification;
  final bool emailNotification;
  UserConfigs({
    Key? key,
    required this.oldBirthdate,
    this.oldEmail,
    this.oldName,
    this.oldPhone,
    this.gender,
    required this.smsNotification,
    required this.emailNotification,
  }) : super(key: key);

  static const double? userConfigsFieldWidth = 510;

  @override
  State<UserConfigs> createState() => _UserConfigsState();
}

class _UserConfigsState extends State<UserConfigs> {
  bool isNameValid = false;
  bool isPhoneValid = false;
  bool isBirthdateValid = false;
  bool isPreviousMan = false;
  bool isPreviousWoman = false;
  bool isMan = false;
  bool isWoman = false;
  bool isMail = false;
  bool isSMS = false;
  bool isNameReadOnly = true;
  bool isPasswordReadOnly = true;
  bool isBirthdateReadOnly = true;

  final _formKey = GlobalKey<FormState>();
  TextEditingController? _nameTextController;
  TextEditingController? _emailTextController;
  TextEditingController? _phoneTextController;
  TextEditingController? _birthdateTextController;
  String? formattedBirthdate;

  @override
  void initState() {
    super.initState();
    if (widget.oldBirthdate != null)
      formattedBirthdate =
          widget.oldBirthdate!.substring(0, 10).split('-').reversed.join('.');
    _nameTextController = TextEditingController(text: widget.oldName);
    _emailTextController = TextEditingController(text: widget.oldEmail);
    _phoneTextController = TextEditingController(text: widget.oldPhone);
    _birthdateTextController =
        TextEditingController(text: formattedBirthdate ?? '');

    isMail = widget.emailNotification;
    isSMS = widget.smsNotification;

    if (widget.gender != null) isPreviousMan = isMan = widget.gender == 'male';
    if (widget.gender != null)
      isPreviousWoman = isWoman = widget.gender == 'female';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: UserConfigs.userConfigsFieldWidth,
      child: Column(
        children: [
          Column(
            children: [
              Gap.h1_5,
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Text(
                      AppLocalizations.of(context)!.anAccount,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      style: TextStyle(
                        fontSize: 28,
                        fontWeight: FontWeight.w500,
                        fontFeatures: const [FontFeature.proportionalFigures()],
                      ),
                    ),
                  ),
                ],
              ),
              Form(
                key: _formKey,
                autovalidateMode: AutovalidateMode.always,
                child: Column(
                  children: [
                    Gap.h1_5,
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppLocalizations.of(context)!.name,
                          style: TextStyle(
                            color: AppColors.greyTextColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Gap.h_5,
                        PandasBaseTextInput(
                          controller: _nameTextController,
                          hintText: AppLocalizations.of(context)!.name,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context).nextFocus();
                          },
                          readOnly: isNameReadOnly,
                          onChanged: (value) {
                            if (value.length >= 3 && value != widget.oldName) {
                              setState(() {
                                isNameValid = true;
                              });
                            } else {
                              setState(() {
                                isNameValid = false;
                              });
                            }
                          },
                          tail: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    isNameReadOnly = !isNameReadOnly;
                                  });
                                },
                                child: SvgPicture.asset(
                                  AppLocalizations.of(context)!.editIcon,
                                  color: Colors.white,
                                  height: Dimens.iconSize,
                                ),
                              ),
                              Gap.w_5,
                            ],
                          ),
                        ),
                        Gap.h_75,
                        Text(
                          AppLocalizations.of(context)!.email,
                          style: TextStyle(
                            color: AppColors.greyTextColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Gap.h_5,
                        PandasBaseTextInput(
                          controller: _emailTextController,
                          hintText: AppLocalizations.of(context)!.email,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context).nextFocus();
                          },
                          readOnly: true,
                        ),
                        Gap.h_75,
                        Text(
                          AppLocalizations.of(context)!.phone,
                          style: TextStyle(
                            color: AppColors.greyTextColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Gap.h_5,
                        PandasBaseTextInput(
                          controller: _phoneTextController,
                          hintText: AppLocalizations.of(context)!
                              .enterYourPhoneNumber,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context).nextFocus();
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                            NumberTextInputFormatter(),
                          ],
                          onChanged: (value) {
                            if (value.length >= 6 && value != widget.oldPhone) {
                              setState(() {
                                isPhoneValid = true;
                              });
                            } else {
                              setState(() {
                                isPhoneValid = false;
                              });
                            }
                          },
                          readOnly: isPasswordReadOnly,
                          tail: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    isPasswordReadOnly = !isPasswordReadOnly;
                                  });
                                },
                                child: SvgPicture.asset(
                                  AppLocalizations.of(context)!.editIcon,
                                  color: Colors.white,
                                  height: Dimens.iconSize,
                                ),
                              ),
                              Gap.w_5,
                            ],
                          ),
                        ),
                        Gap.h_75,
                        Text(
                          AppLocalizations.of(context)!.age,
                          style: TextStyle(
                            color: AppColors.greyTextColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        Gap.h_5,
                        PandasBaseTextInput(
                          hintText: AppLocalizations.of(context)!.ageExample,
                          controller: _birthdateTextController,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context).nextFocus();
                          },
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                            BirthdateFormatter(),
                          ],
                          onChanged: (value) {
                            if (isValidDate(value) &&
                                value != widget.oldBirthdate) {
                              setState(() {
                                isBirthdateValid = true;
                              });
                            } else {
                              setState(() {
                                isBirthdateValid = false;
                              });
                            }
                          },
                          readOnly: isBirthdateReadOnly,
                          tail: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              InkWell(
                                onTap: () {
                                  setState(() {
                                    isBirthdateReadOnly = !isBirthdateReadOnly;
                                  });
                                },
                                child: SvgPicture.asset(
                                  AppLocalizations.of(context)!.editIcon,
                                  height: Dimens.iconSize,
                                ),
                              ),
                              Gap.w_5,
                            ],
                          ),
                        ),
                        Gap.h,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              AppLocalizations.of(context)!.yourGender,
                              style: TextStyle(
                                color: AppColors.darkGreyTextColor,
                                fontSize: 16,
                              ),
                            ),
                            Row(
                              children: [
                                PandasCheckbox(
                                    value: isMan,
                                    onChanged: (value) {
                                      setState(() {
                                        isMan = true;
                                        isWoman = false;
                                      });
                                    }),
                                Gap.w_5,
                                Gap.w_25,
                                Text(
                                  AppLocalizations.of(context)!.man,
                                ),
                                Gap.w_5,
                                Gap.w_25,
                                PandasCheckbox(
                                    value: isWoman,
                                    onChanged: (value) {
                                      setState(() {
                                        isWoman = true;
                                        isMan = false;
                                      });
                                    }),
                                Gap.w_5,
                                Gap.w_25,
                                Text(
                                  AppLocalizations.of(context)!.woman,
                                )
                              ],
                            ),
                          ],
                        ),
                        Gap.h1_25,
                      ],
                    ),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: Text(
                                AppLocalizations.of(context)!.notifications,
                                textAlign: TextAlign.center,
                                maxLines: 2,
                                style: TextStyle(
                                  fontSize: 28,
                                  fontWeight: FontWeight.w500,
                                  fontFeatures: const [
                                    FontFeature.proportionalFigures()
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        Gap.h1_5,
                        Container(
                          padding: Insets.a,
                          decoration: BoxDecoration(
                            color: AppColors.backgroundTextInputColor,
                            borderRadius: BorderRadius.circular(
                                Dimens.cornerRadius * 0.75),
                          ),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    AppLocalizations.of(context)!
                                        .emailNewsletter,
                                    style: TextStyle(
                                      fontSize: 16,
                                    ),
                                  ),
                                  PandasCheckbox(
                                      value: isMail,
                                      onChanged: (value) {
                                        setState(() {
                                          isMail = !isMail;
                                        });
                                      }),
                                ],
                              ),
                              Gap.h_25,
                              Divider(),
                              Gap.h_25,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    AppLocalizations.of(context)!.smsMailing,
                                    style: TextStyle(
                                      fontSize: 16,
                                    ),
                                  ),
                                  PandasCheckbox(
                                      value: isSMS,
                                      onChanged: (value) {
                                        setState(() {
                                          isSMS = !isSMS;
                                        });
                                      }),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Gap.h1_5,
                    CustomButton(
                      onTap: () async {
                        if (isPhoneValid ||
                            isNameValid ||
                            isBirthdateValid ||
                            isMail != widget.emailNotification ||
                            isSMS != widget.smsNotification ||
                            isMan != isPreviousMan ||
                            isWoman != isPreviousWoman) {
                          getIt.get<UserBloc>().add(UserEvent.updateProfile(
                                name: _nameTextController?.text,
                                gender: isMan
                                    ? 'male'
                                    : isWoman
                                        ? 'female'
                                        : null,
                                phone: _phoneTextController?.text,
                                birthdate: _birthdateTextController?.text,
                                emailNotification: isMail,
                                smsNotification: isSMS,
                                messageBloc: getIt.get<MessageBloc>()
                                  ..add(MessageEvent.errorMessage('')),
                              ));
                        }
                      },
                      child: Text(
                        AppLocalizations.of(context)!.save,
                        style: TextStyle(
                          fontSize: 18,
                          color: isPhoneValid ||
                                  isNameValid ||
                                  isBirthdateValid ||
                                  isMail != widget.emailNotification ||
                                  isSMS != widget.smsNotification ||
                                  isMan != isPreviousMan ||
                                  isWoman != isPreviousWoman
                              ? Colors.white
                              : AppColors.darkGreyTextColor,
                          fontFeatures: const [
                            FontFeature.proportionalFigures()
                          ],
                        ),
                      ),
                      width: double.infinity,
                      backgroundColor: isPhoneValid ||
                              isNameValid ||
                              isBirthdateValid ||
                              isMail != widget.emailNotification ||
                              isSMS != widget.smsNotification ||
                              isMan != isPreviousMan ||
                              isWoman != isPreviousWoman
                          ? AppColors.buttonColor
                          : AppColors.veryDarkGray,
                    ),
                    Gap.h1_5,
                    CancelButton(
                      onTap: () async {
                        Blackout.of(context)!.setBlackoutValue(true);
                        await showDialog<String>(
                          barrierDismissible: false,
                          barrierColor: Colors.transparent,
                          context: context,
                          builder: (BuildContext context) =>
                              ChangePasswordPopup(),
                        );
                        Blackout.of(context)!.setBlackoutValue(false);
                      },
                      child: Text(
                        AppLocalizations.of(context)!.changePassword,
                        style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontFeatures: const [
                            FontFeature.proportionalFigures()
                          ],
                        ),
                      ),
                      width: double.infinity,
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
