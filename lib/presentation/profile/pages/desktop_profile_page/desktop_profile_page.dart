import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import '../../../../../core/ui/ui_kit.dart';
import 'widgets/profile_page_content.dart';

class DesktopProfilePage extends StatefulWidget {
  const DesktopProfilePage({Key? key}) : super(key: key);

  @override
  State<DesktopProfilePage> createState() => _DesktopProfilePageState();
}

class _DesktopProfilePageState extends State<DesktopProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: Column(
        children: [
          NavigationHeader(),
          Expanded(
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Gap.h1_5,
                        ProfilePageContent(),
                        Gap.h2,
                        Footer(),
                      ],
                    ),
                  ),
                ),
                BlackoutContainer(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
