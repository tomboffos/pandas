import 'dart:async';
import 'dart:html' as html;
import 'dart:ui' as ui;
import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/core/utils/translations_helper.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/domain/episode/bloc/episode_bloc.dart';
import 'package:nine_pandas/domain/history/bloc/history_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import 'widgets/comments_column.dart';
import 'widgets/season_tile.dart';

class TabletPlayerPage extends StatefulWidget {
  final int id;
  final int movieId;
  final int seasonNumber;

  const TabletPlayerPage({
    Key? key,
    @PathParam('id') required this.id,
    @PathParam('movieId') required this.movieId,
    @PathParam('seasonNumber') required this.seasonNumber,
  }) : super(key: key);

  static const double _logoHeight = 49;
  static const double _logoWidth = 375;
  static const double _playerHeight = 497;
  static const double _episodesWidth = 290;

  @override
  State<TabletPlayerPage> createState() => _TabletPlayerPageState();
}

class _TabletPlayerPageState extends State<TabletPlayerPage> {
  String? season;
  String? episode;
  bool isNew = true;
  final html.IFrameElement _iframeElement = html.IFrameElement();
  late HtmlElementView _htmlElementView;

  late StreamSubscription<html.MessageEvent> logsListener;
  late final EpisodeBloc _episodeBloc;

  @override
  void initState() {
    super.initState();
    _episodeBloc = BlocProvider.of<EpisodeBloc>(context);
    _handleLogs();
    _htmlElementView = HtmlElementView(
      viewType: widget.movieId.toString(),
    );
  }

  @override
  void dispose() {
    logsListener.cancel();
    super.dispose();
  }

  _initIframe(String viewType, String? iframeUrl) {
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
      viewType,
      (int viewId) => html.Element.html(
          '<iframe src="$iframeUrl" frameBorder="0" allowfullscreen></iframe>',
          validator: new html.NodeValidatorBuilder()
            ..allowHtml5()
            ..allowElement('webview', attributes: ['*'])
            ..allowElement('iframe', attributes: ['*']))
        ..style.width = '100%'
        ..style.height = '100%',
    );
    _iframeElement.src = iframeUrl;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: BlocConsumer<EpisodeBloc, EpisodeState>(
        listener: (context, state) => state.maybeWhen(
          fetchedEpisode: (episode) {
            if ((widget.seasonNumber > episode.seasons!.length) ||
                (episode.seasons![widget.seasonNumber - 1].isPurchased !=
                        true &&
                    episode.seasons![widget.seasonNumber - 1].price != null)) {
              if (episode.episodeType != EpisodeType.trailer) {
                AutoRouter.of(context).navigate(MainRoute());
              }
            }
            return _initIframe(widget.movieId.toString(),
                _iframeElement.src = episode.player!.iframeUrl);
          },
          orElse: () => Container(),
        ),
        builder: (context, state) => state.when(
          loading: () => PandasLoadingView(),
          fetchedEpisode: (episode) => buildBody(context, episode),
          error: () => PandasServerErrorWidget(
            reloadCallback: () => _episodeBloc.add(
              EpisodeEvent.fetchEpisode(
                movieId: widget.movieId,
                id: widget.id,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildBody(BuildContext context, Episode episode) {
    return BlocListener<UserBloc, UserState>(
      listener: (context, state) {
        state.whenOrNull(logout: () {
          if (episode.seasons![widget.seasonNumber - 1].price != null) {
            if (episode.episodeType != EpisodeType.trailer) {
              AutoRouter.of(context).navigate(MainRoute());
            }
          }
        });
      },
      child: Column(
        children: [
          NavigationHeader(),
          Expanded(
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: Dimens.horizontalPadding * 3,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Gap.h1_5,
                            NavigationRow(
                              currentPageText:
                                  AppLocalizations.of(context)!.seasonNumber(
                                getNumberToText(widget.seasonNumber, context),
                              ),
                            ),
                            Gap.h1_5,
                            Center(
                              child: CachedNetworkImage(
                                imageUrl: episode.data?.logo ??
                                    'https://www.edigitalagency.com.au/wp-content/uploads/Youtube-logo-png.png',
                                width: TabletPlayerPage._logoWidth,
                                height: TabletPlayerPage._logoHeight,
                                alignment: Alignment.center,
                              ),
                            ),
                            Gap.h2,
                            Align(
                              alignment: Alignment.center,
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width,
                                height: TabletPlayerPage._playerHeight,
                                child: _htmlElementView,
                              ),
                            ),
                            Gap.h2,
                            Text(
                              episode.data?.episodeDescription ??
                                  'В участок к капитану Малюге попадает звезда YouTube — Андрей Gan_13_ Борисов. Пытаясь разобраться в деталях происшествия, Малюга сталкивается с новой для него реальностью — интернет звёзды и челленджи. На следующий день слитое в сеть видео с допросом Gan_13_ мгновенно вызывает резонанс у армии его поклонников. Жизнь капитана Малюги уже никогда не будет прежней.',
                              style: TextStyle(
                                fontSize: 18,
                                color: AppColors.greyText,
                                height: 1.3,
                              ),
                            ),
                            Gap.h1_5,
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  flex: 80,
                                  child: CommentsColumn(
                                    comments: episode.comments,
                                    episode: episode,
                                    movieId: widget.movieId,
                                  ),
                                ),
                                Gap.w2,
                                SizedBox(
                                  width: TabletPlayerPage._episodesWidth,
                                  child: Column(
                                    children: [
                                      if (episode.seasons != null)
                                        for (int i = 0;
                                            i < episode.seasons!.length;
                                            i++)
                                          SeasonTile(
                                            movieId: widget.movieId,
                                            season: episode.seasons![i],
                                            currentEpisode: episode,
                                            isPurchased:
                                                episode.seasons![i].isPurchased,
                                          ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Gap.h4,
                          ],
                        ),
                      ),
                      Footer(),
                    ],
                  ),
                ),
                BlackoutContainer(),
              ],
            ),
          )
        ],
      ),
    );
  }

  _handleLogs() {
    logsListener = html.window.onMessage.listen((event) {
      final data = event.data;
      switch (data['event']) {
        case 'new':
          isNew = true;

          final serialData = data['id'].toString().split('-');
          // serialData[0] is voice over hash
          season = serialData[1];
          episode = serialData[2];

          break;
        case 'vast_ready':
          break;
        case 'play':
          if (isNew) {
            isNew = false;

            //add history
            getIt.get<HistoryBloc>().add(HistoryEvent.addHistory(widget.id));
          }
          break;
        case 'paused':
          break;
        default:
      }
    });
  }
}
