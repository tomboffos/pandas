import 'dart:ui';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/data/movie/models/season/season.dart';
import 'package:nine_pandas/domain/analytics/bloc/analytics_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_up/sing_up_page.dart';
import 'package:nine_pandas/presentation/lottery/presentation/pages/lottery_page.dart';

import '../../../../../injectable.dart';
import 'player_episode_item.dart';

class SeasonTile extends StatefulWidget {
  final Season season;
  final Episode currentEpisode;
  final int movieId;
  final bool? isPurchased;
  const SeasonTile({
    Key? key,
    required this.season,
    required this.movieId,
    required this.isPurchased,
    required this.currentEpisode,
  }) : super(key: key);

  @override
  State<SeasonTile> createState() => _SeasonTileState();
}

class _SeasonTileState extends State<SeasonTile> {
  bool isExpanded = true;
  final isAccesible = false;
  bool didFillUpForm = false;

  late bool? isPurchased;

  @override
  void initState() {
    super.initState();
    if (widget.isPurchased != null) {
      isPurchased = widget.isPurchased;
    } else {
      isPurchased = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Divider(),
        Column(
          children: [
            if (isExpanded)
              for (int i = 0; i < 1; i++)
                Theme(
                  data: Theme.of(context).copyWith(
                      dividerColor: Colors.transparent,
                      listTileTheme: ListTileThemeData(
                        contentPadding: Insets.w_5,
                      )),
                  child: BlocBuilder<UserBloc, UserState>(
                    builder: (context, state) {
                      return ExpansionTile(
                          initiallyExpanded: false,
                          expandedAlignment: Alignment.bottomLeft,
                          title: Row(
                            children: [
                              Text(
                                AppLocalizations.of(context)!.seasonNumberDigit(
                                    widget.season.seasonNumber),
                                style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                    color: AppColors.veryLightGray,
                                    height: 1.3,
                                    fontFeatures: const [
                                      FontFeature.proportionalFigures()
                                    ],
                                    fontFamily: 'Roboto'),
                              ),
                              Spacer(),
                              if (widget.season.price != null) ...[
                                Text(
                                  AppLocalizations.of(context)!
                                      .priceWithCurrency(
                                          widget.season.price ?? 0),
                                  style: TextStyle(
                                    fontSize: 16,
                                    height: 1.3,
                                  ),
                                ),
                                Gap.w_5,
                                GradientButton(
                                  height: Dimens.buttonHeight * 0.67,
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: Dimens.horizontalPadding,
                                    vertical: Dimens.verticalPadding * 0.5,
                                  ),
                                  child: Text(
                                    AppLocalizations.of(context)!.buy,
                                    style: TextStyle(
                                        color: AppColors.veryLightGray,
                                        fontSize: 12,
                                        height: 1.3,
                                        fontFeatures: const [
                                          FontFeature.proportionalFigures()
                                        ],
                                        fontFamily: 'Roboto'),
                                  ),
                                  onTap: () async {
                                    getIt<AnalyticsBloc>().add(
                                      AnalyticsEvent
                                          .sendPressedBuyButtonPlayerPage(
                                        widget.movieId.toString(),
                                        '',
                                        widget.season.seasonNumber.toString(),
                                        widget.currentEpisode.episodeNumber
                                            .toString(),
                                        getIt<UserBloc>().state is Logged
                                            ? (getIt<UserBloc>().state
                                                    as Logged)
                                                .user
                                                .id
                                            : null,
                                      ),
                                    );
                                    await showDialog<String>(
                                      barrierDismissible: false,
                                      barrierColor:
                                          Colors.black.withOpacity(0.6),
                                      context: context,
                                      builder: (BuildContext context) =>
                                          state.maybeWhen(
                                        logged: (user) => LotteryPage(),
                                        orElse: () => SingUpPage(
                                          title: AppLocalizations.of(context)!
                                              .singUpForPaymant,
                                        ),
                                      ),
                                    );
                                  },
                                ),
                              ],
                              if (isAccesible && !didFillUpForm)
                                GradientButton(
                                  height: Dimens.buttonHeight * 0.67,
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: Dimens.horizontalPadding,
                                    vertical: Dimens.verticalPadding * 0.5,
                                  ),
                                  child: Text(
                                    AppLocalizations.of(context)!.fillUpForm,
                                    style: TextStyle(
                                        color: AppColors.veryLightGray,
                                        fontSize: 12,
                                        height: 1.3,
                                        fontFeatures: const [
                                          FontFeature.proportionalFigures()
                                        ],
                                        fontFamily: 'Roboto'),
                                  ),
                                  onTap: () async {
                                    await showDialog<String>(
                                      barrierDismissible: false,
                                      barrierColor:
                                          Colors.black.withOpacity(0.6),
                                      context: context,
                                      builder: (BuildContext context) =>
                                          state.maybeWhen(
                                        logged: (user) => LotteryPage(),
                                        orElse: () => SingUpPage(
                                          title: AppLocalizations.of(context)!
                                              .singUpForPaymant,
                                        ),
                                      ),
                                    );
                                  },
                                ),
                            ],
                          ),
                          children: widget.season.episodes
                              .map((episode) => Padding(
                                  padding: Insets.h1_5,
                                  child: PlayerEpisodeItem(
                                    movieId: widget.movieId,
                                    episode: episode,
                                    season: widget.season,
                                    seasonNumber: widget.season.seasonNumber,
                                  )))
                              .toList());
                    },
                  ),
                ),
          ],
        )
      ],
    );
  }
}
