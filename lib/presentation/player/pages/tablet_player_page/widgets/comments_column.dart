import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/data/player/models/comment.dart';
import 'package:nine_pandas/domain/episode/bloc/episode_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_up/sing_up_page.dart';
import 'package:nine_pandas/presentation/comments/presentation/comment_card/widget/comment_card.dart';
import 'package:nine_pandas/presentation/comments/presentation/send_comment_widget/widget/send_comment_widget.dart';

class CommentsColumn extends StatefulWidget {
  final List<Comment>? comments;
  final Episode episode;
  final int movieId;
  CommentsColumn(
      {Key? key,
      required this.comments,
      required this.episode,
      required this.movieId})
      : super(key: key);

  @override
  State<CommentsColumn> createState() => _CommentsColumnState();
}

class _CommentsColumnState extends State<CommentsColumn> {
  GlobalKey buttonKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Divider(),
        Gap.h,
        Row(
          children: [
            Text(
              AppLocalizations.of(context)!
                  .commentsCount(widget.comments?.length ?? 0),
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  height: 1.3,
                  color: AppColors.veryLightGray,
                  fontFeatures: const [FontFeature.proportionalFigures()],
                  fontFamily: 'Roboto'),
            ),
            Gap.w,
            InkWell(
              onTap: () {
                _showSortingMenu(context, buttonKey, widget.episode);
              },
              child: Row(
                key: buttonKey,
                children: [
                  Icon(Icons.sort),
                  Gap.w,
                  Text(
                    AppLocalizations.of(context)!.sort,
                    style: TextStyle(
                        fontSize: 16,
                        height: 1.3,
                        color: AppColors.veryLightGray,
                        fontFeatures: const [FontFeature.proportionalFigures()],
                        fontFamily: 'Roboto'),
                  ),
                ],
              ),
            )
          ],
        ),
        Gap.h1_5,
        BlocBuilder<UserBloc, UserState>(
          builder: (context, state) => state.maybeWhen(
              orElse: () => Center(
                    child: AuthNeeded(),
                  ),
              logged: (user) => SendCommentWidget(onSendComment: (comment) {
                    BlocProvider.of<EpisodeBloc>(context).add(
                        EpisodeEvent.postComment(
                            comment: comment,
                            movieCollection: widget.movieId,
                            episodeId: widget.episode.id));
                  })),
        ),
        Gap.h,
        if (widget.comments != null)
          ListView.builder(
            itemBuilder: (context, i) => Padding(
              padding: Insets.h,
              child: CommentCard(
                onTapUpVote: () {
                  BlocProvider.of<EpisodeBloc>(context).add(
                      EpisodeEvent.addVoteToComment(
                          episodeId: widget.episode.id,
                          movieId: widget.movieId,
                          commentId: widget.comments![i].id,
                          vote: 1));
                },
                onTapDownVote: () {
                  BlocProvider.of<EpisodeBloc>(context).add(
                      EpisodeEvent.addVoteToComment(
                          episodeId: widget.episode.id,
                          movieId: widget.movieId,
                          commentId: widget.comments![i].id,
                          vote: -1));
                },
                comment: widget.comments![i],
              ),
            ),
            itemCount: widget.comments!.length,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
          ),
        Gap.h_5,
        // InkWell(
        //   onTap: () {},
        //   child: Center(
        //     child: Text(
        //       AppLocalizations.of(context)!.showMoreComments(234),
        //       style: TextStyle(
        //         fontSize: 12,
        //         color: AppColors.veryLightGray,
        //         height: 1.3,
        //       ),
        //     ),
        //   ),
        // )
      ],
    );
  }
}

class AuthNeeded extends StatelessWidget {
  const AuthNeeded({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Insets.a,
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          children: [
            TextSpan(
              text: AppLocalizations.of(context)!.serialPageLogin,
              recognizer: TapGestureRecognizer()
                ..onTap = (() async {
                  await showDialog<String>(
                    barrierDismissible: false,
                    barrierColor: Colors.black.withOpacity(0.6),
                    context: context,
                    builder: (BuildContext context) => SingUpPage(),
                  );
                }),
              style: TextStyle(
                fontSize: 14,
                color: AppColors.veryLightGray,
                decoration: TextDecoration.underline,
              ),
            ),
            TextSpan(
              text: AppLocalizations.of(context)!.toSendReview,
              style: TextStyle(
                color: AppColors.darkGreyTextColor,
                fontSize: 14,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

void _showSortingMenu(
    BuildContext context, GlobalKey buttonKey, Episode episode) async {
  double left = buttonKey.globalPaintBounds!.left;
  double bottom = buttonKey.globalPaintBounds!.bottom;
  await showMenu(
    color: AppColors.carcassBackground,
    context: context,
    position: RelativeRect.fromLTRB(
        left, bottom + Dimens.verticalPadding, left, bottom),
    items: [
      PopupMenuItem(
        height: 0,
        onTap: () {},
        padding: Insets.w_5,
        value: 0,
        textStyle: TextStyle(
          fontSize: 16,
          color: AppColors.veryLightGray,
          fontWeight: FontWeight.bold,
          height: 1.3,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            Gap.h,
            GestureDetector(
              child: Text(AppLocalizations.of(context)!.popularFirst),
              onTap: () {
                BlocProvider.of<EpisodeBloc>(context)
                    .add(EpisodeEvent.sortByLikes(episode: episode));
                Navigator.pop(context);
              },
            ),
            Gap.h,
            Divider(),
            Gap.h,
            GestureDetector(
                onTap: () {
                  BlocProvider.of<EpisodeBloc>(context)
                      .add(EpisodeEvent.sortByDate(episode: episode));
                  Navigator.pop(context);
                },
                child: Text(AppLocalizations.of(context)!.newFirst)),
            Gap.h,
          ],
        ),
      ),
    ],
    elevation: 8.0,
  );
}
