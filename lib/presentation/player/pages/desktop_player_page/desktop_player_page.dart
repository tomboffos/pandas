import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'dart:html' as html;
import 'dart:ui' as ui;
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/core/ui/widgets/constrained_width_flexible.dart';
import 'package:nine_pandas/core/utils/translations_helper.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/data/movie/models/season/season.dart';
import 'package:nine_pandas/domain/episode/bloc/episode_bloc.dart';
import 'package:nine_pandas/domain/history/bloc/history_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';

import 'widgets/comments_column.dart';
import 'widgets/season_tile.dart';

class DesktopPlayerPage extends StatefulWidget {
  const DesktopPlayerPage({
    Key? key,
    @PathParam('id') required this.id,
    @PathParam('movieId') required this.movieId,
    @PathParam('seasonNumber') required this.seasonNumber,
  }) : super(key: key);

  final int id;
  final int movieId;
  final int seasonNumber;
  static const double _logoHeight = 49;
  static const double _logoWidth = 375;
  static const double _playerHeight = 595;
  static const double _episodesMaxWidth = 375;
  static const double _episodesMinWidth = 255;

  @override
  State<DesktopPlayerPage> createState() => _DesktopPlayerPageState();
}

class _DesktopPlayerPageState extends State<DesktopPlayerPage> {
  String? season;
  String? episode;
  bool isNew = true;
  final html.IFrameElement _iframeElement = html.IFrameElement();
  late HtmlElementView _htmlElementView;

  late StreamSubscription<html.MessageEvent> logsListener;
  late final EpisodeBloc _episodeBloc;

  late final Season currentSeason;

  @override
  void initState() {
    super.initState();
    _episodeBloc = BlocProvider.of<EpisodeBloc>(context);
    _handleLogs();
    _htmlElementView = HtmlElementView(
      viewType: widget.movieId.toString(),
    );
  }

  @override
  void dispose() {
    logsListener.cancel();
    super.dispose();
  }

  _initIframe(String viewType, String? iframeUrl) {
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
      viewType,
      (int viewId) => html.Element.html(
          '<iframe src="$iframeUrl" frameBorder="0" allowfullscreen></iframe>',
          validator: new html.NodeValidatorBuilder()
            ..allowHtml5()
            ..allowElement('webview', attributes: ['*'])
            ..allowElement('iframe', attributes: ['*']))
        ..style.width = '100%'
        ..style.height = '100%',
    );
    _iframeElement.src = iframeUrl;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: BlocConsumer<EpisodeBloc, EpisodeState>(
        listener: (context, state) {
          state.whenOrNull(
            fetchedEpisode: (episode) {
              if ((widget.seasonNumber > episode.seasons!.length) ||
                  (episode.seasons![widget.seasonNumber - 1].isPurchased !=
                          true &&
                      episode.seasons![widget.seasonNumber - 1].price !=
                          null)) {
                if (episode.episodeType != EpisodeType.trailer) {
                  AutoRouter.of(context).navigate(MainRoute());
                }
              }
              return _initIframe(widget.movieId.toString(),
                  _iframeElement.src = episode.player!.iframeUrl);
            },
          );
        },
        builder: (context, state) => state.when(
          loading: () => PandasLoadingView(),
          error: () => PandasServerErrorWidget(
            reloadCallback: () => _episodeBloc.add(
              EpisodeEvent.fetchEpisode(
                movieId: widget.movieId,
                id: widget.id,
              ),
            ),
          ),
          fetchedEpisode: (episode) => Scaffold(
            backgroundColor: AppColors.scaffoldBgColor,
            body: BlocListener<UserBloc, UserState>(
              listener: (context, state) {
                state.whenOrNull(
                  logout: () {
                    if (episode.seasons![widget.seasonNumber - 1].price !=
                        null) {
                      if (episode.episodeType != EpisodeType.trailer) {
                        AutoRouter.of(context).navigate(MainRoute());
                      }
                    }
                  },
                );
              },
              child: Column(
                children: [
                  NavigationHeader(),
                  Expanded(
                    child: Stack(
                      children: [
                        SingleChildScrollView(
                          child: Column(
                            children: [
                              Padding(
                                padding: Insets.w10,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Gap.h1_5,
                                    NavigationRow(
                                      currentPageText:
                                          AppLocalizations.of(context)!
                                              .seasonNumber(
                                        getNumberToText(
                                            widget.seasonNumber, context),
                                      ),
                                    ),
                                    Gap.h_25,
                                    Center(
                                      child: CachedNetworkImage(
                                        imageUrl: episode.data?.logo ??
                                            'https://www.edigitalagency.com.au/wp-content/uploads/Youtube-logo-png.png',
                                        width: DesktopPlayerPage._logoWidth,
                                        height: DesktopPlayerPage._logoHeight,
                                        alignment: Alignment.center,
                                      ),
                                    ),
                                    Gap.h3,
                                    LayoutBuilder(
                                      builder: (context, constraints) => Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            flex: 3,
                                            child: Column(
                                              children: [
                                                Align(
                                                  alignment: Alignment.center,
                                                  child: SizedBox(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width /
                                                            1.5,
                                                    height: DesktopPlayerPage
                                                        ._playerHeight,
                                                    child: _htmlElementView,
                                                  ),
                                                ),
                                                Gap.h2,
                                                Text(
                                                  episode.data
                                                          ?.episodeDescription ??
                                                      'В участок к капитану Малюге попадает звезда YouTube — Андрей Gan_13_ Борисов. Пытаясь разобраться в деталях происшествия, Малюга сталкивается с новой для него реальностью — интернет звёзды и челленджи. На следующий день слитое в сеть видео с допросом Gan_13_ мгновенно вызывает резонанс у армии его поклонников. Жизнь капитана Малюги уже никогда не будет прежней.',
                                                  style: TextStyle(
                                                    fontSize: 18,
                                                    color: AppColors.greyText,
                                                    height: 1.3,
                                                  ),
                                                ),
                                                Gap.h2,
                                                CommentsColumn(
                                                  movieId: widget.movieId,
                                                  episode: episode,
                                                  comments: episode.comments,
                                                ),
                                                Gap.h4,
                                              ],
                                            ),
                                          ),
                                          Gap.w2,
                                          ConstrainedWidthFlexible(
                                            flex: 1,
                                            flexSum: 4,
                                            maxWidth: DesktopPlayerPage
                                                ._episodesMaxWidth,
                                            minWidth: DesktopPlayerPage
                                                ._episodesMinWidth,
                                            outerConstraints: constraints,
                                            child: Column(
                                              children: [
                                                if (episode.seasons != null)
                                                  for (int i = 0;
                                                      i <
                                                          episode
                                                              .seasons!.length;
                                                      i++)
                                                    SeasonTile(
                                                      movieId: widget.movieId,
                                                      season:
                                                          episode.seasons![i],
                                                      currentEpisode: episode,
                                                      isPurchased: episode
                                                          .seasons![i]
                                                          .isPurchased,
                                                    ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Gap.h2,
                                  ],
                                ),
                              ),
                              Footer(),
                            ],
                          ),
                        ),
                        BlackoutContainer(),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _handleLogs() {
    logsListener = html.window.onMessage.listen((event) {
      final data = event.data;
      switch (data['event']) {
        case 'new':
          isNew = true;

          final serialData = data['id'].toString().split('-');
          // serialData[0] is voice over hash
          season = serialData[1];
          episode = serialData[2];

          break;
        case 'vast_ready':
          break;
        case 'play':
          if (isNew) {
            isNew = false;

            //add history
            getIt.get<HistoryBloc>().add(HistoryEvent.addHistory(widget.id));
          }
          break;
        case 'paused':
          break;
        default:
      }
    });
  }
}
