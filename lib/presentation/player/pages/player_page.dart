import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/analytics/bloc/analytics_bloc.dart';
import 'package:nine_pandas/domain/episode/bloc/episode_bloc.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import 'desktop_player_page/desktop_player_page.dart';
import 'mobile_player_page/mobile_player_page.dart';
import 'tablet_player_page/tablet_player_page.dart';

class PlayerPage extends StatefulWidget {
  final int id;
  final int movieId;
  final int seasonNumber;

  const PlayerPage({
    Key? key,
    @PathParam('id') required this.id,
    @PathParam('movieId') required this.movieId,
    @PathParam('seasonNumber') required this.seasonNumber,
  }) : super(key: key);

  @override
  State<PlayerPage> createState() => _PlayerPageState();
}

class _PlayerPageState extends State<PlayerPage> {
  bool isBlackout = false;

  final EpisodeBloc _episodeBloc = getIt.get<EpisodeBloc>();

  void setBlackoutValue(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  void initState() {
    super.initState();
    _episodeBloc
        .add(EpisodeEvent.fetchEpisode(movieId: widget.movieId, id: widget.id));
  }

  @override
  void dispose() {
    super.dispose();
    _episodeBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: setBlackoutValue,
      child: BlocProvider.value(
        value: _episodeBloc,
        child: BlocListener<EpisodeBloc, EpisodeState>(
          listener: (context, state) {
            state.whenOrNull(
              fetchedEpisode: (episode) => getIt<AnalyticsBloc>().add(
                AnalyticsEvent.sendPlayerPageOpened(
                  widget.movieId.toString(),
                  episode.movieCollection!.movieTitle,
                  widget.seasonNumber.toString(),
                  episode.episodeNumber.toString(),
                  getIt<UserBloc>().state is Logged
                      ? (getIt<UserBloc>().state as Logged).user.id
                      : null,
                ),
              ),
            );
          },
          child: BlocListener<UserBloc, UserState>(
            listener: (context, state) {
              _episodeBloc.add(EpisodeEvent.fetchEpisode(
                  movieId: widget.movieId, id: widget.id));
            },
            child: BlocListener<LocalizationBloc, LocalizationState>(
              listener: (context, state) {
                _episodeBloc.add(EpisodeEvent.fetchEpisode(
                    movieId: widget.movieId, id: widget.id));
              },
              child: LayoutHandler(
                mobileView: MobilePlayerPage(
                  id: widget.id,
                  movieId: widget.movieId,
                  seasonNumber: widget.seasonNumber,
                ),
                tabletView: TabletPlayerPage(
                  id: widget.id,
                  movieId: widget.movieId,
                  seasonNumber: widget.seasonNumber,
                ),
                desktopView: DesktopPlayerPage(
                  id: widget.id,
                  movieId: widget.movieId,
                  seasonNumber: widget.seasonNumber,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
