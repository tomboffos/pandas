import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/data/movie/models/season/season.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_up/sing_up_page.dart';
import 'package:nine_pandas/presentation/lottery/presentation/pages/lottery_page.dart';

class PlayerEpisodeItem extends StatefulWidget {
  final Episode episode;
  final Season season;
  final int movieId;
  final int seasonNumber;
  const PlayerEpisodeItem({
    Key? key,
    required this.episode,
    required this.season,
    required this.movieId,
    required this.seasonNumber,
  }) : super(key: key);

  static const double width = 326;
  static const double frameHeight = 194;
  static const double playButtonBgDiameter = 50;

  @override
  State<PlayerEpisodeItem> createState() => _PlayerEpisodeItemState();
}

class _PlayerEpisodeItemState extends State<PlayerEpisodeItem> {
  final isAccessible = true;
  final isTrailer = false;
  final isMovie = false;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: PlayerEpisodeItem.width,
      child: Opacity(
        opacity: isAccessible ? 1.0 : 0.3,
        child: Column(
          children: [
            InkWell(
              onTap: () async {
                if (widget.episode.episodeType == EpisodeType.trailer) {
                  AutoRouter.of(context).navigate(
                    PlayerRoute(
                        id: widget.episode.id,
                        movieId: widget.movieId,
                        seasonNumber: widget.season.seasonNumber),
                  );
                } else {
                  switch (widget.season.isPurchased) {
                    case true:
                      AutoRouter.of(context).navigate(
                        PlayerRoute(
                            id: widget.episode.id,
                            movieId: widget.movieId,
                            seasonNumber: widget.seasonNumber),
                      );
                      break;
                    case false:
                      if (widget.season.price != null) {
                        await showDialog<String>(
                          barrierDismissible: false,
                          barrierColor: Colors.black.withOpacity(0.6),
                          context: context,
                          builder: (BuildContext context) => LotteryPage(),
                        );
                      } else {
                        AutoRouter.of(context).navigate(
                          PlayerRoute(
                              id: widget.episode.id,
                              movieId: widget.movieId,
                              seasonNumber: widget.seasonNumber),
                        );
                      }
                      break;
                    default:
                      if (widget.season.price != null) {
                        await showDialog<String>(
                          barrierDismissible: false,
                          barrierColor: Colors.black.withOpacity(0.6),
                          context: context,
                          builder: (BuildContext context) => SingUpPage(
                            title:
                                AppLocalizations.of(context)!.singUpForPaymant,
                          ),
                        );
                      } else {
                        AutoRouter.of(context).navigate(
                          PlayerRoute(
                              id: widget.episode.id,
                              movieId: widget.movieId,
                              seasonNumber: widget.seasonNumber),
                        );
                      }
                  }
                }
              },
              child: Stack(
                children: [
                  Container(
                    clipBehavior: Clip.antiAlias,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(Dimens.cornerRadius * 1.75),
                    ),
                    child: CachedNetworkImage(
                      imageUrl: widget.episode.data?.episodePoster ??
                          'https://lumiere-a.akamaihd.net/v1/images/the-last-jedi-theatrical-poster-tall-a_6a776211.jpeg?region=0%2C53%2C1536%2C768&width=960',
                      height: PlayerEpisodeItem.frameHeight,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned.fill(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: PlayerEpisodeItem.playButtonBgDiameter,
                          height: PlayerEpisodeItem.playButtonBgDiameter,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white.withOpacity(0.85),
                          ),
                          child: Icon(
                            Icons.play_arrow_rounded,
                            color: Colors.black,
                            size: 40,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Gap.h_5,
            Padding(
              padding: Insets.w_5,
              child: Row(
                children: [
                  Text(
                    isTrailer
                        ? AppLocalizations.of(context)!.trailer
                        : isMovie
                            ? AppLocalizations.of(context)!.movie
                            : AppLocalizations.of(context)!
                                .episodeNumber(widget.episode.episodeNumber),
                    style: TextStyle(
                      fontSize: 13,
                      height: 1.3,
                      color: AppColors.greyTextColor,
                    ),
                  ),
                  Spacer(),
                  Text(
                    AppLocalizations.of(context)!.videoDurationMinutesAndHours(
                        widget.episode.videoDuration ?? 0, 0),
                    style: TextStyle(
                      height: 1.3,
                      color: AppColors.greyTextColor,
                      fontSize: 13,
                    ),
                  )
                ],
              ),
            ),
            if (!isTrailer) ...[
              Gap.h_5,
              Padding(
                padding: Insets.w_5,
                child: Text(
                  widget.episode.data?.episodeDescription ??
                      'Эксклюзивная режиссерская версия, в которой собран весь сезон в полный метр. Эксклюзивная режиссерская версия, в которой собран весь сезон в полный метр.',
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    fontSize: 13,
                    color: AppColors.veryLightGray,
                    fontWeight: FontWeight.w500,
                    height: 1.3,
                  ),
                ),
              ),
            ],
          ],
        ),
      ),
    );
  }
}
