import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/data/html_reading_page/models/html_reading.dart';
import 'package:nine_pandas/domain/html_reading_page/bloc/html_reader_page_bloc.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import '../../../../../core/ui/ui_kit.dart';
import 'widgets/lottery_terms_page_content.dart';

class DesktopLotteryTermsPage extends StatefulWidget {
  const DesktopLotteryTermsPage({Key? key}) : super(key: key);

  @override
  State<DesktopLotteryTermsPage> createState() =>
      _DesktopLotteryTermsPageState();
}

class _DesktopLotteryTermsPageState extends State<DesktopLotteryTermsPage> {
  late final HtmlReadingPageBloc _policyBloc;

  @override
  void initState() {
    _policyBloc = BlocProvider.of<HtmlReadingPageBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: BlocProvider.value(
        value: _policyBloc,
        child: BlocBuilder<HtmlReadingPageBloc, HtmlReadingPageState>(
          builder: (context, state) {
            return Column(
              children: [
                NavigationHeader(),
                Expanded(
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          children: [
                            state.when(
                              loaded: (policy) => LotteryTermsPageContent(
                                htmlData: policy.data.text,
                              ),
                              loading: () => ConstrainedBox(
                                  constraints: BoxConstraints(
                                      minHeight:
                                          MediaQuery.of(context).size.height /
                                              1.25),
                                  child: PandasLoadingView()),
                              error: () => PandasServerErrorWidget(
                                reloadCallback: () => _policyBloc.add(
                                  HtmlReadingPageEvent.fetchData(
                                    HtmlDocType.privacy,
                                  ),
                                ),
                              ),
                            ),
                            Gap.h2,
                            Footer(),
                          ],
                        ),
                      ),
                      BlackoutContainer(),
                    ],
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
