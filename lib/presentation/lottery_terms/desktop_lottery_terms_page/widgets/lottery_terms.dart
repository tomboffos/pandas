import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';

class LotteryTermsWidget extends StatelessWidget {
  const LotteryTermsWidget({Key? key, required this.htmlData})
      : super(key: key);

  static const double privacPolicyMaxWidth = 1121;
  final String htmlData;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(maxWidth: LotteryTermsWidget.privacPolicyMaxWidth),
      child: Column(children: [
        HtmlWidget(htmlData),
      ]),
    );
  }
}
