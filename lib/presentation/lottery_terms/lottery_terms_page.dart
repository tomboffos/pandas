import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/data/html_reading_page/models/html_reading.dart';
import 'package:nine_pandas/domain/html_reading_page/bloc/html_reader_page_bloc.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/presentation/lottery_terms/tablet_lottery_terms_page/tablet_lottery_terms_page.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import 'desktop_lottery_terms_page/desktop_lottery_terms_page.dart';
import 'mobile_lottery_terms_page/mobile_lottery_terms_page.dart';

class LotteryTermsPage extends StatefulWidget {
  const LotteryTermsPage({Key? key}) : super(key: key);

  @override
  State<LotteryTermsPage> createState() => LotteryTermsPageState();
}

class LotteryTermsPageState extends State<LotteryTermsPage> {
  bool isBlackout = false;
  final HtmlReadingPageBloc _htmlReadingPageBloc =
      getIt.get<HtmlReadingPageBloc>();

  @override
  void initState() {
    super.initState();
    _htmlReadingPageBloc
        .add(HtmlReadingPageEvent.fetchData(HtmlDocType.lottery));
  }

  @override
  void dispose() {
    _htmlReadingPageBloc.close();
    super.dispose();
  }

  void isBlackoutChange(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: isBlackoutChange,
      child: BlocListener<LocalizationBloc, LocalizationState>(
        listener: (context, state) {
          _htmlReadingPageBloc
              .add(HtmlReadingPageEvent.fetchData(HtmlDocType.lottery));
        },
        child: BlocProvider.value(
          value: _htmlReadingPageBloc,
          child: LayoutHandler(
            mobileView: MobileLotteryTermsPage(),
            desktopView: DesktopLotteryTermsPage(),
            tabletView: TabletLotteryTermsPage(),
          ),
        ),
      ),
    );
  }
}
