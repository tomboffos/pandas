import '../../../../../../core/ui/ui_kit.dart';
import 'lottery_terms.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LotteryTermsPageContent extends StatelessWidget {
  final String htmlData;

  const LotteryTermsPageContent({Key? key, required this.htmlData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(minHeight: MediaQuery.of(context).size.height),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Gap.h1_5,
          NavigationRow(
            currentPageText: AppLocalizations.of(context)!.termsOfDraw,
          ),
          Gap.h1_5,
          LotteryTermsWidget(htmlData: htmlData),
        ],
      ),
    );
  }
}
