import 'package:auto_route/auto_route.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nine_pandas/core/ui/constants/social_links.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../core/ui/ui_kit.dart';

class DesktopAndTabletFooter extends StatefulWidget {
  DesktopAndTabletFooter({Key? key}) : super(key: key);

  static double _height = 193.0;

  @override
  State<DesktopAndTabletFooter> createState() => _DesktopAndTabletFooterState();
}

class _DesktopAndTabletFooterState extends State<DesktopAndTabletFooter> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: AppColors.carcassBackground,
      child: ConstrainedBox(
        constraints: BoxConstraints(minHeight: DesktopAndTabletFooter._height),
        child: Padding(
          padding:
              LayoutHandler.isTabletLayout(context) ? Insets.w2 : Insets.w10,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset(
                    AppLocalizations.of(context)!.logoDarkImage,
                    height: Dimens.iconSize,
                  ),
                  Row(
                    children: [
                      PandasTextButton(
                        onTap: () {
                          final Uri _emailLaunchUri = Uri(
                            scheme: 'mailto',
                            path: 'help@9pandas.ru',
                            query: encodeQueryParameters(<String, String>{
                              'subject': 'Обратная связь 9Pandas',
                            }),
                          );
                          launchUrl(_emailLaunchUri);
                        },
                        text: AppLocalizations.of(context)!.supportCenter,
                        fontSize: 14,
                      ),
                      Gap.w1_5,
                      PandasTextButton(
                        onTap: () {
                          AutoRouter.of(context).navigate(LotteryTermsRoute());
                        },
                        text: AppLocalizations.of(context)!.termsOfDraw,
                        fontSize: 14,
                      ),
                      Gap.w1_5,
                      PandasTextButton(
                        onTap: () {
                          AutoRouter.of(context).navigate(FaqRoute());
                        },
                        text: AppLocalizations.of(context)!.faq,
                        fontSize: 14,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      InkWell(
                        onTap: () {
                          launchUrl(Uri.parse(SocialLinks.youtubeLink));
                        },
                        child: SvgPicture.asset(
                          AppLocalizations.of(context)!.youtubeImage,
                          height: Dimens.iconSize * 1.3,
                        ),
                      ),
                      Gap.w1_5,
                      InkWell(
                        onTap: () {
                          launchUrl(Uri.parse(SocialLinks.tikTokLink));
                        },
                        child: SvgPicture.asset(
                          AppLocalizations.of(context)!.tiktokImage,
                          height: Dimens.iconSize * 1.3,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Text(
                        AppLocalizations.of(context)!.copyright,
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                      Gap.w,
                      PandasTextButton(
                        onTap: () {
                          AutoRouter.of(context).navigate(UserAgreementRoute());
                        },
                        text: AppLocalizations.of(context)!.userAgreement,
                        textDecoration: TextDecoration.underline,
                        fontSize: 12,
                      ),
                      Gap.w,
                      PandasTextButton(
                        onTap: () {
                          AutoRouter.of(context).navigate(PrivacyPolicyRoute());
                        },
                        text: AppLocalizations.of(context)!.privacyPolicy,
                        textDecoration: TextDecoration.underline,
                        fontSize: 12,
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Text(
                        AppLocalizations.of(context)!.madeBy,
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                      Gap.w_5,
                      SvgPicture.asset(
                        AppLocalizations.of(context)!.vidaqImage,
                        height: Dimens.iconSize * 1.5,
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  String encodeQueryParameters(Map<String, String> params) {
    return params.entries
        .map((e) =>
            '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
        .join('&');
  }
}
