import '../../../../core/ui/ui_kit.dart';

// ignore: must_be_immutable
class Blackout extends InheritedWidget {
  final ValueChanged<bool> setBlackoutValue;

  static of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<Blackout>();

  bool isBlackout;

  Blackout({
    Key? key,
    required Widget child,
    required this.isBlackout,
    required this.setBlackoutValue,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(Blackout oldWidget) {
    return this.isBlackout != oldWidget.isBlackout ||
        oldWidget.setBlackoutValue != setBlackoutValue;
  }
}
