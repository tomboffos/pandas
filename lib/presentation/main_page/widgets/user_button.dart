import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/core/ui/on_hover/list_on_hover.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/navigation/auth_guard.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';
import '../../../../core/ui/ui_kit.dart';
import 'blackout_inherited.dart';

class UserButton extends StatefulWidget {
  UserButton({Key? key, this.refreshRequest}) : super(key: key);

  final Function()? refreshRequest;
  @override
  State<UserButton> createState() => _UserButtonState();
}

class _UserButtonState extends State<UserButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () async {
          Blackout.of(context).setBlackoutValue(true);
          await showDialog<String>(
              barrierColor: Colors.transparent,
              useSafeArea: false,
              useRootNavigator: false,
              context: context,
              builder: (BuildContext context) {
                return UserPopup();
              });
          Blackout.of(context).setBlackoutValue(false);
        },
        child: Image.asset(
          AppLocalizations.of(context)!.userIcon,
          height: Dimens.iconSize * 1.5,
        ));
  }
}

class UserPopup extends StatefulWidget {
  UserPopup({
    Key? key,
    this.refreshRequest,
  }) : super(key: key);

  final Function()? refreshRequest;

  static const double _desktopLocalizationPopupHeight = 194;
  static const double _desktopLocalizationPopupWidth = 337;
  static const double _desktopRightLocalizationPopupPosition = 250;
  static const double _tabletRightLocalizationPopupPosition = 32;
  static const double _topLocalizationPopupPosition = 69;

  @override
  State<UserPopup> createState() => _UserPopupState();
}

class _UserPopupState extends State<UserPopup> {
  bool isLogedIn = false;

  @override
  Widget build(BuildContext context) {
    List<AutoRouteGuard>? guards = AutoRouter.of(context).current.route.guards;
    return Material(
      type: MaterialType.transparency,
      child: Stack(
        children: [
          Positioned(
            top: UserPopup._topLocalizationPopupPosition,
            right: LayoutHandler.isTabletLayout(context)
                ? UserPopup._tabletRightLocalizationPopupPosition
                : UserPopup._desktopRightLocalizationPopupPosition,
            child: Container(
              width: UserPopup._desktopLocalizationPopupWidth,
              height: UserPopup._desktopLocalizationPopupHeight,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Dimens.cornerRadius),
                  color: AppColors.carcassBackground),
              child: SingleChildScrollView(
                child: Padding(
                  padding: Insets.w_5,
                  child: PointerInterceptor(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Gap.h1_5,
                          ListOnHover(builder: (isHovered) {
                            return Container(
                              padding: Insets.a + Insets.w4,
                              decoration: BoxDecoration(
                                color: isHovered
                                    ? AppColors.veryDarkGray
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(
                                    Dimens.cornerRadius * 1.5),
                              ),
                              child: PandasTextButton(
                                onTap: () {
                                  Navigator.of(context).pop();
                                  AutoRouter.of(context)
                                      .navigate(ProfileRoute());
                                },
                                text: AppLocalizations.of(context)!
                                    .personalAccount,
                                fontSize: 18,
                              ),
                            );
                          }),
                          ListOnHover(builder: (isHovered) {
                            return Container(
                              padding: Insets.a + Insets.w4,
                              decoration: BoxDecoration(
                                color: isHovered
                                    ? AppColors.veryDarkGray
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(
                                    Dimens.cornerRadius * 1.5),
                              ),
                              child: PandasTextButton(
                                onTap: () {
                                  Navigator.of(context).pop();

                                  AutoRouter.of(context)
                                      .navigate(LibraryRoute());
                                },
                                text: AppLocalizations.of(context)!.yourLibrary,
                                fontSize: 18,
                              ),
                            );
                          }),
                          ListOnHover(builder: (isHovered) {
                            return Container(
                              padding: Insets.a + Insets.w4,
                              decoration: BoxDecoration(
                                color: isHovered
                                    ? AppColors.veryDarkGray
                                    : Colors.transparent,
                                borderRadius: BorderRadius.circular(
                                    Dimens.cornerRadius * 1.5),
                              ),
                              child: PandasTextButton(
                                onTap: () {
                                  BlocProvider.of<UserBloc>(context)
                                      .add(UserEvent.logout());
                                  Navigator.of(context).pop();

                                  guards.forEach((pages) {
                                    if (pages is AuthGuard) {
                                      AutoRouter.of(context)
                                          .navigate(MainRoute());
                                    }
                                  });
                                },
                                text: AppLocalizations.of(context)!.singOut,
                                fontSize: 12,
                                textColor: AppColors.greyTextColor,
                              ),
                            );
                          }),
                          Gap.h,
                        ]),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
