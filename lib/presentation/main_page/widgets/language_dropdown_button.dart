import 'dart:ui';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/constants/countries_code.dart';
import 'package:nine_pandas/core/ui/on_hover/list_on_hover.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

import '../../../../core/ui/ui_kit.dart';
import 'blackout_inherited.dart';

class LanguageDropDownButton extends StatefulWidget {
  LanguageDropDownButton({Key? key, this.refreshRequest}) : super(key: key);

  final Function()? refreshRequest;

  @override
  State<LanguageDropDownButton> createState() => _LanguageDropDownButtonState();
}

class _LanguageDropDownButtonState extends State<LanguageDropDownButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        Blackout.of(context).setBlackoutValue(true);
        await showDialog<String>(
          barrierColor: Colors.transparent,
          useSafeArea: false,
          useRootNavigator: false,
          context: context,
          builder: (BuildContext context) {
            return LanguagePopup(refreshRequest: widget.refreshRequest);
          },
        );
        Blackout.of(context).setBlackoutValue(false);
      },
      child: Card(
        child: Padding(
          padding: Insets.a_25 + Insets.w_5,
          child: BlocBuilder<LocalizationBloc, LocalizationState>(
            builder: (context, state) {
              return state.when(
                ruContent: (locale) => Row(
                  children: [
                    Image.asset(
                      Countries.getCountries(context)
                          .firstWhere(
                              (element) => element.countryCode == locale)
                          .countryFlag,
                      height: Dimens.iconSize,
                    ),
                    Gap.w_5,
                    Text(locale),
                  ],
                ),
                elseContent: (locale) => Row(
                  children: [
                    Image.asset(
                      Countries.getCountries(context)
                          .firstWhere(
                              (element) => element.countryCode == locale)
                          .countryFlag,
                      height: Dimens.iconSize,
                    ),
                    Gap.w_5,
                    Text(locale),
                  ],
                ),
                initial: () => Container(),
              );
            },
          ),
        ),
        elevation: 5,
        clipBehavior: Clip.antiAlias,
        shape: StadiumBorder(
            side: BorderSide(
          color: Colors.white,
          width: 2,
        )),
      ),
    );
  }
}

class LanguagePopup extends StatefulWidget {
  LanguagePopup({
    Key? key,
    this.refreshRequest,
  }) : super(key: key);

  final Function()? refreshRequest;

  static const double _desktopLocalizationPopupHeight = 271;
  static const double _desktopLocalizationPopupWidth = 321;
  static const double _desktopRightLocalizationPopupPosition = 160;
  static const double _tabletRightLocalizationPopupPosition = 32;
  static const double _topLocalizationPopupPosition = 69;
  static const double _mobileLocalizationPopupPadding = 18;

  @override
  State<LanguagePopup> createState() => _LanguagePopupState();
}

class _LanguagePopupState extends State<LanguagePopup> {
  bool isLogedIn = false;

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Stack(
        children: [
          Positioned(
            top: LanguagePopup._topLocalizationPopupPosition,
            right: LayoutHandler.isTabletLayout(context)
                ? LanguagePopup._tabletRightLocalizationPopupPosition
                : LanguagePopup._desktopRightLocalizationPopupPosition,
            child: Container(
              width: LanguagePopup._desktopLocalizationPopupWidth,
              height: LanguagePopup._desktopLocalizationPopupHeight,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Dimens.cornerRadius),
                  color: AppColors.carcassBackground),
              child: SingleChildScrollView(
                child: Padding(
                  padding: Insets.a_5 + Insets.h_75,
                  child: PointerInterceptor(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: Countries.getCountries(context)
                          .map((country) => PopupMenuItem<String>(
                                value: country.countryCode,
                                onTap: () {},
                                child: InkWell(
                                  onTap: () {
                                    getIt.get<LocalizationBloc>().add(
                                        LocalizationEvent.changeLanguage(
                                            country.countryCode));

                                    Navigator.of(context).pop();
                                    Blackout.of(context)!
                                        .setBlackoutValue(false);
                                  },
                                  child: ListOnHover(builder: (isHovered) {
                                    return Container(
                                      decoration: BoxDecoration(
                                          color: isHovered
                                              ? AppColors.veryDarkGray
                                              : Colors.transparent,
                                          borderRadius: BorderRadius.circular(
                                              Dimens.cornerRadius * 1.5)),
                                      padding: Insets.a,
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            country.countryFlag,
                                            height: Dimens.iconSize * 1.5,
                                          ),
                                          Gap.w,
                                          Text(country.countryName,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontFeatures: const [
                                                    FontFeature
                                                        .proportionalFigures()
                                                  ],
                                                  fontSize: 14)),
                                        ],
                                      ),
                                    );
                                  }),
                                ),
                              ))
                          .toList(),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
