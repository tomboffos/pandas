import '../../../../core/ui/ui_kit.dart';
import 'desktop_and_tablet_footer.dart';
import 'mobile/mobile_footer/mobile_footer.dart';

class Footer extends StatelessWidget {
  Footer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutHandler(
      mobileView: MobileFooter(),
      desktopView: DesktopAndTabletFooter(),
      tabletView: DesktopAndTabletFooter(),
    );
  }
}
