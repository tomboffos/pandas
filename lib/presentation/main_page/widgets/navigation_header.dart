import '../../../../core/ui/ui_kit.dart';
import 'desktop_and_tablet_navigation_header.dart';
import 'mobile/mobile_navigation_header/mobile_navigation_header.dart';

class NavigationHeader extends StatelessWidget {
  NavigationHeader({Key? key, this.refreshRequest}) : super(key: key);
  final Function()? refreshRequest;

  @override
  Widget build(BuildContext context) {
    return LayoutHandler(
      mobileView: MobileNavigationHeader(),
      desktopView:
          DesktopAndTabletNavigationHeader(refreshRequest: refreshRequest),
      tabletView:
          DesktopAndTabletNavigationHeader(refreshRequest: refreshRequest),
    );
  }
}
