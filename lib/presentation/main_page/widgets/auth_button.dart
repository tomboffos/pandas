import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_up/sing_up_page.dart';

import 'blackout_inherited.dart';

class AuthButton extends StatelessWidget {
  const AuthButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: PandasTextButton(
        onTap: () async {
          await showDialog<String>(
            barrierColor: Colors.black.withOpacity(0.6),
            barrierDismissible: false,
            context: context,
            builder: (BuildContext context) => SingUpPage(),
          );
        },
        text: AppLocalizations.of(context)!.singInOrSingUp,
        fontSize: 18,
      ),
    );
  }
}
