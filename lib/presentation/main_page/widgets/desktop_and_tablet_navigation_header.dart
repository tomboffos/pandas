import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../core/ui/ui_kit.dart';
import 'auth_button.dart';
import 'language_dropdown_button.dart';
import 'user_button.dart';

class DesktopAndTabletNavigationHeader extends StatefulWidget {
  DesktopAndTabletNavigationHeader({Key? key, this.refreshRequest})
      : super(key: key);
  static double _height = 59.0;
  final Function()? refreshRequest;

  @override
  State<DesktopAndTabletNavigationHeader> createState() =>
      _DesktopAndTabletNavigationHeaderState();
}

class _DesktopAndTabletNavigationHeaderState
    extends State<DesktopAndTabletNavigationHeader> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        return Container(
          width: double.infinity,
          height: DesktopAndTabletNavigationHeader._height,
          color: AppColors.carcassBackground,
          child: Padding(
            padding:
                LayoutHandler.isTabletLayout(context) ? Insets.w2 : Insets.w10,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    AutoRouter.of(context).navigate(MainRoute());
                  },
                  child: SvgPicture.asset(
                    AppLocalizations.of(context)!.logoDarkImage,
                    height: Dimens.iconSize,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    state.maybeWhen(
                      logged: (user) => UserButton(),
                      orElse: () => AuthButton(),
                    ),
                    Gap.w1_5,
                    InkWell(onTap: () {}, child: LanguageDropDownButton()),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
