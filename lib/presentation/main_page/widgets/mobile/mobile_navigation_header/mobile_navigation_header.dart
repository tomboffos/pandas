import 'package:auto_route/auto_route.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'mobile_language_dropdown_button.dart';
import 'mobile_menu_button.dart';

class MobileNavigationHeader extends StatelessWidget {
  const MobileNavigationHeader({Key? key}) : super(key: key);

  static double _height = 59;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MobileNavigationHeader._height,
      color: AppColors.carcassBackground,
      child: Padding(
        padding: Insets.w,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                InkWell(
                  onTap: () => AutoRouter.of(context).navigate(MainRoute()),
                  child: SvgPicture.asset(
                    AppLocalizations.of(context)!.logoDarkImage,
                    height: Dimens.iconSize,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                MobileLanguageDropDownButton(),
                MobileMenuButton(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
