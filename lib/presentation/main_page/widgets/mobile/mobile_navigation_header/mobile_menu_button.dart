import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';
import '../../../../../../core/ui/ui_kit.dart';
import '../../../../../../navigation/router.dart';
import 'mobile_auth_button.dart';

class MobileMenuButton extends StatefulWidget {
  static double _height = 59;

  @override
  State<MobileMenuButton> createState() => MobileMenuButtonState();
}

class MobileMenuButtonState extends State<MobileMenuButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: IconButton(
      icon: Icon(Icons.menu),
      onPressed: () async {
        await showDialog<String>(
          barrierColor: Colors.black.withOpacity(.6),
          context: context,
          builder: (BuildContext context) => UserPopupPage(),
        );
      },
    ));
  }
}

class UserPopupPage extends StatelessWidget {
  UserPopupPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: BlocBuilder<UserBloc, UserState>(
        builder: (context, state) {
          return Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                PointerInterceptor(
                  child: Container(
                    width: double.infinity,
                    height: MobileMenuButton._height,
                    color: AppColors.carcassBackground,
                    child: Padding(
                      padding: Insets.w + Insets.h_5,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  AutoRouter.of(context).navigate(MainRoute());
                                },
                                child: SvgPicture.asset(
                                  AppLocalizations.of(context)!.logoDarkImage,
                                  height: Dimens.iconSize,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text('',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    fontSize: 20,
                                  )),
                              IconButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  icon: Icon(
                                    Icons.close,
                                    size: Dimens.iconSize * 1.5,
                                  ))
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(Dimens.cornerRadius * 1.75),
                      bottomRight: Radius.circular(Dimens.cornerRadius * 1.75)),
                  child: Container(
                    color: AppColors.carcassBackground,
                    child: PointerInterceptor(
                      child: Column(children: [
                        Gap.h_5,
                        state.maybeWhen(
                          logged: (user) => _UserInfo(),
                          orElse: () => Padding(
                            padding: Insets.a2,
                            child: MobileAuthButton(),
                          ),
                        ),
                      ]),
                    ),
                  ),
                ),
              ]);
        },
      ),
    );
  }
}

class _UserInfo extends StatelessWidget {
  const _UserInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        PandasTextButton(
          onTap: () {
            AutoRouter.of(context).removeLast();
            AutoRouter.of(context).navigate(ProfileRoute());
          },
          text: AppLocalizations.of(context)!.account,
          fontSize: 18,
        ),
        Gap.h2,
        Gap.h_5,
        PandasTextButton(
          onTap: () {
            Navigator.of(context).pop();
            AutoRouter.of(context).navigate(LibraryRoute());
          },
          text: AppLocalizations.of(context)!.yourLibrary,
          fontSize: 18,
        ),
        Gap.h2,
        Gap.h_25,
        PandasTextButton(
          onTap: () {
            BlocProvider.of<UserBloc>(context).add(UserEvent.logout());
          },
          text: AppLocalizations.of(context)!.singOut,
          fontSize: 12,
          textColor: AppColors.greyTextColor,
        ),
        Gap.h2,
      ],
    );
  }
}
