import 'package:auto_route/auto_route.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:nine_pandas/core/ui/constants/social_links.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../../core/ui/ui_kit.dart';
import '../../../../../../navigation/router.dart';

class MobileFooter extends StatefulWidget {
  MobileFooter({Key? key}) : super(key: key);

  static double _height = 352.0;

  @override
  State<MobileFooter> createState() => _MobileFooterFooterState();
}

class _MobileFooterFooterState extends State<MobileFooter> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MobileFooter._height,
      color: AppColors.carcassBackground,
      child: Padding(
        padding: Insets.w,
        child: Column(
          children: [
            Column(
              children: [
                Gap.h2,
                SvgPicture.asset(
                  AppLocalizations.of(context)!.logoDarkImage,
                  height: Dimens.iconSize,
                ),
                Gap.h2,
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    PandasTextButton(
                      onTap: () {
                        final Uri _emailLaunchUri = Uri(
                          scheme: 'mailto',
                          path: 'help@9pandas.ru',
                          query: encodeQueryParameters(<String, String>{
                            'subject': 'Обратная связь 9Pandas',
                          }),
                        );
                        launchUrl(_emailLaunchUri);
                      },
                      text: AppLocalizations.of(context)!.supportCenter,
                      fontSize: 14,
                    ),
                    Gap.w1_5,
                    PandasTextButton(
                      onTap: () {
                        AutoRouter.of(context).navigate(LotteryTermsRoute());
                      },
                      text: AppLocalizations.of(context)!.termsOfDraw,
                      fontSize: 14,
                    ),
                    Gap.w1_5,
                    PandasTextButton(
                      onTap: () {
                        AutoRouter.of(context).navigate(FaqRoute());
                      },
                      text: AppLocalizations.of(context)!.faq,
                      fontSize: 14,
                    ),
                  ],
                ),
                Gap.h2,
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        launchUrl(Uri.parse(SocialLinks.youtubeLink));
                      },
                      child: SvgPicture.asset(
                        AppLocalizations.of(context)!.youtubeImage,
                        height: Dimens.iconSize * 1.3,
                      ),
                    ),
                    Gap.w1_5,
                    InkWell(
                      onTap: () {
                        launchUrl(Uri.parse(SocialLinks.tikTokLink));
                      },
                      child: SvgPicture.asset(
                        AppLocalizations.of(context)!.tiktokImage,
                        height: Dimens.iconSize * 1.3,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            Gap.h1_5,
            Divider(),
            Gap.h1_5,
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.copyright,
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    Gap.h,
                    PandasTextButton(
                      onTap: () {
                        AutoRouter.of(context).navigate(UserAgreementRoute());
                      },
                      text: AppLocalizations.of(context)!.userAgreement,
                      textDecoration: TextDecoration.underline,
                      fontSize: 12,
                    ),
                    Gap.h,
                    PandasTextButton(
                      onTap: () {
                        AutoRouter.of(context).navigate(PrivacyPolicyRoute());
                      },
                      text: AppLocalizations.of(context)!.privacyPolicy,
                      textDecoration: TextDecoration.underline,
                      fontSize: 12,
                    ),
                  ],
                ),
                Gap.h1_5,
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      AppLocalizations.of(context)!.madeBy,
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    ),
                    Gap.w_5,
                    SvgPicture.asset(
                      AppLocalizations.of(context)!.vidaqImage,
                      height: Dimens.iconSize * 1.5,
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  String encodeQueryParameters(Map<String, String> params) {
    return params.entries
        .map((e) =>
            '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
        .join('&');
  }
}
