import 'dart:developer';

import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/analytics/bloc/analytics_bloc.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/domain/movies/bloc/bloc/movies_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/change_password/change_password_popup.dart';
import 'package:nine_pandas/presentation/forgot_password/recovery_password_popup/recovery_password_popup.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import 'desktop_main_page/widgets/desktop_main_page.dart';
import 'mobile_main_page/mobile_main_page.dart';
import 'tablet_main_page/tablet_main_page.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => MainPageState();
}

class MainPageState extends State<MainPage> {
  bool isBlackout = false;

  @override
  void initState() {
    super.initState();
    getIt.get<MoviesBloc>().add(MoviesEvent.getMovies());

    getIt<AnalyticsBloc>().add(
      AnalyticsEvent.sendApplicationLaunched(getIt<UserBloc>().state is Logged
          ? (getIt<UserBloc>().state as Logged).user.id
          : null),
    );

    if (Uri.base.queryParameters['hash'] != null) {
      AutoRouter.of(context).navigate(
          NewPasswordRoute(hash: Uri.base.queryParameters['hash'].toString()));
    }
  }

  void setBlackoutChange(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: setBlackoutChange,
      child: MultiBlocListener(
        listeners: [
          BlocListener<LocalizationBloc, LocalizationState>(
            listener: (context, state) {
              getIt.get<MoviesBloc>().add(MoviesEvent.getMovies());
            },
          ),
        ],
        child: BlocProvider.value(
          value: getIt.get<MoviesBloc>(),
          child: LayoutHandler(
            mobileView: MobileMainPage(),
            desktopView: DesktopMainPage(),
            tabletView: TabletMainPage(),
          ),
        ),
      ),
    );
  }
}
