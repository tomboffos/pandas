import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/domain/age_restriction/bloc/age_restriction_bloc.dart';
import 'package:nine_pandas/domain/movies/bloc/bloc/movies_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/age_restriction/presentation/page/age_restriction.dart';
import 'package:nine_pandas/presentation/localization/presentation/pages/tablet_localization_detected/tablet_localization_detected.dart';
import 'package:nine_pandas/presentation/main_page/pages/tablet_main_page/widgets/series_card_item.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import '../../../../../core/ui/ui_kit.dart';

class TabletMainPage extends StatefulWidget {
  const TabletMainPage({Key? key}) : super(key: key);
  static GlobalKey tabletMainPageKey = GlobalKey();

  @override
  State<TabletMainPage> createState() => _TabletMainPageState();
}

class _TabletMainPageState extends State<TabletMainPage> {
  AgeRestrictionBloc _ageRestrictionBloc = getIt.get<AgeRestrictionBloc>();

  @override
  void initState() {
    super.initState();
    _ageRestrictionBloc.add(AgeRestrictionGetConfirmationEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _ageRestrictionBloc,
      child: Scaffold(
        key: TabletMainPage.tabletMainPageKey,
        backgroundColor: AppColors.scaffoldBgColor,
        body: Column(
          children: [
            NavigationHeader(
              refreshRequest: () =>
                  getIt.get<MoviesBloc>().add(MoviesEvent.getMovies()),
            ),
            BlocListener<AgeRestrictionBloc, AgeRestrictionState>(
              listener: (context, state) async {
                if (state is AgeRestrictionNotGetConfirmation) {
                  Blackout.of(TabletMainPage
                          .tabletMainPageKey.currentState!.context)!
                      .setBlackoutValue(true);
                  await showDialog<String>(
                    barrierColor: Colors.transparent,
                    context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context) => AgeRestriction(),
                  );

                  await showDialog<String>(
                    barrierColor: Colors.transparent,
                    context: context,
                    builder: (BuildContext context) =>
                        TabletLocalizationPopup(),
                  );
                }
              },
              child: Expanded(
                child: Stack(
                  children: [
                    SingleChildScrollView(
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                            minHeight: MediaQuery.of(context).size.height),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Gap.h1_5,
                            BlocBuilder<MoviesBloc, MoviesState>(
                              builder: (context, state) {
                                return state.maybeWhen(
                                  dataLoaded: (movies) => Padding(
                                    padding: Insets.w2,
                                    child: Column(
                                      children: [
                                        for (int i = 0; i < movies.length; i++)
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: Dimens.verticalPadding *
                                                    1.5),
                                            child: InkWell(
                                              onTap: () {
                                                if (movies[i].isReleased) {
                                                  AutoRouter.of(context)
                                                      .navigate(
                                                    MovieRoute(
                                                      id: movies[i].id,
                                                    ),
                                                  );
                                                }
                                              },
                                              child: SeriesCardItem(
                                                movieReleaseTag:
                                                    movies[i].data != null
                                                        ? movies[i]
                                                            .data!
                                                            .movieReleaseTag
                                                        : null,
                                                imageUrl:
                                                    movies[i].posters!.tablet,
                                                genres: movies[i].genres!,
                                                title: movies[i].data != null
                                                    ? movies[i].data!.movieTitle
                                                    : 'Скоро',
                                                description:
                                                    movies[i].data != null
                                                        ? movies[i]
                                                            .data!
                                                            .movieSubTitle
                                                        : '',
                                              ),
                                            ),
                                          ),
                                      ],
                                    ),
                                  ),
                                  error: () => PandasServerErrorWidget(
                                    reloadCallback: () {
                                      getIt.get<MoviesBloc>().add(
                                            MoviesEvent.getMovies(),
                                          );
                                    },
                                  ),
                                  orElse: () => PandasLoadingView(),
                                );
                              },
                            ),
                            Gap.h2,
                            Footer(),
                          ],
                        ),
                      ),
                    ),
                    BlackoutContainer(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
