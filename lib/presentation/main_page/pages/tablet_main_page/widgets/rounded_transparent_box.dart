import 'package:nine_pandas/core/ui/ui_kit.dart';

class RoundedTransparentBox extends StatelessWidget {
  const RoundedTransparentBox({
    Key? key,
    required this.text,
    this.isGenre = true,
  }) : super(key: key);

  final String text;
  final bool isGenre;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: isGenre ? null : AppColors.highlightColor,
        borderRadius: BorderRadius.circular(Dimens.cornerRadius * 3),
        border: Border.all(color: isGenre ? Colors.white : Colors.transparent),
      ),
      height: 24,
      padding: Insets.w_75,
      child: Align(
        alignment: Alignment.center,
        child: Text(
          text,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
            fontSize: 12,
          ),
        ),
      ),
    );
  }
}
