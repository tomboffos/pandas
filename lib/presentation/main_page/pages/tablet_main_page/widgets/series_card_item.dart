import 'package:cached_network_image/cached_network_image.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/movie/models/genre/genre.dart';

import 'rounded_transparent_box.dart';

class SeriesCardItem extends StatelessWidget {
  const SeriesCardItem({
    Key? key,
    required this.genres,
    required this.title,
    required this.description,
    this.imageUrl,
    this.movieReleaseTag,
    this.isLibrary = false,
    this.isWatched = true,
  }) : super(key: key);

  final List<Genre> genres;
  final String title;
  final String description;
  final String? imageUrl;
  final String? movieReleaseTag;
  final bool isLibrary;
  final bool isWatched;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.zero,
      elevation: 8.0,
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius * 2.5),
      ),
      child: Stack(
        children: [
          CachedNetworkImage(
            fit: BoxFit.cover,
            colorBlendMode: BlendMode.darken,
            color: Colors.black.withOpacity(0.6),
            imageUrl: imageUrl ??
                'https://i.ytimg.com/vi/Yr0QlzaSFQc/maxresdefault.jpg',
          ),
          Positioned.fill(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RoundedTransparentBox(
                      text: movieReleaseTag ??
                          AppLocalizations.of(context)!.newSeason,
                      isGenre: false,
                    ),
                  ],
                ),
                Gap.h_5,
                Text(
                  title,
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                Gap.h_5,
                Text(
                  description,
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.greyText,
                  ),
                ),
              ],
            ),
          ),
          Positioned.fill(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  height: 48,
                  width: double.infinity,
                  color: AppColors.menuBackground,
                  child: Row(
                    children: genres
                        .map((genre) => Padding(
                              padding: const EdgeInsets.only(
                                  left: Dimens.horizontalPadding * 0.75),
                              child: RoundedTransparentBox(
                                text: genre.name,
                              ),
                            ))
                        .toList(),
                  )),
            ),
          ),
          if (isLibrary)
            Positioned(
              top: Dimens.verticalPadding,
              right: Dimens.horizontalPadding,
              child: Container(
                child: RoundedTransparentBox(
                  text: isWatched
                      ? AppLocalizations.of(context)!.viewed
                      : AppLocalizations.of(context)!.bought,
                ),
              ),
            )
        ],
      ),
    );
  }
}
