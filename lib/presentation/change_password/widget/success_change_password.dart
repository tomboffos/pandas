import 'package:nine_pandas/core/ui/ui_kit.dart';

import 'desktop_success_change_password_page.dart';
import 'mobile_success_change_password_page.dart';

class SuccessChangePasswordPage extends StatefulWidget {
  const SuccessChangePasswordPage({Key? key}) : super(key: key);

  @override
  State<SuccessChangePasswordPage> createState() =>
      _SuccessChangePasswordPageState();
}

class _SuccessChangePasswordPageState extends State<SuccessChangePasswordPage> {
  @override
  Widget build(BuildContext context) {
    return LayoutHandler(
      mobileView: MobileSuccessChangePasswordPage(),
      desktopView: DesktopAndTabletSuccessChangePasswordPage(),
      tabletView: DesktopAndTabletSuccessChangePasswordPage(),
    );
  }
}
