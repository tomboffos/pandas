import 'dart:ui';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/domain/change_password/bloc/change_password_bloc.dart';
import '../../../../core/ui/ui_kit.dart';
import 'widget/success_change_password.dart';

class DesktopAndTabletChangePasswordPopup extends StatelessWidget {
  const DesktopAndTabletChangePasswordPopup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: DesktopAndTabletChangePasswordPopupFormField(),
    );
  }
}

class DesktopAndTabletChangePasswordPopupFormField extends StatefulWidget {
  const DesktopAndTabletChangePasswordPopupFormField({Key? key})
      : super(key: key);

  static const EdgeInsets insets = EdgeInsets.fromLTRB(52.0, 40.0, 52.0, 52.0);

  static const double singInFormFieldHeight = 250;
  static const double singInFormFieldWidth = 556;

  @override
  State<DesktopAndTabletChangePasswordPopupFormField> createState() =>
      _DesktopAndTabletChangePasswordPopupFormFieldState();
}

class _DesktopAndTabletChangePasswordPopupFormFieldState
    extends State<DesktopAndTabletChangePasswordPopupFormField> {
  bool isPasswordValid = false;
  bool isOldPasswordIncorrect = false;
  bool isOldPasswordValid = false;
  bool? isPasswordsNotMatch;
  bool isObscured = true;

  late ChangePasswordBloc _changePasswordBloc =
      BlocProvider.of<ChangePasswordBloc>(context);

  final _formKey = GlobalKey<FormState>();
  TextEditingController _oldPasswordTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();
  TextEditingController _repeatPasswordTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: DesktopAndTabletChangePasswordPopupFormField.insets,
            width: DesktopAndTabletChangePasswordPopupFormField
                .singInFormFieldWidth,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: DesktopAndTabletChangePasswordPopupFormField
                      .singInFormFieldHeight),
              child: Form(
                key: _formKey,
                autovalidateMode: AutovalidateMode.always,
                child: BlocConsumer<ChangePasswordBloc, ChangePasswordState>(
                  listener: (context, state) {
                    state.maybeWhen(
                      incorrectOldPassword: () {
                        isOldPasswordIncorrect = true;
                      },
                      changePasswordSuccess: () async {
                        Navigator.of(context).pop();
                        await showDialog<String>(
                          barrierColor: Colors.black.withOpacity(0.6),
                          context: context,
                          builder: (BuildContext context) =>
                              SuccessChangePasswordPage(),
                        );
                      },
                      orElse: () => Container(),
                    );
                  },
                  builder: (context, state) {
                    return state.maybeWhen(
                        loading: () => Center(
                              child: CircularProgressIndicator(),
                            ),
                        orElse: () => Column(
                              children: [
                                Gap.h2,
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Expanded(
                                      child: Text(
                                        AppLocalizations.of(context)!
                                            .changePassword,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: 26,
                                          fontWeight: FontWeight.w500,
                                          fontFeatures: const [
                                            FontFeature.proportionalFigures()
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Gap.h2,
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      AppLocalizations.of(context)!.oldPassword,
                                      style: TextStyle(
                                        color: AppColors.greyTextColor,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontFeatures: const [
                                          FontFeature.proportionalFigures()
                                        ],
                                      ),
                                    ),
                                    Gap.h_5,
                                    PandasBaseTextInput(
                                      isError: isOldPasswordIncorrect,
                                      controller: _oldPasswordTextController,
                                      hintText: AppLocalizations.of(context)!
                                          .password,
                                      isObscured: isObscured,
                                      onFieldSubmitted: (_) {
                                        FocusScope.of(context).nextFocus();
                                      },
                                      onChanged: (value) {
                                        if (value.length >= 6) {
                                          setState(() {
                                            isOldPasswordValid = true;
                                          });
                                        } else {
                                          setState(() {
                                            isOldPasswordValid = false;
                                          });
                                        }
                                      },
                                      tail: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          IconButton(
                                            icon: Icon(
                                              isObscured
                                                  ? Icons.visibility_outlined
                                                  : Icons
                                                      .visibility_off_outlined,
                                              color: Colors.white,
                                              size: Dimens.iconSize * 1.2,
                                            ),
                                            onPressed: () => setState(() {
                                              isObscured = !isObscured;
                                            }),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Gap.h_25,
                                    if (isOldPasswordIncorrect)
                                      Text(
                                        AppLocalizations.of(context)!
                                            .wrongPassword,
                                        style: TextStyle(
                                          color: AppColors.error,
                                          fontSize: 12,
                                        ),
                                      ),
                                    Gap.h_25,
                                    Gap.h1_25,
                                    Text(
                                      AppLocalizations.of(context)!.password,
                                      style: TextStyle(
                                        color: AppColors.greyTextColor,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontFeatures: const [
                                          FontFeature.proportionalFigures()
                                        ],
                                      ),
                                    ),
                                    Gap.h_5,
                                    PandasBaseTextInput(
                                      controller: _passwordTextController,
                                      hintText: AppLocalizations.of(context)!
                                          .password,
                                      isObscured: isObscured,
                                      onFieldSubmitted: (_) {
                                        FocusScope.of(context).nextFocus();
                                      },
                                      onChanged: (value) {
                                        if (value ==
                                                _repeatPasswordTextController
                                                    .text &&
                                            value.length >= 6) {
                                          setState(() {
                                            isPasswordValid = true;
                                          });
                                        } else {
                                          setState(() {
                                            isPasswordValid = false;
                                          });
                                        }
                                      },
                                      tail: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          IconButton(
                                            icon: Icon(
                                              isObscured
                                                  ? Icons.visibility_outlined
                                                  : Icons
                                                      .visibility_off_outlined,
                                              color: Colors.white,
                                              size: Dimens.iconSize * 1.2,
                                            ),
                                            onPressed: () => setState(() {
                                              isObscured = !isObscured;
                                            }),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Gap.h_25,
                                    Gap.h1_25,
                                    Text(
                                      AppLocalizations.of(context)!
                                          .repeatPassword,
                                      style: TextStyle(
                                        color: AppColors.greyTextColor,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        fontFeatures: const [
                                          FontFeature.proportionalFigures()
                                        ],
                                      ),
                                    ),
                                    Gap.h_5,
                                    PandasBaseTextInput(
                                      controller: _repeatPasswordTextController,
                                      hintText: AppLocalizations.of(context)!
                                          .password,
                                      isError: isPasswordsNotMatch != null
                                          ? isPasswordsNotMatch
                                          : false,
                                      isObscured: isObscured,
                                      onFieldSubmitted: (_) {
                                        FocusScope.of(context).nextFocus();
                                      },
                                      onChanged: (value) {
                                        if (value ==
                                                _passwordTextController.text &&
                                            value.length >= 6) {
                                          setState(() {
                                            isPasswordValid = true;
                                            isPasswordsNotMatch = false;
                                          });
                                        } else {
                                          setState(() {
                                            isPasswordValid = false;
                                            if (value.length >= 6) {
                                              isPasswordsNotMatch = true;
                                            }
                                            if (value.length < 6) {
                                              isPasswordsNotMatch = false;
                                            }
                                          });
                                        }
                                      },
                                      tail: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: [
                                          IconButton(
                                            icon: Icon(
                                              isObscured
                                                  ? Icons.visibility_outlined
                                                  : Icons
                                                      .visibility_off_outlined,
                                              color: Colors.white,
                                              size: Dimens.iconSize * 1.2,
                                            ),
                                            onPressed: () => setState(() {
                                              isObscured = !isObscured;
                                            }),
                                          ),
                                        ],
                                      ),
                                    ),
                                    if (isPasswordsNotMatch ?? false)
                                      TweenAnimationBuilder(
                                        duration: Duration(milliseconds: 300),
                                        curve: Curves.ease,
                                        tween: Tween(begin: 1.0, end: 0.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Text(
                                              AppLocalizations.of(context)!
                                                  .passwordsNotMatch,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 14,
                                                color: AppColors.error,
                                                fontFeatures: const [
                                                  FontFeature
                                                      .proportionalFigures()
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        builder: (context, value, child) {
                                          return Transform.translate(
                                            offset: Offset(
                                              0.0,
                                              double.parse(value.toString()) *
                                                  -5,
                                            ),
                                            child: child,
                                          );
                                        },
                                      ),
                                    Gap.h1_5,
                                    if (isOldPasswordIncorrect)
                                      TweenAnimationBuilder(
                                        duration: Duration(milliseconds: 200),
                                        curve: Curves.ease,
                                        tween: Tween(begin: 1.0, end: 0.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              AppLocalizations.of(context)!
                                                  .oldPasswordIncorrect,
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: AppColors.error,
                                                fontFeatures: const [
                                                  FontFeature
                                                      .proportionalFigures()
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        builder: (context, value, child) {
                                          return Transform.translate(
                                            offset: Offset(
                                              0.0,
                                              double.parse(value.toString()) *
                                                  -5,
                                            ),
                                            child: child,
                                          );
                                        },
                                      ),
                                    state.maybeWhen(
                                      error: (e) => Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!
                                                .errorServer,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 16,
                                              color: AppColors.error,
                                              fontFeatures: const [
                                                FontFeature
                                                    .proportionalFigures()
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                      orElse: () => Container(),
                                    ),
                                    Gap.h1_5,
                                    CustomButton(
                                      onTap: () {
                                        if (isPasswordValid &&
                                            isOldPasswordValid) {
                                          _changePasswordBloc.add(
                                              ChangePasswordEvent
                                                  .changePassword(
                                            _oldPasswordTextController.text,
                                            _passwordTextController.text,
                                            _repeatPasswordTextController.text,
                                          ));
                                        }
                                      },
                                      child: Text(
                                        AppLocalizations.of(context)!.next,
                                        style: TextStyle(
                                          fontSize: 18,
                                          color: isPasswordValid &&
                                                  isOldPasswordValid
                                              ? Colors.white
                                              : AppColors.darkGreyTextColor,
                                          fontFeatures: const [
                                            FontFeature.proportionalFigures()
                                          ],
                                        ),
                                      ),
                                      width: double.infinity,
                                      backgroundColor:
                                          isPasswordValid && isOldPasswordValid
                                              ? AppColors.buttonColor
                                              : AppColors.veryDarkGray,
                                    ),
                                    Gap.h,
                                    CancelButton(
                                      onTap: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Text(
                                        AppLocalizations.of(context)!.back,
                                        style: TextStyle(
                                          fontSize: 18,
                                          fontFeatures: const [
                                            FontFeature.proportionalFigures()
                                          ],
                                        ),
                                      ),
                                      width: double.infinity,
                                      backgroundColor: AppColors.veryDarkGray,
                                    ),
                                  ],
                                ),
                              ],
                            ));
                  },
                ),
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
