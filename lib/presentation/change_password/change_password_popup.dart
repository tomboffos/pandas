import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/change_password/bloc/change_password_bloc.dart';
import 'package:nine_pandas/injectable.dart';

import 'desktop_and_tablet_change_password_popup.dart';
import 'mobile_change_password_popup.dart';

class ChangePasswordPopup extends StatefulWidget {
  const ChangePasswordPopup({Key? key}) : super(key: key);

  @override
  State<ChangePasswordPopup> createState() => _ChangePasswordPopupState();
}

class _ChangePasswordPopupState extends State<ChangePasswordPopup> {
  late ChangePasswordBloc _changePasswordBloc;
  @override
  void initState() {
    super.initState();
    _changePasswordBloc = getIt.get<ChangePasswordBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _changePasswordBloc,
      child: LayoutHandler(
        mobileView: MobileChangePasswordPopup(),
        desktopView: DesktopAndTabletChangePasswordPopup(),
        tabletView: DesktopAndTabletChangePasswordPopup(),
      ),
    );
  }
}
