import 'dart:ui';

import 'package:nine_pandas/presentation/main_page/pages/tablet_main_page/tablet_main_page.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';
import 'package:nine_pandas/presentation/main_page/widgets/language_dropdown_button.dart';

import '../../../../../core/ui/ui_kit.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TabletLocalizationDetectedPage extends StatefulWidget {
  @override
  State<TabletLocalizationDetectedPage> createState() =>
      DesktopLocalizationDetectedPageState();
}

class DesktopLocalizationDetectedPageState
    extends State<TabletLocalizationDetectedPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: IconButton(
      icon: Icon(Icons.menu),
      onPressed: () async {
        await showDialog<String>(
          barrierColor: Colors.black.withOpacity(0.6),
          context: context,
          builder: (BuildContext context) => TabletLocalizationPopup(),
        );
      },
    ));
  }
}

class TabletLocalizationPopup extends StatefulWidget {
  TabletLocalizationPopup({Key? key}) : super(key: key);
  static const double _desktopLocalizationPopupHeight = 200;
  static const double _desktopLocalizationPopupWidth = 418;
  static const double _rightLocalizationPopupPosition = 32;
  static const double _topLocalizationPopupPosition = 69;
  static const double _widthButton = 176;

  @override
  State<TabletLocalizationPopup> createState() =>
      _TabletLocalizationPopupState();
}

class _TabletLocalizationPopupState extends State<TabletLocalizationPopup> {
  bool isLogedIn = false;

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Stack(
        children: [
          Positioned(
            top: TabletLocalizationPopup._topLocalizationPopupPosition,
            right: TabletLocalizationPopup._rightLocalizationPopupPosition,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Dimens.cornerRadius),
                  color: AppColors.carcassBackground),
              width: TabletLocalizationPopup._desktopLocalizationPopupWidth,
              height: TabletLocalizationPopup._desktopLocalizationPopupHeight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .identifiedYourLanguageCorrectly,
                    style: TextStyle(
                      color: AppColors.veryLightGray,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset(
                        AppLocalizations.of(context)!.ruFlagIcon,
                        height: Dimens.iconSize * 1.5,
                      ),
                      Gap.w,
                      Text('United States',
                          style: TextStyle(
                              color: AppColors.darkGreyTextColor,
                              fontWeight: FontWeight.w500,
                              fontFeatures: const [
                                FontFeature.proportionalFigures()
                              ],
                              fontSize: 14)),
                    ],
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      CustomButton(
                          onTap: () {
                            Navigator.of(context).pop();
                            Blackout.of(TabletMainPage
                                    .tabletMainPageKey.currentState!.context)!
                                .setBlackoutValue(false);
                          },
                          width: TabletLocalizationPopup._widthButton,
                          backgroundColor: AppColors.buttonColor,
                          child: Text(
                            AppLocalizations.of(context)!.yes,
                            style: TextStyle(
                              color: AppColors.veryLightGray,
                              fontSize: 18,
                            ),
                          )),
                      Gap.w_75,
                      CancelButton(
                          onTap: () async {
                            Navigator.of(context).pop();

                            await showDialog<String>(
                              barrierColor: Colors.transparent,
                              useSafeArea: false,
                              useRootNavigator: false,
                              context: context,
                              builder: (BuildContext context) {
                                return LanguagePopup();
                              },
                            );
                            Blackout.of(TabletMainPage
                                    .tabletMainPageKey.currentState!.context)!
                                .setBlackoutValue(false);
                          },
                          width: TabletLocalizationPopup._widthButton,
                          child: Text(
                            AppLocalizations.of(context)!.no,
                            style: TextStyle(
                              color: AppColors.veryLightGray,
                              fontSize: 18,
                            ),
                          ))
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
