import 'dart:ui';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/constants/countries_code.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/presentation/main_page/pages/mobile_main_page/mobile_main_page.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';
import 'package:nine_pandas/presentation/main_page/widgets/mobile/mobile_navigation_header/mobile_language_dropdown_button.dart';

import '../../../../../core/ui/ui_kit.dart';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class MobileLocalizationDetectedPage extends StatefulWidget {
  @override
  State<MobileLocalizationDetectedPage> createState() =>
      MobileLocalizationDetectedPageState();
}

class MobileLocalizationDetectedPageState
    extends State<MobileLocalizationDetectedPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: IconButton(
      icon: Icon(Icons.menu),
      onPressed: () async {
        await showDialog<String>(
          barrierColor: Colors.black.withOpacity(0.6),
          context: context,
          builder: (BuildContext context) => MobileLocalizationPopup(),
        );
      },
    ));
  }
}

class MobileLocalizationPopup extends StatefulWidget {
  MobileLocalizationPopup({Key? key}) : super(key: key);
  static const double _desktopLocalizationPopupHeight = 200;
  // static const double _desktopLocalizationPopupWidth = 418;
  static const double _rightLocalizationPopupPosition = 16;
  static const double _topLocalizationPopupPosition = 69;
  // static const double _widthButton = 176;
  @override
  State<MobileLocalizationPopup> createState() =>
      _MobileLocalizationPopupState();
}

class _MobileLocalizationPopupState extends State<MobileLocalizationPopup> {
  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Stack(
        children: [
          Positioned(
            top: MobileLocalizationPopup._topLocalizationPopupPosition,
            right: MobileLocalizationPopup._rightLocalizationPopupPosition,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Dimens.cornerRadius),
                  color: AppColors.carcassBackground),
              width: MediaQuery.of(context).size.width / 1.25,
              height: MobileLocalizationPopup._desktopLocalizationPopupHeight,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .identifiedYourLanguageCorrectly,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: AppColors.veryLightGray,
                    ),
                  ),
                  BlocBuilder<LocalizationBloc, LocalizationState>(
                    builder: (context, state) {
                      return state.when(
                        ruContent: (locale) => Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              Countries.getCountries(context)
                                  .firstWhere((element) =>
                                      element.countryCode == locale)
                                  .countryFlag,
                              height: Dimens.iconSize * 1.5,
                            ),
                            Gap.w,
                            Text(
                                Countries.getCountries(context)
                                    .firstWhere((element) =>
                                        element.countryCode == locale)
                                    .countryName,
                                style: TextStyle(
                                    color: AppColors.darkGreyTextColor,
                                    fontWeight: FontWeight.w500,
                                    fontFeatures: const [
                                      FontFeature.proportionalFigures()
                                    ],
                                    fontSize: 14)),
                          ],
                        ),
                        elseContent: (locale) => Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image.asset(
                              Countries.getCountries(context)
                                  .firstWhere((element) =>
                                      element.countryCode == locale)
                                  .countryFlag,
                              height: Dimens.iconSize * 1.5,
                            ),
                            Gap.w,
                            Text(
                                Countries.getCountries(context)
                                    .firstWhere((element) =>
                                        element.countryCode == locale)
                                    .countryName,
                                style: TextStyle(
                                    color: AppColors.darkGreyTextColor,
                                    fontWeight: FontWeight.w500,
                                    fontFeatures: const [
                                      FontFeature.proportionalFigures()
                                    ],
                                    fontSize: 14)),
                          ],
                        ),
                        initial: () => Container(),
                      );
                    },
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Gap.w1_5,
                      Expanded(
                        child: CustomButton(
                            onTap: () {
                              Navigator.of(context).pop();
                              Blackout.of(MobileMainPage
                                      .mobileMainPageKey.currentState!.context)!
                                  .setBlackoutValue(false);
                            },
                            backgroundColor: AppColors.buttonColor,
                            child: Text(
                              AppLocalizations.of(context)!.yes,
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontSize: 18,
                              ),
                            )),
                      ),
                      Gap.w_75,
                      Expanded(
                        child: CancelButton(
                          onTap: () async {
                            Navigator.of(context).pop();
                            await showDialog<String>(
                              barrierColor: Colors.transparent,
                              useSafeArea: false,
                              context: context,
                              builder: (BuildContext context) {
                                return LanguagePopup();
                              },
                            );
                            Blackout.of(MobileMainPage
                                    .mobileMainPageKey.currentState!.context)!
                                .setBlackoutValue(false);
                          },
                          child: Text(
                            AppLocalizations.of(context)!.no,
                            style: TextStyle(
                              color: AppColors.veryLightGray,
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                      Gap.w1_5,
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
