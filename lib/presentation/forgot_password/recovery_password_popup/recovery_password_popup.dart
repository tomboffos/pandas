import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:nine_pandas/injectable.dart';

import 'widget/desktop_and_tablet_recovery_password_page.dart';
import 'widget/mobile_recovery_password_page.dart';

class RecoveryPasswordPopoup extends StatefulWidget {
  const RecoveryPasswordPopoup({Key? key}) : super(key: key);

  @override
  State<RecoveryPasswordPopoup> createState() => _RecoveryPasswordPopoupState();
}

class _RecoveryPasswordPopoupState extends State<RecoveryPasswordPopoup> {
  late ForgotPasswordBloc _forgotPasswordBloc;

  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = getIt.get<ForgotPasswordBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _forgotPasswordBloc,
      child: LayoutHandler(
        mobileView: MobileRecoveryPasswordField(),
        desktopView: DesktopAndTabletRecoveryPasswordField(),
        tabletView: DesktopAndTabletRecoveryPasswordField(),
      ),
    );
  }
}
