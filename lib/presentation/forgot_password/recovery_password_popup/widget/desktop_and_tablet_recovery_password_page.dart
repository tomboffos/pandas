import 'dart:ui';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/domain/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/bloc/sing_in_recovery_bloc/bloc/sing_in_recovery_bloc_bloc.dart';

import '../../../../../../core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../../../core/utils/string_helper.dart';
import '../email_cofirmation/email_confirmation.dart';

class DesktopAndTabletRecoveryPasswordField extends StatefulWidget {
  const DesktopAndTabletRecoveryPasswordField({Key? key}) : super(key: key);

  static const EdgeInsets insets = EdgeInsets.fromLTRB(52.0, 40.0, 52.0, 52.0);

  static const double singInFormFieldHeight = 250;
  static const double singInFormFieldWidth = 556;

  @override
  State<DesktopAndTabletRecoveryPasswordField> createState() =>
      _DesktopAndTabletRecoveryPasswordFieldState();
}

class _DesktopAndTabletRecoveryPasswordFieldState
    extends State<DesktopAndTabletRecoveryPasswordField> {
  bool isEmailValid = false;
  bool isUserNotExist = false;

  late ForgotPasswordBloc _forgotPasswordBloc;

  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = BlocProvider.of<ForgotPasswordBloc>(context);
  }

  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SingInRecoveryBloc, SingInRecoveryState>(
      builder: (context, state) {
        return SingleChildScrollView(
          child: Stack(
            children: [
              Container(
                padding: DesktopAndTabletRecoveryPasswordField.insets,
                width:
                    DesktopAndTabletRecoveryPasswordField.singInFormFieldWidth,
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      minHeight: DesktopAndTabletRecoveryPasswordField
                          .singInFormFieldHeight),
                  child: Form(
                    key: _formKey,
                    autovalidateMode: AutovalidateMode.always,
                    child:
                        BlocConsumer<ForgotPasswordBloc, ForgotPasswordState>(
                            listener: (context, state) {
                      state.whenOrNull(
                          userNotExists: () => isUserNotExist = true,
                          loaded: () async {
                            Navigator.of(context).pop();
                            await showDialog<String>(
                              barrierDismissible: false,
                              barrierColor: Colors.black.withOpacity(0.6),
                              context: context,
                              builder: (BuildContext context) =>
                                  EmailConfirmationPage(
                                      email: _emailTextController.text),
                            );
                          });
                    }, builder: (context, state) {
                      return state.maybeWhen(
                        loading: () => Center(
                          child: CircularProgressIndicator(),
                        ),
                        orElse: () => Column(
                          children: [
                            Gap.h2,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Text(
                                    AppLocalizations.of(context)!
                                        .recoverPassword,
                                    textAlign: TextAlign.center,
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500,
                                      fontFeatures: const [
                                        FontFeature.proportionalFigures()
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Gap.h2,
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  AppLocalizations.of(context)!.email,
                                  style: TextStyle(
                                    color: AppColors.greyTextColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                Gap.h_5,
                                PandasBaseTextInput(
                                  isError: isUserNotExist,
                                  controller: _emailTextController,
                                  hintText: AppLocalizations.of(context)!.email,
                                  onFieldSubmitted: (_) {
                                    FocusScope.of(context).nextFocus();
                                  },
                                  onChanged: (value) {
                                    if (isEmailAddress(value)) {
                                      setState(() {
                                        isEmailValid = true;
                                      });
                                    } else {
                                      setState(() {
                                        isEmailValid = false;
                                      });
                                    }
                                  },
                                  tail: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Gap.w_5,
                                    ],
                                  ),
                                ),
                                Gap.h1_5,
                                if (isUserNotExist)
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        AppLocalizations.of(context)!
                                            .userNotExists,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 16,
                                          color: AppColors.error,
                                          fontFeatures: const [
                                            FontFeature.proportionalFigures()
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                Gap.h_25,
                                Gap.h1_25,
                                CustomButton(
                                  onTap: () async {
                                    if (isEmailValid) {
                                      _forgotPasswordBloc.add(
                                          ForgotPasswordEvent.recoveryPassword(
                                              _emailTextController.text));
                                    }
                                  },
                                  child: Text(
                                    AppLocalizations.of(context)!.send,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: isEmailValid
                                          ? Colors.white
                                          : AppColors.darkGreyTextColor,
                                      fontFeatures: const [
                                        FontFeature.proportionalFigures()
                                      ],
                                    ),
                                  ),
                                  width: double.infinity,
                                  backgroundColor: isEmailValid
                                      ? AppColors.buttonColor
                                      : AppColors.veryDarkGray,
                                ),
                                Gap.h,
                                CancelButton(
                                  onTap: () {
                                    context
                                        .read<SingInRecoveryBloc>()
                                        .add(OpenSingInFieldEvent());
                                  },
                                  child: Text(
                                    AppLocalizations.of(context)!.back,
                                    style: TextStyle(
                                      fontSize: 18,
                                      fontFeatures: const [
                                        FontFeature.proportionalFigures()
                                      ],
                                    ),
                                  ),
                                  width: double.infinity,
                                  backgroundColor: AppColors.veryDarkGray,
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    }),
                  ),
                ),
              ),
              Positioned(
                right: 12,
                top: 12,
                child: IconButton(
                  onPressed: () => Navigator.of(context).pop(),
                  icon: Icon(
                    Icons.close,
                    color: AppColors.greyText,
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
