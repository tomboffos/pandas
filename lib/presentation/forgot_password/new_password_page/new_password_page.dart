import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import 'widget/desktop_and_tablet_new_password_page.dart';
import 'widget/mobile_new_password_page.dart';

class NewPasswordPage extends StatefulWidget {
  final String hash;
  const NewPasswordPage({
    Key? key,
    @PathParam('hash') required this.hash,
  }) : super(key: key);

  @override
  State<NewPasswordPage> createState() => _NewPasswordPageState();
}

class _NewPasswordPageState extends State<NewPasswordPage> {
  bool isBlackout = false;

  void isBlackoutChange(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  late ForgotPasswordBloc _forgotPasswordBloc;

  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = getIt.get<ForgotPasswordBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: isBlackoutChange,
      child: BlocProvider.value(
        value: _forgotPasswordBloc,
        child: LayoutHandler(
          mobileView: MobileNewPasswordPage(hash: widget.hash),
          desktopView: DesktopAndTabletNewPasswordPage(
            hash: widget.hash,
          ),
          tabletView: DesktopAndTabletNewPasswordPage(
            hash: widget.hash,
          ),
        ),
      ),
    );
  }
}
