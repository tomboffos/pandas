import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/domain/forgot_password/bloc/forgot_password_bloc.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import '../../../../../../core/ui/ui_kit.dart';
// ignore: avoid_web_libraries_in_flutter
import 'dart:html' as html;

class MobileNewPasswordPage extends StatefulWidget {
  final String hash;
  const MobileNewPasswordPage({Key? key, required this.hash}) : super(key: key);
  static const EdgeInsets insets = EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0);

  static const double singInFormFieldHeight = 250;
  static const double singInFormFieldWidth = 556;

  @override
  State<MobileNewPasswordPage> createState() => _MobileNewPasswordPageState();
}

class _MobileNewPasswordPageState extends State<MobileNewPasswordPage> {
  bool isPasswordValid = false;
  bool isObscured = true;
  bool? isPasswordsNotMatch;

  late ForgotPasswordBloc _forgotPasswordBloc;

  @override
  void initState() {
    super.initState();
    _forgotPasswordBloc = BlocProvider.of<ForgotPasswordBloc>(context);
  }

  final _formKey = GlobalKey<FormState>();
  TextEditingController _passwordTextController = TextEditingController();
  TextEditingController _repeatPasswordTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.scaffoldBgColor,
      child: Column(
        children: [
          NavigationHeader(),
          BlocConsumer<ForgotPasswordBloc, ForgotPasswordState>(
            listener: (context, state) {
              state.whenOrNull(
                loaded: () => AutoRouter.of(context).navigate(MainRoute()),
              );
            },
            builder: (context, state) {
              return state.maybeWhen(
                loading: () => PandasLoadingView(),
                error: () => PandasServerErrorWidget(
                    reloadCallback: () => html.window.location.reload()),
                orElse: () => Expanded(
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          children: [
                            Column(
                              children: [
                                Gap.h1_5,
                                Padding(
                                  padding: Insets.w,
                                  child: NavigationRow(
                                    rootPageText: AppLocalizations.of(context)!
                                        .personalAccount,
                                    currentPageText:
                                        AppLocalizations.of(context)!.profile,
                                  ),
                                ),
                                Container(
                                  padding: MobileNewPasswordPage.insets,
                                  width: MobileNewPasswordPage
                                      .singInFormFieldWidth,
                                  child: ConstrainedBox(
                                    constraints: BoxConstraints(
                                        minHeight: MobileNewPasswordPage
                                            .singInFormFieldHeight),
                                    child: Form(
                                      key: _formKey,
                                      autovalidateMode: AutovalidateMode.always,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Expanded(
                                                child: Text(
                                                  AppLocalizations.of(context)!
                                                      .comeUpANewPassword,
                                                  textAlign: TextAlign.center,
                                                  maxLines: 2,
                                                  style: TextStyle(
                                                      fontSize: 24,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontFeatures: const [
                                                        FontFeature
                                                            .proportionalFigures()
                                                      ],
                                                      fontFamily: 'Roboto'),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Gap.h2,
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                AppLocalizations.of(context)!
                                                    .password,
                                                style: TextStyle(
                                                    color:
                                                        AppColors.greyTextColor,
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily: 'Roboto'),
                                              ),
                                              Gap.h_5,
                                              PandasBaseTextInput(
                                                controller:
                                                    _passwordTextController,
                                                hintText: AppLocalizations.of(
                                                        context)!
                                                    .password,
                                                isObscured: isObscured,
                                                onFieldSubmitted: (_) {
                                                  FocusScope.of(context)
                                                      .nextFocus();
                                                },
                                                onChanged: (value) {
                                                  if (value ==
                                                          _repeatPasswordTextController
                                                              .text &&
                                                      value.length >= 6) {
                                                    setState(() {
                                                      isPasswordValid = true;
                                                    });
                                                  } else {
                                                    setState(() {
                                                      isPasswordValid = false;
                                                    });
                                                  }
                                                },
                                                tail: Row(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    IconButton(
                                                      icon: Icon(
                                                        isObscured
                                                            ? Icons
                                                                .visibility_outlined
                                                            : Icons
                                                                .visibility_off_outlined,
                                                        color: Colors.white,
                                                        size: Dimens.iconSize *
                                                            1.2,
                                                      ),
                                                      onPressed: () =>
                                                          setState(() {
                                                        isObscured =
                                                            !isObscured;
                                                      }),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              Gap.h_25,
                                              Gap.h1_25,
                                              Text(
                                                AppLocalizations.of(context)!
                                                    .repeatPassword,
                                                style: TextStyle(
                                                    color:
                                                        AppColors.greyTextColor,
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.w400,
                                                    fontFamily: 'Roboto'),
                                              ),
                                              Gap.h_5,
                                              PandasBaseTextInput(
                                                controller:
                                                    _repeatPasswordTextController,
                                                hintText: AppLocalizations.of(
                                                        context)!
                                                    .password,
                                                isError:
                                                    isPasswordsNotMatch != null
                                                        ? isPasswordsNotMatch
                                                        : false,
                                                isObscured: isObscured,
                                                onFieldSubmitted: (_) {
                                                  FocusScope.of(context)
                                                      .nextFocus();
                                                },
                                                onChanged: (value) {
                                                  if (value ==
                                                          _passwordTextController
                                                              .text &&
                                                      value.length >= 6) {
                                                    setState(() {
                                                      isPasswordValid = true;
                                                      isPasswordsNotMatch =
                                                          false;
                                                    });
                                                  } else {
                                                    setState(() {
                                                      isPasswordValid = false;
                                                      if (value.length >= 6) {
                                                        isPasswordsNotMatch =
                                                            true;
                                                      }
                                                      if (value.length < 6) {
                                                        isPasswordsNotMatch =
                                                            false;
                                                      }
                                                    });
                                                  }
                                                },
                                                tail: Row(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: [
                                                    IconButton(
                                                      icon: Icon(
                                                        isObscured
                                                            ? Icons
                                                                .visibility_outlined
                                                            : Icons
                                                                .visibility_off_outlined,
                                                        color: Colors.white,
                                                        size: Dimens.iconSize *
                                                            1.2,
                                                      ),
                                                      onPressed: () =>
                                                          setState(() {
                                                        isObscured =
                                                            !isObscured;
                                                      }),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              if (isPasswordsNotMatch ?? false)
                                                TweenAnimationBuilder(
                                                  duration: Duration(
                                                      milliseconds: 300),
                                                  curve: Curves.ease,
                                                  tween: Tween(
                                                      begin: 1.0, end: 0.0),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Text(
                                                        AppLocalizations.of(
                                                                context)!
                                                            .passwordsNotMatch,
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                          fontSize: 14,
                                                          color:
                                                              AppColors.error,
                                                          fontFeatures: const [
                                                            FontFeature
                                                                .proportionalFigures()
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  builder:
                                                      (context, value, child) {
                                                    return Transform.translate(
                                                      offset: Offset(
                                                        0.0,
                                                        double.parse(value
                                                                .toString()) *
                                                            -5,
                                                      ),
                                                      child: child,
                                                    );
                                                  },
                                                ),
                                              Gap.h_25,
                                              Gap.h1_5,
                                              CustomButton(
                                                onTap: () {
                                                  if (isPasswordValid) {
                                                    _forgotPasswordBloc.add(
                                                        ForgotPasswordEvent.newPassword(
                                                            widget.hash,
                                                            _passwordTextController
                                                                .text,
                                                            _repeatPasswordTextController
                                                                .text));
                                                  }
                                                },
                                                child: Text(
                                                  AppLocalizations.of(context)!
                                                      .next,
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      color: isPasswordValid
                                                          ? Colors.white
                                                          : AppColors
                                                              .darkGreyTextColor,
                                                      fontFeatures: const [
                                                        FontFeature
                                                            .proportionalFigures()
                                                      ],
                                                      fontFamily: 'Roboto'),
                                                ),
                                                width: double.infinity,
                                                backgroundColor: isPasswordValid
                                                    ? AppColors.buttonColor
                                                    : AppColors.veryDarkGray,
                                              ),
                                              Gap.h,
                                              CancelButton(
                                                onTap: () {
                                                  AutoRouter.of(context)
                                                      .navigate(MainRoute());
                                                },
                                                child: Text(
                                                  AppLocalizations.of(context)!
                                                      .cancel,
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontFeatures: const [
                                                        FontFeature
                                                            .proportionalFigures()
                                                      ],
                                                      fontFamily: 'Roboto'),
                                                ),
                                                width: double.infinity,
                                                backgroundColor:
                                                    AppColors.veryDarkGray,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Gap.h4,
                            Footer(),
                          ],
                        ),
                      ),
                      BlackoutContainer(),
                    ],
                  ),
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
