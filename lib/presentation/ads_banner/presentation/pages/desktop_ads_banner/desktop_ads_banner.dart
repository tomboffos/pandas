import 'package:cached_network_image/cached_network_image.dart';
import 'package:nine_pandas/data/movie/models/ads_banner/ads_banner.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../core/ui/ui_kit.dart';

class DesktopAdsBannerPage extends StatefulWidget {
  const DesktopAdsBannerPage({Key? key, required this.adsBanner})
      : super(key: key);
  final AdsBanner adsBanner;
  @override
  State<DesktopAdsBannerPage> createState() => _DesktopAdsBannerPageState();
}

class _DesktopAdsBannerPageState extends State<DesktopAdsBannerPage> {
  Uri? _url;
  @override
  void initState() {
    super.initState();
    _url = Uri.parse(widget.adsBanner.link ?? '');
  }

  Future<void> _launchUrl() async {
    if (widget.adsBanner.link != null) {
      await launchUrl(_url!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Insets.w10,
      child: InkWell(
        onTap: _launchUrl,
        child: CachedNetworkImage(
          imageBuilder: (context, imageProvider) => Container(
            width: double.infinity,
            height: 190,
            decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.all(Radius.circular(Dimens.cornerRadius * 1.75)),
              image: DecorationImage(
                image: imageProvider,
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          imageUrl: widget.adsBanner.images.desktop,
        ),
      ),
    );
  }
}
