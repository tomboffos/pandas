import 'package:nine_pandas/core/ui/ui_kit.dart';
import '../../../../../data/movie/models/ads_banner/ads_banner.dart'
    as ads_banner;

import 'desktop_ads_banner/desktop_ads_banner.dart';
import 'mobile_ads_banner/mobile_ads_banner.dart';
import 'tablet_ads_banner/tablet_ads_banner.dart';

class AdsBanner extends StatelessWidget {
  AdsBanner({Key? key, required this.adsBanner}) : super(key: key);
  final ads_banner.AdsBanner adsBanner;
  @override
  Widget build(BuildContext context) {
    return LayoutHandler(
      mobileView: MobileAdsBannerPage(adsBanner: adsBanner),
      desktopView: DesktopAdsBannerPage(adsBanner: adsBanner),
      tabletView: TabletAdsBannerPage(adsBanner: adsBanner),
    );
  }
}
