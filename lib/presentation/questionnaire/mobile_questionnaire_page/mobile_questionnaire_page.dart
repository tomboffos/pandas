import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/core/ui/formatters/birthdate_formatter.dart';
import 'package:nine_pandas/core/ui/formatters/number_formatter.dart';
import 'package:nine_pandas/core/utils/string_helper.dart';

import '../../../../../core/ui/ui_kit.dart';

class MobileQuestionnairePage extends StatelessWidget {
  const MobileQuestionnairePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: const EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: MobileQuestionnairePopup(),
    );
  }
}

class MobileQuestionnairePopup extends StatefulWidget {
  const MobileQuestionnairePopup({Key? key}) : super(key: key);

  final String? oldEmail = "test@wre.ru";
  final String? oldName = "qweq";
  final String? oldPhone = "";
  final String? oldBirthdate = "";

  static const EdgeInsets insets = EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0);

  static const double questionnaireFormFieldHeight = 500;
  static const double questionnaireFormFieldWidth = 556;

  @override
  State<MobileQuestionnairePopup> createState() =>
      _MobileQuestionnairePopupState();
}

class _MobileQuestionnairePopupState extends State<MobileQuestionnairePopup> {
  late bool isNameValid;
  late bool isEmailValid;
  late bool isPhoneValid;
  late bool isBirthdateValid;
  bool isMan = false;
  bool isWoman = false;
  bool isMail = false;
  bool isSMS = false;
  bool isChecked = false;

  final _formKey = GlobalKey<FormState>();
  TextEditingController? _nameTextController;
  TextEditingController? _emailTextController;
  TextEditingController? _phoneTextController;
  TextEditingController? _birthdateTextController;

  @override
  void initState() {
    super.initState();
    _nameTextController = TextEditingController(text: widget.oldName);
    _emailTextController = TextEditingController(text: widget.oldEmail);
    _phoneTextController = TextEditingController(text: widget.oldPhone);
    _birthdateTextController = TextEditingController(text: widget.oldBirthdate);

    if (widget.oldName != null)
      isNameValid = widget.oldName!.length > 3 ? true : false;
    if (widget.oldEmail != null)
      isEmailValid = isEmailAddress(widget.oldEmail) ? true : false;
    if (widget.oldPhone != null)
      isPhoneValid = widget.oldPhone!.length > 3 ? true : false;
    if (widget.oldBirthdate != null)
      isBirthdateValid = isValidDate(widget.oldBirthdate) ? true : false;
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: MobileQuestionnairePopup.insets,
            width: MobileQuestionnairePopup.questionnaireFormFieldWidth,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight:
                      MobileQuestionnairePopup.questionnaireFormFieldHeight),
              child: Column(
                children: [
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Text(
                              AppLocalizations.of(context)!
                                  .fillOutQuestionnaireForLottery,
                              textAlign: TextAlign.center,
                              maxLines: 2,
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                                fontFeatures: const [
                                  FontFeature.proportionalFigures()
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Form(
                        key: _formKey,
                        autovalidateMode: AutovalidateMode.always,
                        child: Column(
                          children: [
                            Gap.h1_5,
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  AppLocalizations.of(context)!.name,
                                  style: TextStyle(
                                    color: AppColors.greyTextColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                Gap.h_5,
                                PandasBaseTextInput(
                                  controller: _nameTextController,
                                  hintText: AppLocalizations.of(context)!.name,
                                  onFieldSubmitted: (_) {
                                    FocusScope.of(context).nextFocus();
                                  },
                                  onChanged: (value) {
                                    if (value.length >= 3 &&
                                        value != widget.oldName) {
                                      setState(() {
                                        isNameValid = true;
                                      });
                                    } else {
                                      setState(() {
                                        isNameValid = false;
                                      });
                                    }
                                  },
                                ),
                                Gap.h_75,
                                Text(
                                  AppLocalizations.of(context)!.email,
                                  style: TextStyle(
                                    color: AppColors.greyTextColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                Gap.h_5,
                                PandasBaseTextInput(
                                  controller: _emailTextController,
                                  hintText: AppLocalizations.of(context)!.email,
                                  onFieldSubmitted: (_) {
                                    FocusScope.of(context).nextFocus();
                                  },
                                  onChanged: (value) {
                                    if (isEmailAddress(value) &&
                                        value != widget.oldEmail) {
                                      setState(() {
                                        isEmailValid = true;
                                      });
                                    } else {
                                      setState(() {
                                        isEmailValid = false;
                                      });
                                    }
                                  },
                                ),
                                Gap.h_75,
                                Text(
                                  AppLocalizations.of(context)!.phone,
                                  style: TextStyle(
                                    color: AppColors.greyTextColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                Gap.h_5,
                                PandasBaseTextInput(
                                  controller: _phoneTextController,
                                  hintText: AppLocalizations.of(context)!
                                      .enterYourPhoneNumber,
                                  onFieldSubmitted: (_) {
                                    FocusScope.of(context).nextFocus();
                                  },
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly,
                                    NumberTextInputFormatter(),
                                  ],
                                  onChanged: (value) {
                                    if (value.length >= 6 &&
                                        value != widget.oldPhone) {
                                      setState(() {
                                        isPhoneValid = true;
                                      });
                                    } else {
                                      setState(() {
                                        isPhoneValid = false;
                                      });
                                    }
                                  },
                                ),
                                Gap.h_75,
                                Text(
                                  AppLocalizations.of(context)!.age,
                                  style: TextStyle(
                                    color: AppColors.greyTextColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                Gap.h_5,
                                PandasBaseTextInput(
                                  hintText:
                                      AppLocalizations.of(context)!.ageExample,
                                  controller: _birthdateTextController,
                                  onFieldSubmitted: (_) {
                                    FocusScope.of(context).nextFocus();
                                  },
                                  inputFormatters: [
                                    FilteringTextInputFormatter.digitsOnly,
                                    BirthdateFormatter(),
                                  ],
                                  onChanged: (value) {
                                    if (isValidDate(value) &&
                                        value != widget.oldBirthdate) {
                                      setState(() {
                                        isBirthdateValid = true;
                                      });
                                    } else {
                                      setState(() {
                                        isBirthdateValid = false;
                                      });
                                    }
                                  },
                                ),
                                Gap.h,
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      AppLocalizations.of(context)!.yourGender,
                                      style: TextStyle(
                                        color: AppColors.darkGreyTextColor,
                                        fontSize: 16,
                                      ),
                                    ),
                                    Row(
                                      children: [
                                        PandasCheckbox(
                                            value: isMan,
                                            onChanged: (value) {
                                              setState(() {
                                                isMan = true;
                                                isWoman = false;
                                              });
                                            }),
                                        Gap.w_5,
                                        Gap.w_25,
                                        Text(
                                          AppLocalizations.of(context)!.man,
                                        ),
                                        Gap.w_5,
                                        Gap.w_25,
                                        PandasCheckbox(
                                            value: isWoman,
                                            onChanged: (value) {
                                              setState(() {
                                                isWoman = true;
                                                isMan = false;
                                              });
                                            }),
                                        Gap.w_5,
                                        Gap.w_25,
                                        Text(
                                          AppLocalizations.of(context)!.woman,
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                                Gap.h1_25,
                              ],
                            ),
                            Gap.h1_25,
                            Row(
                              children: [
                                PandasCheckbox(
                                    size: 22,
                                    value: isChecked,
                                    onChanged: (value) {
                                      setState(() {
                                        isChecked = value;
                                      });
                                    }),
                                Gap.w_5,
                                Gap.w_25,
                                Flexible(
                                  child: RichText(
                                    maxLines: 3,
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                          text: AppLocalizations.of(context)!
                                              .agreeWith,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                          ),
                                        ),
                                        TextSpan(
                                          text: AppLocalizations.of(context)!
                                              .byPrivacyPolicy,
                                          recognizer: TapGestureRecognizer()
                                            ..onTap =
                                                () => print('Tap Here onTap'),
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            color: Colors.white,
                                            fontSize: 12,
                                          ),
                                        ),
                                        TextSpan(
                                          text:
                                              AppLocalizations.of(context)!.and,
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                          ),
                                        ),
                                        TextSpan(
                                          text: AppLocalizations.of(context)!
                                              .termsOfUse,
                                          recognizer: TapGestureRecognizer()
                                            ..onTap =
                                                () => print('Tap Here onTap'),
                                          style: TextStyle(
                                            decoration:
                                                TextDecoration.underline,
                                            color: Colors.white,
                                            fontSize: 12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Gap.h1_5,
                            CustomButton(
                              onTap: () async {
                                if (isEmailValid &&
                                    isPhoneValid &&
                                    isNameValid &&
                                    isBirthdateValid &&
                                    isChecked &&
                                    (isWoman || isMan)) {}
                              },
                              child: Text(
                                AppLocalizations.of(context)!
                                    .participateInTheCompetition,
                                style: TextStyle(
                                  fontSize: 18,
                                  color: isEmailValid &&
                                          isPhoneValid &&
                                          isNameValid &&
                                          isBirthdateValid &&
                                          isChecked &&
                                          (isWoman || isMan)
                                      ? Colors.white
                                      : AppColors.darkGreyTextColor,
                                  fontFeatures: const [
                                    FontFeature.proportionalFigures()
                                  ],
                                ),
                              ),
                              width: double.infinity,
                              backgroundColor: isEmailValid &&
                                      isPhoneValid &&
                                      isNameValid &&
                                      isBirthdateValid &&
                                      isChecked &&
                                      (isWoman || isMan)
                                  ? AppColors.buttonColor
                                  : AppColors.veryDarkGray,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
