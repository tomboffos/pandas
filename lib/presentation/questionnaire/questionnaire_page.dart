import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';
import 'package:pointer_interceptor/pointer_interceptor.dart';

import '../../../../core/ui/ui_kit.dart';
import 'desktop_questionnaire_page/desktop_questionnaire_page.dart';
import 'mobile_questionnaire_page/mobile_questionnaire_page.dart';
import 'tablet_questionnaire_page/tablet_questionnaire_page.dart';

class QuestionnairePage extends StatefulWidget {
  const QuestionnairePage({Key? key}) : super(key: key);

  @override
  State<QuestionnairePage> createState() => _QuestionnairePageState();
}

class _QuestionnairePageState extends State<QuestionnairePage> {
  bool isBlackout = false;

  void setBlackoutValue(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: setBlackoutValue,
      child: PointerInterceptor(
        child: LayoutHandler(
          mobileView: MobileQuestionnairePage(),
          desktopView: DesktopQuestionnairePage(),
          tabletView: TableQuestionnairePage(),
        ),
      ),
    );
  }
}
