import 'dart:ui';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../../core/ui/ui_kit.dart';

class TabletTrailerPage extends StatelessWidget {
  const TabletTrailerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: TabletTrailerPopup(),
    );
  }
}

class TabletTrailerPopup extends StatefulWidget {
  const TabletTrailerPopup({Key? key}) : super(key: key);

  static const EdgeInsets insets = EdgeInsets.fromLTRB(41.0, 40.0, 41.0, 52.0);

  static const double singInFormFieldHeight = 400;
  static const double singInFormFieldWidth = 556;

  @override
  State<TabletTrailerPopup> createState() => _TabletTrailerPopupState();
}

class _TabletTrailerPopupState extends State<TabletTrailerPopup> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
