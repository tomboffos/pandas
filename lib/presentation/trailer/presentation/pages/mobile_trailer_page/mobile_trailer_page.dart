import 'dart:ui';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../../core/ui/ui_kit.dart';

class MobileTrailerPage extends StatelessWidget {
  const MobileTrailerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: const EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: MobileTrailerPopup(),
    );
  }
}

class MobileTrailerPopup extends StatefulWidget {
  const MobileTrailerPopup({Key? key}) : super(key: key);

  @override
  State<MobileTrailerPopup> createState() => _MobileTrailerPopupState();
}

class _MobileTrailerPopupState extends State<MobileTrailerPopup> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
