import 'dart:ui';

import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../../../../../core/ui/ui_kit.dart';

class DesktopTrailerPage extends StatelessWidget {
  const DesktopTrailerPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: DesktopTrailerPopup(),
    );
  }
}

class DesktopTrailerPopup extends StatefulWidget {
  const DesktopTrailerPopup({Key? key}) : super(key: key);

  @override
  State<DesktopTrailerPopup> createState() => _DesktopTrailerPopupState();
}

class _DesktopTrailerPopupState extends State<DesktopTrailerPopup> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
