import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import '../../../../core/ui/ui_kit.dart';
import 'desktop_trailer_page/desktop_trailer_page.dart';
import 'mobile_trailer_page/mobile_trailer_page.dart';
import 'tablet_trailer_page/tablet_trailer_page.dart';

class TrailerPage extends StatefulWidget {
  const TrailerPage({Key? key}) : super(key: key);

  @override
  State<TrailerPage> createState() => _TrailerPageState();
}

class _TrailerPageState extends State<TrailerPage> {
  bool isBlackout = false;

  void setBlackoutValue(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: setBlackoutValue,
      child: LayoutHandler(
        mobileView: MobileTrailerPage(),
        desktopView: DesktopTrailerPage(),
        tabletView: TabletTrailerPage(),
      ),
    );
  }
}
