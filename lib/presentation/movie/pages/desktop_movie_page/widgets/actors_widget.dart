import 'package:cached_network_image/cached_network_image.dart';
import 'package:nine_pandas/core/ui/scroll_behavior.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/data/movie/models/actor/actor.dart';

class ActorsWidget extends StatefulWidget {
  const ActorsWidget({
    Key? key,
    required this.actors,
  }) : super(key: key);

  static const double _height = 272;
  static const double _frameDiameter = 240;

  final List<Actor> actors;

  @override
  State<ActorsWidget> createState() => _ActorsWidgetState();
}

class _ActorsWidgetState extends State<ActorsWidget> {
  final ScrollController _scrollController = ScrollController();
  static const _scrollDuration = 500;
  double _scrollPosition = 0;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      setState(() {
        _scrollPosition = _scrollController.position.pixels;
      });
    });

    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        _scrollPosition = _scrollController.position.pixels;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: ActorsWidget._height,
          child: ScrollConfiguration(
            behavior: PandasScrollBehavior(),
            child: ListView.builder(
              controller: _scrollController,
              scrollDirection: Axis.horizontal,
              itemCount: widget.actors.length,
              itemBuilder: ((context, index) => Padding(
                    padding: const EdgeInsets.only(
                        right: Dimens.horizontalPadding * 1.5),
                    child: Column(
                      children: [
                        Card(
                          margin: Insets.zero,
                          elevation: 0,
                          clipBehavior: Clip.antiAlias,
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.circular(Dimens.cornerRadius * 2),
                          ),
                          child: CachedNetworkImage(
                            imageUrl: widget.actors[index].image,
                            width: ActorsWidget._frameDiameter,
                            height: ActorsWidget._frameDiameter,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Gap.h_5,
                        Text(
                          widget.actors[index].name,
                          textAlign: TextAlign.center,
                          style: const TextStyle(fontSize: 18, height: 1.3),
                        )
                      ],
                    ),
                  )),
            ),
          ),
        ),
        if (_scrollController.hasClients &&
            _scrollPosition != 0 &&
            _scrollController.position.maxScrollExtent > 0)
          Positioned(
            left: Dimens.horizontalPadding * 10,
            top: ActorsWidget._height / 3,
            child: InkWell(
              onTap: () {
                _scrollController.animateTo(
                    ((_scrollController.position.pixels -
                                MediaQuery.of(context).size.width / 2) >
                            0)
                        ? (_scrollController.position.pixels -
                            MediaQuery.of(context).size.width / 2)
                        : 0,
                    duration: Duration(milliseconds: _scrollDuration),
                    curve: Curves.easeInOut);
              },
              child: Icon(
                Icons.keyboard_arrow_left,
                size: Dimens.iconSize * 3,
              ),
            ),
          ),
        if (_scrollController.hasClients &&
            _scrollPosition != _scrollController.position.maxScrollExtent &&
            _scrollController.position.maxScrollExtent > 0)
          Positioned(
            right: Dimens.horizontalPadding * 5,
            top: ActorsWidget._height / 2.75,
            child: InkWell(
              onTap: () {
                _scrollController.animateTo(
                    ((_scrollController.position.pixels +
                                MediaQuery.of(context).size.width / 2) <
                            _scrollController.position.maxScrollExtent)
                        ? (_scrollController.position.pixels +
                            MediaQuery.of(context).size.width / 2)
                        : _scrollController.position.maxScrollExtent,
                    duration: Duration(milliseconds: _scrollDuration),
                    curve: Curves.easeInOut);
              },
              child: Icon(
                Icons.keyboard_arrow_right,
                size: Dimens.iconSize * 3,
              ),
            ),
          ),
      ],
    );
  }
}
