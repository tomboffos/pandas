import 'package:nine_pandas/core/ui/ui_kit.dart';

class RoundedFilledBox extends StatelessWidget {
  const RoundedFilledBox({
    Key? key,
    required this.child,
    this.padding,
  }) : super(key: key);

  final Widget child;
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.highlightColor,
        borderRadius: BorderRadius.circular(Dimens.cornerRadius * 3),
      ),
      padding: padding ?? Insets.w_75 + Insets.h_25,
      child: Align(alignment: Alignment.center, child: child),
    );
  }
}
