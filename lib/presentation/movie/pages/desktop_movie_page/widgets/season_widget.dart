import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/scroll_behavior.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/core/utils/translations_helper.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/data/movie/models/season/season.dart';
import 'package:nine_pandas/domain/analytics/bloc/analytics_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_up/sing_up_page.dart';
import 'package:nine_pandas/presentation/lottery/presentation/pages/lottery_page.dart';
import 'package:nine_pandas/presentation/movie/pages/desktop_movie_page/widgets/episode_item.dart';
import 'package:nine_pandas/presentation/movie/pages/desktop_movie_page/widgets/rounded_filled_box.dart';

import '../../../../../injectable.dart';

class SeasonWidget extends StatefulWidget {
  SeasonWidget(
      {Key? key,
      required this.season,
      required this.movieId,
      required this.movieTitle})
      : super(key: key);

  final Season season;
  final int movieId;
  final String movieTitle;

  @override
  State<SeasonWidget> createState() => _SeasonWidgetState();
}

class _SeasonWidgetState extends State<SeasonWidget> {
  final ScrollController _scrollController = ScrollController();

  static const double _darkeningWidth = 400;
  static const double _height = 410;
  static const _scrollDuration = 500;
  double _scrollPosition = 0;
  late bool? isPurchased;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      setState(() {
        _scrollPosition = _scrollController.position.pixels;
      });
    });
    if (widget.season.isPurchased != null) {
      isPurchased = widget.season.isPurchased;
    } else {
      isPurchased = false;
    }

    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        _scrollPosition = _scrollController.position.pixels;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Gap.h4,
        Divider(endIndent: Dimens.horizontalPadding * 10),
        Gap.h1_5,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Text(
                  AppLocalizations.of(context)!.seasonNumber(
                      getNumberToText(widget.season.seasonNumber, context)),
                  style: const TextStyle(
                    fontSize: 28,
                    fontWeight: FontWeight.w500,
                    height: 1.3,
                  ),
                ),
                Gap.w1_5,
                RoundedFilledBox(
                  child: Text(
                    AppLocalizations.of(context)!.newEpisodes(2),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 12,
                      height: 1.3,
                    ),
                  ),
                ),
              ],
            ),
            if (widget.season.price != null && !isPurchased!)
              Row(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .priceWithCurrency(widget.season.price!),
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                      height: 1.3,
                    ),
                  ),
                  Gap.w,
                  BlocBuilder<UserBloc, UserState>(
                    builder: (context, state) {
                      return GradientButton(
                        child: Text(
                          AppLocalizations.of(context)!.buy,
                          style: TextStyle(
                            color: AppColors.veryLightGray,
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            height: 1.3,
                          ),
                        ),
                        onTap: () async {
                          getIt<AnalyticsBloc>().add(
                            AnalyticsEvent.sendPressedBuyButtonMoviePage(
                              widget.movieId.toString(),
                              widget.movieTitle,
                              getIt<UserBloc>().state is Logged
                                  ? (getIt<UserBloc>().state as Logged).user.id
                                  : null,
                            ),
                          );
                          await showDialog<String>(
                            barrierDismissible: false,
                            barrierColor: Colors.black.withOpacity(0.6),
                            context: context,
                            builder: (BuildContext context) => state.maybeWhen(
                              logged: (user) => LotteryPage(),
                              orElse: () => SingUpPage(
                                title: AppLocalizations.of(context)!
                                    .singUpForPaymant,
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                  SizedBox(width: Dimens.horizontalSpace * 10),
                ],
              )
          ],
        ),
        Gap.h2,
        Stack(
          children: [
            SizedBox(
              height: _height,
              child: ScrollConfiguration(
                behavior: PandasScrollBehavior(),
                child: ListView.builder(
                  controller: _scrollController,
                  scrollDirection: Axis.horizontal,
                  itemCount: widget.season.episodes.length,
                  itemBuilder: (context, index) => Padding(
                    padding: const EdgeInsets.only(
                            right: Dimens.horizontalPadding * 2) +
                        const EdgeInsets.only(
                            top: Dimens.horizontalPadding / 2.5),
                    child: InkWell(
                      onTap: () async {
                        if (widget.season.episodes[index].episodeType ==
                            EpisodeType.trailer) {
                          AutoRouter.of(context).navigate(
                            PlayerRoute(
                                id: widget.season.episodes[index].id,
                                movieId: widget.movieId,
                                seasonNumber: widget.season.seasonNumber),
                          );
                        } else {
                          switch (widget.season.isPurchased) {
                            case true:
                              AutoRouter.of(context).navigate(
                                PlayerRoute(
                                    id: widget.season.episodes[index].id,
                                    movieId: widget.movieId,
                                    seasonNumber: widget.season.seasonNumber),
                              );
                              break;
                            case false:
                              if (widget.season.price != null) {
                                await showDialog<String>(
                                  barrierDismissible: false,
                                  barrierColor: Colors.black.withOpacity(0.6),
                                  context: context,
                                  builder: (BuildContext context) =>
                                      LotteryPage(),
                                );
                              } else {
                                AutoRouter.of(context).navigate(
                                  PlayerRoute(
                                      id: widget.season.episodes[index].id,
                                      movieId: widget.movieId,
                                      seasonNumber: widget.season.seasonNumber),
                                );
                              }
                              break;

                            default:
                              if (widget.season.price != null) {
                                await showDialog<String>(
                                  barrierDismissible: false,
                                  barrierColor: Colors.black.withOpacity(0.6),
                                  context: context,
                                  builder: (BuildContext context) => SingUpPage(
                                    title: AppLocalizations.of(context)!
                                        .singUpForPaymant,
                                  ),
                                );
                              } else {
                                AutoRouter.of(context).navigate(
                                  PlayerRoute(
                                      id: widget.season.episodes[index].id,
                                      movieId: widget.movieId,
                                      seasonNumber: widget.season.seasonNumber),
                                );
                              }
                          }
                        }
                      },
                      child: EpisodeItem(
                        episode: widget.season.episodes[index],
                        seasonNumber: widget.season.seasonNumber,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned.fill(
              child: AnimatedOpacity(
                opacity: _scrollController.hasClients &&
                        _scrollPosition !=
                            _scrollController.position.maxScrollExtent &&
                        _scrollController.position.maxScrollExtent > 0
                    ? 1
                    : 0,
                duration: Duration(milliseconds: 200),
                child: IgnorePointer(
                  child: Container(
                    width: _darkeningWidth,
                    alignment: Alignment.topRight,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.centerRight,
                        end: Alignment.center,
                        colors: [
                          AppColors.scaffoldBgColor.withOpacity(.61),
                          AppColors.scaffoldBgColor.withOpacity(0),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Positioned.fill(
              child: AnimatedOpacity(
                opacity: _scrollPosition != 0 ? 1 : 0,
                duration: Duration(milliseconds: 200),
                child: IgnorePointer(
                  child: Container(
                    width: _darkeningWidth,
                    alignment: Alignment.topLeft,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.centerLeft,
                        end: Alignment.center,
                        colors: [
                          AppColors.scaffoldBgColor.withOpacity(.61),
                          AppColors.scaffoldBgColor.withOpacity(0),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            if (_scrollController.hasClients &&
                _scrollPosition != 0 &&
                _scrollController.position.maxScrollExtent > 0)
              Positioned(
                left: Dimens.horizontalPadding * 10,
                top: EpisodeItem.frameHeight / 2,
                child: InkWell(
                  onTap: () {
                    _scrollController.animateTo(
                        ((_scrollController.position.pixels -
                                    MediaQuery.of(context).size.width / 2) >
                                0)
                            ? (_scrollController.position.pixels -
                                MediaQuery.of(context).size.width / 2)
                            : 0,
                        duration: Duration(milliseconds: _scrollDuration),
                        curve: Curves.easeInOut);
                  },
                  child: Icon(
                    Icons.keyboard_arrow_left,
                    size: Dimens.iconSize * 3,
                  ),
                ),
              ),
            if (_scrollController.hasClients &&
                _scrollPosition != _scrollController.position.maxScrollExtent &&
                _scrollController.position.maxScrollExtent > 0)
              Positioned(
                right: Dimens.horizontalPadding * 10,
                top: EpisodeItem.frameHeight / 2,
                child: InkWell(
                  onTap: () {
                    _scrollController.animateTo(
                        ((_scrollController.position.pixels +
                                    MediaQuery.of(context).size.width / 2) <
                                _scrollController.position.maxScrollExtent)
                            ? (_scrollController.position.pixels +
                                MediaQuery.of(context).size.width / 2)
                            : _scrollController.position.maxScrollExtent,
                        duration: Duration(milliseconds: _scrollDuration),
                        curve: Curves.easeInOut);
                  },
                  child: Icon(
                    Icons.keyboard_arrow_right,
                    size: Dimens.iconSize * 3,
                  ),
                ),
              ),
          ],
        ),
      ],
    );
  }
}
