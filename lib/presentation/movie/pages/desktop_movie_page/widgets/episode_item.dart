import 'package:cached_network_image/cached_network_image.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/core/utils/date_helper.dart';
import 'package:nine_pandas/core/utils/string_helper.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/core/utils/translations_helper.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';

class EpisodeItem extends StatefulWidget {
  EpisodeItem({Key? key, required this.episode, required this.seasonNumber})
      : super(key: key);

  final Episode episode;
  final int seasonNumber;

  static const double _width = 510;
  static const double frameHeight = 324;
  static const double _playButtonBgDiameter = 80;

  @override
  State<EpisodeItem> createState() => _EpisodeItemState();
}

class _EpisodeItemState extends State<EpisodeItem> {
  final isAccessible = true;

  bool isHovered = false;

  void onEntered(bool isHovered) => setState(() {
        this.isHovered = isHovered;
      });

  @override
  Widget build(BuildContext context) {
    final hoveredTransorm = Matrix4.identity()
      ..scale(1.01)
      ..translate(-3, -7);
    final transform = isHovered ? hoveredTransorm : Matrix4.identity();
    return SizedBox(
      width: EpisodeItem._width,
      child: Opacity(
        opacity: isAccessible ? 1.0 : 0.5,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          MouseRegion(
            onEnter: (event) => onEntered(
              true,
            ),
            onExit: (event) => onEntered(
              false,
            ),
            child: Stack(
              children: [
                AnimatedContainer(
                  duration: Duration(
                    milliseconds: 200,
                  ),
                  curve: Curves.ease,
                  transform: transform,
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.circular(Dimens.cornerRadius * 2),
                  ),
                  child: Stack(
                    children: [
                      CachedNetworkImage(
                        imageUrl: widget.episode.data?.episodePoster ??
                            'https://lumiere-a.akamaihd.net/v1/images/the-last-jedi-theatrical-poster-tall-a_6a776211.jpeg?region=0%2C53%2C1536%2C768&width=960',
                        height: EpisodeItem.frameHeight,
                        fit: BoxFit.cover,
                      ),
                      Positioned.fill(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: EpisodeItem._playButtonBgDiameter,
                              height: EpisodeItem._playButtonBgDiameter,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white.withOpacity(0.85),
                              ),
                              child: Icon(
                                Icons.play_arrow_rounded,
                                color: Colors.black,
                                size: 56,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                // if (isNewEpisode)
                //   Positioned(
                //     top: Dimens.verticalPadding,
                //     right: Dimens.horizontalPadding,
                //     child: RoundedFilledBox(
                //       child: Text(
                //         AppLocalizations.of(context)!.newEpisodes(1),
                //         style: TextStyle(
                //           fontWeight: FontWeight.bold,
                //           fontSize: 12,
                //           height: 1.3,
                //         ),
                //       ),
                //     ),
                //   ),
              ],
            ),
          ),
          Gap.h_5,
          Row(
            children: [
              Row(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .seasonNumber(
                            getNumberToText(widget.seasonNumber, context))
                        .toUpperCase(),
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w200,
                        height: 1.3,
                        letterSpacing: 1.1,
                        color: AppColors.greyTextColor,
                        fontFamily: 'Roboto'),
                  ),
                  Text(
                    ' | ',
                    style: TextStyle(
                        fontSize: 16,
                        height: 1.3,
                        letterSpacing: 1.1,
                        fontWeight: FontWeight.w200,
                        color: AppColors.greyTextColor,
                        fontFamily: 'Roboto'),
                  ),
                  Text(
                    episodeTypeStringFromEnum(context, episode: widget.episode),
                    style: TextStyle(
                        fontSize: 16,
                        height: 1.3,
                        letterSpacing: 1.1,
                        fontWeight: FontWeight.w500,
                        fontFamily: 'Roboto'),
                  ),
                ],
              ),
              Spacer(),
              if (widget.episode.videoDuration != null)
                Text(
                  AppLocalizations.of(context)!.videoDurationMinutesAndHours(
                    widget.episode.videoDuration! % 60,
                    DateHelper.hoursFromMinutes(widget.episode.videoDuration!),
                  ),
                  style: TextStyle(
                    height: 1.3,
                    color: AppColors.greyTextColor,
                    fontSize: 16,
                  ),
                )
            ],
          ),
          Gap.h_5,
          Text(
            widget.episode.data?.episodeDescription ?? '',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.left,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: AppColors.veryLightGray,
              height: 1.3,
            ),
          ),
        ]),
      ),
    );
  }
}
