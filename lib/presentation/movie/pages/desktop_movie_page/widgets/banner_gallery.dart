import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/core/utils/translations_helper.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';
import 'package:nine_pandas/domain/analytics/bloc/analytics_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_up/sing_up_page.dart';
import 'package:nine_pandas/presentation/lottery/presentation/pages/lottery_page.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../injectable.dart';
import 'rounded_filled_box.dart';

class BannerGallery extends StatelessWidget {
  const BannerGallery({Key? key, required this.movie}) : super(key: key);

  static const double _carouselHeight = 825;
  static const double _logoWidth = 640;
  static const double _logoHeight = 81;
  static const double _textBoxWidth = 552;
  static const double _awardsHeight = 100;

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        PandasCarouselWidget(
          height: _carouselHeight,
          autoplayInterval: const Duration(seconds: 5),
          autoplayDuration: const Duration(milliseconds: 500),
          items: movie.banners!
              .map(
                (banner) => CachedNetworkImage(
                  fit: BoxFit.cover,
                  alignment: Alignment.topCenter,
                  width: double.infinity,
                  imageUrl: banner.images.desktop,
                ),
              )
              .toList(),
        ),
        Positioned(
          top: Dimens.verticalPadding * 4,
          right: Dimens.verticalPadding * 10,
          child: Image.asset(
            AppLocalizations.of(context)!.onlyAdultsIcon,
            height: Dimens.iconSize * 3,
            fit: BoxFit.cover,
          ),
        ),
        Positioned(
          top: Dimens.verticalPadding * 13,
          left: Dimens.horizontalPadding * 10,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (movie.data?.movieReleaseTag != null)
                RoundedFilledBox(
                  padding: Insets.w + Insets.h_5,
                  child: Text(
                    movie.data!.movieReleaseTag!.toUpperCase(),
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                      height: 1.3,
                    ),
                  ),
                ),
              Gap.h_75,
              if (movie.data != null)
                CachedNetworkImage(
                  imageUrl: movie.data!.logo,
                  width: _logoWidth,
                  height: _logoHeight,
                  alignment: Alignment.centerLeft,
                ),
              Gap.h_75,
              if (movie.data != null)
                SizedBox(
                  width: _textBoxWidth,
                  child: Text(
                    movie.data!.movieDescription,
                    style: TextStyle(
                      fontSize: 24,
                      color: AppColors.veryLightGray,
                      fontWeight: FontWeight.w500,
                      height: 1.3,
                    ),
                  ),
                ),
              Gap.h1_5,
              Row(
                children: [
                  if (movie.imdbRating != null) ...[
                    SvgPicture.asset(
                      AppLocalizations.of(context)!.imdbIcon,
                      height: Dimens.iconSize * 1.75,
                    ),
                    Gap.w,
                    Text(
                      movie.imdbRating.toString(),
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w900,
                        height: 1.3,
                        color: AppColors.veryLightGray,
                      ),
                    ),
                    Gap.w2,
                  ],
                  if (movie.kinopoiskRating != null) ...[
                    SvgPicture.asset(
                      AppLocalizations.of(context)!.kinopoiskIcon,
                      height: Dimens.iconSize,
                    ),
                    Gap.w,
                    Text(
                      movie.kinopoiskRating.toString(),
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w900,
                        height: 1.3,
                        color: AppColors.veryLightGray,
                      ),
                    ),
                  ]
                ],
              ),
              Gap.h2_5,
              Row(
                children: [
                  BlocBuilder<UserBloc, UserState>(
                    builder: (context, state) {
                      return GradientButton(
                        onTap: () async {
                          if (movie.seasons?[1].isPurchased == true) {
                            AutoRouter.of(context).navigate(PlayerRoute(
                                id: movie.seasons![1].episodes[1].id,
                                movieId: movie.id,
                                seasonNumber: movie.seasons![1].seasonNumber));
                          } else {
                            getIt<AnalyticsBloc>().add(
                              AnalyticsEvent
                                  .sendPressedWatchSecondSeasonNotBought(
                                movie.id.toString(),
                                movie.data!.movieTitle.toString(),
                                getIt<UserBloc>().state is Logged
                                    ? (getIt<UserBloc>().state as Logged)
                                        .user
                                        .id
                                    : null,
                              ),
                            );
                            await showDialog<String>(
                              barrierDismissible: false,
                              barrierColor: Colors.black.withOpacity(0.6),
                              context: context,
                              builder: (BuildContext context) =>
                                  state.maybeWhen(
                                logged: (user) => LotteryPage(),
                                orElse: () => SingUpPage(
                                  title: AppLocalizations.of(context)!
                                      .singUpForPaymant,
                                ),
                              ),
                            );
                          }
                        },
                        child: Row(
                          children: [
                            Icon(
                              Icons.play_arrow_rounded,
                              size: 24,
                            ),
                            Gap.w_75,
                            Text(
                              AppLocalizations.of(context)!.watch +
                                  ' ' +
                                  AppLocalizations.of(context)!
                                      .seasonNumber(getNumberToText(2, context))
                                      .toLowerCase(),
                              style: TextStyle(
                                color: AppColors.veryLightGray,
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                height: 1.3,
                                fontFeatures: const [
                                  FontFeature.proportionalFigures(),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                  Gap.w,
                  if (movie.data?.trailer != null)
                    CustomButton(
                      onTap: () async {
                        launchUrl(Uri.parse(movie.data!.trailer!));
                      },
                      child: Text(
                        AppLocalizations.of(context)!.trailer,
                        style: TextStyle(
                          fontSize: 18,
                          color: AppColors.veryLightGray,
                          height: 1.3,
                          fontFeatures: const [
                            FontFeature.proportionalFigures()
                          ],
                        ),
                      ),
                      backgroundColor: Colors.transparent,
                      borderColor: AppColors.lightGray,
                    ),
                ],
              )
            ],
          ),
        ),
        Positioned.fill(
          bottom: Dimens.verticalPadding * 2,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              ClipRRect(
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                  child: Container(
                    padding: Insets.h + Insets.w2,
                    height: _awardsHeight,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(Dimens.cornerRadius * 2),
                      color: AppColors.awardsBgColor,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(AppLocalizations.of(context)!.award1),
                        Gap.w,
                        Image.asset(AppLocalizations.of(context)!.award2),
                        Gap.w,
                        Image.asset(AppLocalizations.of(context)!.award3),
                        Gap.w,
                        Image.asset(AppLocalizations.of(context)!.award4),
                        Gap.w,
                        Image.asset(AppLocalizations.of(context)!.award5),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
