import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/domain/analytics/bloc/analytics_bloc.dart';
import 'package:nine_pandas/domain/movie/bloc/bloc/movie_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';
import 'package:nine_pandas/presentation/movie/pages/desktop_movie_page/widgets/actors_widget.dart';
import 'package:nine_pandas/presentation/movie/pages/desktop_movie_page/widgets/banner_gallery.dart';
import 'package:nine_pandas/presentation/ads_banner/presentation/pages/ads_banner.dart';

import 'widgets/season_widget.dart';

class DesktopMoviePage extends StatefulWidget {
  const DesktopMoviePage({
    Key? key,
    required this.id,
  }) : super(key: key);
  final int id;

  @override
  State<DesktopMoviePage> createState() => _DesktopMoviePageState();
}

class _DesktopMoviePageState extends State<DesktopMoviePage> {
  late MovieBloc _movieBloc;

  @override
  void initState() {
    _movieBloc = BlocProvider.of<MovieBloc>(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: BlocBuilder<MovieBloc, MovieState>(
        builder: (context, state) {
          return state.maybeWhen(
            orElse: () => PandasLoadingView(),
            error: (error) => PandasServerErrorWidget(
              reloadCallback: () => _movieBloc.add(
                MovieEvent.fetchMovie(widget.id),
              ),
            ),
            dataLoaded: (movie) {
              getIt<AnalyticsBloc>().add(
                AnalyticsEvent.sendMoviePageOpened(
                  movie.id.toString(),
                  movie.data!.movieTitle,
                  getIt<UserBloc>().state is Logged
                      ? (getIt<UserBloc>().state as Logged).user.id
                      : null,
                ),
              );
              return Column(
                children: [
                  NavigationHeader(
                    refreshRequest: () => getIt.get<MovieBloc>().add(
                          MovieEvent.fetchMovie(widget.id),
                        ),
                  ),
                  Expanded(
                    child: Stack(
                      children: [
                        SingleChildScrollView(
                          child: ConstrainedBox(
                            constraints: BoxConstraints(
                                minHeight: MediaQuery.of(context).size.height),
                            child: Column(
                              children: [
                                BannerGallery(movie: movie),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: Dimens.horizontalPadding * 10),
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Gap.h4,
                                      Divider(
                                          endIndent:
                                              Dimens.horizontalPadding * 10),
                                      Gap.h1_5,
                                      Text(
                                        AppLocalizations.of(context)!.actors,
                                        style: const TextStyle(
                                          fontSize: 24,
                                          fontWeight: FontWeight.w500,
                                          height: 1.3,
                                        ),
                                      ),
                                      Gap.h1_5,
                                      ActorsWidget(actors: movie.actors!),
                                      ...movie.seasons!
                                          .map(
                                            (season) => SeasonWidget(
                                              season: season,
                                              movieId: widget.id,
                                              movieTitle:
                                                  movie.data!.movieTitle,
                                            ),
                                          )
                                          .toList(),
                                      Gap.h4,
                                    ],
                                  ),
                                ),
                                AdsBanner(
                                  adsBanner: movie.adsBanner!,
                                ),
                                Gap.h4,
                                Footer(),
                              ],
                            ),
                          ),
                        ),
                        BlackoutContainer(),
                      ],
                    ),
                  )
                ],
              );
            },
          );
        },
      ),
    );
  }
}
