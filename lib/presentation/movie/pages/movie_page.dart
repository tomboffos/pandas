import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/domain/movie/bloc/bloc/movie_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import 'desktop_movie_page/desktop_movie_page.dart';
import 'mobile_movie_page/mobile_movie_page.dart';
import 'tablet_movie_page/tablet_movie_page.dart';

class MoviePage extends StatefulWidget {
  final int id;

  const MoviePage({
    Key? key,
    @PathParam('id') required this.id,
  }) : super(key: key);

  @override
  State<MoviePage> createState() => _MoviePageState();
}

class _MoviePageState extends State<MoviePage> {
  bool isBlackout = false;

  final MovieBloc _movieBloc = getIt.get<MovieBloc>();

  @override
  void initState() {
    _movieBloc.add(MovieEvent.fetchMovie(widget.id));
    super.initState();
  }

  @override
  void dispose() {
    _movieBloc.close();
    super.dispose();
  }

  void setBlackoutValue(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: setBlackoutValue,
      child: BlocListener<UserBloc, UserState>(
        listener: (context, state) {
          _movieBloc.add(MovieEvent.fetchMovie(widget.id));
        },
        child: BlocListener<LocalizationBloc, LocalizationState>(
          listener: (context, state) {
            _movieBloc.add(MovieEvent.fetchMovie(widget.id));
          },
          child: BlocProvider.value(
            value: _movieBloc,
            child: LayoutHandler(
              mobileView: MobileMoviePage(id: widget.id),
              tabletView: TabletMoviePage(id: widget.id),
              desktopView: DesktopMoviePage(id: widget.id),
            ),
          ),
        ),
      ),
    );
  }
}
