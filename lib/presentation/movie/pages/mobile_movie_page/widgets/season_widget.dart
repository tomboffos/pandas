import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/core/utils/translations_helper.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/data/movie/models/season/season.dart';
import 'package:nine_pandas/domain/analytics/bloc/analytics_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_up/sing_up_page.dart';
import 'package:nine_pandas/presentation/lottery/presentation/pages/lottery_page.dart';
import 'package:nine_pandas/presentation/movie/pages/mobile_movie_page/widgets/episode_item.dart';

import '../../../../../injectable.dart';

class SeasonWidget extends StatefulWidget {
  SeasonWidget({
    Key? key,
    required this.season,
    required this.movieId,
    required this.movieTitle,
  }) : super(key: key);

  final Season season;
  final int movieId;
  final String movieTitle;

  @override
  State<SeasonWidget> createState() => _SeasonWidgetState();
}

class _SeasonWidgetState extends State<SeasonWidget> {
  final ScrollController _scrollController = ScrollController();

  static const double _height = 278;

  late final bool? isPurchased;

  @override
  void initState() {
    super.initState();

    if (widget.season.isPurchased != null) {
      isPurchased = widget.season.isPurchased;
    } else {
      isPurchased = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Gap.h3,
        Divider(endIndent: Dimens.horizontalPadding),
        Gap.h1_25,
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              AppLocalizations.of(context)!.seasonNumber(
                  getNumberToText(widget.season.seasonNumber, context)),
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                height: 1.3,
              ),
            ),
            if (widget.season.price != null && !isPurchased!)
              Row(
                children: [
                  Text(
                    AppLocalizations.of(context)!
                        .priceWithCurrency(widget.season.price!),
                    style: TextStyle(
                      fontSize: 16,
                      height: 1.3,
                    ),
                  ),
                  Gap.w,
                  BlocBuilder<UserBloc, UserState>(
                    builder: (context, state) {
                      return GradientButton(
                        height: Dimens.buttonHeight * 0.67,
                        padding: const EdgeInsets.symmetric(
                          horizontal: Dimens.horizontalPadding,
                          vertical: Dimens.verticalPadding * 0.5,
                        ),
                        child: Text(
                          AppLocalizations.of(context)!.buy,
                          style: TextStyle(
                            color: AppColors.veryLightGray,
                            fontSize: 12,
                            height: 1.3,
                          ),
                        ),
                        onTap: () async {
                          getIt<AnalyticsBloc>().add(
                            AnalyticsEvent.sendPressedBuyButtonMoviePage(
                              widget.movieId.toString(),
                              widget.movieTitle,
                              getIt<UserBloc>().state is Logged
                                  ? (getIt<UserBloc>().state as Logged).user.id
                                  : null,
                            ),
                          );
                          await showDialog<String>(
                            barrierDismissible: false,
                            barrierColor: Colors.black.withOpacity(0.6),
                            context: context,
                            builder: (BuildContext context) => state.maybeWhen(
                              logged: (user) => LotteryPage(),
                              orElse: () => SingUpPage(
                                title: AppLocalizations.of(context)!
                                    .singUpForPaymant,
                              ),
                            ),
                          );
                        },
                      );
                    },
                  ),
                  SizedBox(width: Dimens.horizontalSpace),
                ],
              )
          ],
        ),
        Gap.h2,
        SizedBox(
          height: _height,
          child: ListView.builder(
            controller: _scrollController,
            scrollDirection: Axis.horizontal,
            itemCount: widget.season.episodes.length,
            itemBuilder: (context, index) => InkWell(
                onTap: () async {
                  if (widget.season.episodes[index].episodeType ==
                      EpisodeType.trailer) {
                    AutoRouter.of(context).navigate(
                      PlayerRoute(
                          id: widget.season.episodes[index].id,
                          movieId: widget.movieId,
                          seasonNumber: widget.season.seasonNumber),
                    );
                  } else {
                    switch (widget.season.isPurchased) {
                      case true:
                        AutoRouter.of(context).navigate(
                          PlayerRoute(
                              id: widget.season.episodes[index].id,
                              movieId: widget.movieId,
                              seasonNumber: widget.season.seasonNumber),
                        );
                        break;
                      case false:
                        if (widget.season.price != null) {
                          await showDialog<String>(
                            barrierDismissible: false,
                            barrierColor: Colors.black.withOpacity(0.6),
                            context: context,
                            builder: (BuildContext context) => LotteryPage(),
                          );
                        } else {
                          AutoRouter.of(context).navigate(
                            PlayerRoute(
                                id: widget.season.episodes[index].id,
                                movieId: widget.movieId,
                                seasonNumber: widget.season.seasonNumber),
                          );
                        }
                        break;
                      default:
                        if (widget.season.price != null) {
                          await showDialog<String>(
                            barrierDismissible: false,
                            barrierColor: Colors.black.withOpacity(0.6),
                            context: context,
                            builder: (BuildContext context) => SingUpPage(
                              title: AppLocalizations.of(context)!
                                  .singUpForPaymant,
                            ),
                          );
                        } else {
                          AutoRouter.of(context).navigate(
                            PlayerRoute(
                                id: widget.season.episodes[index].id,
                                movieId: widget.movieId,
                                seasonNumber: widget.season.seasonNumber),
                          );
                        }
                    }
                  }
                },
                child: EpisodeItem(
                  episode: widget.season.episodes[index],
                  season: widget.season,
                )),
          ),
        ),
      ],
    );
  }
}
