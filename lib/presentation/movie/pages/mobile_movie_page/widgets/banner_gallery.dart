import 'dart:ui';

import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/core/utils/translations_helper.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';
import 'package:nine_pandas/domain/analytics/bloc/analytics_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/auth/presentation/pages/sing_up/sing_up_page.dart';
import 'package:nine_pandas/presentation/lottery/presentation/pages/lottery_page.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../../../injectable.dart';
import 'rounded_filled_box.dart';

class BannerGallery extends StatelessWidget {
  const BannerGallery({Key? key, required this.movie}) : super(key: key);

  static const double _carouselHeight = 414;
  static const double _bottomElementsHeight = 127;
  static const double _logoWidth = 640;
  static const double _logoHeight = 46;
  static const double _awardsHeight = 38;
  static const double _trailerButtonWidth = 81;

  final Movie movie;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: _carouselHeight + _bottomElementsHeight / 2,
          child: Stack(
            children: [
              PandasCarouselWidget(
                height: _carouselHeight,
                autoplayInterval: const Duration(seconds: 5),
                autoplayDuration: const Duration(milliseconds: 500),
                items: movie.banners!
                    .map(
                      (banner) => CachedNetworkImage(
                        fit: BoxFit.cover,
                        alignment: Alignment.topCenter,
                        width: double.infinity,
                        imageUrl: banner.images.mobile,
                      ),
                    )
                    .toList(),
              ),
              Positioned(
                top: Dimens.verticalPadding * 2,
                right: Dimens.horizontalPadding * 2,
                child: Image.asset(
                  AppLocalizations.of(context)!.onlyAdultsIcon,
                  height: Dimens.iconSize * 3,
                  fit: BoxFit.cover,
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: 200,
                  margin:
                      const EdgeInsets.only(bottom: _bottomElementsHeight / 2),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                        AppColors.scaffoldBgColor,
                        AppColors.scaffoldBgColor.withOpacity(0),
                      ],
                      stops: [0, 0.4],
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                left: Dimens.horizontalPadding,
                right: Dimens.horizontalPadding,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    if (movie.data != null)
                      CachedNetworkImage(
                        imageUrl: movie.data!.logo,
                        width: _logoWidth,
                        height: _logoHeight,
                        alignment: Alignment.centerLeft,
                      ),
                    Gap.h_75,
                    if (movie.data != null)
                      Text(
                        movie.data!.movieDescription,
                        style: TextStyle(
                          fontSize: 16,
                          color: AppColors.veryLightGray,
                          height: 1.3,
                          fontFeatures: const [
                            FontFeature.proportionalFigures()
                          ],
                          fontFamily: 'Roboto',
                        ),
                      ),
                    Gap.h_75,
                    Row(
                      children: [
                        if (movie.data?.movieReleaseTag != null) ...[
                          RoundedFilledBox(
                            padding: const EdgeInsets.symmetric(
                              vertical: Dimens.verticalPadding * 0.125,
                              horizontal: Dimens.horizontalSpace * 0.75,
                            ),
                            child: Text(
                              movie.data!.movieReleaseTag!.toUpperCase(),
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 12,
                                height: 1.3,
                                fontFeatures: const [
                                  FontFeature.proportionalFigures()
                                ],
                                fontFamily: 'Roboto',
                              ),
                            ),
                          ),
                          Gap.w_5,
                        ],
                        if (movie.imdbRating != null) ...[
                          SvgPicture.asset(
                            AppLocalizations.of(context)!.imdbIcon,
                            height: Dimens.iconSize,
                          ),
                          Gap.w_5,
                          Text(
                            movie.imdbRating.toString(),
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w900,
                              height: 1.3,
                              color: AppColors.veryLightGray,
                              fontFeatures: const [
                                FontFeature.proportionalFigures()
                              ],
                              fontFamily: 'Roboto',
                            ),
                          ),
                          Gap.w_5,
                        ],
                        if (movie.kinopoiskRating != null) ...[
                          SvgPicture.asset(
                            AppLocalizations.of(context)!.kinopoiskIcon,
                            height: Dimens.iconSize * 0.75,
                          ),
                          Gap.w_5,
                          Text(
                            movie.kinopoiskRating.toString(),
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w900,
                              height: 1.3,
                              color: AppColors.veryLightGray,
                              fontFeatures: const [
                                FontFeature.proportionalFigures()
                              ],
                              fontFamily: 'Roboto',
                            ),
                          ),
                        ]
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: Insets.w,
          child: Column(
            children: [
              Gap.h1_5,
              ClipRRect(
                child: BackdropFilter(
                  filter: ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
                  child: Container(
                    padding: Insets.h,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.circular(Dimens.cornerRadius * 2),
                      color: AppColors.awardsBgColor,
                    ),
                    child: SizedBox(
                      height: _awardsHeight,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image.asset(AppLocalizations.of(context)!.award1),
                          Image.asset(AppLocalizations.of(context)!.award2),
                          Image.asset(AppLocalizations.of(context)!.award3),
                          Image.asset(AppLocalizations.of(context)!.award4),
                          Image.asset(AppLocalizations.of(context)!.award5),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Gap.h1_5,
              SizedBox(
                height: Dimens.buttonHeight * 0.8,
                child: Row(
                  children: [
                    BlocBuilder<UserBloc, UserState>(
                      builder: (context, state) {
                        return GradientButton(
                          onTap: () async {
                            if (movie.seasons?[1].isPurchased == true) {
                              AutoRouter.of(context).navigate(PlayerRoute(
                                  id: movie.seasons![1].episodes[1].id,
                                  movieId: movie.id,
                                  seasonNumber:
                                      movie.seasons![1].seasonNumber));
                            } else {
                              getIt<AnalyticsBloc>().add(
                                AnalyticsEvent
                                    .sendPressedWatchSecondSeasonNotBought(
                                  movie.id.toString(),
                                  movie.data!.movieTitle.toString(),
                                  getIt<UserBloc>().state is Logged
                                      ? (getIt<UserBloc>().state as Logged)
                                          .user
                                          .id
                                      : null,
                                ),
                              );
                              await showDialog<String>(
                                barrierDismissible: false,
                                barrierColor: Colors.black.withOpacity(0.6),
                                context: context,
                                builder: (BuildContext context) =>
                                    state.maybeWhen(
                                  logged: (user) => LotteryPage(),
                                  orElse: () => SingUpPage(
                                    title: AppLocalizations.of(context)!
                                        .singUpForPaymant,
                                  ),
                                ),
                              );
                            }
                          },
                          padding: const EdgeInsets.symmetric(
                            vertical: Dimens.verticalPadding * 0.2,
                            horizontal: Dimens.verticalPadding,
                          ),
                          child: Row(
                            children: [
                              Icon(
                                Icons.play_arrow_rounded,
                                size: 24,
                              ),
                              Gap.w_5,
                              Column(
                                children: [
                                  SizedBox(
                                    height: Dimens.verticalPadding * 0.56,
                                  ),
                                  Text(
                                    AppLocalizations.of(context)!.watch +
                                        ' ' +
                                        AppLocalizations.of(context)!
                                            .seasonNumber(
                                                getNumberToText(2, context))
                                            .toLowerCase(),
                                    style: TextStyle(
                                      color: AppColors.veryLightGray,
                                      fontSize: 12,
                                      // height: 1.3,
                                      fontFeatures: const [
                                        FontFeature.proportionalFigures()
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                    Gap.w,
                    if (movie.data?.trailer != null)
                      CustomButton(
                        width: _trailerButtonWidth,
                        onTap: () {
                          launchUrl(Uri.parse(movie.data!.trailer!));
                        },
                        child: Text(
                          AppLocalizations.of(context)!.trailer,
                          style: TextStyle(
                            fontSize: 12,
                            color: AppColors.veryLightGray,
                            height: 1.3,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        backgroundColor: Colors.transparent,
                        borderColor: AppColors.lightGray,
                      ),
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}
