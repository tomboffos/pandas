import 'package:cached_network_image/cached_network_image.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/data/movie/models/actor/actor.dart';

class ActorsWidget extends StatelessWidget {
  const ActorsWidget({
    Key? key,
    required this.actors,
  }) : super(key: key);

  static const double _height = 177;
  static const double _frameDiameter = 150;

  final List<Actor> actors;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: _height,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: actors.length,
        itemBuilder: ((context, index) => Padding(
              padding:
                  const EdgeInsets.only(right: Dimens.horizontalPadding * 1.5),
              child: Column(
                children: [
                  Card(
                    margin: Insets.zero,
                    elevation: 0,
                    clipBehavior: Clip.antiAlias,
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.circular(Dimens.cornerRadius * 1.75),
                    ),
                    child: CachedNetworkImage(
                      imageUrl: actors[index].image,
                      width: _frameDiameter,
                      height: _frameDiameter,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Gap.h_5,
                  Text(
                    actors[index].name,
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 14, height: 1.3),
                  )
                ],
              ),
            )),
      ),
    );
  }
}
