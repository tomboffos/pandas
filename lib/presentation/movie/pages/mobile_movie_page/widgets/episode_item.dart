import 'package:cached_network_image/cached_network_image.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/core/utils/date_helper.dart';
import 'package:nine_pandas/core/utils/string_helper.dart';
import 'package:nine_pandas/core/utils/translations_helper.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/movie/models/season/season.dart';

class EpisodeItem extends StatelessWidget {
  EpisodeItem({Key? key, required this.episode, required this.season})
      : super(key: key);

  final Episode episode;
  final Season season;

  static const double _width = 326;
  static const double _frameHeight = 194;
  static const double _playButtonBgDiameter = 50;

  final isAccessible = true;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      child: Opacity(
        opacity: isAccessible ? 1.0 : 0.5,
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  clipBehavior: Clip.antiAlias,
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.circular(Dimens.cornerRadius * 1.75),
                  ),
                  child: CachedNetworkImage(
                    imageUrl: episode.data?.episodePoster ??
                        'https://lumiere-a.akamaihd.net/v1/images/the-last-jedi-theatrical-poster-tall-a_6a776211.jpeg?region=0%2C53%2C1536%2C768&width=960',
                    height: _frameHeight,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned.fill(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: _playButtonBgDiameter,
                        height: _playButtonBgDiameter,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white.withOpacity(0.85),
                        ),
                        child: Icon(
                          Icons.play_arrow_rounded,
                          color: Colors.black,
                          size: 40,
                        ),
                      ),
                    ],
                  ),
                ),
                // if (isNewEpisode)
                //   Positioned(
                //     top: Dimens.verticalPadding,
                //     right: Dimens.horizontalPadding,
                //     child: RoundedFilledBox(
                //       child: Text(
                //         AppLocalizations.of(context)!.newEpisodes(1),
                //         style: TextStyle(
                //           fontWeight: FontWeight.bold,
                //           fontSize: 12,
                //           height: 1.3,
                //         ),
                //       ),
                //     ),
                //   ),
              ],
            ),
            Gap.h_5,
            Padding(
              padding: Insets.w,
              child: Row(
                children: [
                  Row(
                    children: [
                      Text(
                        AppLocalizations.of(context)!
                            .seasonNumber(
                                getNumberToText(season.seasonNumber, context))
                            .toUpperCase(),
                        style: TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w200,
                            height: 1.3,
                            letterSpacing: 1.1,
                            color: AppColors.greyTextColor,
                            fontFamily: 'Roboto'),
                      ),
                      Text(
                        ' | ',
                        style: TextStyle(
                            fontSize: 13,
                            height: 1.3,
                            letterSpacing: 1.1,
                            fontWeight: FontWeight.w200,
                            color: AppColors.greyTextColor,
                            fontFamily: 'Roboto'),
                      ),
                      Text(
                        episodeTypeStringFromEnum(context, episode: episode),
                        style: TextStyle(
                            fontSize: 13,
                            height: 1.3,
                            letterSpacing: 1.1,
                            fontWeight: FontWeight.w500,
                            fontFamily: 'Roboto'),
                      ),
                    ],
                  ),
                  Spacer(),
                  if (episode.videoDuration != null)
                    Text(
                      AppLocalizations.of(context)!
                          .videoDurationMinutesAndHours(
                        episode.videoDuration! % 60,
                        DateHelper.hoursFromMinutes(episode.videoDuration!),
                      ),
                      style: TextStyle(
                        height: 1.3,
                        color: AppColors.greyTextColor,
                        fontSize: 13,
                      ),
                    )
                ],
              ),
            ),
            Gap.h_5,
            Padding(
              padding: Insets.w,
              child: Text(
                episode.data?.episodeDescription ?? '',
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontSize: 13,
                  color: AppColors.veryLightGray,
                  height: 1.3,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
