import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import '../../../../core/ui/ui_kit.dart';
import 'desktop_not_found_page/desktop_not_found_page.dart';
import 'mobile_not_found_page/mobile_not_found_page.dart';
import 'tablet_not_found_page/tablet_not_found_page.dart';

class NotFoundPage extends StatefulWidget {
  const NotFoundPage({Key? key}) : super(key: key);

  @override
  State<NotFoundPage> createState() => _NotFoundPageState();
}

class _NotFoundPageState extends State<NotFoundPage> {
  bool isBlackout = false;

  void setBlackoutValue(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: setBlackoutValue,
      child: LayoutHandler(
        mobileView: MobileNotFoundPage(),
        desktopView: DesktopNotFoundPage(),
        tabletView: TabletNotFoundPage(),
      ),
    );
  }
}
