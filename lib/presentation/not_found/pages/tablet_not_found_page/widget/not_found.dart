import 'package:auto_route/auto_route.dart';
import 'package:nine_pandas/navigation/router.dart';

import '../../../../../../core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NotFound extends StatelessWidget {
  const NotFound({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints:
          BoxConstraints(minHeight: MediaQuery.of(context).size.height / 1.25),
      child: Center(
        child: Column(
          children: [
            Text(
              AppLocalizations.of(context)!.oops,
              style: TextStyle(
                color: Colors.white,
                fontSize: 38,
                fontWeight: FontWeight.w700,
              ),
            ),
            RichText(
              text: TextSpan(
                children: [
                  TextSpan(
                    text: AppLocalizations.of(context)!.pageNotFound,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  TextSpan(
                    text: '.',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
            Text(
              AppLocalizations.of(context)!.notFoundCode,
              style: TextStyle(
                color: Colors.white,
                fontSize: 210,
                fontWeight: FontWeight.w600,
              ),
            ),
            CancelButton(
              onTap: () {
                AutoRouter.of(context).navigate(MainRoute());
              },
              width: 230,
              child: Text(
                AppLocalizations.of(context)!.backToMain,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
