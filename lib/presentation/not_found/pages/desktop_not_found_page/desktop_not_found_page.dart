import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import 'widget/not_found.dart';

class DesktopNotFoundPage extends StatelessWidget {
  const DesktopNotFoundPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.carcassBackground,
      body: Column(
        children: [
          NavigationHeader(),
          Expanded(
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Gap.h1_5,
                        NotFound(),
                        Gap.h2,
                        Footer(),
                      ],
                    ),
                  ),
                ),
                BlackoutContainer(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
