import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/library/model/libraryData/library_data.dart';

import '../../../../../../core/ui/ui_kit.dart';
import 'series_list.dart';

// ignore: must_be_immutable
class EnabledSeries extends StatelessWidget {
  EnabledSeries({Key? key, required this.library}) : super(key: key);
  List<LibraryData> library;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Insets.w10,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Gap.h1_5,
          NavigationRow(
            rootPageText: AppLocalizations.of(context)!.personalAccount,
            currentPageText: AppLocalizations.of(context)!.yourLibrary,
          ),
          SeriesList(library: library),
        ],
      ),
    );
  }
}
