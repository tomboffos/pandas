// ignore_for_file: must_be_immutable

import 'package:auto_route/auto_route.dart';
import 'package:nine_pandas/data/library/model/libraryData/library_data.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/main_page/pages/desktop_main_page/widgets/series_card_item.dart';

import '../../../../../core/ui/ui_kit.dart';

class SeriesList extends StatelessWidget {
  List<LibraryData> library;
  SeriesList({Key? key, required this.library}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        for (int i = 0; i < library.length; i++) Gap.h1_5,
        GridView.count(
          crossAxisCount: 2,
          childAspectRatio: 780 / 474,
          shrinkWrap: true,
          mainAxisSpacing: Dimens.verticalPadding * 2,
          crossAxisSpacing: Dimens.horizontalPadding * 2,
          children: [
            ...library.map(
              (library) => InkWell(
                onTap: () {
                  AutoRouter.of(context).navigate(MovieRoute(id: library.id));
                },
                child: SeriesCardItem(
                  isReleased: library.movieCollection.isReleased,
                  movieReleaseTag: library.movieCollection.data != null
                      ? library.movieCollection.data!.movieReleaseTag
                      : null,
                  imageUrl: library.movieCollection.posters!.desktop,
                  genres: library.movieCollection.genres!,
                  title: library.movieCollection.data != null
                      ? library.movieCollection.data!.movieTitle
                      : 'Скоро',
                  description: library.movieCollection.data != null
                      ? library.movieCollection.data!.movieSubTitle
                      : '',
                ),
              ),
            )
          ],
        ),
        Gap.h1_5,
      ],
    );
  }
}
