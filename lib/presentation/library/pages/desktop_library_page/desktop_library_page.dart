import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/data/library/model/library/library.dart';
import 'package:nine_pandas/domain/library/bloc/library_bloc.dart';
import 'package:nine_pandas/presentation/ads_banner/presentation/pages/ads_banner.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import '../../../../../core/ui/ui_kit.dart';
import 'widgets/empty_library.dart';
import 'widgets/enabled_series.dart';

class DesktopLibraryPage extends StatefulWidget {
  const DesktopLibraryPage({Key? key}) : super(key: key);

  @override
  State<DesktopLibraryPage> createState() => _DesktopLibraryPageState();
}

class _DesktopLibraryPageState extends State<DesktopLibraryPage> {
  late LibraryBloc libraryBloc;

  @override
  void initState() {
    libraryBloc = BlocProvider.of<LibraryBloc>(context);
    super.initState();
  }

  bool isEmpty = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: Column(
        children: [
          NavigationHeader(),
          Expanded(
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height),
                    child: BlocBuilder<LibraryBloc, LibraryState>(
                      builder: (context, state) {
                        return state.when(
                          dataLoaded: (Library library) {
                            return Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                library.results.length == 0
                                    ? EmptyLibrary()
                                    : EnabledSeries(library: library.results),
                                Gap.h,
                                AdsBanner(adsBanner: library.adsBanner),
                                Gap.h3,
                                Footer(),
                              ],
                            );
                          },
                          dataLoading: () => PandasLoadingView(),
                          error: () => PandasServerErrorWidget(
                            reloadCallback: () =>
                                libraryBloc.add(LibraryEvent.getMovies()),
                          ),
                        );
                      },
                    ),
                  ),
                ),
                BlackoutContainer(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
