import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/domain/library/bloc/library_bloc.dart';
import 'package:nine_pandas/presentation/ads_banner/presentation/pages/ads_banner.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import '../../../../../core/ui/ui_kit.dart';
import 'widgets/empty_library.dart';
import 'widgets/enabled_series.dart';

class TabletLibraryPage extends StatefulWidget {
  const TabletLibraryPage({Key? key}) : super(key: key);

  @override
  State<TabletLibraryPage> createState() => _TabletLibraryPageState();
}

class _TabletLibraryPageState extends State<TabletLibraryPage> {
  bool isEmpty = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: Column(
        children: [
          NavigationHeader(),
          Expanded(
              child: Stack(
            children: [
              SingleChildScrollView(
                child: ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height),
                    child: BlocBuilder<LibraryBloc, LibraryState>(
                        builder: (context, state) {
                      return state.maybeWhen(
                        orElse: () => SizedBox.shrink(),
                        dataLoaded: (library) => Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            library.results.length > 0
                                ? EnabledSeries(
                                    library: library.results,
                                  )
                                : EmptyLibrary(),
                            AdsBanner(
                              adsBanner: library.adsBanner,
                            ),
                            Footer(),
                          ],
                        ),
                      );
                    })),
              ),
              BlackoutContainer(),
            ],
          ))
        ],
      ),
    );
  }
}
