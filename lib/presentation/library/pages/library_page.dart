import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/library/bloc/library_bloc.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';

import 'desktop_library_page/desktop_library_page.dart';
import 'mobile_library_page/mobile_library_page.dart';
import 'tablet_profile_page/tablet_library_page.dart';

class LibraryPage extends StatefulWidget {
  const LibraryPage({Key? key}) : super(key: key);

  @override
  State<LibraryPage> createState() => LibraryPageState();
}

class LibraryPageState extends State<LibraryPage> {
  bool isBlackout = false;
  final LibraryBloc _movieBloc = getIt.get<LibraryBloc>();

  @override
  void initState() {
    _movieBloc.add(LibraryEvent.getMovies());
    super.initState();
  }

  @override
  void dispose() {
    _movieBloc.close();
    super.dispose();
  }

  void isBlackoutChange(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: isBlackoutChange,
      child: BlocProvider.value(
        value: _movieBloc,
        child: BlocListener<LocalizationBloc, LocalizationState>(
          listener: (context, state) {
            _movieBloc.add(LibraryEvent.getMovies());
          },
          child: LayoutHandler(
            mobileView: MobileLibraryPage(),
            desktopView: DesktopLibraryPage(),
            tabletView: TabletLibraryPage(),
          ),
        ),
      ),
    );
  }
}
