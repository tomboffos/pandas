import 'package:auto_route/auto_route.dart';
import 'package:nine_pandas/data/library/model/libraryData/library_data.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:nine_pandas/presentation/main_page/pages/mobile_main_page/widgets/series_card_item.dart';

import '../../../../../core/ui/ui_kit.dart';

class SeriesList extends StatelessWidget {
  final List<LibraryData> library;
  const SeriesList({Key? key, required this.library}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Insets.h1_5,
      child: Column(
        children: [
          for (int i = 0; i < library.length; i++)
            Padding(
              padding:
                  const EdgeInsets.only(bottom: Dimens.verticalPadding * 1.5),
              child: InkWell(
                onTap: () {
                  AutoRouter.of(context)
                      .navigate(MovieRoute(id: library[i].movieCollection.id));
                },
                child: SeriesCardItem(
                  movieReleaseTag: library[i].movieCollection.data != null
                      ? library[i].movieCollection.data!.movieReleaseTag
                      : null,
                  imageUrl: library[i].movieCollection.posters!.mobile,
                  genres: library[i].movieCollection.genres!,
                  title: library[i].movieCollection.data != null
                      ? library[i].movieCollection.data!.movieTitle
                      : '',
                  description: library[i].movieCollection.data != null
                      ? library[i].movieCollection.data!.movieSubTitle
                      : '',
                ),
              ),
            )
        ],
      ),
    );
  }
}
