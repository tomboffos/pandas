import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../../../../core/ui/ui_kit.dart';

class EmptyLibrary extends StatelessWidget {
  const EmptyLibrary({Key? key}) : super(key: key);
  static const double minHeightEmptyLibrary = 400;

  @override
  Widget build(BuildContext context) {
    return ConstrainedBox(
      constraints: BoxConstraints(minHeight: minHeightEmptyLibrary),
      child: Padding(
        padding: Insets.w,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Gap.h1_5,
            NavigationRow(
              rootPageText: AppLocalizations.of(context)!.personalAccount,
              currentPageText: AppLocalizations.of(context)!.yourLibrary,
            ),
            Gap.h3,
            Text(
              AppLocalizations.of(context)!.emptyLibrary,
              style: TextStyle(
                color: AppColors.veryLightGray,
                fontSize: 24,
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
