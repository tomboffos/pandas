import 'dart:ui';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../../../../core/ui/ui_kit.dart';

class MobileChangePasswordPopup extends StatelessWidget {
  const MobileChangePasswordPopup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      insetPadding: EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      contentPadding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Dimens.cornerRadius),
      ),
      backgroundColor: AppColors.carcassBackground,
      content: MobileChangePasswordPopupFormField(),
    );
  }
}

class MobileChangePasswordPopupFormField extends StatefulWidget {
  const MobileChangePasswordPopupFormField({Key? key}) : super(key: key);

  static const EdgeInsets insets = EdgeInsets.fromLTRB(16.0, 64.0, 16.0, 28.0);

  static const double singInFormFieldHeight = 250;

  @override
  State<MobileChangePasswordPopupFormField> createState() =>
      _MobileChangePasswordPopupFormFieldState();
}

class _MobileChangePasswordPopupFormFieldState
    extends State<MobileChangePasswordPopupFormField> {
  bool isPasswordValid = false;
  bool isOldPasswordIncorrect = false;
  bool isOldPasswordValid = false;
  bool isObscured = true;

  final _formKey = GlobalKey<FormState>();
  TextEditingController _oldPasswordTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();
  TextEditingController _repeatPasswordTextController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Stack(
        children: [
          Container(
            padding: MobileChangePasswordPopupFormField.insets,
            width: double.infinity,
            child: ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight:
                      MobileChangePasswordPopupFormField.singInFormFieldHeight,
                  minWidth: MediaQuery.of(context).size.width),
              child: Form(
                key: _formKey,
                autovalidateMode: AutovalidateMode.always,
                child: Column(
                  children: [
                    Gap.h,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                          child: Text(
                            AppLocalizations.of(context)!.changePassword,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              fontFeatures: const [
                                FontFeature.proportionalFigures()
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Gap.h2,
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppLocalizations.of(context)!.oldPassword,
                          style: TextStyle(
                            color: AppColors.greyTextColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        Gap.h_5,
                        PandasBaseTextInput(
                          isError: isOldPasswordIncorrect,
                          controller: _oldPasswordTextController,
                          hintText: AppLocalizations.of(context)!.password,
                          isObscured: isObscured,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context).nextFocus();
                          },
                          onChanged: (value) {
                            if (value.length >= 6) {
                              setState(() {
                                isOldPasswordValid = true;
                              });
                            } else {
                              setState(() {
                                isOldPasswordValid = false;
                              });
                            }
                          },
                          tail: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              IconButton(
                                icon: Icon(
                                  isObscured
                                      ? Icons.visibility_outlined
                                      : Icons.visibility_off_outlined,
                                  color: Colors.white,
                                  size: Dimens.iconSize * 1.2,
                                ),
                                onPressed: () => setState(() {
                                  isObscured = !isObscured;
                                }),
                              ),
                            ],
                          ),
                        ),
                        Gap.h_25,
                        if (isOldPasswordIncorrect)
                          Text(
                            AppLocalizations.of(context)!.wrongPassword,
                            style: TextStyle(
                              color: AppColors.error,
                              fontSize: 12,
                            ),
                          ),
                        Gap.h_25,
                        Gap.h1_25,
                        Text(
                          AppLocalizations.of(context)!.password,
                          style: TextStyle(
                            color: AppColors.greyTextColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        Gap.h_5,
                        PandasBaseTextInput(
                          controller: _passwordTextController,
                          hintText: AppLocalizations.of(context)!.password,
                          isObscured: isObscured,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context).nextFocus();
                          },
                          onChanged: (value) {
                            if (value == _repeatPasswordTextController.text &&
                                value.length >= 6) {
                              setState(() {
                                isPasswordValid = true;
                              });
                            } else {
                              setState(() {
                                isPasswordValid = false;
                              });
                            }
                          },
                          tail: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              IconButton(
                                icon: Icon(
                                  isObscured
                                      ? Icons.visibility_outlined
                                      : Icons.visibility_off_outlined,
                                  color: Colors.white,
                                  size: Dimens.iconSize * 1.2,
                                ),
                                onPressed: () => setState(() {
                                  isObscured = !isObscured;
                                }),
                              ),
                            ],
                          ),
                        ),
                        Gap.h_25,
                        Gap.h1_25,
                        Text(
                          AppLocalizations.of(context)!.repeatPassword,
                          style: TextStyle(
                            color: AppColors.greyTextColor,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            fontFeatures: const [
                              FontFeature.proportionalFigures()
                            ],
                          ),
                        ),
                        Gap.h_5,
                        PandasBaseTextInput(
                          controller: _repeatPasswordTextController,
                          hintText: AppLocalizations.of(context)!.password,
                          isObscured: isObscured,
                          onFieldSubmitted: (_) {
                            FocusScope.of(context).nextFocus();
                          },
                          onChanged: (value) {
                            if (value == _passwordTextController.text &&
                                value.length >= 6) {
                              setState(() {
                                isPasswordValid = true;
                              });
                            } else {
                              setState(() {
                                isPasswordValid = false;
                              });
                            }
                          },
                          tail: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              IconButton(
                                icon: Icon(
                                  isObscured
                                      ? Icons.visibility_outlined
                                      : Icons.visibility_off_outlined,
                                  color: Colors.white,
                                  size: Dimens.iconSize * 1.2,
                                ),
                                onPressed: () => setState(() {
                                  isObscured = !isObscured;
                                }),
                              ),
                            ],
                          ),
                        ),
                        Gap.h_25,
                        Gap.h1_5,
                        CustomButton(
                          onTap: () {
                            if (isPasswordValid && isOldPasswordValid) {}
                          },
                          child: Text(
                            AppLocalizations.of(context)!.next,
                            style: TextStyle(
                              fontSize: 18,
                              color: isPasswordValid && isOldPasswordValid
                                  ? Colors.white
                                  : AppColors.darkGreyTextColor,
                              fontFeatures: const [
                                FontFeature.proportionalFigures()
                              ],
                            ),
                          ),
                          width: double.infinity,
                          backgroundColor: isPasswordValid && isOldPasswordValid
                              ? AppColors.buttonColor
                              : AppColors.veryDarkGray,
                        ),
                        Gap.h,
                        CancelButton(
                          onTap: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            AppLocalizations.of(context)!.back,
                            style: TextStyle(
                              fontSize: 18,
                              fontFeatures: const [
                                FontFeature.proportionalFigures()
                              ],
                            ),
                          ),
                          width: double.infinity,
                          backgroundColor: AppColors.veryDarkGray,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            right: 12,
            top: 12,
            child: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.close,
                color: AppColors.greyText,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
