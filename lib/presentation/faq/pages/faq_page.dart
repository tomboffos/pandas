import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:nine_pandas/domain/faq/bloc/faq_bloc.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/presentation/main_page/widgets/blackout_inherited.dart';
import 'desktop_faq_page/desktop_faq_page.dart';
import 'mobile_faq_page/mobile_faq_page.dart';
import 'tablet_faq_page/tablet_faq_page.dart';

class FaqPage extends StatefulWidget {
  const FaqPage({Key? key}) : super(key: key);

  @override
  State<FaqPage> createState() => FaqPageState();
}

class FaqPageState extends State<FaqPage> {
  bool isBlackout = false;

  late final FaqBloc _faqBloc;

  @override
  void initState() {
    super.initState();
    _faqBloc = getIt.get<FaqBloc>()..add(FaqEvent.fetchData());
  }

  void isBlackoutChange(bool newValue) {
    setState(() {
      isBlackout = newValue;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Blackout(
      isBlackout: isBlackout,
      setBlackoutValue: isBlackoutChange,
      child: BlocListener<LocalizationBloc, LocalizationState>(
        listener: (context, state) {
          _faqBloc.add(FaqEvent.fetchData());
        },
        child: BlocProvider.value(
          value: _faqBloc,
          child: LayoutHandler(
            mobileView: MobileFaqPage(),
            desktopView: DesktopFaqPage(),
            tabletView: TabletFaqPage(),
          ),
        ),
      ),
    );
  }
}
