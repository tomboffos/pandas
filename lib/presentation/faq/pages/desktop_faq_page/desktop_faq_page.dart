import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nine_pandas/core/ui/widgets/blackout_container.dart';
import 'package:nine_pandas/domain/faq/bloc/faq_bloc.dart';
import 'package:nine_pandas/presentation/main_page/widgets/footer.dart';
import 'package:nine_pandas/presentation/main_page/widgets/navigation_header.dart';

import '../../../../../core/ui/ui_kit.dart';
import 'widgets/faq_page_content.dart';

class DesktopFaqPage extends StatefulWidget {
  const DesktopFaqPage({Key? key}) : super(key: key);

  @override
  State<DesktopFaqPage> createState() => _DesktopFaqPageState();
}

class _DesktopFaqPageState extends State<DesktopFaqPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.scaffoldBgColor,
      body: BlocBuilder<FaqBloc, FaqState>(
        builder: (context, state) {
          return state.when(
            loaded: (faqs) => Column(
              children: [
                NavigationHeader(),
                Expanded(
                  child: Stack(
                    children: [
                      SingleChildScrollView(
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                              minHeight: MediaQuery.of(context).size.height),
                          child: Column(
                            children: [
                              Gap.h1_5,
                              FaqPageContent(faqs: faqs),
                              Gap.h2,
                              Footer(),
                            ],
                          ),
                        ),
                      ),
                      BlackoutContainer(),
                    ],
                  ),
                )
              ],
            ),
            error: () => PandasServerErrorWidget(
              reloadCallback: () => BlocProvider.of<FaqBloc>(context).add(
                FaqEvent.fetchData(),
              ),
            ),
            loading: () => PandasLoadingView(),
          );
        },
      ),
    );
  }
}
