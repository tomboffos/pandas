import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/faq/models/faq_model.dart';

import '../../../../../../core/ui/ui_kit.dart';
import 'common_faqs.dart';

class FaqPageContent extends StatelessWidget {
  final List<FaqModel> faqs;

  const FaqPageContent({Key? key, required this.faqs}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Insets.w10,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          NavigationRow(currentPageText: AppLocalizations.of(context)!.faq),
          ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: MediaQuery.of(context).size.height * 0.75),
              child: CommonFaqs(faqs: faqs)),
        ],
      ),
    );
  }
}
