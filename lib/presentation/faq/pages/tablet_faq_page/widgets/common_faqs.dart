import 'package:nine_pandas/core/ui/ui_kit.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:nine_pandas/data/faq/models/faq_model.dart';

class CommonFaqs extends StatefulWidget {
  const CommonFaqs({Key? key, required this.faqs}) : super(key: key);
  static const double faqsFieldWidth = 768;

  final List<FaqModel> faqs;

  @override
  State<CommonFaqs> createState() => _CommonFaqsState();
}

class _CommonFaqsState extends State<CommonFaqs> {
  bool isOpened = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: CommonFaqs.faqsFieldWidth,
      child: Column(
        children: [
          Gap.h1_5,
          Text(
            AppLocalizations.of(context)!.faqs,
            style: TextStyle(
              fontSize: 38,
              color: Colors.white,
              fontWeight: FontWeight.w700,
            ),
          ),
          Gap.h1_5,
          Text(
            AppLocalizations.of(context)!.faqsDescription,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16,
              color: AppColors.darkGreyTextColor,
              fontWeight: FontWeight.w500,
            ),
          ),
          Gap.h2,
          Column(
            children: widget.faqs.map((faq) {
              return Column(
                children: [
                  Divider(),
                  Gap.h_75,
                  ConstrainedBox(
                    constraints: BoxConstraints(minHeight: 55),
                    child: Theme(
                      data: Theme.of(context)
                          .copyWith(dividerColor: Colors.transparent),
                      child: ExpansionTile(
                        title: Text(
                          faq.data.title,
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        onExpansionChanged: (value) {
                          setState(() {
                            isOpened = value;
                          });
                        },
                        children: [
                          Gap.h,
                          ListTile(
                            title: Text(
                              faq.data.text,
                              style: TextStyle(fontWeight: FontWeight.w400),
                            ),
                          ),
                          Gap.h,
                        ],
                        collapsedIconColor: Colors.white,
                      ),
                    ),
                  ),
                ],
              );
            }).toList(),
          ),
        ],
      ),
    );
  }
}
