import 'package:auto_route/auto_route.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/domain/localization/bloc/bloc/localization_bloc.dart';
import 'package:nine_pandas/domain/message/bloc/message_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:nine_pandas/navigation/auth_guard.dart';
import 'package:nine_pandas/navigation/router.dart';
import 'package:url_strategy/url_strategy.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'core/ui/ui_kit.dart';
import 'package:universal_io/io.dart';

import 'firebase_configs.dart';

Future<void> main() async {
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  await Firebase.initializeApp(options: DefaultFirebaseConfig.platformOptions);

  await configureInjection(Environment.prod);

  HttpOverrides.global = SSLHttpOverrides();
  setPathUrlStrategy();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  static const String appName = '9Pandas';
  static const String restorationId = 'root';
  static const List<Locale> supportedLocales = [
    Locale('ru', ''),
    Locale('en', ''),
    // Locale('tr', ''),
    // Locale('hi', ''),
    // Locale('es', ''),
    // Locale('fr', ''),
    // Locale('ar', ''),
    // Locale('pt', ''),
  ];
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _appRouter = AppRouter(
    authGuard: AuthGuard(),
  )..delegate();

  @override
  Future<void> didChangeDependencies() async {
    super.didChangeDependencies();
    init = getIt.allReady();
  }

  Future? init;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: init,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return RootRestorationScope(
              restorationId: MyApp.restorationId,
              child: MultiBlocProvider(
                providers: [
                  BlocProvider(
                    create: (context) => getIt.get<UserBloc>(),
                  ),
                  BlocProvider(
                    create: (context) => getIt.get<MessageBloc>(),
                  ),
                  BlocProvider.value(
                    value: getIt.get<LocalizationBloc>()
                      ..add(LocalizationEvent.autoLangDetection()),
                  ),
                ],
                child: BlocBuilder<LocalizationBloc, LocalizationState>(
                  builder: (context, state) {
                    return MaterialApp.router(
                      debugShowCheckedModeBanner: false,
                      title:
                          '9pandas.tv —  маркетплейс для просмотра и монетизации оригинального контента',
                      theme: appThemeData,
                      locale: state.when(
                          ruContent: (locale) {
                            return Locale('ru');
                          },
                          elseContent: (locale) {
                            return Locale('en');
                          },
                          initial: () {}),
                      routerDelegate: AutoRouterDelegate(
                        _appRouter,
                      ),
                      routeInformationParser: _appRouter.defaultRouteParser(),
                      localizationsDelegates: const [
                        AppLocalizations.delegate,
                        GlobalMaterialLocalizations.delegate,
                        GlobalWidgetsLocalizations.delegate,
                        GlobalCupertinoLocalizations.delegate,
                      ],
                      supportedLocales: MyApp.supportedLocales,
                    );
                  },
                ),
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        });
  }
}

class SSLHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
