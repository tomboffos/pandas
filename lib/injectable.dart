import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/injectable.config.dart';

final getIt = GetIt.instance;

@InjectableInit(
  initializerName: r'$initGetIt', // default
  preferRelativeImports: true, // default
  asExtension: false,
  // default
)
@injectableInit
Future<void> configureInjection(String env) async {
  await $initGetIt(getIt, environment: env);
}

void configureDependencies() => $initGetIt(getIt);
