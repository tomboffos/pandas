import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/faq/models/faq_model.dart';

abstract class FaqRepository {
  Future<Either<Failure, List<FaqModel>>> getFaq(String locale);
}
