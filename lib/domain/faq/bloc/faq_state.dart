part of 'faq_bloc.dart';

@freezed
class FaqState with _$FaqState {
  const factory FaqState.loading() = _Loading;
  const factory FaqState.loaded(List<FaqModel> faqs) = Loaded;
  const factory FaqState.error() = _Error;
}
