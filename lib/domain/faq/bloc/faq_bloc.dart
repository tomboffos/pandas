import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/data/faq/models/faq_model.dart';
import 'package:nine_pandas/domain/faq/usecases/get_faq.dart';
import 'package:nine_pandas/domain/localization/usecases/get_localization.dart';

part 'faq_event.dart';
part 'faq_state.dart';
part 'faq_bloc.freezed.dart';

@injectable
class FaqBloc extends Bloc<FaqEvent, FaqState> {
  final GetLocalization _getLocalization;
  final GetFaq _getFaq;

  FaqBloc(this._getFaq, this._getLocalization) : super(_Loading()) {
    on<_FetchData>((event, emit) async {
      emit(_Loading());
      final String currentLocale = await _getLocalization() ?? 'ru';
      final res = await _getFaq(currentLocale.toLowerCase());

      emit(res.fold((l) => _Error(), (r) => Loaded(r)));
    });
  }
}
