import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/faq/models/faq_model.dart';
import 'package:nine_pandas/domain/faq/repository/faq_repository.dart';

@injectable
class GetFaq {
  final FaqRepository _faqRepository;

  GetFaq(this._faqRepository);

  Future<Either<Failure, List<FaqModel>>> call(String locale) async {
    return await _faqRepository.getFaq(locale);
  }
}
