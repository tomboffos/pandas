part of 'message_bloc.dart';

@freezed
class MessageState with _$MessageState {
  const factory MessageState.initial() = _Initial;
  const factory MessageState.error(String message) = _Error;
  const factory MessageState.info(String message) = _Info;
}
