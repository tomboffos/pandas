part of 'message_bloc.dart';

@freezed
class MessageEvent with _$MessageEvent {
  const factory MessageEvent.errorMessage(String message) = _ErrorMessage;
  const factory MessageEvent.infoMessage(String message) = _InfoMessage;
}
