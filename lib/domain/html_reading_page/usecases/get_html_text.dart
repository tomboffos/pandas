import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/html_reading_page/models/html_reading.dart';
import 'package:nine_pandas/domain/html_reading_page/repository/html_reading_page_repository.dart';

@injectable
class GetHtmlText {
  final HtmlReadingPageRepository _htmlReadingPageRepository;

  GetHtmlText(this._htmlReadingPageRepository);

  Future<Either<Failure, HtmlReading>> call(
      String docType, String locale) async {
    return await _htmlReadingPageRepository.getHtmlDoc(docType, locale);
  }
}
