part of 'html_reader_page_bloc.dart';

@freezed
class HtmlReadingPageState with _$HtmlReadingPageState {
  const factory HtmlReadingPageState.loading() = _Loading;
  const factory HtmlReadingPageState.error() = _Error;
  const factory HtmlReadingPageState.loaded(HtmlReading policy) = _Loaded;
}
