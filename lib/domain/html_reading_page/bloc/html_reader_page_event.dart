part of 'html_reader_page_bloc.dart';

@freezed
class HtmlReadingPageEvent with _$HtmlReadingPageEvent {
  const factory HtmlReadingPageEvent.started() = _Started;
  const factory HtmlReadingPageEvent.fetchData(String docType) = _FetchData;
}
