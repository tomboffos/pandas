import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/data/html_reading_page/models/html_reading.dart';
import 'package:nine_pandas/domain/localization/usecases/get_localization.dart';

import '../usecases/get_html_text.dart';

part 'html_reader_page_event.dart';
part 'html_reader_page_state.dart';
part 'html_reader_page_bloc.freezed.dart';

@injectable
class HtmlReadingPageBloc
    extends Bloc<HtmlReadingPageEvent, HtmlReadingPageState> {
  final GetHtmlText _getHtmlText;
  final GetLocalization _getLocalization;

  HtmlReadingPageBloc(this._getHtmlText, this._getLocalization)
      : super(_Loading()) {
    on<_FetchData>((event, emit) async {
      emit(_Loading());
      final String currentLocale = await _getLocalization() ?? 'ru';
      final res =
          await _getHtmlText(event.docType, currentLocale.toLowerCase());

      emit(res.fold((failure) => _Error(), (htmlDoc) => _Loaded(htmlDoc)));
    });
  }
}
