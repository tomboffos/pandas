import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/html_reading_page/models/html_reading.dart';

abstract class HtmlReadingPageRepository {
  Future<Either<Failure, HtmlReading>> getHtmlDoc(
      String docType, String locale);
}
