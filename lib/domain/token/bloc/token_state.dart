part of 'token_bloc.dart';

@freezed
class TokenState with _$TokenState {
  const factory TokenState.initial() = _Initial;
  const factory TokenState.loading() = _Loading;
  const factory TokenState.loaded(Token? token) = _Loaded;
}
