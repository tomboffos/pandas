import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/data/token/model/token.dart';

import '../usecases/refresh_token.dart';

part 'token_event.dart';
part 'token_state.dart';
part 'token_bloc.freezed.dart';

@singleton
class TokenBloc extends Bloc<TokenEvent, TokenState> {
  final RefreshToken _refreshToken;

  TokenBloc._create(this._refreshToken, TokenState initialState)
      : super(_Initial()) {
    on<_RefreshToken>((event, emit) async {
      await _getTokenOrFailure(emit);
    });
  }

  @factoryMethod
  static Future<TokenBloc> create({
    required RefreshToken refreshToken,
  }) async {
    final token = await refreshToken();
    return TokenBloc._create(
      refreshToken,
      _Loaded(token),
    );
  }

  _getTokenOrFailure(Emitter<TokenState> emit) async {
    final token = await _refreshToken();
    emit(_Loaded(token));
  }
}
