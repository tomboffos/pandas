import 'package:injectable/injectable.dart';
import 'package:nine_pandas/data/token/model/token.dart';
import 'package:nine_pandas/domain/token/token_repository.dart';

@injectable
class RefreshToken {
  final TokenRepository repository;

  RefreshToken(this.repository);

  Future<Token?> call() async {
    return await repository.refreshToken();
  }
}
