import 'package:nine_pandas/data/token/model/token.dart';

abstract class TokenRepository {
  Future<Token?> refreshToken();
}
