import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';
import 'package:nine_pandas/domain/localization/usecases/get_localization.dart';
import 'package:nine_pandas/domain/movies/usecases/get_movies.dart';

part 'movies_event.dart';
part 'movies_state.dart';
part 'movies_bloc.freezed.dart';

@singleton
class MoviesBloc extends Bloc<MoviesEvent, MoviesState> {
  GetMovies _getMovies;
  final GetLocalization _getLocalization;
  MoviesBloc(this._getMovies, this._getLocalization) : super(_Initial()) {
    on<_GetMovie>((event, emit) async {
      emit(_DataLoading());
      try {
        final String currentLocale = await _getLocalization() ?? 'ru';
        final res = await _getMovies(currentLocale.toLowerCase());
        res.fold((failure) {
          if (failure is ServerFailure) {
            emit(_Error());
          }
        }, (movies) async {
          emit(_DataLoaded(movies));
        });
      } catch (e) {
        emit(_Error());
      }
    });
  }
}
