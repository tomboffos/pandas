part of 'movies_bloc.dart';

@freezed
class MoviesState with _$MoviesState {
  const factory MoviesState.initial() = _Initial;
  const factory MoviesState.dataLoaded(List<Movie> movies) = _DataLoaded;
  const factory MoviesState.dataLoading() = _DataLoading;
  const factory MoviesState.error() = _Error;
}
