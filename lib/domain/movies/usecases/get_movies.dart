import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';
import 'package:nine_pandas/domain/movies/repository/main_page_repository.dart';

@injectable
class GetMovies {
  final MoviesRepository repository;

  GetMovies(this.repository);

  Future<Either<Failure, List<Movie>>> call(String locale) async {
    return repository.getMovies(locale);
  }
}
