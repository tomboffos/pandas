import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';

abstract class MoviesRepository {
  Future<Either<Failure, List<Movie>>> getMovies(String locale);
  Future<Either<Failure, Movie>> getMovieById(int id, String locale);
}
