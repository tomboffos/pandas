import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';

import '../repository/change_password_repository.dart';

@injectable
class ChangePassword {
  final ChangePasswordRepository _changePasswordRepository;

  ChangePassword(this._changePasswordRepository);

  Future<Either<Failure, dynamic>> call({
    required String oldPassword,
    required String newPassword,
    required String repeatPassword,
  }) async {
    return await _changePasswordRepository.changePassword(
      oldPassword: oldPassword,
      newPassword: newPassword,
      repeatPassword: repeatPassword,
    );
  }
}
