part of 'change_password_bloc.dart';

@freezed
class ChangePasswordState with _$ChangePasswordState {
  const factory ChangePasswordState.initial() = _Initial;
  const factory ChangePasswordState.loading() = _Loading;
  const factory ChangePasswordState.error(String error) = _Error;
  const factory ChangePasswordState.changePasswordSuccess() =
      _ChangePasswordSuccess;
  const factory ChangePasswordState.incorrectOldPassword() =
      _IncorrectOldPassword;
}
