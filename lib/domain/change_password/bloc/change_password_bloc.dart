import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';

import '../usecases/change_password.dart';

part 'change_password_event.dart';
part 'change_password_state.dart';
part 'change_password_bloc.freezed.dart';

@injectable
class ChangePasswordBloc
    extends Bloc<ChangePasswordEvent, ChangePasswordState> {
  final ChangePassword _changePassword;

  ChangePasswordBloc(this._changePassword) : super(_Initial()) {
    on<_ChangePassword>(((event, emit) async {
      emit(_Loading());
      final res = await _changePassword(
        oldPassword: event.oldPassword,
        newPassword: event.newPassword,
        repeatPassword: event.repeatPassword,
      );

      res.fold((l) {
        if (l is ServerFailure) {
          emit(_Error(l.message ?? 'Error'));
        }
      }, (r) {
        if (r is OldPasswordIncorrect) {
          emit(_IncorrectOldPassword());
        } else {
          emit(_ChangePasswordSuccess());
        }
      });
    }));
  }
}
