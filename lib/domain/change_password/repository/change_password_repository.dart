import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';

abstract class ChangePasswordRepository {
  Future<Either<Failure, dynamic>> changePassword({
    required String oldPassword,
    required String newPassword,
    required String repeatPassword,
  });
}
