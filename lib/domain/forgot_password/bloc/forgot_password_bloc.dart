import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';

import '../usecases/new_password.dart';
import '../usecases/recovery_password.dart';

part 'forgot_password_event.dart';
part 'forgot_password_state.dart';
part 'forgot_password_bloc.freezed.dart';

@injectable
class ForgotPasswordBloc
    extends Bloc<ForgotPasswordEvent, ForgotPasswordState> {
  NewPassword _newPassword;
  RecoveryPassword _recoveryPassword;
  ForgotPasswordBloc(this._newPassword, this._recoveryPassword)
      : super(_Initial()) {
    on<_RecoveryPassword>((event, emit) async {
      try {
        emit(_Loading());
        final res = await _recoveryPassword(event.email);

        res.fold((failure) {
          emit(_Error());
        }, (response) {
          if (response is UserNotExists) {
            emit(_UserNotExists());
          } else {
            emit(_Loaded());
          }
        });
      } catch (e) {
        emit(_Error());
      }
    });

    on<_NewPassword>((event, emit) async {
      try {
        emit(_Loading());
        final res = await _newPassword(
            event.hash, event.password, event.repeatPassword);

        res.fold((failure) => emit(_Error()), (response) => emit(_Loaded()));
      } catch (e) {
        emit(_Error());
      }
    });
  }
}
