part of 'forgot_password_bloc.dart';

@freezed
class ForgotPasswordState with _$ForgotPasswordState {
  const factory ForgotPasswordState.initial() = _Initial;
  const factory ForgotPasswordState.loading() = _Loading;
  const factory ForgotPasswordState.error() = _Error;
  const factory ForgotPasswordState.userNotExists() = _UserNotExists;
  const factory ForgotPasswordState.loaded() = _Loaded;
}
