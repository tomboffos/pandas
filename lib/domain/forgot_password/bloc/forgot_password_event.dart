part of 'forgot_password_bloc.dart';

@freezed
class ForgotPasswordEvent with _$ForgotPasswordEvent {
  const factory ForgotPasswordEvent.recoveryPassword(String email) =
      _RecoveryPassword;
  const factory ForgotPasswordEvent.newPassword(
    String hash,
    String password,
    String repeatPassword,
  ) = _NewPassword;
}
