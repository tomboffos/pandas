import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/forgot_password/repository/forgot_password_repository.dart';

@injectable
class NewPassword {
  final ForgotPasswordRepository _forgotPasswordRepository;

  NewPassword(this._forgotPasswordRepository);

  Future<Either<Failure, dynamic>> call(
    String hash,
    String password,
    String repeatPassword,
  ) async {
    return await _forgotPasswordRepository.newPassword(
        hash, password, repeatPassword);
  }
}
