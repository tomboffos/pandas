import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/forgot_password/repository/forgot_password_repository.dart';

@injectable
class RecoveryPassword {
  final ForgotPasswordRepository _forgotPasswordRepository;

  RecoveryPassword(this._forgotPasswordRepository);

  Future<Either<Failure, dynamic>> call(String email) async {
    return await _forgotPasswordRepository.recoveryPassword(email);
  }
}
