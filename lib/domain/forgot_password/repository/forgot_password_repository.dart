import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';

abstract class ForgotPasswordRepository {
  Future<Either<Failure, dynamic>> recoveryPassword(String email);
  Future<Either<Failure, dynamic>> newPassword(
      String hash, String password, String repeatPassword);
}
