import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/domain/localization/usecases/get_localization.dart';
import 'package:nine_pandas/domain/localization/usecases/save_localization.dart';
import 'package:universal_io/io.dart';

part 'localization_event.dart';
part 'localization_state.dart';
part 'localization_bloc.freezed.dart';

@singleton
class LocalizationBloc extends Bloc<LocalizationEvent, LocalizationState> {
  GetLocalization _getLocalization;
  SaveLocalization _saveLocalization;
  LocalizationBloc(this._getLocalization, this._saveLocalization)
      : super(_Initial()) {
    on<_ChangeLanguage>((event, emit) async {
      if (event.locale == 'Ru') {
        emit(_RuChanged(event.locale));
      } else {
        emit(_ElseChanged(event.locale));
      }
      await _saveLocalization(event.locale);
    });

    on<_AutoLangDetection>((event, emit) async {
      final result = await _getLocalization();

      final String currentLocale =
          result ?? (Platform.localeName.split('-')[0] == 'ru' ? 'Ru' : 'En');
      await _saveLocalization(currentLocale);
      add(_ChangeLanguage(currentLocale));
    });
    add(LocalizationEvent.autoLangDetection());
  }
}
