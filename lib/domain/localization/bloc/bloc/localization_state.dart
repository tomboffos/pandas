part of 'localization_bloc.dart';

@freezed
class LocalizationState with _$LocalizationState {
  const factory LocalizationState.initial() = _Initial;
  const factory LocalizationState.ruContent(String locale) = _RuChanged;
  const factory LocalizationState.elseContent(String locale) = _ElseChanged;
}
