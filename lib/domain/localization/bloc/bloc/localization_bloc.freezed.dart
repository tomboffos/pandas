// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'localization_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LocalizationEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String locale) changeLanguage,
    required TResult Function() autoLangDetection,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String locale)? changeLanguage,
    TResult? Function()? autoLangDetection,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String locale)? changeLanguage,
    TResult Function()? autoLangDetection,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ChangeLanguage value) changeLanguage,
    required TResult Function(_AutoLangDetection value) autoLangDetection,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ChangeLanguage value)? changeLanguage,
    TResult? Function(_AutoLangDetection value)? autoLangDetection,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ChangeLanguage value)? changeLanguage,
    TResult Function(_AutoLangDetection value)? autoLangDetection,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LocalizationEventCopyWith<$Res> {
  factory $LocalizationEventCopyWith(
          LocalizationEvent value, $Res Function(LocalizationEvent) then) =
      _$LocalizationEventCopyWithImpl<$Res, LocalizationEvent>;
}

/// @nodoc
class _$LocalizationEventCopyWithImpl<$Res, $Val extends LocalizationEvent>
    implements $LocalizationEventCopyWith<$Res> {
  _$LocalizationEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_ChangeLanguageCopyWith<$Res> {
  factory _$$_ChangeLanguageCopyWith(
          _$_ChangeLanguage value, $Res Function(_$_ChangeLanguage) then) =
      __$$_ChangeLanguageCopyWithImpl<$Res>;
  @useResult
  $Res call({String locale});
}

/// @nodoc
class __$$_ChangeLanguageCopyWithImpl<$Res>
    extends _$LocalizationEventCopyWithImpl<$Res, _$_ChangeLanguage>
    implements _$$_ChangeLanguageCopyWith<$Res> {
  __$$_ChangeLanguageCopyWithImpl(
      _$_ChangeLanguage _value, $Res Function(_$_ChangeLanguage) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? locale = null,
  }) {
    return _then(_$_ChangeLanguage(
      null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_ChangeLanguage implements _ChangeLanguage {
  const _$_ChangeLanguage(this.locale);

  @override
  final String locale;

  @override
  String toString() {
    return 'LocalizationEvent.changeLanguage(locale: $locale)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChangeLanguage &&
            (identical(other.locale, locale) || other.locale == locale));
  }

  @override
  int get hashCode => Object.hash(runtimeType, locale);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChangeLanguageCopyWith<_$_ChangeLanguage> get copyWith =>
      __$$_ChangeLanguageCopyWithImpl<_$_ChangeLanguage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String locale) changeLanguage,
    required TResult Function() autoLangDetection,
  }) {
    return changeLanguage(locale);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String locale)? changeLanguage,
    TResult? Function()? autoLangDetection,
  }) {
    return changeLanguage?.call(locale);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String locale)? changeLanguage,
    TResult Function()? autoLangDetection,
    required TResult orElse(),
  }) {
    if (changeLanguage != null) {
      return changeLanguage(locale);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ChangeLanguage value) changeLanguage,
    required TResult Function(_AutoLangDetection value) autoLangDetection,
  }) {
    return changeLanguage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ChangeLanguage value)? changeLanguage,
    TResult? Function(_AutoLangDetection value)? autoLangDetection,
  }) {
    return changeLanguage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ChangeLanguage value)? changeLanguage,
    TResult Function(_AutoLangDetection value)? autoLangDetection,
    required TResult orElse(),
  }) {
    if (changeLanguage != null) {
      return changeLanguage(this);
    }
    return orElse();
  }
}

abstract class _ChangeLanguage implements LocalizationEvent {
  const factory _ChangeLanguage(final String locale) = _$_ChangeLanguage;

  String get locale;
  @JsonKey(ignore: true)
  _$$_ChangeLanguageCopyWith<_$_ChangeLanguage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_AutoLangDetectionCopyWith<$Res> {
  factory _$$_AutoLangDetectionCopyWith(_$_AutoLangDetection value,
          $Res Function(_$_AutoLangDetection) then) =
      __$$_AutoLangDetectionCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_AutoLangDetectionCopyWithImpl<$Res>
    extends _$LocalizationEventCopyWithImpl<$Res, _$_AutoLangDetection>
    implements _$$_AutoLangDetectionCopyWith<$Res> {
  __$$_AutoLangDetectionCopyWithImpl(
      _$_AutoLangDetection _value, $Res Function(_$_AutoLangDetection) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_AutoLangDetection implements _AutoLangDetection {
  const _$_AutoLangDetection();

  @override
  String toString() {
    return 'LocalizationEvent.autoLangDetection()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_AutoLangDetection);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String locale) changeLanguage,
    required TResult Function() autoLangDetection,
  }) {
    return autoLangDetection();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(String locale)? changeLanguage,
    TResult? Function()? autoLangDetection,
  }) {
    return autoLangDetection?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String locale)? changeLanguage,
    TResult Function()? autoLangDetection,
    required TResult orElse(),
  }) {
    if (autoLangDetection != null) {
      return autoLangDetection();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_ChangeLanguage value) changeLanguage,
    required TResult Function(_AutoLangDetection value) autoLangDetection,
  }) {
    return autoLangDetection(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_ChangeLanguage value)? changeLanguage,
    TResult? Function(_AutoLangDetection value)? autoLangDetection,
  }) {
    return autoLangDetection?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_ChangeLanguage value)? changeLanguage,
    TResult Function(_AutoLangDetection value)? autoLangDetection,
    required TResult orElse(),
  }) {
    if (autoLangDetection != null) {
      return autoLangDetection(this);
    }
    return orElse();
  }
}

abstract class _AutoLangDetection implements LocalizationEvent {
  const factory _AutoLangDetection() = _$_AutoLangDetection;
}

/// @nodoc
mixin _$LocalizationState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String locale) ruContent,
    required TResult Function(String locale) elseContent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(String locale)? ruContent,
    TResult? Function(String locale)? elseContent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String locale)? ruContent,
    TResult Function(String locale)? elseContent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_RuChanged value) ruContent,
    required TResult Function(_ElseChanged value) elseContent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_RuChanged value)? ruContent,
    TResult? Function(_ElseChanged value)? elseContent,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_RuChanged value)? ruContent,
    TResult Function(_ElseChanged value)? elseContent,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LocalizationStateCopyWith<$Res> {
  factory $LocalizationStateCopyWith(
          LocalizationState value, $Res Function(LocalizationState) then) =
      _$LocalizationStateCopyWithImpl<$Res, LocalizationState>;
}

/// @nodoc
class _$LocalizationStateCopyWithImpl<$Res, $Val extends LocalizationState>
    implements $LocalizationStateCopyWith<$Res> {
  _$LocalizationStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$LocalizationStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'LocalizationState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String locale) ruContent,
    required TResult Function(String locale) elseContent,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(String locale)? ruContent,
    TResult? Function(String locale)? elseContent,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String locale)? ruContent,
    TResult Function(String locale)? elseContent,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_RuChanged value) ruContent,
    required TResult Function(_ElseChanged value) elseContent,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_RuChanged value)? ruContent,
    TResult? Function(_ElseChanged value)? elseContent,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_RuChanged value)? ruContent,
    TResult Function(_ElseChanged value)? elseContent,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements LocalizationState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_RuChangedCopyWith<$Res> {
  factory _$$_RuChangedCopyWith(
          _$_RuChanged value, $Res Function(_$_RuChanged) then) =
      __$$_RuChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({String locale});
}

/// @nodoc
class __$$_RuChangedCopyWithImpl<$Res>
    extends _$LocalizationStateCopyWithImpl<$Res, _$_RuChanged>
    implements _$$_RuChangedCopyWith<$Res> {
  __$$_RuChangedCopyWithImpl(
      _$_RuChanged _value, $Res Function(_$_RuChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? locale = null,
  }) {
    return _then(_$_RuChanged(
      null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_RuChanged implements _RuChanged {
  const _$_RuChanged(this.locale);

  @override
  final String locale;

  @override
  String toString() {
    return 'LocalizationState.ruContent(locale: $locale)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RuChanged &&
            (identical(other.locale, locale) || other.locale == locale));
  }

  @override
  int get hashCode => Object.hash(runtimeType, locale);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RuChangedCopyWith<_$_RuChanged> get copyWith =>
      __$$_RuChangedCopyWithImpl<_$_RuChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String locale) ruContent,
    required TResult Function(String locale) elseContent,
  }) {
    return ruContent(locale);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(String locale)? ruContent,
    TResult? Function(String locale)? elseContent,
  }) {
    return ruContent?.call(locale);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String locale)? ruContent,
    TResult Function(String locale)? elseContent,
    required TResult orElse(),
  }) {
    if (ruContent != null) {
      return ruContent(locale);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_RuChanged value) ruContent,
    required TResult Function(_ElseChanged value) elseContent,
  }) {
    return ruContent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_RuChanged value)? ruContent,
    TResult? Function(_ElseChanged value)? elseContent,
  }) {
    return ruContent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_RuChanged value)? ruContent,
    TResult Function(_ElseChanged value)? elseContent,
    required TResult orElse(),
  }) {
    if (ruContent != null) {
      return ruContent(this);
    }
    return orElse();
  }
}

abstract class _RuChanged implements LocalizationState {
  const factory _RuChanged(final String locale) = _$_RuChanged;

  String get locale;
  @JsonKey(ignore: true)
  _$$_RuChangedCopyWith<_$_RuChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ElseChangedCopyWith<$Res> {
  factory _$$_ElseChangedCopyWith(
          _$_ElseChanged value, $Res Function(_$_ElseChanged) then) =
      __$$_ElseChangedCopyWithImpl<$Res>;
  @useResult
  $Res call({String locale});
}

/// @nodoc
class __$$_ElseChangedCopyWithImpl<$Res>
    extends _$LocalizationStateCopyWithImpl<$Res, _$_ElseChanged>
    implements _$$_ElseChangedCopyWith<$Res> {
  __$$_ElseChangedCopyWithImpl(
      _$_ElseChanged _value, $Res Function(_$_ElseChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? locale = null,
  }) {
    return _then(_$_ElseChanged(
      null == locale
          ? _value.locale
          : locale // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_ElseChanged implements _ElseChanged {
  const _$_ElseChanged(this.locale);

  @override
  final String locale;

  @override
  String toString() {
    return 'LocalizationState.elseContent(locale: $locale)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ElseChanged &&
            (identical(other.locale, locale) || other.locale == locale));
  }

  @override
  int get hashCode => Object.hash(runtimeType, locale);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ElseChangedCopyWith<_$_ElseChanged> get copyWith =>
      __$$_ElseChangedCopyWithImpl<_$_ElseChanged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function(String locale) ruContent,
    required TResult Function(String locale) elseContent,
  }) {
    return elseContent(locale);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function(String locale)? ruContent,
    TResult? Function(String locale)? elseContent,
  }) {
    return elseContent?.call(locale);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function(String locale)? ruContent,
    TResult Function(String locale)? elseContent,
    required TResult orElse(),
  }) {
    if (elseContent != null) {
      return elseContent(locale);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_RuChanged value) ruContent,
    required TResult Function(_ElseChanged value) elseContent,
  }) {
    return elseContent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_RuChanged value)? ruContent,
    TResult? Function(_ElseChanged value)? elseContent,
  }) {
    return elseContent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_RuChanged value)? ruContent,
    TResult Function(_ElseChanged value)? elseContent,
    required TResult orElse(),
  }) {
    if (elseContent != null) {
      return elseContent(this);
    }
    return orElse();
  }
}

abstract class _ElseChanged implements LocalizationState {
  const factory _ElseChanged(final String locale) = _$_ElseChanged;

  String get locale;
  @JsonKey(ignore: true)
  _$$_ElseChangedCopyWith<_$_ElseChanged> get copyWith =>
      throw _privateConstructorUsedError;
}
