part of 'localization_bloc.dart';

@freezed
class LocalizationEvent with _$LocalizationEvent {
  const factory LocalizationEvent.changeLanguage(String locale) =
      _ChangeLanguage;
  const factory LocalizationEvent.autoLangDetection() = _AutoLangDetection;
}
