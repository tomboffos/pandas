import 'package:injectable/injectable.dart';
import 'package:nine_pandas/domain/localization/repository/localization_repository.dart';

@injectable
class GetLocalization {
  final LocalizationRepository localizationRepository;

  GetLocalization(this.localizationRepository);

  Future<String?> call() async {
    final String currentLocalization =
        await localizationRepository.getLocalization();
    if (currentLocalization.isNotEmpty) {
      return currentLocalization;
    } else {
      return null;
    }
  }
}
