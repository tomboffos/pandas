import 'package:injectable/injectable.dart';
import 'package:nine_pandas/domain/localization/repository/localization_repository.dart';

@injectable
class SaveLocalization {
  final LocalizationRepository ageRestrictionRepository;

  SaveLocalization(this.ageRestrictionRepository);

  Future call(String locale) async {
    return await ageRestrictionRepository.saveLocalization(locale);
  }
}
