abstract class LocalizationRepository {
  Future saveLocalization(String locale);
  Future getLocalization();
}
