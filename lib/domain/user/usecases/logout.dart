import 'package:injectable/injectable.dart';
import 'package:nine_pandas/domain/user/repository/user_repository.dart';

@injectable
class Logout {
  final UserRepository _userRepository;

  Logout(this._userRepository);

  Future call() async {
    await _userRepository.logout();
  }
}
