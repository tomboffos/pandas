import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/user/models/user.dart';
import 'package:nine_pandas/domain/user/repository/user_repository.dart';

@injectable
class UpdateUser {
  final UserRepository _userRepository;

  UpdateUser(this._userRepository);

  Future<Either<Failure, User>> call(
      {String? name,
      String? gender,
      String? phone,
      String? birthdate,
      required bool emailNotification,
      required bool smsNotification,
      required int userId}) async {
    return await _userRepository.updateUser(
        name: name,
        gender: gender,
        phone: phone,
        birthdate: birthdate,
        emailNotification: emailNotification,
        smsNotification: smsNotification,
        userId: userId);
  }
}
