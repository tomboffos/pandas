import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/user/models/user.dart';
import 'package:nine_pandas/domain/user/repository/user_repository.dart';

@injectable
class GetMe {
  final UserRepository _userRepository;

  GetMe(this._userRepository);

  Future<Either<Failure, User>> call() async {
    return await _userRepository.getMe();
  }
}
