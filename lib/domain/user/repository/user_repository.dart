import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/user/models/user.dart';

abstract class UserRepository {
  Future<Either<Failure, dynamic>> loginUser(
      {required String username, required String password});

  Future<Either<Failure, User>> getMe();

  Future logout();

  Future<Either<Failure, User>> updateUser(
      {String? name,
      String? gender,
      String? phone,
      String? birthdate,
      required bool emailNotification,
      required bool smsNotification,
      required int userId});
}
