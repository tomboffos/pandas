part of 'user_bloc.dart';

@freezed
class UserState with _$UserState {
  const factory UserState.initial() = _Initial;
  const factory UserState.loading() = _Loading;
  const factory UserState.error(String error) = _Error;
  const factory UserState.logged(User user) = Logged;
  const factory UserState.loginSuccess() = _LoginSuccess;
  const factory UserState.userNotVerified() = _UserNotVerified;
  const factory UserState.updateSuccess() = _UpdateSuccess;
  const factory UserState.logout() = _LogOut;
  const factory UserState.recoveryPassword(String hash) = _RecoveryPassword;
}
