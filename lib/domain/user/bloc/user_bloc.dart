import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/user/models/user.dart';
import 'package:nine_pandas/domain/message/bloc/message_bloc.dart';
import 'package:nine_pandas/domain/user/usecases/get_me.dart';
import 'package:nine_pandas/domain/user/usecases/logout.dart';
import 'package:nine_pandas/domain/user/usecases/update_user.dart';

part 'user_event.dart';
part 'user_state.dart';
part 'user_bloc.freezed.dart';

@lazySingleton
class UserBloc extends Bloc<UserEvent, UserState> {
  final GetMe _getMe;
  final Logout _logout;
  final UpdateUser _updateUser;

  UserBloc(
    this._getMe,
    this._logout,
    this._updateUser,
  ) : super(_Initial()) {
    on<Authorize>((event, emit) async {
      final user = await _getMe();
      user.fold((failure) {}, (user) {
        if (user.isVerified) {
          emit(Logged(user));
        } else {
          emit(_UserNotVerified());
          _logout();
        }
      });
    });

    // User Log out
    on<_Logout>((event, emit) async {
      _logout();
      emit(_LogOut());
      emit(_Initial());
    });

    on<_UpdateProfile>((event, emit) async {
      emit(_Loading());
      final res = await _updateUser(
          name: event.name,
          gender: event.gender,
          phone: event.phone,
          birthdate: event.birthdate!.isNotEmpty ? event.birthdate : null,
          emailNotification: event.emailNotification,
          smsNotification: event.smsNotification,
          userId: 89);
      res.fold((l) {
        if (l is ServerFailure) {
          emit(_Error('Error'));
          event.messageBloc
              .add(MessageEvent.errorMessage(l.message ?? 'Error'));
        }
      }, (r) {
        emit(_UpdateSuccess());
        add(Authorize());
      });
    });

    on<_RecoveryPasswordEvent>((event, emit) {
      emit(UserState.recoveryPassword(event.hash));
    });

    add(Authorize());
  }
}
