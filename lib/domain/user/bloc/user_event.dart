part of 'user_bloc.dart';

@freezed
class UserEvent with _$UserEvent {
  const factory UserEvent.logout() = _Logout;
  const factory UserEvent.authorize() = Authorize;
  const factory UserEvent.recoveryPasswordEvent(String hash) =
      _RecoveryPasswordEvent;

  const factory UserEvent.updateProfile({
    String? name,
    String? gender,
    String? phone,
    String? birthdate,
    required bool emailNotification,
    required bool smsNotification,
    required MessageBloc messageBloc,
  }) = _UpdateProfile;
}
