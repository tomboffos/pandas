// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$UserEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() logout,
    required TResult Function() authorize,
    required TResult Function(String hash) recoveryPasswordEvent,
    required TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)
        updateProfile,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? logout,
    TResult? Function()? authorize,
    TResult? Function(String hash)? recoveryPasswordEvent,
    TResult? Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? logout,
    TResult Function()? authorize,
    TResult Function(String hash)? recoveryPasswordEvent,
    TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Logout value) logout,
    required TResult Function(Authorize value) authorize,
    required TResult Function(_RecoveryPasswordEvent value)
        recoveryPasswordEvent,
    required TResult Function(_UpdateProfile value) updateProfile,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Logout value)? logout,
    TResult? Function(Authorize value)? authorize,
    TResult? Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult? Function(_UpdateProfile value)? updateProfile,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Logout value)? logout,
    TResult Function(Authorize value)? authorize,
    TResult Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult Function(_UpdateProfile value)? updateProfile,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserEventCopyWith<$Res> {
  factory $UserEventCopyWith(UserEvent value, $Res Function(UserEvent) then) =
      _$UserEventCopyWithImpl<$Res, UserEvent>;
}

/// @nodoc
class _$UserEventCopyWithImpl<$Res, $Val extends UserEvent>
    implements $UserEventCopyWith<$Res> {
  _$UserEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_LogoutCopyWith<$Res> {
  factory _$$_LogoutCopyWith(_$_Logout value, $Res Function(_$_Logout) then) =
      __$$_LogoutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LogoutCopyWithImpl<$Res>
    extends _$UserEventCopyWithImpl<$Res, _$_Logout>
    implements _$$_LogoutCopyWith<$Res> {
  __$$_LogoutCopyWithImpl(_$_Logout _value, $Res Function(_$_Logout) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Logout implements _Logout {
  const _$_Logout();

  @override
  String toString() {
    return 'UserEvent.logout()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Logout);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() logout,
    required TResult Function() authorize,
    required TResult Function(String hash) recoveryPasswordEvent,
    required TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)
        updateProfile,
  }) {
    return logout();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? logout,
    TResult? Function()? authorize,
    TResult? Function(String hash)? recoveryPasswordEvent,
    TResult? Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
  }) {
    return logout?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? logout,
    TResult Function()? authorize,
    TResult Function(String hash)? recoveryPasswordEvent,
    TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
    required TResult orElse(),
  }) {
    if (logout != null) {
      return logout();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Logout value) logout,
    required TResult Function(Authorize value) authorize,
    required TResult Function(_RecoveryPasswordEvent value)
        recoveryPasswordEvent,
    required TResult Function(_UpdateProfile value) updateProfile,
  }) {
    return logout(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Logout value)? logout,
    TResult? Function(Authorize value)? authorize,
    TResult? Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult? Function(_UpdateProfile value)? updateProfile,
  }) {
    return logout?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Logout value)? logout,
    TResult Function(Authorize value)? authorize,
    TResult Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult Function(_UpdateProfile value)? updateProfile,
    required TResult orElse(),
  }) {
    if (logout != null) {
      return logout(this);
    }
    return orElse();
  }
}

abstract class _Logout implements UserEvent {
  const factory _Logout() = _$_Logout;
}

/// @nodoc
abstract class _$$AuthorizeCopyWith<$Res> {
  factory _$$AuthorizeCopyWith(
          _$Authorize value, $Res Function(_$Authorize) then) =
      __$$AuthorizeCopyWithImpl<$Res>;
}

/// @nodoc
class __$$AuthorizeCopyWithImpl<$Res>
    extends _$UserEventCopyWithImpl<$Res, _$Authorize>
    implements _$$AuthorizeCopyWith<$Res> {
  __$$AuthorizeCopyWithImpl(
      _$Authorize _value, $Res Function(_$Authorize) _then)
      : super(_value, _then);
}

/// @nodoc

class _$Authorize implements Authorize {
  const _$Authorize();

  @override
  String toString() {
    return 'UserEvent.authorize()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Authorize);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() logout,
    required TResult Function() authorize,
    required TResult Function(String hash) recoveryPasswordEvent,
    required TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)
        updateProfile,
  }) {
    return authorize();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? logout,
    TResult? Function()? authorize,
    TResult? Function(String hash)? recoveryPasswordEvent,
    TResult? Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
  }) {
    return authorize?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? logout,
    TResult Function()? authorize,
    TResult Function(String hash)? recoveryPasswordEvent,
    TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
    required TResult orElse(),
  }) {
    if (authorize != null) {
      return authorize();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Logout value) logout,
    required TResult Function(Authorize value) authorize,
    required TResult Function(_RecoveryPasswordEvent value)
        recoveryPasswordEvent,
    required TResult Function(_UpdateProfile value) updateProfile,
  }) {
    return authorize(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Logout value)? logout,
    TResult? Function(Authorize value)? authorize,
    TResult? Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult? Function(_UpdateProfile value)? updateProfile,
  }) {
    return authorize?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Logout value)? logout,
    TResult Function(Authorize value)? authorize,
    TResult Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult Function(_UpdateProfile value)? updateProfile,
    required TResult orElse(),
  }) {
    if (authorize != null) {
      return authorize(this);
    }
    return orElse();
  }
}

abstract class Authorize implements UserEvent {
  const factory Authorize() = _$Authorize;
}

/// @nodoc
abstract class _$$_RecoveryPasswordEventCopyWith<$Res> {
  factory _$$_RecoveryPasswordEventCopyWith(_$_RecoveryPasswordEvent value,
          $Res Function(_$_RecoveryPasswordEvent) then) =
      __$$_RecoveryPasswordEventCopyWithImpl<$Res>;
  @useResult
  $Res call({String hash});
}

/// @nodoc
class __$$_RecoveryPasswordEventCopyWithImpl<$Res>
    extends _$UserEventCopyWithImpl<$Res, _$_RecoveryPasswordEvent>
    implements _$$_RecoveryPasswordEventCopyWith<$Res> {
  __$$_RecoveryPasswordEventCopyWithImpl(_$_RecoveryPasswordEvent _value,
      $Res Function(_$_RecoveryPasswordEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hash = null,
  }) {
    return _then(_$_RecoveryPasswordEvent(
      null == hash
          ? _value.hash
          : hash // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_RecoveryPasswordEvent implements _RecoveryPasswordEvent {
  const _$_RecoveryPasswordEvent(this.hash);

  @override
  final String hash;

  @override
  String toString() {
    return 'UserEvent.recoveryPasswordEvent(hash: $hash)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RecoveryPasswordEvent &&
            (identical(other.hash, hash) || other.hash == hash));
  }

  @override
  int get hashCode => Object.hash(runtimeType, hash);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RecoveryPasswordEventCopyWith<_$_RecoveryPasswordEvent> get copyWith =>
      __$$_RecoveryPasswordEventCopyWithImpl<_$_RecoveryPasswordEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() logout,
    required TResult Function() authorize,
    required TResult Function(String hash) recoveryPasswordEvent,
    required TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)
        updateProfile,
  }) {
    return recoveryPasswordEvent(hash);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? logout,
    TResult? Function()? authorize,
    TResult? Function(String hash)? recoveryPasswordEvent,
    TResult? Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
  }) {
    return recoveryPasswordEvent?.call(hash);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? logout,
    TResult Function()? authorize,
    TResult Function(String hash)? recoveryPasswordEvent,
    TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
    required TResult orElse(),
  }) {
    if (recoveryPasswordEvent != null) {
      return recoveryPasswordEvent(hash);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Logout value) logout,
    required TResult Function(Authorize value) authorize,
    required TResult Function(_RecoveryPasswordEvent value)
        recoveryPasswordEvent,
    required TResult Function(_UpdateProfile value) updateProfile,
  }) {
    return recoveryPasswordEvent(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Logout value)? logout,
    TResult? Function(Authorize value)? authorize,
    TResult? Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult? Function(_UpdateProfile value)? updateProfile,
  }) {
    return recoveryPasswordEvent?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Logout value)? logout,
    TResult Function(Authorize value)? authorize,
    TResult Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult Function(_UpdateProfile value)? updateProfile,
    required TResult orElse(),
  }) {
    if (recoveryPasswordEvent != null) {
      return recoveryPasswordEvent(this);
    }
    return orElse();
  }
}

abstract class _RecoveryPasswordEvent implements UserEvent {
  const factory _RecoveryPasswordEvent(final String hash) =
      _$_RecoveryPasswordEvent;

  String get hash;
  @JsonKey(ignore: true)
  _$$_RecoveryPasswordEventCopyWith<_$_RecoveryPasswordEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UpdateProfileCopyWith<$Res> {
  factory _$$_UpdateProfileCopyWith(
          _$_UpdateProfile value, $Res Function(_$_UpdateProfile) then) =
      __$$_UpdateProfileCopyWithImpl<$Res>;
  @useResult
  $Res call(
      {String? name,
      String? gender,
      String? phone,
      String? birthdate,
      bool emailNotification,
      bool smsNotification,
      MessageBloc messageBloc});
}

/// @nodoc
class __$$_UpdateProfileCopyWithImpl<$Res>
    extends _$UserEventCopyWithImpl<$Res, _$_UpdateProfile>
    implements _$$_UpdateProfileCopyWith<$Res> {
  __$$_UpdateProfileCopyWithImpl(
      _$_UpdateProfile _value, $Res Function(_$_UpdateProfile) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = freezed,
    Object? gender = freezed,
    Object? phone = freezed,
    Object? birthdate = freezed,
    Object? emailNotification = null,
    Object? smsNotification = null,
    Object? messageBloc = null,
  }) {
    return _then(_$_UpdateProfile(
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      gender: freezed == gender
          ? _value.gender
          : gender // ignore: cast_nullable_to_non_nullable
              as String?,
      phone: freezed == phone
          ? _value.phone
          : phone // ignore: cast_nullable_to_non_nullable
              as String?,
      birthdate: freezed == birthdate
          ? _value.birthdate
          : birthdate // ignore: cast_nullable_to_non_nullable
              as String?,
      emailNotification: null == emailNotification
          ? _value.emailNotification
          : emailNotification // ignore: cast_nullable_to_non_nullable
              as bool,
      smsNotification: null == smsNotification
          ? _value.smsNotification
          : smsNotification // ignore: cast_nullable_to_non_nullable
              as bool,
      messageBloc: null == messageBloc
          ? _value.messageBloc
          : messageBloc // ignore: cast_nullable_to_non_nullable
              as MessageBloc,
    ));
  }
}

/// @nodoc

class _$_UpdateProfile implements _UpdateProfile {
  const _$_UpdateProfile(
      {this.name,
      this.gender,
      this.phone,
      this.birthdate,
      required this.emailNotification,
      required this.smsNotification,
      required this.messageBloc});

  @override
  final String? name;
  @override
  final String? gender;
  @override
  final String? phone;
  @override
  final String? birthdate;
  @override
  final bool emailNotification;
  @override
  final bool smsNotification;
  @override
  final MessageBloc messageBloc;

  @override
  String toString() {
    return 'UserEvent.updateProfile(name: $name, gender: $gender, phone: $phone, birthdate: $birthdate, emailNotification: $emailNotification, smsNotification: $smsNotification, messageBloc: $messageBloc)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateProfile &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.gender, gender) || other.gender == gender) &&
            (identical(other.phone, phone) || other.phone == phone) &&
            (identical(other.birthdate, birthdate) ||
                other.birthdate == birthdate) &&
            (identical(other.emailNotification, emailNotification) ||
                other.emailNotification == emailNotification) &&
            (identical(other.smsNotification, smsNotification) ||
                other.smsNotification == smsNotification) &&
            (identical(other.messageBloc, messageBloc) ||
                other.messageBloc == messageBloc));
  }

  @override
  int get hashCode => Object.hash(runtimeType, name, gender, phone, birthdate,
      emailNotification, smsNotification, messageBloc);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UpdateProfileCopyWith<_$_UpdateProfile> get copyWith =>
      __$$_UpdateProfileCopyWithImpl<_$_UpdateProfile>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() logout,
    required TResult Function() authorize,
    required TResult Function(String hash) recoveryPasswordEvent,
    required TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)
        updateProfile,
  }) {
    return updateProfile(name, gender, phone, birthdate, emailNotification,
        smsNotification, messageBloc);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? logout,
    TResult? Function()? authorize,
    TResult? Function(String hash)? recoveryPasswordEvent,
    TResult? Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
  }) {
    return updateProfile?.call(name, gender, phone, birthdate,
        emailNotification, smsNotification, messageBloc);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? logout,
    TResult Function()? authorize,
    TResult Function(String hash)? recoveryPasswordEvent,
    TResult Function(
            String? name,
            String? gender,
            String? phone,
            String? birthdate,
            bool emailNotification,
            bool smsNotification,
            MessageBloc messageBloc)?
        updateProfile,
    required TResult orElse(),
  }) {
    if (updateProfile != null) {
      return updateProfile(name, gender, phone, birthdate, emailNotification,
          smsNotification, messageBloc);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Logout value) logout,
    required TResult Function(Authorize value) authorize,
    required TResult Function(_RecoveryPasswordEvent value)
        recoveryPasswordEvent,
    required TResult Function(_UpdateProfile value) updateProfile,
  }) {
    return updateProfile(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Logout value)? logout,
    TResult? Function(Authorize value)? authorize,
    TResult? Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult? Function(_UpdateProfile value)? updateProfile,
  }) {
    return updateProfile?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Logout value)? logout,
    TResult Function(Authorize value)? authorize,
    TResult Function(_RecoveryPasswordEvent value)? recoveryPasswordEvent,
    TResult Function(_UpdateProfile value)? updateProfile,
    required TResult orElse(),
  }) {
    if (updateProfile != null) {
      return updateProfile(this);
    }
    return orElse();
  }
}

abstract class _UpdateProfile implements UserEvent {
  const factory _UpdateProfile(
      {final String? name,
      final String? gender,
      final String? phone,
      final String? birthdate,
      required final bool emailNotification,
      required final bool smsNotification,
      required final MessageBloc messageBloc}) = _$_UpdateProfile;

  String? get name;
  String? get gender;
  String? get phone;
  String? get birthdate;
  bool get emailNotification;
  bool get smsNotification;
  MessageBloc get messageBloc;
  @JsonKey(ignore: true)
  _$$_UpdateProfileCopyWith<_$_UpdateProfile> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$UserState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserStateCopyWith<$Res> {
  factory $UserStateCopyWith(UserState value, $Res Function(UserState) then) =
      _$UserStateCopyWithImpl<$Res, UserState>;
}

/// @nodoc
class _$UserStateCopyWithImpl<$Res, $Val extends UserState>
    implements $UserStateCopyWith<$Res> {
  _$UserStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_InitialCopyWith<$Res> {
  factory _$$_InitialCopyWith(
          _$_Initial value, $Res Function(_$_Initial) then) =
      __$$_InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitialCopyWithImpl<$Res>
    extends _$UserStateCopyWithImpl<$Res, _$_Initial>
    implements _$$_InitialCopyWith<$Res> {
  __$$_InitialCopyWithImpl(_$_Initial _value, $Res Function(_$_Initial) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Initial implements _Initial {
  const _$_Initial();

  @override
  String toString() {
    return 'UserState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements UserState {
  const factory _Initial() = _$_Initial;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$UserStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'UserState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements UserState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$UserStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$_Error(
      null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'UserState.error(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Error &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      __$$_ErrorCopyWithImpl<_$_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements UserState {
  const factory _Error(final String error) = _$_Error;

  String get error;
  @JsonKey(ignore: true)
  _$$_ErrorCopyWith<_$_Error> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoggedCopyWith<$Res> {
  factory _$$LoggedCopyWith(_$Logged value, $Res Function(_$Logged) then) =
      __$$LoggedCopyWithImpl<$Res>;
  @useResult
  $Res call({User user});

  $UserCopyWith<$Res> get user;
}

/// @nodoc
class __$$LoggedCopyWithImpl<$Res>
    extends _$UserStateCopyWithImpl<$Res, _$Logged>
    implements _$$LoggedCopyWith<$Res> {
  __$$LoggedCopyWithImpl(_$Logged _value, $Res Function(_$Logged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? user = null,
  }) {
    return _then(_$Logged(
      null == user
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $UserCopyWith<$Res> get user {
    return $UserCopyWith<$Res>(_value.user, (value) {
      return _then(_value.copyWith(user: value));
    });
  }
}

/// @nodoc

class _$Logged implements Logged {
  const _$Logged(this.user);

  @override
  final User user;

  @override
  String toString() {
    return 'UserState.logged(user: $user)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Logged &&
            (identical(other.user, user) || other.user == user));
  }

  @override
  int get hashCode => Object.hash(runtimeType, user);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$LoggedCopyWith<_$Logged> get copyWith =>
      __$$LoggedCopyWithImpl<_$Logged>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) {
    return logged(user);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) {
    return logged?.call(user);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (logged != null) {
      return logged(user);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) {
    return logged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) {
    return logged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (logged != null) {
      return logged(this);
    }
    return orElse();
  }
}

abstract class Logged implements UserState {
  const factory Logged(final User user) = _$Logged;

  User get user;
  @JsonKey(ignore: true)
  _$$LoggedCopyWith<_$Logged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_LoginSuccessCopyWith<$Res> {
  factory _$$_LoginSuccessCopyWith(
          _$_LoginSuccess value, $Res Function(_$_LoginSuccess) then) =
      __$$_LoginSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoginSuccessCopyWithImpl<$Res>
    extends _$UserStateCopyWithImpl<$Res, _$_LoginSuccess>
    implements _$$_LoginSuccessCopyWith<$Res> {
  __$$_LoginSuccessCopyWithImpl(
      _$_LoginSuccess _value, $Res Function(_$_LoginSuccess) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_LoginSuccess implements _LoginSuccess {
  const _$_LoginSuccess();

  @override
  String toString() {
    return 'UserState.loginSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_LoginSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) {
    return loginSuccess();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) {
    return loginSuccess?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (loginSuccess != null) {
      return loginSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) {
    return loginSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) {
    return loginSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (loginSuccess != null) {
      return loginSuccess(this);
    }
    return orElse();
  }
}

abstract class _LoginSuccess implements UserState {
  const factory _LoginSuccess() = _$_LoginSuccess;
}

/// @nodoc
abstract class _$$_UserNotVerifiedCopyWith<$Res> {
  factory _$$_UserNotVerifiedCopyWith(
          _$_UserNotVerified value, $Res Function(_$_UserNotVerified) then) =
      __$$_UserNotVerifiedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UserNotVerifiedCopyWithImpl<$Res>
    extends _$UserStateCopyWithImpl<$Res, _$_UserNotVerified>
    implements _$$_UserNotVerifiedCopyWith<$Res> {
  __$$_UserNotVerifiedCopyWithImpl(
      _$_UserNotVerified _value, $Res Function(_$_UserNotVerified) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UserNotVerified implements _UserNotVerified {
  const _$_UserNotVerified();

  @override
  String toString() {
    return 'UserState.userNotVerified()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UserNotVerified);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) {
    return userNotVerified();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) {
    return userNotVerified?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (userNotVerified != null) {
      return userNotVerified();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) {
    return userNotVerified(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) {
    return userNotVerified?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (userNotVerified != null) {
      return userNotVerified(this);
    }
    return orElse();
  }
}

abstract class _UserNotVerified implements UserState {
  const factory _UserNotVerified() = _$_UserNotVerified;
}

/// @nodoc
abstract class _$$_UpdateSuccessCopyWith<$Res> {
  factory _$$_UpdateSuccessCopyWith(
          _$_UpdateSuccess value, $Res Function(_$_UpdateSuccess) then) =
      __$$_UpdateSuccessCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_UpdateSuccessCopyWithImpl<$Res>
    extends _$UserStateCopyWithImpl<$Res, _$_UpdateSuccess>
    implements _$$_UpdateSuccessCopyWith<$Res> {
  __$$_UpdateSuccessCopyWithImpl(
      _$_UpdateSuccess _value, $Res Function(_$_UpdateSuccess) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_UpdateSuccess implements _UpdateSuccess {
  const _$_UpdateSuccess();

  @override
  String toString() {
    return 'UserState.updateSuccess()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_UpdateSuccess);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) {
    return updateSuccess();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) {
    return updateSuccess?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (updateSuccess != null) {
      return updateSuccess();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) {
    return updateSuccess(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) {
    return updateSuccess?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (updateSuccess != null) {
      return updateSuccess(this);
    }
    return orElse();
  }
}

abstract class _UpdateSuccess implements UserState {
  const factory _UpdateSuccess() = _$_UpdateSuccess;
}

/// @nodoc
abstract class _$$_LogOutCopyWith<$Res> {
  factory _$$_LogOutCopyWith(_$_LogOut value, $Res Function(_$_LogOut) then) =
      __$$_LogOutCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LogOutCopyWithImpl<$Res>
    extends _$UserStateCopyWithImpl<$Res, _$_LogOut>
    implements _$$_LogOutCopyWith<$Res> {
  __$$_LogOutCopyWithImpl(_$_LogOut _value, $Res Function(_$_LogOut) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_LogOut implements _LogOut {
  const _$_LogOut();

  @override
  String toString() {
    return 'UserState.logout()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_LogOut);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) {
    return logout();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) {
    return logout?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (logout != null) {
      return logout();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) {
    return logout(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) {
    return logout?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (logout != null) {
      return logout(this);
    }
    return orElse();
  }
}

abstract class _LogOut implements UserState {
  const factory _LogOut() = _$_LogOut;
}

/// @nodoc
abstract class _$$_RecoveryPasswordCopyWith<$Res> {
  factory _$$_RecoveryPasswordCopyWith(
          _$_RecoveryPassword value, $Res Function(_$_RecoveryPassword) then) =
      __$$_RecoveryPasswordCopyWithImpl<$Res>;
  @useResult
  $Res call({String hash});
}

/// @nodoc
class __$$_RecoveryPasswordCopyWithImpl<$Res>
    extends _$UserStateCopyWithImpl<$Res, _$_RecoveryPassword>
    implements _$$_RecoveryPasswordCopyWith<$Res> {
  __$$_RecoveryPasswordCopyWithImpl(
      _$_RecoveryPassword _value, $Res Function(_$_RecoveryPassword) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hash = null,
  }) {
    return _then(_$_RecoveryPassword(
      null == hash
          ? _value.hash
          : hash // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_RecoveryPassword implements _RecoveryPassword {
  const _$_RecoveryPassword(this.hash);

  @override
  final String hash;

  @override
  String toString() {
    return 'UserState.recoveryPassword(hash: $hash)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RecoveryPassword &&
            (identical(other.hash, hash) || other.hash == hash));
  }

  @override
  int get hashCode => Object.hash(runtimeType, hash);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RecoveryPasswordCopyWith<_$_RecoveryPassword> get copyWith =>
      __$$_RecoveryPasswordCopyWithImpl<_$_RecoveryPassword>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(String error) error,
    required TResult Function(User user) logged,
    required TResult Function() loginSuccess,
    required TResult Function() userNotVerified,
    required TResult Function() updateSuccess,
    required TResult Function() logout,
    required TResult Function(String hash) recoveryPassword,
  }) {
    return recoveryPassword(hash);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? initial,
    TResult? Function()? loading,
    TResult? Function(String error)? error,
    TResult? Function(User user)? logged,
    TResult? Function()? loginSuccess,
    TResult? Function()? userNotVerified,
    TResult? Function()? updateSuccess,
    TResult? Function()? logout,
    TResult? Function(String hash)? recoveryPassword,
  }) {
    return recoveryPassword?.call(hash);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(String error)? error,
    TResult Function(User user)? logged,
    TResult Function()? loginSuccess,
    TResult Function()? userNotVerified,
    TResult Function()? updateSuccess,
    TResult Function()? logout,
    TResult Function(String hash)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (recoveryPassword != null) {
      return recoveryPassword(hash);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Initial value) initial,
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(Logged value) logged,
    required TResult Function(_LoginSuccess value) loginSuccess,
    required TResult Function(_UserNotVerified value) userNotVerified,
    required TResult Function(_UpdateSuccess value) updateSuccess,
    required TResult Function(_LogOut value) logout,
    required TResult Function(_RecoveryPassword value) recoveryPassword,
  }) {
    return recoveryPassword(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Initial value)? initial,
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(Logged value)? logged,
    TResult? Function(_LoginSuccess value)? loginSuccess,
    TResult? Function(_UserNotVerified value)? userNotVerified,
    TResult? Function(_UpdateSuccess value)? updateSuccess,
    TResult? Function(_LogOut value)? logout,
    TResult? Function(_RecoveryPassword value)? recoveryPassword,
  }) {
    return recoveryPassword?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Initial value)? initial,
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(Logged value)? logged,
    TResult Function(_LoginSuccess value)? loginSuccess,
    TResult Function(_UserNotVerified value)? userNotVerified,
    TResult Function(_UpdateSuccess value)? updateSuccess,
    TResult Function(_LogOut value)? logout,
    TResult Function(_RecoveryPassword value)? recoveryPassword,
    required TResult orElse(),
  }) {
    if (recoveryPassword != null) {
      return recoveryPassword(this);
    }
    return orElse();
  }
}

abstract class _RecoveryPassword implements UserState {
  const factory _RecoveryPassword(final String hash) = _$_RecoveryPassword;

  String get hash;
  @JsonKey(ignore: true)
  _$$_RecoveryPasswordCopyWith<_$_RecoveryPassword> get copyWith =>
      throw _privateConstructorUsedError;
}
