import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';
import 'package:nine_pandas/domain/movies/repository/main_page_repository.dart';

@injectable
class GetMovie {
  final MoviesRepository repository;

  GetMovie(this.repository);

  Future<Either<Failure, Movie>> call(int id, String locale) async {
    return repository.getMovieById(id, locale);
  }
}
