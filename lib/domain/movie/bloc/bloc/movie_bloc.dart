import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';
import 'package:nine_pandas/domain/localization/usecases/get_localization.dart';
import 'package:nine_pandas/domain/movie/usecases/get_movie.dart';

part 'movie_event.dart';
part 'movie_state.dart';
part 'movie_bloc.freezed.dart';

@injectable
class MovieBloc extends Bloc<MovieEvent, MovieState> {
  final GetMovie _getMovie;
  final GetLocalization _getLocalization;
  MovieBloc({
    required GetMovie getMovie,
    required GetLocalization getLocalization,
  })  : _getMovie = getMovie,
        _getLocalization = getLocalization,
        super(_Initial()) {
    on<_FetchMovie>((event, emit) async {
      emit(_Loading());

      await _eitherLoadedOrErrorState(emit, event);
    });
  }

  _eitherLoadedOrErrorState(Emitter emit, _FetchMovie event) async {
    final String currentLocale = await _getLocalization() ?? 'ru';
    final Future<Either<Failure, Movie>>? seriesOrFailure =
        _getMovie(event.id, currentLocale.toLowerCase());
    emit(
      (await seriesOrFailure)!.fold(
        (failure) => _Error(failure),
        (movie) {
          return _Loaded(movie);
        },
      ),
    );
  }
}
