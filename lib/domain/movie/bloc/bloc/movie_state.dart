part of 'movie_bloc.dart';

@freezed
class MovieState with _$MovieState {
  const factory MovieState.initial() = _Initial;
  const factory MovieState.dataLoaded(Movie movie) = _Loaded;
  const factory MovieState.dataLoading() = _Loading;
  const factory MovieState.error(Failure failure) = _Error;
}
