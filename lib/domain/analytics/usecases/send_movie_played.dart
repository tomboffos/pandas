import 'package:injectable/injectable.dart';

import '../analytics_repositories.dart';

@injectable
class SendMoviePlayed {
  final AnalyticsRepository repository;

  SendMoviePlayed(this.repository);

  Future call(String movieId, String movieTitle, String? season,
      String? episode, int? userId) async {
    return await repository.sendMoviePlayedEvent(
        movieId, movieTitle, season, episode, userId);
  }
}
