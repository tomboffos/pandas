import 'package:injectable/injectable.dart';

import '../analytics_repositories.dart';

@injectable
class SendPressedWatchSecondSeasonNotBought {
  final AnalyticsRepository repository;

  SendPressedWatchSecondSeasonNotBought(this.repository);

  Future call(String movieId, String movieTitle, int? userId) async {
    return await repository.sendPressedWatchSecondSeasonNotBoughtEvent(
        movieId, movieTitle, userId);
  }
}
