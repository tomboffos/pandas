import 'package:injectable/injectable.dart';

import '../analytics_repositories.dart';

@injectable
class SendPlayerPageOpened {
  final AnalyticsRepository repository;

  SendPlayerPageOpened(this.repository);

  Future call(String movieId, String movieTitle, String? season,
      String? episode, int? userId) async {
    return await repository.sendPlayerPageOpenedEvent(
        movieId, movieTitle, season, episode, userId);
  }
}
