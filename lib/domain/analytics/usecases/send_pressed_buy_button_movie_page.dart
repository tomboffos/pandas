import 'package:injectable/injectable.dart';

import '../analytics_repositories.dart';

@injectable
class SendPressedBuyButtonMoviePage {
  final AnalyticsRepository repository;

  SendPressedBuyButtonMoviePage(this.repository);

  Future call(String movieId, String movieTitle, int? userId) async {
    return await repository.sendPressedBuyButtonMoviePageEvent(
        movieId, movieTitle, userId);
  }
}
