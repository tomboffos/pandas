import 'package:injectable/injectable.dart';

import '../analytics_repositories.dart';

@injectable
class SendPressedBuyButtonPlayerPage {
  final AnalyticsRepository repository;

  SendPressedBuyButtonPlayerPage(this.repository);

  Future call(String movieId, String movieTitle, String? season,
      String? episode, int? userId) async {
    return await repository.sendPressedBuyButtonPlayerPageEvent(
        movieId, movieTitle, season, episode, userId);
  }
}
