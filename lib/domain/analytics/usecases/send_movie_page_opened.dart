import 'package:injectable/injectable.dart';

import '../analytics_repositories.dart';

@injectable
class SendMoviePageOpened {
  final AnalyticsRepository repository;

  SendMoviePageOpened(this.repository);

  Future call(String movieId, String movieTitle, int? userId) async {
    return await repository.sendMoviePageOpenedEvent(
        movieId, movieTitle, userId);
  }
}
