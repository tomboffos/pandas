import 'package:injectable/injectable.dart';

import '../analytics_repositories.dart';

@injectable
class SendApplicationLaunched {
  final AnalyticsRepository repository;

  SendApplicationLaunched(this.repository);

  Future call(int? userId) async {
    return await repository.sendApplicationLaunchedEvent(userId);
  }
}
