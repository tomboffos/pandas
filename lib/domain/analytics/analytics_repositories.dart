abstract class AnalyticsRepository {
  Future sendApplicationLaunchedEvent(int? userId);
  Future sendMoviePageOpenedEvent(
      String movieId, String movieTitle, int? userId);
  Future sendMoviePlayedEvent(String movieId, String movieTitle, String? season,
      String? episode, int? userId);
  Future sendPlayerPageOpenedEvent(String movieId, String movieTitle,
      String? season, String? episode, int? userId);
  Future sendPressedWatchSecondSeasonNotBoughtEvent(
      String movieId, String movieTitle, int? userId);
  Future sendPressedBuyButtonMoviePageEvent(
      String movieId, String movieTitle, int? userId);
  Future sendPressedBuyButtonPlayerPageEvent(String movieId, String movieTitle,
      String? season, String? episode, int? userId);
}
