part of 'analytics_bloc.dart';

@freezed
class AnalyticsState with _$AnalyticsState {
  const factory AnalyticsState.loaded() = _Loaded;
  const factory AnalyticsState.error() = _Error;
}
