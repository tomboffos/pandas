import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

import '../usecases/send_application_launched.dart';
import '../usecases/send_movie_page_opened.dart';
import '../usecases/send_movie_played.dart';
import '../usecases/send_player_page_opened.dart';
import '../usecases/send_pressed_buy_button_movie_page.dart';
import '../usecases/send_pressed_buy_button_player_page.dart';
import '../usecases/send_pressed_watch_second_season_not_bought.dart';

part 'analytics_event.dart';
part 'analytics_state.dart';
part 'analytics_bloc.freezed.dart';

@lazySingleton
class AnalyticsBloc extends Bloc<AnalyticsEvent, AnalyticsState> {
  final SendApplicationLaunched _sendApplicationLaunched;
  final SendMoviePageOpened _sendMoviePageOpened;
  final SendMoviePlayed _sendMoviePlayed;
  final SendPlayerPageOpened _sendPlayerPageOpened;
  final SendPressedBuyButtonMoviePage _sendPressedBuyButtonMoviePage;
  final SendPressedBuyButtonPlayerPage _sendPressedBuyButtonPlayerPage;
  final SendPressedWatchSecondSeasonNotBought
      _sendPressedWatchSecondSeasonNotBought;

  AnalyticsBloc(
    this._sendApplicationLaunched,
    this._sendMoviePageOpened,
    this._sendMoviePlayed,
    this._sendPlayerPageOpened,
    this._sendPressedBuyButtonMoviePage,
    this._sendPressedBuyButtonPlayerPage,
    this._sendPressedWatchSecondSeasonNotBought,
  ) : super(_Loaded()) {
    on<_SendApplicationLaunchedEvent>((event, emit) {
      _sendApplicationLaunched(
        event.userId,
      );
    });
    on<_SendMoviePageOpenedEvent>((event, emit) {
      _sendMoviePageOpened(
        event.movieId,
        event.movieTitle,
        event.userId,
      );
    });
    on<_SendMoviePlayedEvent>((event, emit) {
      _sendMoviePlayed(
        event.movieId,
        event.movieTitle,
        event.season,
        event.episode,
        event.userId,
      );
    });
    on<_SendPlayerPageOpenedEvent>((event, emit) {
      _sendPlayerPageOpened(
        event.movieId,
        event.movieTitle,
        event.season,
        event.episode,
        event.userId,
      );
    });
    on<_SendPressedBuyButtonMoviePageEvent>((event, emit) {
      _sendPressedBuyButtonMoviePage(
        event.movieId,
        event.movieTitle,
        event.userId,
      );
    });
    on<_SendPressedBuyButtonPlayerPageEvent>((event, emit) {
      _sendPressedBuyButtonPlayerPage(
        event.movieId,
        event.movieTitle,
        event.season,
        event.episode,
        event.userId,
      );
    });
    on<_SendPressedWatchSecondSeasonNotBoughtEvent>((event, emit) {
      _sendPressedWatchSecondSeasonNotBought(
        event.movieId,
        event.movieTitle,
        event.userId,
      );
    });
  }
}
