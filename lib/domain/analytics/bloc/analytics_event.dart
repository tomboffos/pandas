part of 'analytics_bloc.dart';

@freezed
class AnalyticsEvent with _$AnalyticsEvent {
  const factory AnalyticsEvent.sendApplicationLaunched(
    int? userId,
  ) = _SendApplicationLaunchedEvent;
  const factory AnalyticsEvent.sendMoviePageOpened(
    String movieId,
    String movieTitle,
    int? userId,
  ) = _SendMoviePageOpenedEvent;
  // not added
  const factory AnalyticsEvent.sendMoviePlayed(
      String movieId,
      String movieTitle,
      String? season,
      String? episode,
      int? userId) = _SendMoviePlayedEvent;
  const factory AnalyticsEvent.sendPlayerPageOpened(
      String movieId,
      String movieTitle,
      String? season,
      String? episode,
      int? userId) = _SendPlayerPageOpenedEvent;
  const factory AnalyticsEvent.sendPressedBuyButtonMoviePage(
    String movieId,
    String movieTitle,
    int? userId,
  ) = _SendPressedBuyButtonMoviePageEvent;
  const factory AnalyticsEvent.sendPressedBuyButtonPlayerPage(
    String movieId,
    String movieTitle,
    String? season,
    String? episode,
    int? userId,
  ) = _SendPressedBuyButtonPlayerPageEvent;
  const factory AnalyticsEvent.sendPressedWatchSecondSeasonNotBought(
    String movieId,
    String movieTitle,
    int? userId,
  ) = _SendPressedWatchSecondSeasonNotBoughtEvent;
}
