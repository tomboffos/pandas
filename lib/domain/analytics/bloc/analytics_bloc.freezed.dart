// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'analytics_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AnalyticsEvent {
  int? get userId => throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int? userId) sendApplicationLaunched,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendMoviePageOpened,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendMoviePlayed,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPlayerPageOpened,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedBuyButtonMoviePage,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedWatchSecondSeasonNotBought,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int? userId)? sendApplicationLaunched,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int? userId)? sendApplicationLaunched,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendApplicationLaunchedEvent value)
        sendApplicationLaunched,
    required TResult Function(_SendMoviePageOpenedEvent value)
        sendMoviePageOpened,
    required TResult Function(_SendMoviePlayedEvent value) sendMoviePlayed,
    required TResult Function(_SendPlayerPageOpenedEvent value)
        sendPlayerPageOpened,
    required TResult Function(_SendPressedBuyButtonMoviePageEvent value)
        sendPressedBuyButtonMoviePage,
    required TResult Function(_SendPressedBuyButtonPlayerPageEvent value)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)
        sendPressedWatchSecondSeasonNotBought,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult? Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult? Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult? Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult? Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AnalyticsEventCopyWith<AnalyticsEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AnalyticsEventCopyWith<$Res> {
  factory $AnalyticsEventCopyWith(
          AnalyticsEvent value, $Res Function(AnalyticsEvent) then) =
      _$AnalyticsEventCopyWithImpl<$Res, AnalyticsEvent>;
  @useResult
  $Res call({int? userId});
}

/// @nodoc
class _$AnalyticsEventCopyWithImpl<$Res, $Val extends AnalyticsEvent>
    implements $AnalyticsEventCopyWith<$Res> {
  _$AnalyticsEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(_value.copyWith(
      userId: freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_SendApplicationLaunchedEventCopyWith<$Res>
    implements $AnalyticsEventCopyWith<$Res> {
  factory _$$_SendApplicationLaunchedEventCopyWith(
          _$_SendApplicationLaunchedEvent value,
          $Res Function(_$_SendApplicationLaunchedEvent) then) =
      __$$_SendApplicationLaunchedEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int? userId});
}

/// @nodoc
class __$$_SendApplicationLaunchedEventCopyWithImpl<$Res>
    extends _$AnalyticsEventCopyWithImpl<$Res, _$_SendApplicationLaunchedEvent>
    implements _$$_SendApplicationLaunchedEventCopyWith<$Res> {
  __$$_SendApplicationLaunchedEventCopyWithImpl(
      _$_SendApplicationLaunchedEvent _value,
      $Res Function(_$_SendApplicationLaunchedEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userId = freezed,
  }) {
    return _then(_$_SendApplicationLaunchedEvent(
      freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$_SendApplicationLaunchedEvent implements _SendApplicationLaunchedEvent {
  const _$_SendApplicationLaunchedEvent(this.userId);

  @override
  final int? userId;

  @override
  String toString() {
    return 'AnalyticsEvent.sendApplicationLaunched(userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SendApplicationLaunchedEvent &&
            (identical(other.userId, userId) || other.userId == userId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SendApplicationLaunchedEventCopyWith<_$_SendApplicationLaunchedEvent>
      get copyWith => __$$_SendApplicationLaunchedEventCopyWithImpl<
          _$_SendApplicationLaunchedEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int? userId) sendApplicationLaunched,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendMoviePageOpened,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendMoviePlayed,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPlayerPageOpened,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedBuyButtonMoviePage,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendApplicationLaunched(userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int? userId)? sendApplicationLaunched,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendApplicationLaunched?.call(userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int? userId)? sendApplicationLaunched,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendApplicationLaunched != null) {
      return sendApplicationLaunched(userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendApplicationLaunchedEvent value)
        sendApplicationLaunched,
    required TResult Function(_SendMoviePageOpenedEvent value)
        sendMoviePageOpened,
    required TResult Function(_SendMoviePlayedEvent value) sendMoviePlayed,
    required TResult Function(_SendPlayerPageOpenedEvent value)
        sendPlayerPageOpened,
    required TResult Function(_SendPressedBuyButtonMoviePageEvent value)
        sendPressedBuyButtonMoviePage,
    required TResult Function(_SendPressedBuyButtonPlayerPageEvent value)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendApplicationLaunched(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult? Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult? Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult? Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult? Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendApplicationLaunched?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendApplicationLaunched != null) {
      return sendApplicationLaunched(this);
    }
    return orElse();
  }
}

abstract class _SendApplicationLaunchedEvent implements AnalyticsEvent {
  const factory _SendApplicationLaunchedEvent(final int? userId) =
      _$_SendApplicationLaunchedEvent;

  @override
  int? get userId;
  @override
  @JsonKey(ignore: true)
  _$$_SendApplicationLaunchedEventCopyWith<_$_SendApplicationLaunchedEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SendMoviePageOpenedEventCopyWith<$Res>
    implements $AnalyticsEventCopyWith<$Res> {
  factory _$$_SendMoviePageOpenedEventCopyWith(
          _$_SendMoviePageOpenedEvent value,
          $Res Function(_$_SendMoviePageOpenedEvent) then) =
      __$$_SendMoviePageOpenedEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String movieId, String movieTitle, int? userId});
}

/// @nodoc
class __$$_SendMoviePageOpenedEventCopyWithImpl<$Res>
    extends _$AnalyticsEventCopyWithImpl<$Res, _$_SendMoviePageOpenedEvent>
    implements _$$_SendMoviePageOpenedEventCopyWith<$Res> {
  __$$_SendMoviePageOpenedEventCopyWithImpl(_$_SendMoviePageOpenedEvent _value,
      $Res Function(_$_SendMoviePageOpenedEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? movieId = null,
    Object? movieTitle = null,
    Object? userId = freezed,
  }) {
    return _then(_$_SendMoviePageOpenedEvent(
      null == movieId
          ? _value.movieId
          : movieId // ignore: cast_nullable_to_non_nullable
              as String,
      null == movieTitle
          ? _value.movieTitle
          : movieTitle // ignore: cast_nullable_to_non_nullable
              as String,
      freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$_SendMoviePageOpenedEvent implements _SendMoviePageOpenedEvent {
  const _$_SendMoviePageOpenedEvent(this.movieId, this.movieTitle, this.userId);

  @override
  final String movieId;
  @override
  final String movieTitle;
  @override
  final int? userId;

  @override
  String toString() {
    return 'AnalyticsEvent.sendMoviePageOpened(movieId: $movieId, movieTitle: $movieTitle, userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SendMoviePageOpenedEvent &&
            (identical(other.movieId, movieId) || other.movieId == movieId) &&
            (identical(other.movieTitle, movieTitle) ||
                other.movieTitle == movieTitle) &&
            (identical(other.userId, userId) || other.userId == userId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, movieId, movieTitle, userId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SendMoviePageOpenedEventCopyWith<_$_SendMoviePageOpenedEvent>
      get copyWith => __$$_SendMoviePageOpenedEventCopyWithImpl<
          _$_SendMoviePageOpenedEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int? userId) sendApplicationLaunched,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendMoviePageOpened,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendMoviePlayed,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPlayerPageOpened,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedBuyButtonMoviePage,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendMoviePageOpened(movieId, movieTitle, userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int? userId)? sendApplicationLaunched,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendMoviePageOpened?.call(movieId, movieTitle, userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int? userId)? sendApplicationLaunched,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendMoviePageOpened != null) {
      return sendMoviePageOpened(movieId, movieTitle, userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendApplicationLaunchedEvent value)
        sendApplicationLaunched,
    required TResult Function(_SendMoviePageOpenedEvent value)
        sendMoviePageOpened,
    required TResult Function(_SendMoviePlayedEvent value) sendMoviePlayed,
    required TResult Function(_SendPlayerPageOpenedEvent value)
        sendPlayerPageOpened,
    required TResult Function(_SendPressedBuyButtonMoviePageEvent value)
        sendPressedBuyButtonMoviePage,
    required TResult Function(_SendPressedBuyButtonPlayerPageEvent value)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendMoviePageOpened(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult? Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult? Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult? Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult? Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendMoviePageOpened?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendMoviePageOpened != null) {
      return sendMoviePageOpened(this);
    }
    return orElse();
  }
}

abstract class _SendMoviePageOpenedEvent implements AnalyticsEvent {
  const factory _SendMoviePageOpenedEvent(
          final String movieId, final String movieTitle, final int? userId) =
      _$_SendMoviePageOpenedEvent;

  String get movieId;
  String get movieTitle;
  @override
  int? get userId;
  @override
  @JsonKey(ignore: true)
  _$$_SendMoviePageOpenedEventCopyWith<_$_SendMoviePageOpenedEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SendMoviePlayedEventCopyWith<$Res>
    implements $AnalyticsEventCopyWith<$Res> {
  factory _$$_SendMoviePlayedEventCopyWith(_$_SendMoviePlayedEvent value,
          $Res Function(_$_SendMoviePlayedEvent) then) =
      __$$_SendMoviePlayedEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String movieId,
      String movieTitle,
      String? season,
      String? episode,
      int? userId});
}

/// @nodoc
class __$$_SendMoviePlayedEventCopyWithImpl<$Res>
    extends _$AnalyticsEventCopyWithImpl<$Res, _$_SendMoviePlayedEvent>
    implements _$$_SendMoviePlayedEventCopyWith<$Res> {
  __$$_SendMoviePlayedEventCopyWithImpl(_$_SendMoviePlayedEvent _value,
      $Res Function(_$_SendMoviePlayedEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? movieId = null,
    Object? movieTitle = null,
    Object? season = freezed,
    Object? episode = freezed,
    Object? userId = freezed,
  }) {
    return _then(_$_SendMoviePlayedEvent(
      null == movieId
          ? _value.movieId
          : movieId // ignore: cast_nullable_to_non_nullable
              as String,
      null == movieTitle
          ? _value.movieTitle
          : movieTitle // ignore: cast_nullable_to_non_nullable
              as String,
      freezed == season
          ? _value.season
          : season // ignore: cast_nullable_to_non_nullable
              as String?,
      freezed == episode
          ? _value.episode
          : episode // ignore: cast_nullable_to_non_nullable
              as String?,
      freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$_SendMoviePlayedEvent implements _SendMoviePlayedEvent {
  const _$_SendMoviePlayedEvent(
      this.movieId, this.movieTitle, this.season, this.episode, this.userId);

  @override
  final String movieId;
  @override
  final String movieTitle;
  @override
  final String? season;
  @override
  final String? episode;
  @override
  final int? userId;

  @override
  String toString() {
    return 'AnalyticsEvent.sendMoviePlayed(movieId: $movieId, movieTitle: $movieTitle, season: $season, episode: $episode, userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SendMoviePlayedEvent &&
            (identical(other.movieId, movieId) || other.movieId == movieId) &&
            (identical(other.movieTitle, movieTitle) ||
                other.movieTitle == movieTitle) &&
            (identical(other.season, season) || other.season == season) &&
            (identical(other.episode, episode) || other.episode == episode) &&
            (identical(other.userId, userId) || other.userId == userId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, movieId, movieTitle, season, episode, userId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SendMoviePlayedEventCopyWith<_$_SendMoviePlayedEvent> get copyWith =>
      __$$_SendMoviePlayedEventCopyWithImpl<_$_SendMoviePlayedEvent>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int? userId) sendApplicationLaunched,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendMoviePageOpened,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendMoviePlayed,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPlayerPageOpened,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedBuyButtonMoviePage,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendMoviePlayed(movieId, movieTitle, season, episode, userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int? userId)? sendApplicationLaunched,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendMoviePlayed?.call(movieId, movieTitle, season, episode, userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int? userId)? sendApplicationLaunched,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendMoviePlayed != null) {
      return sendMoviePlayed(movieId, movieTitle, season, episode, userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendApplicationLaunchedEvent value)
        sendApplicationLaunched,
    required TResult Function(_SendMoviePageOpenedEvent value)
        sendMoviePageOpened,
    required TResult Function(_SendMoviePlayedEvent value) sendMoviePlayed,
    required TResult Function(_SendPlayerPageOpenedEvent value)
        sendPlayerPageOpened,
    required TResult Function(_SendPressedBuyButtonMoviePageEvent value)
        sendPressedBuyButtonMoviePage,
    required TResult Function(_SendPressedBuyButtonPlayerPageEvent value)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendMoviePlayed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult? Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult? Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult? Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult? Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendMoviePlayed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendMoviePlayed != null) {
      return sendMoviePlayed(this);
    }
    return orElse();
  }
}

abstract class _SendMoviePlayedEvent implements AnalyticsEvent {
  const factory _SendMoviePlayedEvent(
      final String movieId,
      final String movieTitle,
      final String? season,
      final String? episode,
      final int? userId) = _$_SendMoviePlayedEvent;

  String get movieId;
  String get movieTitle;
  String? get season;
  String? get episode;
  @override
  int? get userId;
  @override
  @JsonKey(ignore: true)
  _$$_SendMoviePlayedEventCopyWith<_$_SendMoviePlayedEvent> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SendPlayerPageOpenedEventCopyWith<$Res>
    implements $AnalyticsEventCopyWith<$Res> {
  factory _$$_SendPlayerPageOpenedEventCopyWith(
          _$_SendPlayerPageOpenedEvent value,
          $Res Function(_$_SendPlayerPageOpenedEvent) then) =
      __$$_SendPlayerPageOpenedEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String movieId,
      String movieTitle,
      String? season,
      String? episode,
      int? userId});
}

/// @nodoc
class __$$_SendPlayerPageOpenedEventCopyWithImpl<$Res>
    extends _$AnalyticsEventCopyWithImpl<$Res, _$_SendPlayerPageOpenedEvent>
    implements _$$_SendPlayerPageOpenedEventCopyWith<$Res> {
  __$$_SendPlayerPageOpenedEventCopyWithImpl(
      _$_SendPlayerPageOpenedEvent _value,
      $Res Function(_$_SendPlayerPageOpenedEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? movieId = null,
    Object? movieTitle = null,
    Object? season = freezed,
    Object? episode = freezed,
    Object? userId = freezed,
  }) {
    return _then(_$_SendPlayerPageOpenedEvent(
      null == movieId
          ? _value.movieId
          : movieId // ignore: cast_nullable_to_non_nullable
              as String,
      null == movieTitle
          ? _value.movieTitle
          : movieTitle // ignore: cast_nullable_to_non_nullable
              as String,
      freezed == season
          ? _value.season
          : season // ignore: cast_nullable_to_non_nullable
              as String?,
      freezed == episode
          ? _value.episode
          : episode // ignore: cast_nullable_to_non_nullable
              as String?,
      freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$_SendPlayerPageOpenedEvent implements _SendPlayerPageOpenedEvent {
  const _$_SendPlayerPageOpenedEvent(
      this.movieId, this.movieTitle, this.season, this.episode, this.userId);

  @override
  final String movieId;
  @override
  final String movieTitle;
  @override
  final String? season;
  @override
  final String? episode;
  @override
  final int? userId;

  @override
  String toString() {
    return 'AnalyticsEvent.sendPlayerPageOpened(movieId: $movieId, movieTitle: $movieTitle, season: $season, episode: $episode, userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SendPlayerPageOpenedEvent &&
            (identical(other.movieId, movieId) || other.movieId == movieId) &&
            (identical(other.movieTitle, movieTitle) ||
                other.movieTitle == movieTitle) &&
            (identical(other.season, season) || other.season == season) &&
            (identical(other.episode, episode) || other.episode == episode) &&
            (identical(other.userId, userId) || other.userId == userId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, movieId, movieTitle, season, episode, userId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SendPlayerPageOpenedEventCopyWith<_$_SendPlayerPageOpenedEvent>
      get copyWith => __$$_SendPlayerPageOpenedEventCopyWithImpl<
          _$_SendPlayerPageOpenedEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int? userId) sendApplicationLaunched,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendMoviePageOpened,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendMoviePlayed,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPlayerPageOpened,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedBuyButtonMoviePage,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPlayerPageOpened(movieId, movieTitle, season, episode, userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int? userId)? sendApplicationLaunched,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPlayerPageOpened?.call(
        movieId, movieTitle, season, episode, userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int? userId)? sendApplicationLaunched,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendPlayerPageOpened != null) {
      return sendPlayerPageOpened(movieId, movieTitle, season, episode, userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendApplicationLaunchedEvent value)
        sendApplicationLaunched,
    required TResult Function(_SendMoviePageOpenedEvent value)
        sendMoviePageOpened,
    required TResult Function(_SendMoviePlayedEvent value) sendMoviePlayed,
    required TResult Function(_SendPlayerPageOpenedEvent value)
        sendPlayerPageOpened,
    required TResult Function(_SendPressedBuyButtonMoviePageEvent value)
        sendPressedBuyButtonMoviePage,
    required TResult Function(_SendPressedBuyButtonPlayerPageEvent value)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPlayerPageOpened(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult? Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult? Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult? Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult? Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPlayerPageOpened?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendPlayerPageOpened != null) {
      return sendPlayerPageOpened(this);
    }
    return orElse();
  }
}

abstract class _SendPlayerPageOpenedEvent implements AnalyticsEvent {
  const factory _SendPlayerPageOpenedEvent(
      final String movieId,
      final String movieTitle,
      final String? season,
      final String? episode,
      final int? userId) = _$_SendPlayerPageOpenedEvent;

  String get movieId;
  String get movieTitle;
  String? get season;
  String? get episode;
  @override
  int? get userId;
  @override
  @JsonKey(ignore: true)
  _$$_SendPlayerPageOpenedEventCopyWith<_$_SendPlayerPageOpenedEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SendPressedBuyButtonMoviePageEventCopyWith<$Res>
    implements $AnalyticsEventCopyWith<$Res> {
  factory _$$_SendPressedBuyButtonMoviePageEventCopyWith(
          _$_SendPressedBuyButtonMoviePageEvent value,
          $Res Function(_$_SendPressedBuyButtonMoviePageEvent) then) =
      __$$_SendPressedBuyButtonMoviePageEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String movieId, String movieTitle, int? userId});
}

/// @nodoc
class __$$_SendPressedBuyButtonMoviePageEventCopyWithImpl<$Res>
    extends _$AnalyticsEventCopyWithImpl<$Res,
        _$_SendPressedBuyButtonMoviePageEvent>
    implements _$$_SendPressedBuyButtonMoviePageEventCopyWith<$Res> {
  __$$_SendPressedBuyButtonMoviePageEventCopyWithImpl(
      _$_SendPressedBuyButtonMoviePageEvent _value,
      $Res Function(_$_SendPressedBuyButtonMoviePageEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? movieId = null,
    Object? movieTitle = null,
    Object? userId = freezed,
  }) {
    return _then(_$_SendPressedBuyButtonMoviePageEvent(
      null == movieId
          ? _value.movieId
          : movieId // ignore: cast_nullable_to_non_nullable
              as String,
      null == movieTitle
          ? _value.movieTitle
          : movieTitle // ignore: cast_nullable_to_non_nullable
              as String,
      freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$_SendPressedBuyButtonMoviePageEvent
    implements _SendPressedBuyButtonMoviePageEvent {
  const _$_SendPressedBuyButtonMoviePageEvent(
      this.movieId, this.movieTitle, this.userId);

  @override
  final String movieId;
  @override
  final String movieTitle;
  @override
  final int? userId;

  @override
  String toString() {
    return 'AnalyticsEvent.sendPressedBuyButtonMoviePage(movieId: $movieId, movieTitle: $movieTitle, userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SendPressedBuyButtonMoviePageEvent &&
            (identical(other.movieId, movieId) || other.movieId == movieId) &&
            (identical(other.movieTitle, movieTitle) ||
                other.movieTitle == movieTitle) &&
            (identical(other.userId, userId) || other.userId == userId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, movieId, movieTitle, userId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SendPressedBuyButtonMoviePageEventCopyWith<
          _$_SendPressedBuyButtonMoviePageEvent>
      get copyWith => __$$_SendPressedBuyButtonMoviePageEventCopyWithImpl<
          _$_SendPressedBuyButtonMoviePageEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int? userId) sendApplicationLaunched,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendMoviePageOpened,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendMoviePlayed,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPlayerPageOpened,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedBuyButtonMoviePage,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedBuyButtonMoviePage(movieId, movieTitle, userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int? userId)? sendApplicationLaunched,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedBuyButtonMoviePage?.call(movieId, movieTitle, userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int? userId)? sendApplicationLaunched,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendPressedBuyButtonMoviePage != null) {
      return sendPressedBuyButtonMoviePage(movieId, movieTitle, userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendApplicationLaunchedEvent value)
        sendApplicationLaunched,
    required TResult Function(_SendMoviePageOpenedEvent value)
        sendMoviePageOpened,
    required TResult Function(_SendMoviePlayedEvent value) sendMoviePlayed,
    required TResult Function(_SendPlayerPageOpenedEvent value)
        sendPlayerPageOpened,
    required TResult Function(_SendPressedBuyButtonMoviePageEvent value)
        sendPressedBuyButtonMoviePage,
    required TResult Function(_SendPressedBuyButtonPlayerPageEvent value)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedBuyButtonMoviePage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult? Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult? Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult? Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult? Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedBuyButtonMoviePage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendPressedBuyButtonMoviePage != null) {
      return sendPressedBuyButtonMoviePage(this);
    }
    return orElse();
  }
}

abstract class _SendPressedBuyButtonMoviePageEvent implements AnalyticsEvent {
  const factory _SendPressedBuyButtonMoviePageEvent(
          final String movieId, final String movieTitle, final int? userId) =
      _$_SendPressedBuyButtonMoviePageEvent;

  String get movieId;
  String get movieTitle;
  @override
  int? get userId;
  @override
  @JsonKey(ignore: true)
  _$$_SendPressedBuyButtonMoviePageEventCopyWith<
          _$_SendPressedBuyButtonMoviePageEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SendPressedBuyButtonPlayerPageEventCopyWith<$Res>
    implements $AnalyticsEventCopyWith<$Res> {
  factory _$$_SendPressedBuyButtonPlayerPageEventCopyWith(
          _$_SendPressedBuyButtonPlayerPageEvent value,
          $Res Function(_$_SendPressedBuyButtonPlayerPageEvent) then) =
      __$$_SendPressedBuyButtonPlayerPageEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String movieId,
      String movieTitle,
      String? season,
      String? episode,
      int? userId});
}

/// @nodoc
class __$$_SendPressedBuyButtonPlayerPageEventCopyWithImpl<$Res>
    extends _$AnalyticsEventCopyWithImpl<$Res,
        _$_SendPressedBuyButtonPlayerPageEvent>
    implements _$$_SendPressedBuyButtonPlayerPageEventCopyWith<$Res> {
  __$$_SendPressedBuyButtonPlayerPageEventCopyWithImpl(
      _$_SendPressedBuyButtonPlayerPageEvent _value,
      $Res Function(_$_SendPressedBuyButtonPlayerPageEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? movieId = null,
    Object? movieTitle = null,
    Object? season = freezed,
    Object? episode = freezed,
    Object? userId = freezed,
  }) {
    return _then(_$_SendPressedBuyButtonPlayerPageEvent(
      null == movieId
          ? _value.movieId
          : movieId // ignore: cast_nullable_to_non_nullable
              as String,
      null == movieTitle
          ? _value.movieTitle
          : movieTitle // ignore: cast_nullable_to_non_nullable
              as String,
      freezed == season
          ? _value.season
          : season // ignore: cast_nullable_to_non_nullable
              as String?,
      freezed == episode
          ? _value.episode
          : episode // ignore: cast_nullable_to_non_nullable
              as String?,
      freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$_SendPressedBuyButtonPlayerPageEvent
    implements _SendPressedBuyButtonPlayerPageEvent {
  const _$_SendPressedBuyButtonPlayerPageEvent(
      this.movieId, this.movieTitle, this.season, this.episode, this.userId);

  @override
  final String movieId;
  @override
  final String movieTitle;
  @override
  final String? season;
  @override
  final String? episode;
  @override
  final int? userId;

  @override
  String toString() {
    return 'AnalyticsEvent.sendPressedBuyButtonPlayerPage(movieId: $movieId, movieTitle: $movieTitle, season: $season, episode: $episode, userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SendPressedBuyButtonPlayerPageEvent &&
            (identical(other.movieId, movieId) || other.movieId == movieId) &&
            (identical(other.movieTitle, movieTitle) ||
                other.movieTitle == movieTitle) &&
            (identical(other.season, season) || other.season == season) &&
            (identical(other.episode, episode) || other.episode == episode) &&
            (identical(other.userId, userId) || other.userId == userId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, movieId, movieTitle, season, episode, userId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SendPressedBuyButtonPlayerPageEventCopyWith<
          _$_SendPressedBuyButtonPlayerPageEvent>
      get copyWith => __$$_SendPressedBuyButtonPlayerPageEventCopyWithImpl<
          _$_SendPressedBuyButtonPlayerPageEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int? userId) sendApplicationLaunched,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendMoviePageOpened,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendMoviePlayed,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPlayerPageOpened,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedBuyButtonMoviePage,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedBuyButtonPlayerPage(
        movieId, movieTitle, season, episode, userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int? userId)? sendApplicationLaunched,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedBuyButtonPlayerPage?.call(
        movieId, movieTitle, season, episode, userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int? userId)? sendApplicationLaunched,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendPressedBuyButtonPlayerPage != null) {
      return sendPressedBuyButtonPlayerPage(
          movieId, movieTitle, season, episode, userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendApplicationLaunchedEvent value)
        sendApplicationLaunched,
    required TResult Function(_SendMoviePageOpenedEvent value)
        sendMoviePageOpened,
    required TResult Function(_SendMoviePlayedEvent value) sendMoviePlayed,
    required TResult Function(_SendPlayerPageOpenedEvent value)
        sendPlayerPageOpened,
    required TResult Function(_SendPressedBuyButtonMoviePageEvent value)
        sendPressedBuyButtonMoviePage,
    required TResult Function(_SendPressedBuyButtonPlayerPageEvent value)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedBuyButtonPlayerPage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult? Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult? Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult? Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult? Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedBuyButtonPlayerPage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendPressedBuyButtonPlayerPage != null) {
      return sendPressedBuyButtonPlayerPage(this);
    }
    return orElse();
  }
}

abstract class _SendPressedBuyButtonPlayerPageEvent implements AnalyticsEvent {
  const factory _SendPressedBuyButtonPlayerPageEvent(
      final String movieId,
      final String movieTitle,
      final String? season,
      final String? episode,
      final int? userId) = _$_SendPressedBuyButtonPlayerPageEvent;

  String get movieId;
  String get movieTitle;
  String? get season;
  String? get episode;
  @override
  int? get userId;
  @override
  @JsonKey(ignore: true)
  _$$_SendPressedBuyButtonPlayerPageEventCopyWith<
          _$_SendPressedBuyButtonPlayerPageEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SendPressedWatchSecondSeasonNotBoughtEventCopyWith<$Res>
    implements $AnalyticsEventCopyWith<$Res> {
  factory _$$_SendPressedWatchSecondSeasonNotBoughtEventCopyWith(
          _$_SendPressedWatchSecondSeasonNotBoughtEvent value,
          $Res Function(_$_SendPressedWatchSecondSeasonNotBoughtEvent) then) =
      __$$_SendPressedWatchSecondSeasonNotBoughtEventCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String movieId, String movieTitle, int? userId});
}

/// @nodoc
class __$$_SendPressedWatchSecondSeasonNotBoughtEventCopyWithImpl<$Res>
    extends _$AnalyticsEventCopyWithImpl<$Res,
        _$_SendPressedWatchSecondSeasonNotBoughtEvent>
    implements _$$_SendPressedWatchSecondSeasonNotBoughtEventCopyWith<$Res> {
  __$$_SendPressedWatchSecondSeasonNotBoughtEventCopyWithImpl(
      _$_SendPressedWatchSecondSeasonNotBoughtEvent _value,
      $Res Function(_$_SendPressedWatchSecondSeasonNotBoughtEvent) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? movieId = null,
    Object? movieTitle = null,
    Object? userId = freezed,
  }) {
    return _then(_$_SendPressedWatchSecondSeasonNotBoughtEvent(
      null == movieId
          ? _value.movieId
          : movieId // ignore: cast_nullable_to_non_nullable
              as String,
      null == movieTitle
          ? _value.movieTitle
          : movieTitle // ignore: cast_nullable_to_non_nullable
              as String,
      freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$_SendPressedWatchSecondSeasonNotBoughtEvent
    implements _SendPressedWatchSecondSeasonNotBoughtEvent {
  const _$_SendPressedWatchSecondSeasonNotBoughtEvent(
      this.movieId, this.movieTitle, this.userId);

  @override
  final String movieId;
  @override
  final String movieTitle;
  @override
  final int? userId;

  @override
  String toString() {
    return 'AnalyticsEvent.sendPressedWatchSecondSeasonNotBought(movieId: $movieId, movieTitle: $movieTitle, userId: $userId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SendPressedWatchSecondSeasonNotBoughtEvent &&
            (identical(other.movieId, movieId) || other.movieId == movieId) &&
            (identical(other.movieTitle, movieTitle) ||
                other.movieTitle == movieTitle) &&
            (identical(other.userId, userId) || other.userId == userId));
  }

  @override
  int get hashCode => Object.hash(runtimeType, movieId, movieTitle, userId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SendPressedWatchSecondSeasonNotBoughtEventCopyWith<
          _$_SendPressedWatchSecondSeasonNotBoughtEvent>
      get copyWith =>
          __$$_SendPressedWatchSecondSeasonNotBoughtEventCopyWithImpl<
              _$_SendPressedWatchSecondSeasonNotBoughtEvent>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(int? userId) sendApplicationLaunched,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendMoviePageOpened,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendMoviePlayed,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPlayerPageOpened,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedBuyButtonMoviePage,
    required TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(String movieId, String movieTitle, int? userId)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedWatchSecondSeasonNotBought(movieId, movieTitle, userId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function(int? userId)? sendApplicationLaunched,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedWatchSecondSeasonNotBought?.call(
        movieId, movieTitle, userId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(int? userId)? sendApplicationLaunched,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendMoviePageOpened,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendMoviePlayed,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPlayerPageOpened,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedBuyButtonMoviePage,
    TResult Function(String movieId, String movieTitle, String? season,
            String? episode, int? userId)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(String movieId, String movieTitle, int? userId)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendPressedWatchSecondSeasonNotBought != null) {
      return sendPressedWatchSecondSeasonNotBought(movieId, movieTitle, userId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_SendApplicationLaunchedEvent value)
        sendApplicationLaunched,
    required TResult Function(_SendMoviePageOpenedEvent value)
        sendMoviePageOpened,
    required TResult Function(_SendMoviePlayedEvent value) sendMoviePlayed,
    required TResult Function(_SendPlayerPageOpenedEvent value)
        sendPlayerPageOpened,
    required TResult Function(_SendPressedBuyButtonMoviePageEvent value)
        sendPressedBuyButtonMoviePage,
    required TResult Function(_SendPressedBuyButtonPlayerPageEvent value)
        sendPressedBuyButtonPlayerPage,
    required TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedWatchSecondSeasonNotBought(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult? Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult? Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult? Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult? Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult? Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult? Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
  }) {
    return sendPressedWatchSecondSeasonNotBought?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_SendApplicationLaunchedEvent value)?
        sendApplicationLaunched,
    TResult Function(_SendMoviePageOpenedEvent value)? sendMoviePageOpened,
    TResult Function(_SendMoviePlayedEvent value)? sendMoviePlayed,
    TResult Function(_SendPlayerPageOpenedEvent value)? sendPlayerPageOpened,
    TResult Function(_SendPressedBuyButtonMoviePageEvent value)?
        sendPressedBuyButtonMoviePage,
    TResult Function(_SendPressedBuyButtonPlayerPageEvent value)?
        sendPressedBuyButtonPlayerPage,
    TResult Function(_SendPressedWatchSecondSeasonNotBoughtEvent value)?
        sendPressedWatchSecondSeasonNotBought,
    required TResult orElse(),
  }) {
    if (sendPressedWatchSecondSeasonNotBought != null) {
      return sendPressedWatchSecondSeasonNotBought(this);
    }
    return orElse();
  }
}

abstract class _SendPressedWatchSecondSeasonNotBoughtEvent
    implements AnalyticsEvent {
  const factory _SendPressedWatchSecondSeasonNotBoughtEvent(
          final String movieId, final String movieTitle, final int? userId) =
      _$_SendPressedWatchSecondSeasonNotBoughtEvent;

  String get movieId;
  String get movieTitle;
  @override
  int? get userId;
  @override
  @JsonKey(ignore: true)
  _$$_SendPressedWatchSecondSeasonNotBoughtEventCopyWith<
          _$_SendPressedWatchSecondSeasonNotBoughtEvent>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AnalyticsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loaded,
    required TResult Function() error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loaded,
    TResult? Function()? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loaded,
    TResult Function()? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Loaded value)? loaded,
    TResult? Function(_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AnalyticsStateCopyWith<$Res> {
  factory $AnalyticsStateCopyWith(
          AnalyticsState value, $Res Function(AnalyticsState) then) =
      _$AnalyticsStateCopyWithImpl<$Res, AnalyticsState>;
}

/// @nodoc
class _$AnalyticsStateCopyWithImpl<$Res, $Val extends AnalyticsState>
    implements $AnalyticsStateCopyWith<$Res> {
  _$AnalyticsStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_LoadedCopyWith<$Res> {
  factory _$$_LoadedCopyWith(_$_Loaded value, $Res Function(_$_Loaded) then) =
      __$$_LoadedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadedCopyWithImpl<$Res>
    extends _$AnalyticsStateCopyWithImpl<$Res, _$_Loaded>
    implements _$$_LoadedCopyWith<$Res> {
  __$$_LoadedCopyWithImpl(_$_Loaded _value, $Res Function(_$_Loaded) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loaded implements _Loaded {
  const _$_Loaded();

  @override
  String toString() {
    return 'AnalyticsState.loaded()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loaded);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loaded,
    required TResult Function() error,
  }) {
    return loaded();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loaded,
    TResult? Function()? error,
  }) {
    return loaded?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loaded,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Loaded value)? loaded,
    TResult? Function(_Error value)? error,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class _Loaded implements AnalyticsState {
  const factory _Loaded() = _$_Loaded;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$AnalyticsStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Error implements _Error {
  const _$_Error();

  @override
  String toString() {
    return 'AnalyticsState.error()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Error);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loaded,
    required TResult Function() error,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loaded,
    TResult? Function()? error,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loaded,
    TResult Function()? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loaded value) loaded,
    required TResult Function(_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Loaded value)? loaded,
    TResult? Function(_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loaded value)? loaded,
    TResult Function(_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements AnalyticsState {
  const factory _Error() = _$_Error;
}
