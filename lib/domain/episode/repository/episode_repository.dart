import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/data/player/models/comment.dart';

abstract class EpisodeRepository {
  Future<Either<Failure, Episode>> getEpisodeById(
      int movieId, int id, String locale);

  Future<Either<Failure, Comment>> commentUser({
    required String comment,
    required int movieCollection,
    required int episodeId,
  });

  Future<Either<Failure, dynamic>> addVote(
      {required int commentId, required int vote});

  Either<Failure, Episode> sortByLikes({required Episode episode});

  Either<Failure, Episode> sortByDate({required Episode episode});
}
