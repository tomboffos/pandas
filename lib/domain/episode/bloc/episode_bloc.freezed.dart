// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'episode_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$EpisodeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(int movieId, int id, bool? loader) fetchEpisode,
    required TResult Function(
            String comment, int movieCollection, int episodeId)
        postComment,
    required TResult Function(
            int commentId, int vote, int episodeId, int movieId)
        addVoteToComment,
    required TResult Function(Episode episode) sortByLikes,
    required TResult Function(Episode episode) sortByDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult? Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult? Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult? Function(Episode episode)? sortByLikes,
    TResult? Function(Episode episode)? sortByDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult Function(Episode episode)? sortByLikes,
    TResult Function(Episode episode)? sortByDate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchEpisode value) fetchEpisode,
    required TResult Function(_PostComment value) postComment,
    required TResult Function(_AddVote value) addVoteToComment,
    required TResult Function(_SortByLikes value) sortByLikes,
    required TResult Function(_SortByDate value) sortByDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_FetchEpisode value)? fetchEpisode,
    TResult? Function(_PostComment value)? postComment,
    TResult? Function(_AddVote value)? addVoteToComment,
    TResult? Function(_SortByLikes value)? sortByLikes,
    TResult? Function(_SortByDate value)? sortByDate,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchEpisode value)? fetchEpisode,
    TResult Function(_PostComment value)? postComment,
    TResult Function(_AddVote value)? addVoteToComment,
    TResult Function(_SortByLikes value)? sortByLikes,
    TResult Function(_SortByDate value)? sortByDate,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EpisodeEventCopyWith<$Res> {
  factory $EpisodeEventCopyWith(
          EpisodeEvent value, $Res Function(EpisodeEvent) then) =
      _$EpisodeEventCopyWithImpl<$Res, EpisodeEvent>;
}

/// @nodoc
class _$EpisodeEventCopyWithImpl<$Res, $Val extends EpisodeEvent>
    implements $EpisodeEventCopyWith<$Res> {
  _$EpisodeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_StartedCopyWith<$Res> {
  factory _$$_StartedCopyWith(
          _$_Started value, $Res Function(_$_Started) then) =
      __$$_StartedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_StartedCopyWithImpl<$Res>
    extends _$EpisodeEventCopyWithImpl<$Res, _$_Started>
    implements _$$_StartedCopyWith<$Res> {
  __$$_StartedCopyWithImpl(_$_Started _value, $Res Function(_$_Started) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Started with DiagnosticableTreeMixin implements _Started {
  const _$_Started();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EpisodeEvent.started()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'EpisodeEvent.started'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Started);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(int movieId, int id, bool? loader) fetchEpisode,
    required TResult Function(
            String comment, int movieCollection, int episodeId)
        postComment,
    required TResult Function(
            int commentId, int vote, int episodeId, int movieId)
        addVoteToComment,
    required TResult Function(Episode episode) sortByLikes,
    required TResult Function(Episode episode) sortByDate,
  }) {
    return started();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult? Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult? Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult? Function(Episode episode)? sortByLikes,
    TResult? Function(Episode episode)? sortByDate,
  }) {
    return started?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult Function(Episode episode)? sortByLikes,
    TResult Function(Episode episode)? sortByDate,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchEpisode value) fetchEpisode,
    required TResult Function(_PostComment value) postComment,
    required TResult Function(_AddVote value) addVoteToComment,
    required TResult Function(_SortByLikes value) sortByLikes,
    required TResult Function(_SortByDate value) sortByDate,
  }) {
    return started(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_FetchEpisode value)? fetchEpisode,
    TResult? Function(_PostComment value)? postComment,
    TResult? Function(_AddVote value)? addVoteToComment,
    TResult? Function(_SortByLikes value)? sortByLikes,
    TResult? Function(_SortByDate value)? sortByDate,
  }) {
    return started?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchEpisode value)? fetchEpisode,
    TResult Function(_PostComment value)? postComment,
    TResult Function(_AddVote value)? addVoteToComment,
    TResult Function(_SortByLikes value)? sortByLikes,
    TResult Function(_SortByDate value)? sortByDate,
    required TResult orElse(),
  }) {
    if (started != null) {
      return started(this);
    }
    return orElse();
  }
}

abstract class _Started implements EpisodeEvent {
  const factory _Started() = _$_Started;
}

/// @nodoc
abstract class _$$_FetchEpisodeCopyWith<$Res> {
  factory _$$_FetchEpisodeCopyWith(
          _$_FetchEpisode value, $Res Function(_$_FetchEpisode) then) =
      __$$_FetchEpisodeCopyWithImpl<$Res>;
  @useResult
  $Res call({int movieId, int id, bool? loader});
}

/// @nodoc
class __$$_FetchEpisodeCopyWithImpl<$Res>
    extends _$EpisodeEventCopyWithImpl<$Res, _$_FetchEpisode>
    implements _$$_FetchEpisodeCopyWith<$Res> {
  __$$_FetchEpisodeCopyWithImpl(
      _$_FetchEpisode _value, $Res Function(_$_FetchEpisode) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? movieId = null,
    Object? id = null,
    Object? loader = freezed,
  }) {
    return _then(_$_FetchEpisode(
      movieId: null == movieId
          ? _value.movieId
          : movieId // ignore: cast_nullable_to_non_nullable
              as int,
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      loader: freezed == loader
          ? _value.loader
          : loader // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc

class _$_FetchEpisode with DiagnosticableTreeMixin implements _FetchEpisode {
  const _$_FetchEpisode({required this.movieId, required this.id, this.loader});

  @override
  final int movieId;
  @override
  final int id;
  @override
  final bool? loader;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EpisodeEvent.fetchEpisode(movieId: $movieId, id: $id, loader: $loader)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'EpisodeEvent.fetchEpisode'))
      ..add(DiagnosticsProperty('movieId', movieId))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('loader', loader));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FetchEpisode &&
            (identical(other.movieId, movieId) || other.movieId == movieId) &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.loader, loader) || other.loader == loader));
  }

  @override
  int get hashCode => Object.hash(runtimeType, movieId, id, loader);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FetchEpisodeCopyWith<_$_FetchEpisode> get copyWith =>
      __$$_FetchEpisodeCopyWithImpl<_$_FetchEpisode>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(int movieId, int id, bool? loader) fetchEpisode,
    required TResult Function(
            String comment, int movieCollection, int episodeId)
        postComment,
    required TResult Function(
            int commentId, int vote, int episodeId, int movieId)
        addVoteToComment,
    required TResult Function(Episode episode) sortByLikes,
    required TResult Function(Episode episode) sortByDate,
  }) {
    return fetchEpisode(movieId, id, loader);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult? Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult? Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult? Function(Episode episode)? sortByLikes,
    TResult? Function(Episode episode)? sortByDate,
  }) {
    return fetchEpisode?.call(movieId, id, loader);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult Function(Episode episode)? sortByLikes,
    TResult Function(Episode episode)? sortByDate,
    required TResult orElse(),
  }) {
    if (fetchEpisode != null) {
      return fetchEpisode(movieId, id, loader);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchEpisode value) fetchEpisode,
    required TResult Function(_PostComment value) postComment,
    required TResult Function(_AddVote value) addVoteToComment,
    required TResult Function(_SortByLikes value) sortByLikes,
    required TResult Function(_SortByDate value) sortByDate,
  }) {
    return fetchEpisode(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_FetchEpisode value)? fetchEpisode,
    TResult? Function(_PostComment value)? postComment,
    TResult? Function(_AddVote value)? addVoteToComment,
    TResult? Function(_SortByLikes value)? sortByLikes,
    TResult? Function(_SortByDate value)? sortByDate,
  }) {
    return fetchEpisode?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchEpisode value)? fetchEpisode,
    TResult Function(_PostComment value)? postComment,
    TResult Function(_AddVote value)? addVoteToComment,
    TResult Function(_SortByLikes value)? sortByLikes,
    TResult Function(_SortByDate value)? sortByDate,
    required TResult orElse(),
  }) {
    if (fetchEpisode != null) {
      return fetchEpisode(this);
    }
    return orElse();
  }
}

abstract class _FetchEpisode implements EpisodeEvent {
  const factory _FetchEpisode(
      {required final int movieId,
      required final int id,
      final bool? loader}) = _$_FetchEpisode;

  int get movieId;
  int get id;
  bool? get loader;
  @JsonKey(ignore: true)
  _$$_FetchEpisodeCopyWith<_$_FetchEpisode> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_PostCommentCopyWith<$Res> {
  factory _$$_PostCommentCopyWith(
          _$_PostComment value, $Res Function(_$_PostComment) then) =
      __$$_PostCommentCopyWithImpl<$Res>;
  @useResult
  $Res call({String comment, int movieCollection, int episodeId});
}

/// @nodoc
class __$$_PostCommentCopyWithImpl<$Res>
    extends _$EpisodeEventCopyWithImpl<$Res, _$_PostComment>
    implements _$$_PostCommentCopyWith<$Res> {
  __$$_PostCommentCopyWithImpl(
      _$_PostComment _value, $Res Function(_$_PostComment) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? comment = null,
    Object? movieCollection = null,
    Object? episodeId = null,
  }) {
    return _then(_$_PostComment(
      comment: null == comment
          ? _value.comment
          : comment // ignore: cast_nullable_to_non_nullable
              as String,
      movieCollection: null == movieCollection
          ? _value.movieCollection
          : movieCollection // ignore: cast_nullable_to_non_nullable
              as int,
      episodeId: null == episodeId
          ? _value.episodeId
          : episodeId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_PostComment with DiagnosticableTreeMixin implements _PostComment {
  const _$_PostComment(
      {required this.comment,
      required this.movieCollection,
      required this.episodeId});

  @override
  final String comment;
  @override
  final int movieCollection;
  @override
  final int episodeId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EpisodeEvent.postComment(comment: $comment, movieCollection: $movieCollection, episodeId: $episodeId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'EpisodeEvent.postComment'))
      ..add(DiagnosticsProperty('comment', comment))
      ..add(DiagnosticsProperty('movieCollection', movieCollection))
      ..add(DiagnosticsProperty('episodeId', episodeId));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PostComment &&
            (identical(other.comment, comment) || other.comment == comment) &&
            (identical(other.movieCollection, movieCollection) ||
                other.movieCollection == movieCollection) &&
            (identical(other.episodeId, episodeId) ||
                other.episodeId == episodeId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, comment, movieCollection, episodeId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_PostCommentCopyWith<_$_PostComment> get copyWith =>
      __$$_PostCommentCopyWithImpl<_$_PostComment>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(int movieId, int id, bool? loader) fetchEpisode,
    required TResult Function(
            String comment, int movieCollection, int episodeId)
        postComment,
    required TResult Function(
            int commentId, int vote, int episodeId, int movieId)
        addVoteToComment,
    required TResult Function(Episode episode) sortByLikes,
    required TResult Function(Episode episode) sortByDate,
  }) {
    return postComment(comment, movieCollection, episodeId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult? Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult? Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult? Function(Episode episode)? sortByLikes,
    TResult? Function(Episode episode)? sortByDate,
  }) {
    return postComment?.call(comment, movieCollection, episodeId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult Function(Episode episode)? sortByLikes,
    TResult Function(Episode episode)? sortByDate,
    required TResult orElse(),
  }) {
    if (postComment != null) {
      return postComment(comment, movieCollection, episodeId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchEpisode value) fetchEpisode,
    required TResult Function(_PostComment value) postComment,
    required TResult Function(_AddVote value) addVoteToComment,
    required TResult Function(_SortByLikes value) sortByLikes,
    required TResult Function(_SortByDate value) sortByDate,
  }) {
    return postComment(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_FetchEpisode value)? fetchEpisode,
    TResult? Function(_PostComment value)? postComment,
    TResult? Function(_AddVote value)? addVoteToComment,
    TResult? Function(_SortByLikes value)? sortByLikes,
    TResult? Function(_SortByDate value)? sortByDate,
  }) {
    return postComment?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchEpisode value)? fetchEpisode,
    TResult Function(_PostComment value)? postComment,
    TResult Function(_AddVote value)? addVoteToComment,
    TResult Function(_SortByLikes value)? sortByLikes,
    TResult Function(_SortByDate value)? sortByDate,
    required TResult orElse(),
  }) {
    if (postComment != null) {
      return postComment(this);
    }
    return orElse();
  }
}

abstract class _PostComment implements EpisodeEvent {
  const factory _PostComment(
      {required final String comment,
      required final int movieCollection,
      required final int episodeId}) = _$_PostComment;

  String get comment;
  int get movieCollection;
  int get episodeId;
  @JsonKey(ignore: true)
  _$$_PostCommentCopyWith<_$_PostComment> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_AddVoteCopyWith<$Res> {
  factory _$$_AddVoteCopyWith(
          _$_AddVote value, $Res Function(_$_AddVote) then) =
      __$$_AddVoteCopyWithImpl<$Res>;
  @useResult
  $Res call({int commentId, int vote, int episodeId, int movieId});
}

/// @nodoc
class __$$_AddVoteCopyWithImpl<$Res>
    extends _$EpisodeEventCopyWithImpl<$Res, _$_AddVote>
    implements _$$_AddVoteCopyWith<$Res> {
  __$$_AddVoteCopyWithImpl(_$_AddVote _value, $Res Function(_$_AddVote) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? commentId = null,
    Object? vote = null,
    Object? episodeId = null,
    Object? movieId = null,
  }) {
    return _then(_$_AddVote(
      commentId: null == commentId
          ? _value.commentId
          : commentId // ignore: cast_nullable_to_non_nullable
              as int,
      vote: null == vote
          ? _value.vote
          : vote // ignore: cast_nullable_to_non_nullable
              as int,
      episodeId: null == episodeId
          ? _value.episodeId
          : episodeId // ignore: cast_nullable_to_non_nullable
              as int,
      movieId: null == movieId
          ? _value.movieId
          : movieId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_AddVote with DiagnosticableTreeMixin implements _AddVote {
  const _$_AddVote(
      {required this.commentId,
      required this.vote,
      required this.episodeId,
      required this.movieId});

  @override
  final int commentId;
  @override
  final int vote;
  @override
  final int episodeId;
  @override
  final int movieId;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EpisodeEvent.addVoteToComment(commentId: $commentId, vote: $vote, episodeId: $episodeId, movieId: $movieId)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'EpisodeEvent.addVoteToComment'))
      ..add(DiagnosticsProperty('commentId', commentId))
      ..add(DiagnosticsProperty('vote', vote))
      ..add(DiagnosticsProperty('episodeId', episodeId))
      ..add(DiagnosticsProperty('movieId', movieId));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AddVote &&
            (identical(other.commentId, commentId) ||
                other.commentId == commentId) &&
            (identical(other.vote, vote) || other.vote == vote) &&
            (identical(other.episodeId, episodeId) ||
                other.episodeId == episodeId) &&
            (identical(other.movieId, movieId) || other.movieId == movieId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, commentId, vote, episodeId, movieId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_AddVoteCopyWith<_$_AddVote> get copyWith =>
      __$$_AddVoteCopyWithImpl<_$_AddVote>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(int movieId, int id, bool? loader) fetchEpisode,
    required TResult Function(
            String comment, int movieCollection, int episodeId)
        postComment,
    required TResult Function(
            int commentId, int vote, int episodeId, int movieId)
        addVoteToComment,
    required TResult Function(Episode episode) sortByLikes,
    required TResult Function(Episode episode) sortByDate,
  }) {
    return addVoteToComment(commentId, vote, episodeId, movieId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult? Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult? Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult? Function(Episode episode)? sortByLikes,
    TResult? Function(Episode episode)? sortByDate,
  }) {
    return addVoteToComment?.call(commentId, vote, episodeId, movieId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult Function(Episode episode)? sortByLikes,
    TResult Function(Episode episode)? sortByDate,
    required TResult orElse(),
  }) {
    if (addVoteToComment != null) {
      return addVoteToComment(commentId, vote, episodeId, movieId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchEpisode value) fetchEpisode,
    required TResult Function(_PostComment value) postComment,
    required TResult Function(_AddVote value) addVoteToComment,
    required TResult Function(_SortByLikes value) sortByLikes,
    required TResult Function(_SortByDate value) sortByDate,
  }) {
    return addVoteToComment(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_FetchEpisode value)? fetchEpisode,
    TResult? Function(_PostComment value)? postComment,
    TResult? Function(_AddVote value)? addVoteToComment,
    TResult? Function(_SortByLikes value)? sortByLikes,
    TResult? Function(_SortByDate value)? sortByDate,
  }) {
    return addVoteToComment?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchEpisode value)? fetchEpisode,
    TResult Function(_PostComment value)? postComment,
    TResult Function(_AddVote value)? addVoteToComment,
    TResult Function(_SortByLikes value)? sortByLikes,
    TResult Function(_SortByDate value)? sortByDate,
    required TResult orElse(),
  }) {
    if (addVoteToComment != null) {
      return addVoteToComment(this);
    }
    return orElse();
  }
}

abstract class _AddVote implements EpisodeEvent {
  const factory _AddVote(
      {required final int commentId,
      required final int vote,
      required final int episodeId,
      required final int movieId}) = _$_AddVote;

  int get commentId;
  int get vote;
  int get episodeId;
  int get movieId;
  @JsonKey(ignore: true)
  _$$_AddVoteCopyWith<_$_AddVote> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SortByLikesCopyWith<$Res> {
  factory _$$_SortByLikesCopyWith(
          _$_SortByLikes value, $Res Function(_$_SortByLikes) then) =
      __$$_SortByLikesCopyWithImpl<$Res>;
  @useResult
  $Res call({Episode episode});

  $EpisodeCopyWith<$Res> get episode;
}

/// @nodoc
class __$$_SortByLikesCopyWithImpl<$Res>
    extends _$EpisodeEventCopyWithImpl<$Res, _$_SortByLikes>
    implements _$$_SortByLikesCopyWith<$Res> {
  __$$_SortByLikesCopyWithImpl(
      _$_SortByLikes _value, $Res Function(_$_SortByLikes) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? episode = null,
  }) {
    return _then(_$_SortByLikes(
      episode: null == episode
          ? _value.episode
          : episode // ignore: cast_nullable_to_non_nullable
              as Episode,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $EpisodeCopyWith<$Res> get episode {
    return $EpisodeCopyWith<$Res>(_value.episode, (value) {
      return _then(_value.copyWith(episode: value));
    });
  }
}

/// @nodoc

class _$_SortByLikes with DiagnosticableTreeMixin implements _SortByLikes {
  const _$_SortByLikes({required this.episode});

  @override
  final Episode episode;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EpisodeEvent.sortByLikes(episode: $episode)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'EpisodeEvent.sortByLikes'))
      ..add(DiagnosticsProperty('episode', episode));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SortByLikes &&
            (identical(other.episode, episode) || other.episode == episode));
  }

  @override
  int get hashCode => Object.hash(runtimeType, episode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SortByLikesCopyWith<_$_SortByLikes> get copyWith =>
      __$$_SortByLikesCopyWithImpl<_$_SortByLikes>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(int movieId, int id, bool? loader) fetchEpisode,
    required TResult Function(
            String comment, int movieCollection, int episodeId)
        postComment,
    required TResult Function(
            int commentId, int vote, int episodeId, int movieId)
        addVoteToComment,
    required TResult Function(Episode episode) sortByLikes,
    required TResult Function(Episode episode) sortByDate,
  }) {
    return sortByLikes(episode);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult? Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult? Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult? Function(Episode episode)? sortByLikes,
    TResult? Function(Episode episode)? sortByDate,
  }) {
    return sortByLikes?.call(episode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult Function(Episode episode)? sortByLikes,
    TResult Function(Episode episode)? sortByDate,
    required TResult orElse(),
  }) {
    if (sortByLikes != null) {
      return sortByLikes(episode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchEpisode value) fetchEpisode,
    required TResult Function(_PostComment value) postComment,
    required TResult Function(_AddVote value) addVoteToComment,
    required TResult Function(_SortByLikes value) sortByLikes,
    required TResult Function(_SortByDate value) sortByDate,
  }) {
    return sortByLikes(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_FetchEpisode value)? fetchEpisode,
    TResult? Function(_PostComment value)? postComment,
    TResult? Function(_AddVote value)? addVoteToComment,
    TResult? Function(_SortByLikes value)? sortByLikes,
    TResult? Function(_SortByDate value)? sortByDate,
  }) {
    return sortByLikes?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchEpisode value)? fetchEpisode,
    TResult Function(_PostComment value)? postComment,
    TResult Function(_AddVote value)? addVoteToComment,
    TResult Function(_SortByLikes value)? sortByLikes,
    TResult Function(_SortByDate value)? sortByDate,
    required TResult orElse(),
  }) {
    if (sortByLikes != null) {
      return sortByLikes(this);
    }
    return orElse();
  }
}

abstract class _SortByLikes implements EpisodeEvent {
  const factory _SortByLikes({required final Episode episode}) = _$_SortByLikes;

  Episode get episode;
  @JsonKey(ignore: true)
  _$$_SortByLikesCopyWith<_$_SortByLikes> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SortByDateCopyWith<$Res> {
  factory _$$_SortByDateCopyWith(
          _$_SortByDate value, $Res Function(_$_SortByDate) then) =
      __$$_SortByDateCopyWithImpl<$Res>;
  @useResult
  $Res call({Episode episode});

  $EpisodeCopyWith<$Res> get episode;
}

/// @nodoc
class __$$_SortByDateCopyWithImpl<$Res>
    extends _$EpisodeEventCopyWithImpl<$Res, _$_SortByDate>
    implements _$$_SortByDateCopyWith<$Res> {
  __$$_SortByDateCopyWithImpl(
      _$_SortByDate _value, $Res Function(_$_SortByDate) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? episode = null,
  }) {
    return _then(_$_SortByDate(
      episode: null == episode
          ? _value.episode
          : episode // ignore: cast_nullable_to_non_nullable
              as Episode,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $EpisodeCopyWith<$Res> get episode {
    return $EpisodeCopyWith<$Res>(_value.episode, (value) {
      return _then(_value.copyWith(episode: value));
    });
  }
}

/// @nodoc

class _$_SortByDate with DiagnosticableTreeMixin implements _SortByDate {
  const _$_SortByDate({required this.episode});

  @override
  final Episode episode;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EpisodeEvent.sortByDate(episode: $episode)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'EpisodeEvent.sortByDate'))
      ..add(DiagnosticsProperty('episode', episode));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SortByDate &&
            (identical(other.episode, episode) || other.episode == episode));
  }

  @override
  int get hashCode => Object.hash(runtimeType, episode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_SortByDateCopyWith<_$_SortByDate> get copyWith =>
      __$$_SortByDateCopyWithImpl<_$_SortByDate>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() started,
    required TResult Function(int movieId, int id, bool? loader) fetchEpisode,
    required TResult Function(
            String comment, int movieCollection, int episodeId)
        postComment,
    required TResult Function(
            int commentId, int vote, int episodeId, int movieId)
        addVoteToComment,
    required TResult Function(Episode episode) sortByLikes,
    required TResult Function(Episode episode) sortByDate,
  }) {
    return sortByDate(episode);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? started,
    TResult? Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult? Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult? Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult? Function(Episode episode)? sortByLikes,
    TResult? Function(Episode episode)? sortByDate,
  }) {
    return sortByDate?.call(episode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? started,
    TResult Function(int movieId, int id, bool? loader)? fetchEpisode,
    TResult Function(String comment, int movieCollection, int episodeId)?
        postComment,
    TResult Function(int commentId, int vote, int episodeId, int movieId)?
        addVoteToComment,
    TResult Function(Episode episode)? sortByLikes,
    TResult Function(Episode episode)? sortByDate,
    required TResult orElse(),
  }) {
    if (sortByDate != null) {
      return sortByDate(episode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Started value) started,
    required TResult Function(_FetchEpisode value) fetchEpisode,
    required TResult Function(_PostComment value) postComment,
    required TResult Function(_AddVote value) addVoteToComment,
    required TResult Function(_SortByLikes value) sortByLikes,
    required TResult Function(_SortByDate value) sortByDate,
  }) {
    return sortByDate(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Started value)? started,
    TResult? Function(_FetchEpisode value)? fetchEpisode,
    TResult? Function(_PostComment value)? postComment,
    TResult? Function(_AddVote value)? addVoteToComment,
    TResult? Function(_SortByLikes value)? sortByLikes,
    TResult? Function(_SortByDate value)? sortByDate,
  }) {
    return sortByDate?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Started value)? started,
    TResult Function(_FetchEpisode value)? fetchEpisode,
    TResult Function(_PostComment value)? postComment,
    TResult Function(_AddVote value)? addVoteToComment,
    TResult Function(_SortByLikes value)? sortByLikes,
    TResult Function(_SortByDate value)? sortByDate,
    required TResult orElse(),
  }) {
    if (sortByDate != null) {
      return sortByDate(this);
    }
    return orElse();
  }
}

abstract class _SortByDate implements EpisodeEvent {
  const factory _SortByDate({required final Episode episode}) = _$_SortByDate;

  Episode get episode;
  @JsonKey(ignore: true)
  _$$_SortByDateCopyWith<_$_SortByDate> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$EpisodeState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() error,
    required TResult Function(Episode episode) fetchedEpisode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? error,
    TResult? Function(Episode episode)? fetchedEpisode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? error,
    TResult Function(Episode episode)? fetchedEpisode,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(_FetchedEpisode value) fetchedEpisode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(_FetchedEpisode value)? fetchedEpisode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(_FetchedEpisode value)? fetchedEpisode,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EpisodeStateCopyWith<$Res> {
  factory $EpisodeStateCopyWith(
          EpisodeState value, $Res Function(EpisodeState) then) =
      _$EpisodeStateCopyWithImpl<$Res, EpisodeState>;
}

/// @nodoc
class _$EpisodeStateCopyWithImpl<$Res, $Val extends EpisodeState>
    implements $EpisodeStateCopyWith<$Res> {
  _$EpisodeStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res>
    extends _$EpisodeStateCopyWithImpl<$Res, _$_Loading>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Loading with DiagnosticableTreeMixin implements _Loading {
  const _$_Loading();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EpisodeState.loading()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'EpisodeState.loading'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() error,
    required TResult Function(Episode episode) fetchedEpisode,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? error,
    TResult? Function(Episode episode)? fetchedEpisode,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? error,
    TResult Function(Episode episode)? fetchedEpisode,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(_FetchedEpisode value) fetchedEpisode,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(_FetchedEpisode value)? fetchedEpisode,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(_FetchedEpisode value)? fetchedEpisode,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements EpisodeState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_ErrorCopyWith<$Res> {
  factory _$$_ErrorCopyWith(_$_Error value, $Res Function(_$_Error) then) =
      __$$_ErrorCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ErrorCopyWithImpl<$Res>
    extends _$EpisodeStateCopyWithImpl<$Res, _$_Error>
    implements _$$_ErrorCopyWith<$Res> {
  __$$_ErrorCopyWithImpl(_$_Error _value, $Res Function(_$_Error) _then)
      : super(_value, _then);
}

/// @nodoc

class _$_Error with DiagnosticableTreeMixin implements _Error {
  const _$_Error();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EpisodeState.error()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'EpisodeState.error'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Error);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() error,
    required TResult Function(Episode episode) fetchedEpisode,
  }) {
    return error();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? error,
    TResult? Function(Episode episode)? fetchedEpisode,
  }) {
    return error?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? error,
    TResult Function(Episode episode)? fetchedEpisode,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(_FetchedEpisode value) fetchedEpisode,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(_FetchedEpisode value)? fetchedEpisode,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(_FetchedEpisode value)? fetchedEpisode,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class _Error implements EpisodeState {
  const factory _Error() = _$_Error;
}

/// @nodoc
abstract class _$$_FetchedEpisodeCopyWith<$Res> {
  factory _$$_FetchedEpisodeCopyWith(
          _$_FetchedEpisode value, $Res Function(_$_FetchedEpisode) then) =
      __$$_FetchedEpisodeCopyWithImpl<$Res>;
  @useResult
  $Res call({Episode episode});

  $EpisodeCopyWith<$Res> get episode;
}

/// @nodoc
class __$$_FetchedEpisodeCopyWithImpl<$Res>
    extends _$EpisodeStateCopyWithImpl<$Res, _$_FetchedEpisode>
    implements _$$_FetchedEpisodeCopyWith<$Res> {
  __$$_FetchedEpisodeCopyWithImpl(
      _$_FetchedEpisode _value, $Res Function(_$_FetchedEpisode) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? episode = null,
  }) {
    return _then(_$_FetchedEpisode(
      null == episode
          ? _value.episode
          : episode // ignore: cast_nullable_to_non_nullable
              as Episode,
    ));
  }

  @override
  @pragma('vm:prefer-inline')
  $EpisodeCopyWith<$Res> get episode {
    return $EpisodeCopyWith<$Res>(_value.episode, (value) {
      return _then(_value.copyWith(episode: value));
    });
  }
}

/// @nodoc

class _$_FetchedEpisode
    with DiagnosticableTreeMixin
    implements _FetchedEpisode {
  const _$_FetchedEpisode(this.episode);

  @override
  final Episode episode;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'EpisodeState.fetchedEpisode(episode: $episode)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'EpisodeState.fetchedEpisode'))
      ..add(DiagnosticsProperty('episode', episode));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FetchedEpisode &&
            (identical(other.episode, episode) || other.episode == episode));
  }

  @override
  int get hashCode => Object.hash(runtimeType, episode);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_FetchedEpisodeCopyWith<_$_FetchedEpisode> get copyWith =>
      __$$_FetchedEpisodeCopyWithImpl<_$_FetchedEpisode>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() error,
    required TResult Function(Episode episode) fetchedEpisode,
  }) {
    return fetchedEpisode(episode);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? loading,
    TResult? Function()? error,
    TResult? Function(Episode episode)? fetchedEpisode,
  }) {
    return fetchedEpisode?.call(episode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? error,
    TResult Function(Episode episode)? fetchedEpisode,
    required TResult orElse(),
  }) {
    if (fetchedEpisode != null) {
      return fetchedEpisode(episode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Error value) error,
    required TResult Function(_FetchedEpisode value) fetchedEpisode,
  }) {
    return fetchedEpisode(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(_Loading value)? loading,
    TResult? Function(_Error value)? error,
    TResult? Function(_FetchedEpisode value)? fetchedEpisode,
  }) {
    return fetchedEpisode?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Error value)? error,
    TResult Function(_FetchedEpisode value)? fetchedEpisode,
    required TResult orElse(),
  }) {
    if (fetchedEpisode != null) {
      return fetchedEpisode(this);
    }
    return orElse();
  }
}

abstract class _FetchedEpisode implements EpisodeState {
  const factory _FetchedEpisode(final Episode episode) = _$_FetchedEpisode;

  Episode get episode;
  @JsonKey(ignore: true)
  _$$_FetchedEpisodeCopyWith<_$_FetchedEpisode> get copyWith =>
      throw _privateConstructorUsedError;
}
