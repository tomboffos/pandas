import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/data/player/models/comment.dart';
import 'package:nine_pandas/domain/episode/usecases/add_vote.dart';
import 'package:nine_pandas/domain/episode/usecases/get_episode_by_id.dart';
import 'package:nine_pandas/domain/episode/usecases/post_comment.dart';
import 'package:nine_pandas/domain/episode/usecases/sort_by_date.dart';
import 'package:nine_pandas/domain/episode/usecases/sort_by_likes.dart';
import 'package:nine_pandas/domain/localization/usecases/get_localization.dart';

part 'episode_event.dart';
part 'episode_state.dart';
part 'episode_bloc.freezed.dart';

@injectable
class EpisodeBloc extends Bloc<EpisodeEvent, EpisodeState> {
  final GetEpisdeById _getEpisdeById;
  final GetLocalization _getLocalization;
  final PostComment _postComment;
  final AddVote _addVote;
  final SortByLikes _sortByLikes;
  final SortByDate _sortByDate;

  EpisodeBloc(this._getEpisdeById, this._getLocalization, this._postComment,
      this._addVote, this._sortByLikes, this._sortByDate)
      : super(_Loading()) {
    on<_FetchEpisode>((event, emit) async {
      if (event.loader == null) emit(EpisodeState.loading());

      final String currentLocale = await _getLocalization() ?? 'ru';
      Either<Failure, Episode> episodeOrFailure = await _getEpisdeById(
          event.movieId, event.id, currentLocale.toLowerCase());
      episodeOrFailure.fold((failure) => emit(_Error()),
          (Episode episode) => emit(EpisodeState.fetchedEpisode(episode)));
    });
    on<_PostComment>((event, emit) async {
      Either<Failure, Comment> commentOrFailure = await _postComment(
          comment: event.comment,
          movieCollection: event.movieCollection,
          episodeId: event.episodeId);
      commentOrFailure.fold(
        (l) => null,
        (r) => add(
          EpisodeEvent.fetchEpisode(
            movieId: event.movieCollection,
            id: event.episodeId,
            loader: false,
          ),
        ),
      );
    });
    on<_AddVote>((event, emit) async {
      Either<Failure, dynamic> responseOnFailure =
          await _addVote(commentId: event.commentId, vote: event.vote);

      responseOnFailure.fold(
          (l) => null,
          (r) => add(EpisodeEvent.fetchEpisode(
              movieId: event.movieId, id: event.episodeId, loader: false)));
    });
    on<_SortByLikes>((event, emit) {
      Either<Failure, Episode> episodeOrFailure =
          _sortByLikes(episode: event.episode);

      episodeOrFailure.fold(
          (l) => null, (r) => emit(EpisodeState.fetchedEpisode(r)));
    });

    on<_SortByDate>((event, emit) {
      Either<Failure, Episode> episodeOrFailure =
          _sortByDate(episode: event.episode);

      episodeOrFailure.fold(
          (l) => null, (r) => emit(EpisodeState.fetchedEpisode(r)));
    });
  }
}
