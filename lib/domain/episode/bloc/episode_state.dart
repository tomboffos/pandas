part of 'episode_bloc.dart';

@freezed
class EpisodeState with _$EpisodeState {
  const factory EpisodeState.loading() = _Loading;
  const factory EpisodeState.error() = _Error;

  const factory EpisodeState.fetchedEpisode(Episode episode) = _FetchedEpisode;
}
