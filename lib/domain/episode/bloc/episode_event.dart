part of 'episode_bloc.dart';

@freezed
class EpisodeEvent with _$EpisodeEvent {
  const factory EpisodeEvent.started() = _Started;

  const factory EpisodeEvent.fetchEpisode(
      {required int movieId, required int id, bool? loader}) = _FetchEpisode;

  const factory EpisodeEvent.postComment({
    required String comment,
    required int movieCollection,
    required int episodeId,
  }) = _PostComment;

  const factory EpisodeEvent.addVoteToComment(
      {required int commentId,
      required int vote,
      required int episodeId,
      required int movieId}) = _AddVote;

  const factory EpisodeEvent.sortByLikes({
    required Episode episode,
  }) = _SortByLikes;

  const factory EpisodeEvent.sortByDate({required Episode episode}) =
      _SortByDate;
}
