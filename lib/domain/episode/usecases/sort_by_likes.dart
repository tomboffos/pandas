import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/domain/episode/repository/episode_repository.dart';

@injectable
class SortByLikes {
  final EpisodeRepository _episodeRepository;

  SortByLikes(this._episodeRepository);

  Either<Failure, Episode> call({required Episode episode}) {
    return _episodeRepository.sortByLikes(episode: episode);
  }
}
