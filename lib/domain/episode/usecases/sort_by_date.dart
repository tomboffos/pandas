import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/domain/episode/repository/episode_repository.dart';

@injectable
class SortByDate {
  final EpisodeRepository _episodeRepository;

  SortByDate(this._episodeRepository);

  Either<Failure, Episode> call({required Episode episode}) {
    return _episodeRepository.sortByDate(episode: episode);
  }
}
