import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/player/models/comment.dart';
import 'package:nine_pandas/domain/episode/repository/episode_repository.dart';

@injectable
class PostComment {
  final EpisodeRepository _episodeRepository;

  PostComment(this._episodeRepository);

  Future<Either<Failure, Comment>> call({
    required String comment,
    required int movieCollection,
    required int episodeId,
  }) async {
    return await _episodeRepository.commentUser(
      comment: comment,
      episodeId: episodeId,
      movieCollection: movieCollection,
    );
  }
}
