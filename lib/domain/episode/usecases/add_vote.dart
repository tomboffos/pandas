import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/episode/repository/episode_repository.dart';

@injectable
class AddVote {
  final EpisodeRepository _episodeRepository;

  AddVote(this._episodeRepository);

  Future<Either<Failure, dynamic>> call(
      {required int commentId, required int vote}) async {
    return await _episodeRepository.addVote(commentId: commentId, vote: vote);
  }
}
