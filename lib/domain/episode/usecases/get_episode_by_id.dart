import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/episode/models/episode/episode.dart';
import 'package:nine_pandas/domain/episode/repository/episode_repository.dart';

@injectable
class GetEpisdeById {
  final EpisodeRepository _episodeRepository;

  GetEpisdeById(this._episodeRepository);

  Future<Either<Failure, Episode>> call(
      int movieId, int id, String locale) async {
    return await _episodeRepository.getEpisodeById(movieId, id, locale);
  }
}
