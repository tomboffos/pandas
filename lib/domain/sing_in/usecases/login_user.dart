import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';

import '../repository/sing_in_repository.dart';

@injectable
class LoginUser {
  final SingInRepository _singInRepository;

  LoginUser(this._singInRepository);

  Future<Either<Failure, dynamic>> call(
      String userName, String password) async {
    return await _singInRepository.loginUser(
      username: userName,
      password: password,
    );
  }
}
