part of 'sing_in_bloc.dart';

@freezed
class SingInEvent with _$SingInEvent {
  const factory SingInEvent.login(
      String username, String password, MessageBloc messageBloc) = _Login;
}
