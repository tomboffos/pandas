part of 'sing_in_bloc.dart';

@freezed
class SingInState with _$SingInState {
  const factory SingInState.initial() = _Initial;
  const factory SingInState.loading() = _Loading;
  const factory SingInState.error(String error) = _Error;
  const factory SingInState.loginSuccess() = _LoginSuccess;
}
