import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/message/bloc/message_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart' as user_bloc;

import '../usecases/login_user.dart';

part 'sing_in_event.dart';
part 'sing_in_state.dart';
part 'sing_in_bloc.freezed.dart';

@injectable
class SingInBloc extends Bloc<SingInEvent, SingInState> {
  final LoginUser _loginUser;
  final user_bloc.UserBloc _userBloc;
  SingInBloc(this._loginUser, this._userBloc) : super(_Initial()) {
    on<_Login>((event, emit) async {
      emit(_Loading());
      final res = await _loginUser(event.username, event.password);

      res.fold((failure) {
        if (failure is ServerFailure)
          event.messageBloc
              .add(MessageEvent.errorMessage(failure.message ?? 'Error'));
        emit(_Error((failure as ServerFailure).message ?? 'Error'));
        emit(_Initial());
      }, (token) {
        emit(_LoginSuccess());

        _userBloc.add(user_bloc.Authorize());
      });
    });
  }
}
