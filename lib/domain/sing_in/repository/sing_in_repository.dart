import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';

abstract class SingInRepository {
  Future<Either<Failure, dynamic>> loginUser(
      {required String username, required String password});
}
