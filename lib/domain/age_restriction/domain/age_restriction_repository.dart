abstract class AgeRestrictionRepository {
  Future saveConfirmation(bool ageConfirmation);
  Future getConfirmation();
}
