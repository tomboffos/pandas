import 'package:injectable/injectable.dart';

import '../age_restriction_repository.dart';

@injectable
class SaveConfirmation {
  final AgeRestrictionRepository ageRestrictionRepository;

  SaveConfirmation(this.ageRestrictionRepository);

  Future call(bool ageConfirmation) async {
    return await ageRestrictionRepository.saveConfirmation(ageConfirmation);
  }
}
