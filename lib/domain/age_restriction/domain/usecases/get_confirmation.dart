import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';

import '../age_restriction_repository.dart';

@injectable
class GetConfirmation {
  final AgeRestrictionRepository ageRestrictionRepository;

  GetConfirmation(this.ageRestrictionRepository);

  Future<Either<Failure?, bool>> call() async {
    final bool ageConfirmation =
        await ageRestrictionRepository.getConfirmation();
    if (ageConfirmation) {
      return Right(ageConfirmation);
    } else {
      return Left(null);
    }
  }
}
