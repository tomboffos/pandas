import 'package:injectable/injectable.dart';
import 'package:nine_pandas/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constants/preferences.dart';

abstract class AgeRestrictionSharedPreferences {
  Future saveConfirmation(bool ageConfirmation);
  Future getConfirmation();
}

@Injectable(as: AgeRestrictionSharedPreferences)
class AgeRestrictionSharedPreferencesImpl
    implements AgeRestrictionSharedPreferences {
  final prefs = getIt<SharedPreferences>();

  @override
  Future saveConfirmation(bool ageConfirmation) async {
    final result =
        await prefs.setBool(Preferences.ageRestriction, ageConfirmation);
    return result;
  }

  @override
  Future getConfirmation() async {
    if (prefs.getBool(Preferences.ageRestriction) != null) {
      if (prefs.getBool(Preferences.ageRestriction)!) {
        return true;
      }
      return false;
    } else {
      return false;
    }
  }
}
