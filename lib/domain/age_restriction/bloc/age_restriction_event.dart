part of 'age_restriction_bloc.dart';

abstract class AgeRestrictionEvent extends Equatable {
  const AgeRestrictionEvent();

  @override
  List<Object> get props => [];
}

class AgeRestrictionSaveConfirmationEvent extends AgeRestrictionEvent {
  final bool ageRestriction;

  AgeRestrictionSaveConfirmationEvent(this.ageRestriction);

  @override
  List<Object> get props => [ageRestriction];
}

class AgeRestrictionGetConfirmationEvent extends AgeRestrictionEvent {
  AgeRestrictionGetConfirmationEvent();

  @override
  List<Object> get props => [];
}
