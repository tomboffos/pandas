part of 'age_restriction_bloc.dart';

abstract class AgeRestrictionState extends Equatable {
  const AgeRestrictionState();

  @override
  List<Object> get props => [];
}

class AgeRestrictionInitial extends AgeRestrictionState {}

class AgeRestrictionGetConfirmation extends AgeRestrictionState {}

class AgeRestrictionNotGetConfirmation extends AgeRestrictionState {}
