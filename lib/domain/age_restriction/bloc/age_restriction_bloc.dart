import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';

import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/age_restriction/domain/usecases/get_confirmation.dart';
import 'package:nine_pandas/domain/age_restriction/domain/usecases/save_confirmation.dart';

part 'age_restriction_event.dart';
part 'age_restriction_state.dart';

@injectable
class AgeRestrictionBloc
    extends Bloc<AgeRestrictionEvent, AgeRestrictionState> {
  GetConfirmation _getConfirmation;
  SaveConfirmation _saveConfirmation;

  AgeRestrictionBloc(
    GetConfirmation getConfirmation,
    SaveConfirmation saveConfirmation,
  )   : _getConfirmation = getConfirmation,
        _saveConfirmation = saveConfirmation,
        super(AgeRestrictionInitial()) {
    on<AgeRestrictionEvent>((event, emit) async {
      if (event is AgeRestrictionGetConfirmationEvent) {
        await _eitherAgeConfirmationOrErrorState(
            emit, await _getConfirmation());
      }
      if (event is AgeRestrictionSaveConfirmationEvent) {
        await _saveConfirmation(true);
      }
    });
    add(AgeRestrictionGetConfirmationEvent());
  }

  _eitherAgeConfirmationOrErrorState(
    Emitter emit,
    Either<Failure?, bool> confirmationOrFailure,
  ) async {
    emit(confirmationOrFailure.fold((failure) {
      return AgeRestrictionNotGetConfirmation();
    }, (ageConfirmation) {
      if (ageConfirmation) {
        return AgeRestrictionGetConfirmation();
      } else {
        return AgeRestrictionNotGetConfirmation();
      }
    }));
  }
}
