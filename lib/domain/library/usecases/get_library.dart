import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/library/model/library/library.dart';
import 'package:nine_pandas/domain/library/repository/library_repository.dart';

@injectable
class GetLibrary {
  final LibraryRepository _repository;
  GetLibrary(this._repository);

  Future<Either<Failure, Library>> call(String locale) async {
    return await _repository.getLibrary(locale);
  }
}
