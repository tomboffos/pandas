import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/library/model/library/library.dart';

abstract class LibraryRepository {
  Future<Either<Failure, Library>> getLibrary(String locale);
}
