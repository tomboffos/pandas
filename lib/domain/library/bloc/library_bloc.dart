import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/library/model/library/library.dart';
import 'package:nine_pandas/data/movie/models/movie/movie.dart';
import 'package:nine_pandas/domain/localization/usecases/get_localization.dart';
import 'package:injectable/injectable.dart';

import '../usecases/get_library.dart';

part 'library_event.dart';
part 'library_state.dart';
part 'library_bloc.freezed.dart';

@injectable
class LibraryBloc extends Bloc<LibraryEvent, LibraryState> {
  final GetLibrary _getLibrary;
  final GetLocalization _getLocalization;

  LibraryBloc(this._getLibrary, this._getLocalization) : super(_DataLoading()) {
    on<_GetLibrary>((event, emit) async {
      emit(_DataLoading());
      try {
        final String currentLocale = await _getLocalization() ?? 'ru';
        final result = await _getLibrary(currentLocale.toLowerCase());
        result.fold((failure) {
          if (failure is ServerFailure) {
            emit(_Error());
          }
        }, (library) {
          emit(_DataLoaded(library));
        });
      } catch (e) {
        emit(_Error());
      }
    });
  }
}
