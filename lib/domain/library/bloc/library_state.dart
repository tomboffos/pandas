part of 'library_bloc.dart';

@freezed
class LibraryState with _$LibraryState {
  const factory LibraryState.dataLoaded(Library library) = _DataLoaded;
  const factory LibraryState.dataLoading() = _DataLoading;
  const factory LibraryState.error() = _Error;
}
