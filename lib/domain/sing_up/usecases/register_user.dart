import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';

import '../repository/sing_up_repository.dart';

@injectable
class RegisterUser {
  final SingUpRepository _singUpRepository;

  RegisterUser(this._singUpRepository);

  Future<Either<Failure, dynamic>> call(
      String userName, String name, String password) async {
    return await _singUpRepository.registerUser(
        userName: userName, name: name, password: password);
  }
}
