import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';

abstract class SingUpRepository {
  Future<Either<Failure, dynamic>> registerUser({
    required String userName,
    required String name,
    required String password,
  });
}
