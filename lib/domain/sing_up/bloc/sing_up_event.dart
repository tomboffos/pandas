part of 'sing_up_bloc.dart';

@freezed
class SingUpEvent with _$SingUpEvent {
  const factory SingUpEvent.register(String username, String name,
      String password, MessageBloc messageBloc) = _Register;
}
