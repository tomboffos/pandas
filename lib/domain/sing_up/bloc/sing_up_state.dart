part of 'sing_up_bloc.dart';

@freezed
class SingUpState with _$SingUpState {
  const factory SingUpState.initial() = _Initial;
  const factory SingUpState.loading() = _Loading;
  const factory SingUpState.error(String error) = _Error;
  const factory SingUpState.registerSuccess(String username) = _RegisterSuccess;
}
