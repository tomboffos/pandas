import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/domain/message/bloc/message_bloc.dart';
import 'package:nine_pandas/domain/user/bloc/user_bloc.dart' as user_bloc;

import '../usecases/register_user.dart';

part 'sing_up_event.dart';
part 'sing_up_state.dart';
part 'sing_up_bloc.freezed.dart';

@injectable
class SingUpBloc extends Bloc<SingUpEvent, SingUpState> {
  final RegisterUser _registerUser;
  final user_bloc.UserBloc _userBloc;
  SingUpBloc(this._registerUser, this._userBloc) : super(_Initial()) {
    on<_Register>((event, emit) async {
      emit(_Loading());
      try {
        final res =
            await _registerUser(event.username, event.name, event.password);

        res.fold((failure) {
          if (failure is ServerFailure) {
            event.messageBloc
                .add(MessageEvent.errorMessage(failure.message ?? 'Error'));
            emit(_Error(failure.message ?? 'Error'));
            emit(_Initial());
          }
          emit(_Error('Error'));
          emit(_Initial());
        }, (user) async {
          emit(_RegisterSuccess(event.username));
          _userBloc.add(user_bloc.Authorize());
        });
      } catch (e) {
        emit(_Error('Error'));
      }
    });
  }
}
