import 'package:dartz/dartz.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/history/modal/history.dart';

abstract class HistoryRepository {
  Future<Either<Failure, History>> addHistory(int episodeId);
}
