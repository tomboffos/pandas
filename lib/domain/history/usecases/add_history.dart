import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/core/error/failures.dart';
import 'package:nine_pandas/data/history/modal/history.dart';

import '../history_repository.dart';

@injectable
class AddHistory {
  final HistoryRepository _historyRepository;

  AddHistory(this._historyRepository);

  Future<Either<Failure, History>> call(int episodeId) async {
    return await _historyRepository.addHistory(episodeId);
  }
}
