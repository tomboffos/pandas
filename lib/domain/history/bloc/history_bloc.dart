import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:nine_pandas/domain/history/usecases/add_history.dart';

part 'history_event.dart';
part 'history_state.dart';
part 'history_bloc.freezed.dart';

@lazySingleton
class HistoryBloc extends Bloc<HistoryEvent, HistoryState> {
  AddHistory _addHistory;

  HistoryBloc(this._addHistory) : super(_Initial()) {
    on<_AddHistory>((event, emit) async {
      await _addHistory(event.episodeId);
    });
  }
}
